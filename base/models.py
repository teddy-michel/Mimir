from django.db import models
from django.utils.translation import gettext as _, get_language, pgettext


class Attribute(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False)
    value = models.CharField(verbose_name=_("Value"), max_length=200, blank=False)

    class Meta:
        abstract = True
        ordering = ["name"]


class Language(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False)
    code = models.CharField(verbose_name=_("Code"), max_length=3, primary_key=True)  # TODO: min_length=3
    iso1 = models.CharField(verbose_name=_("Code ISO 639-1"), max_length=2, blank=True, null=True)
    iso2 = models.CharField(verbose_name=_("Code ISO 639-2"), max_length=3, blank=True, null=True)
    iso3 = models.CharField(verbose_name=_("Code ISO 639-3"), max_length=3, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = pgettext("language", "Language")
        verbose_name_plural = _("Languages")
        ordering = ["name"]


class Link(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False)
    uri = models.CharField(verbose_name=_("URI"), max_length=200, null=False, blank=False)
    lang = models.ForeignKey(Language, verbose_name=pgettext("language", "Language"), on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        if self.name:
            return str(self.name) + " (" + str(self.uri) + ")"
        else:
            return str(self.uri)

    class Meta:
        abstract = True
        ordering = ["name"]


class Country(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False)
    code = models.CharField(verbose_name=_("Code"), max_length=3, unique=True, null=False, blank=False) # TODO: primary_key
    iso = models.CharField(verbose_name=_("Code ISO 3166-1"), max_length=3, unique=True, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = pgettext("The country", "Country")
        verbose_name_plural = _("Countries")
        ordering = ["name"]


class Person(models.Model):
    name = models.CharField(verbose_name=_("Person name"), max_length=100, blank=False)
    lastname = models.CharField(verbose_name=_("Last name"), max_length=100, blank=True)
    firstname = models.CharField(verbose_name=_("First name"), max_length=100, blank=True)
    gender = models.CharField(verbose_name=_("Gender"), max_length=1, choices=(("U", _("Unknown gender")), ("M", _("Male")), ("F", _("Female")), ("O", _("Other"))), default="U")
    #birthdate = models.DateField(verbose_name=_("Birth date"), null=True, blank=True)
    birthdate = models.CharField(verbose_name=_("Birth date"), max_length=10, null=True, blank=True)
    birthplace = models.CharField(verbose_name=_("Birth place"), max_length=100, blank=True)
    #deathdate = models.DateField(verbose_name=_("Death date"), null=True, blank=True)
    deathdate = models.CharField(verbose_name=_("Death date"), max_length=10, null=True, blank=True)
    deathplace = models.CharField(verbose_name=_("Death place"), max_length=100, blank=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")
        ordering = ["lastname", "name"]


class PersonAttribute(Attribute):
    person = models.ForeignKey(Person, related_name="attributes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Person") + " - " + _("Attribute")
        verbose_name_plural = _("Persons") + " - " + _("Attributes")
        #ordering = ["name"]
        db_table = "base_" + "person_attributes"


class PersonLink(Link):
    person = models.ForeignKey(Person, related_name="links", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Person") + " - " + _("Link")
        verbose_name_plural = _("Persons") + " - " + _("Links")
        #ordering = ["name"]
        db_table = "base_" + "person_links"


class PersonRelation(models.Model):
    type = models.CharField(verbose_name=_("Relation type"), max_length=100, blank=False)  # TODO: clé étrangère ?
    person = models.ForeignKey(Person, related_name="relations", on_delete=models.CASCADE)
    personto = models.ForeignKey(Person, related_name="relations_to", on_delete=models.CASCADE)
    date_start = models.CharField(verbose_name=_("Date start"), max_length=10, null=True, blank=True)
    date_end = models.CharField(verbose_name=_("Date end"), max_length=10, null=True, blank=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)

    class Meta:
        verbose_name = _("Person") + " - " + _("Relation")
        verbose_name_plural = _("Persons") + " - " + _("Relations")
        db_table = "base_" + "person_relations"


class Event(models.Model):
    is_unique = models.BooleanField(verbose_name=_("Unique event"), default=False)
    icon = models.CharField(verbose_name=_("Icon"), max_length=20, blank=False)
    description = models.TextField(verbose_name=_("Description"), blank=True)

    def __str__(self):
        return self.name

    @property
    def name(self):
        name = _("Unknown event")
        names = self.names.all()
        if names:
            current_lang = get_language()
            for nameObj in names:
                name = str(nameObj.name)
                if nameObj.lang.iso1 == current_lang:
                    break
        return name

    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")
        db_table = "base_" + "event"


class EventName(models.Model):
    event = models.ForeignKey(Event, related_name="names", on_delete=models.CASCADE)
    lang = models.ForeignKey(Language, verbose_name=pgettext("language", "Language"), on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False)

    class Meta:
        verbose_name = _("Event name")
        verbose_name_plural = _("Event names")
        db_table = "base_" + "event_names"
        unique_together = ("event", "lang")


class PersonEvent(models.Model):
    event = models.ForeignKey(Event, related_name="events", on_delete=models.CASCADE)
    person = models.ForeignKey(Person, related_name="events", on_delete=models.CASCADE)
    date = models.DateTimeField(verbose_name=_("Date"), null=True, blank=True)
    description = models.TextField(verbose_name=_("Description"), blank=True)

    class Meta:
        verbose_name = _("Person") + " - " + _("Event")
        verbose_name_plural = _("Persons") + " - " + _("Events")
        ordering = ["date"]
        db_table = "base_" + "person_events"


class Group(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False)
    type = models.CharField(verbose_name=_("Type"), max_length=1, choices=(("U", _("Unknown type")), ("S", _("Studies")), ("W", pgettext("to work", "Work")), ("A", _("Group of artists")), ("C", _("Sport club")), ("T", _("Association")), ("O", _("Other"))), default="U")
    info = models.TextField(verbose_name=_("Information"), blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = pgettext("The group", "Group")
        verbose_name_plural = _("Groups")
        ordering = ["name"]


class GroupAttribute(Attribute):
    group = models.ForeignKey(Group, related_name="attributes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = pgettext("The group", "Group") + " - " + _("Attribute")
        verbose_name_plural = _("Groups") + " - " + _("Attributes")
        ordering = ["name"]
        db_table = "base_" + "group_attributes"


class GroupLink(Link):
    group = models.ForeignKey(Group, related_name="links", on_delete=models.CASCADE)

    class Meta:
        verbose_name = pgettext("The group", "Group") + " - " + _("Link")
        verbose_name_plural = _("Groups") + " - " + _("Links")
        #ordering = ["name"]
        db_table = "base_" + "group_links"


class GroupMember(models.Model):
    group = models.ForeignKey(Group, related_name="members", on_delete=models.CASCADE)
    person = models.ForeignKey(Person, related_name="groups", on_delete=models.CASCADE)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)
    date_start = models.CharField(verbose_name=_("Date start"), max_length=10, null=True, blank=True)
    date_end = models.CharField(verbose_name=_("Date end"), max_length=10, null=True, blank=True)

    def __str__(self):
        return str(self.group.name) + " / " + str(self.person)

    class Meta:
        verbose_name = pgettext("The group", "Group") + " - " + _("Member")
        verbose_name_plural = _("Groups") + " - " + _("Members")
        db_table = "base_" + "group_members"


class Tag(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False, unique=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    parent = models.ForeignKey("Tag", verbose_name=_("Parent tag"), related_name="children", on_delete=models.SET_NULL, null=True, blank=True)
    persons_order = models.IntegerField(verbose_name=_("Default persons order"), choices=[(0, _("Name")), (1, _("Information"))], default=0)
    movies_order = models.IntegerField(verbose_name=_("Default movies order"), choices=[(0, _("Title")), (1, _("Date")), (2, _("Information"))], default=1)
    series_order = models.IntegerField(verbose_name=_("Default series order"), choices=[(0, _("Title")), (1, _("Information"))], default=0)
    books_order = models.IntegerField(verbose_name=_("Default books order"), choices=[(0, _("Title")), (1, _("Year")), (2, _("Information"))], default=1)
    games_order = models.IntegerField(verbose_name=_("Default games order"), choices=[(0, _("Title")), (1, _("Year")), (2, _("Information"))], default=1)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")
        ordering = ["name"]


class PersonTag(models.Model):
    person = models.ForeignKey(Person, related_name="tags", on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, related_name="persons", on_delete=models.CASCADE)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)

    class Meta:
        verbose_name = _("Person") + " - " + _("Tag")
        verbose_name_plural = _("Persons") + " - " + _("Tags")
        ordering = ["tag__name", "info"]
        db_table = "base_" + "person_tags"
