from django import template
from django.apps import apps
from django.forms.fields import BooleanField
from django.utils.translation import gettext as _

register = template.Library()


@register.filter
def is_app_installed(value):
    return apps.is_installed(value)


@register.filter
def duration_txt(value):
    if value is None:
        return ""

    if not isinstance(value, int):
        value = int(value)

    durationS = value % 60
    durationM = (value // 60) % 60
    durationH = (value // 3600)

    if durationH == 0:
        if durationM == 0:
            return _("%(seconds)d seconds") % {"seconds": durationS}
        elif durationS == 0:
            return _("%(minutes)d min") % {"minutes": durationM}
        else:
            return _("%(minutes)d min %(seconds)d") % {"minutes": durationM, "seconds": durationS}
    else:
        return _("%(hours)dh%(minutes)02d") % {"hours": durationH, "minutes": durationM}


@register.filter
def get_order(value):
    return (value._order + 1)


@register.filter
def gender_text(value):
    if value == "M":
        return _("Male")
    elif value == "F":
        return _("Female")
    elif value == "O":
        return _("Other")
    else:
        return _("Unknown gender")


@register.filter
def date_format(value):
    if value is None:
        return ""
    if len(value) < 10:
        return value

    dateY = value[0:4]
    dateM = value[5:7]
    dateD = value[8:10]

    if dateY == "0000":
        return ""
    elif dateM == "00":
        return dateY
    elif dateD == "00":
        return dateM + "/" + dateY
    else:
        return dateD + "/" + dateM + "/" + dateY


@register.filter
def date_txt(value):
    if value is None:
        return _("Unknown date")
    if len(value) != 10:
        return value

    months = [_("january"), _("february"), _("march"), _("april"), _("may"), _("june"), _("july"), _("august"), _("september"), _("october"), _("november"), _("december")]

    dateY = value[0:4]
    dateM = value[5:7]
    dateD = value[8:10]

    if dateY == "0000":
        return ""
    elif dateM == "00":
        return "En " + dateY
    elif dateD == "00":
        return "En " + months[int(dateM) - 1] + " " + dateY
    else:
        return "Le " + str(int(dateD)) + " " + months[int(dateM) - 1] + " " + dateY


@register.filter
def date_year(value):
    if value is None:
        return ""
    if len(value) != 10:
        return value

    dateY = value[0:4]
    if dateY == "0000":
        return ""
    else:
        for i in range(4):
            if dateY[i] not in "0123456789":
                return value
        return dateY


@register.filter()
def field_add_class(value, arg):
    if isinstance(value.field, BooleanField):
        return value
    else:
        oldClasses = value.field.widget.attrs.get("class", "")
        if oldClasses:
            oldClasses += " "
        return value.as_widget(attrs={"class": oldClasses + arg})
