# Generated by Django 4.0 on 2021-12-24 20:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('code', models.CharField(max_length=3, unique=True, verbose_name='Code')),
                ('iso', models.CharField(blank=True, max_length=3, null=True, unique=True, verbose_name='Code ISO 3166-1')),
            ],
            options={
                'verbose_name': 'Pays',
                'verbose_name_plural': 'Pays',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_unique', models.BooleanField(default=False, verbose_name='Évènement unique')),
                ('icon', models.CharField(max_length=20, verbose_name='Icône')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
            ],
            options={
                'verbose_name': 'Évènement',
                'verbose_name_plural': 'Évènements',
                'db_table': 'base_event',
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('type', models.CharField(choices=[('U', 'Type inconnu'), ('S', 'Études'), ('W', 'Travail'), ('A', 'Groupe artistique'), ('C', 'Club sportif'), ('T', 'Association'), ('O', 'Autre')], default='U', max_length=1, verbose_name='Type')),
                ('infos', models.TextField(blank=True, verbose_name='Informations')),
                ('creation', models.DateTimeField(auto_now_add=True, verbose_name='Date de création')),
                ('modification', models.DateTimeField(auto_now=True, verbose_name='Date de modification')),
            ],
            options={
                'verbose_name': 'Groupe',
                'verbose_name_plural': 'Groupes',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('code', models.CharField(max_length=3, primary_key=True, serialize=False, verbose_name='Code')),
                ('iso1', models.CharField(blank=True, max_length=2, null=True, verbose_name='Code ISO 639-1')),
                ('iso2', models.CharField(blank=True, max_length=3, null=True, verbose_name='Code ISO 639-2')),
                ('iso3', models.CharField(blank=True, max_length=3, null=True, verbose_name='Code ISO 639-3')),
            ],
            options={
                'verbose_name': 'Langue',
                'verbose_name_plural': 'Langues',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Appellation')),
                ('lastname', models.CharField(blank=True, max_length=100, verbose_name='Nom')),
                ('firstname', models.CharField(blank=True, max_length=100, verbose_name='Prénom')),
                ('gender', models.CharField(choices=[('U', 'Genre inconnu'), ('M', 'Homme'), ('F', 'Femme'), ('O', 'Autre')], default='U', max_length=1, verbose_name='Genre')),
                ('birthdate', models.CharField(blank=True, max_length=10, null=True, verbose_name='Date de naissance')),
                ('birthplace', models.CharField(blank=True, max_length=100, verbose_name='Lieu de naissance')),
                ('deathdate', models.CharField(blank=True, max_length=10, null=True, verbose_name='Date de décès')),
                ('deathplace', models.CharField(blank=True, max_length=100, verbose_name='Lieu de décès')),
                ('infos', models.TextField(blank=True, verbose_name='Informations')),
                ('creation', models.DateTimeField(auto_now_add=True, verbose_name='Date de création')),
                ('modification', models.DateTimeField(auto_now=True, verbose_name='Date de modification')),
            ],
            options={
                'verbose_name': 'Personne',
                'verbose_name_plural': 'Personnes',
                'ordering': ['lastname', 'name'],
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True, verbose_name='Nom')),
                ('infos', models.TextField(blank=True, verbose_name='Informations')),
                ('persons_order', models.IntegerField(choices=[(0, 'Nom'), (1, 'Informations')], default=0, verbose_name='Ordre par défaut des personnes')),
                ('movies_order', models.IntegerField(choices=[(0, 'Titre'), (1, 'Date'), (2, 'Informations')], default=1, verbose_name='Ordre par défaut des films')),
                ('series_order', models.IntegerField(choices=[(0, 'Titre'), (1, 'Informations')], default=0, verbose_name='Ordre par défaut des séries')),
                ('books_order', models.IntegerField(choices=[(0, 'Titre'), (1, 'Année'), (2, 'Informations')], default=1, verbose_name='Ordre par défaut des livres')),
                ('games_order', models.IntegerField(choices=[(0, 'Titre'), (1, 'Année'), (2, 'Informations')], default=1, verbose_name='Ordre par défaut des jeux vidéos')),
                ('creation', models.DateTimeField(auto_now_add=True, verbose_name='Date de création')),
                ('modification', models.DateTimeField(auto_now=True, verbose_name='Date de modification')),
                ('parent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='children', to='base.tag', verbose_name='Tag parent')),
            ],
            options={
                'verbose_name': 'Tag',
                'verbose_name_plural': 'Tags',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='PersonTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('infos', models.CharField(blank=True, max_length=100, verbose_name='Informations')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tags', to='base.person')),
                ('tag', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='persons', to='base.tag')),
            ],
            options={
                'verbose_name': 'Personne - Tag',
                'verbose_name_plural': 'Personnes - Tags',
                'db_table': 'base_person_tags',
                'ordering': ['tag__name', 'infos'],
            },
        ),
        migrations.CreateModel(
            name='PersonRelation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=100, verbose_name='Type de relation')),
                ('date_start', models.CharField(blank=True, max_length=10, null=True, verbose_name='Date de début')),
                ('date_end', models.CharField(blank=True, max_length=10, null=True, verbose_name='Date de fin')),
                ('infos', models.TextField(blank=True, verbose_name='Informations')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='relations', to='base.person')),
                ('personto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='relations_to', to='base.person')),
            ],
            options={
                'verbose_name': 'Personne - Relation',
                'verbose_name_plural': 'Personnes - Relations',
                'db_table': 'base_person_relations',
            },
        ),
        migrations.CreateModel(
            name='PersonLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('uri', models.CharField(max_length=200, verbose_name='URI')),
                ('lang', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='base.language', verbose_name='Langue')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='links', to='base.person')),
            ],
            options={
                'verbose_name': 'Personne - Lien',
                'verbose_name_plural': 'Personnes - Liens',
                'db_table': 'base_person_links',
            },
        ),
        migrations.CreateModel(
            name='PersonEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Date')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='events', to='base.event')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='events', to='base.person')),
            ],
            options={
                'verbose_name': 'Personne - Évènement',
                'verbose_name_plural': 'Personnes - Évènements',
                'db_table': 'base_person_events',
                'ordering': ['date'],
            },
        ),
        migrations.CreateModel(
            name='PersonAttribute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('value', models.CharField(max_length=200, verbose_name='Valeur')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='attributes', to='base.person')),
            ],
            options={
                'verbose_name': 'Personne - Attribut',
                'verbose_name_plural': 'Personnes - Attributs',
                'db_table': 'base_person_attributes',
            },
        ),
        migrations.CreateModel(
            name='GroupMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('infos', models.CharField(blank=True, max_length=100, verbose_name='Informations')),
                ('date_start', models.CharField(blank=True, max_length=10, null=True, verbose_name='Date de début')),
                ('date_end', models.CharField(blank=True, max_length=10, null=True, verbose_name='Date de fin')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='members', to='base.group')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='groups', to='base.person')),
            ],
            options={
                'verbose_name': 'Groupe - Membre',
                'verbose_name_plural': 'Groupes - Membres',
                'db_table': 'base_group_members',
            },
        ),
        migrations.CreateModel(
            name='GroupLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('uri', models.CharField(max_length=200, verbose_name='URI')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='links', to='base.group')),
                ('lang', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='base.language', verbose_name='Langue')),
            ],
            options={
                'verbose_name': 'Groupe - Lien',
                'verbose_name_plural': 'Groupes - Liens',
                'db_table': 'base_group_links',
            },
        ),
        migrations.CreateModel(
            name='GroupAttribute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('value', models.CharField(max_length=200, verbose_name='Valeur')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='attributes', to='base.group')),
            ],
            options={
                'verbose_name': 'Groupe - Attribut',
                'verbose_name_plural': 'Groupes - Attributs',
                'db_table': 'base_group_attributes',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='EventName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='names', to='base.event')),
                ('lang', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='base.language', verbose_name='Langue')),
            ],
            options={
                'verbose_name': "Nom d'évènement",
                'verbose_name_plural': "Noms d'évènement",
                'db_table': 'base_event_names',
                'unique_together': {('event', 'lang')},
            },
        ),
    ]
