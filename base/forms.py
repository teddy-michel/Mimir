from django.forms import ModelForm, Textarea, TextInput, HiddenInput, CharField, IntegerField, Select
from django.utils.translation import gettext as _
from django.core.exceptions import ValidationError

from .models import Person, PersonEvent, PersonRelation, PersonAttribute, Group, GroupMember, GroupAttribute, Tag, \
    PersonTag, PersonLink, GroupLink, Event, EventName, Country, Language


class PersonForm(ModelForm):

    class Meta:
        model = Person
        fields = ["name", "lastname", "firstname", "gender", "birthdate", "birthplace", "deathdate", "deathplace", "info"]
        widgets = {
            "birthdate": TextInput(attrs={"placeholder": "YYYY-MM-DD"}),
            "info": Textarea(attrs={"rows": 4}),
        }

    def clean_birthdate(self):
        birthdate = self.cleaned_data["birthdate"]
        if birthdate is None:
            return None

        birthdate = birthdate.strip()
        if len(birthdate) == 0:
            return None

        if len(birthdate) != 10:
            raise ValidationError(_("Invalid birth date"))

        if birthdate[0] in "0123456789" and birthdate[1] in "0123456789" and birthdate[2] in "0123456789" and birthdate[3] in "0123456789" and birthdate[4] == "-" and birthdate[5] in "0123456789" and birthdate[6] in "0123456789" and birthdate[7] == "-" and birthdate[8] in "0123456789" and birthdate[9] in "0123456789":
            return birthdate

        if birthdate[0] in "0123456789" and birthdate[1] in "0123456789" and birthdate[2] == "/" and birthdate[3] in "0123456789" and birthdate[4] in "0123456789" and birthdate[5] == "/" and birthdate[6] in "0123456789" and birthdate[7] in "0123456789" and birthdate[8] in "0123456789" and birthdate[9] in "0123456789":
            return birthdate[6:10] + "-" + birthdate[3:5] + "-" + birthdate[0:2]

        raise ValidationError(_("Invalid birth date"))

    def clean_deathdate(self):
        deathdate = self.cleaned_data["deathdate"]
        if deathdate is None:
            return None

        deathdate = deathdate.strip()
        if len(deathdate) == 0:
            return None

        if len(deathdate) != 10:
            raise ValidationError(_("Invalid death date"))

        if deathdate[0] in "0123456789" and deathdate[1] in "0123456789" and deathdate[2] in "0123456789" and deathdate[3] in "0123456789" and deathdate[4] == "-" and deathdate[5] in "0123456789" and deathdate[6] in "0123456789" and deathdate[7] == "-" and deathdate[8] in "0123456789" and deathdate[9] in "0123456789":
            return deathdate

        if deathdate[0] in "0123456789" and deathdate[1] in "0123456789" and deathdate[2] == "/" and deathdate[3] in "0123456789" and deathdate[4] in "0123456789" and deathdate[5] == "/" and deathdate[6] in "0123456789" and deathdate[7] in "0123456789" and deathdate[8] in "0123456789" and deathdate[9] in "0123456789":
            return deathdate[6:10] + "-" + deathdate[3:5] + "-" + deathdate[0:2]

        raise ValidationError(_("Invalid death date"))


class EventForm(ModelForm):

    class Meta:
        model = Event
        fields = ["is_unique", "icon", "description"]
        widgets = {
            "description": Textarea(attrs={"rows": 4}),
        }


class EventNameForm(ModelForm):

    class Meta:
        model = EventName
        fields = ["lang", "name"]


class PersonEventForm(ModelForm):

    class Meta:
        model = PersonEvent
        fields = ["event", "date", "description"]
        widgets = {
            "description": Textarea(attrs={"rows": 4}),
        }


class PersonRelationForm(ModelForm):
    personto_id = IntegerField(label=_("Person"), widget=Select(attrs={"class": "select_person"}), required=False)
    personto_name = CharField(widget=HiddenInput(), max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "personto"):
            self.initial["personto_id"] = self.instance.personto.id
            self.initial["personto_name"] = self.instance.personto.name
        else:
            self.initial["personto_id"] = None
            self.initial["personto_name"] = ""

    def _clean_fields(self, *args, **kwargs):
        value = self.data.get(self.add_prefix("person"))
        if value == "-1":
            name = self.data.get(self.add_prefix("personto_name"))
            #self.instance.personto = Person.objects.get_or_create(name=name)[0]
            self.instance.personto = Person.objects.create(name=name)
            self.data = self.data.copy()
            self.data[self.add_prefix("personto")] = self.instance.personto.id
            self.data[self.add_prefix("personto_id")] = self.instance.personto.id
        super(ModelForm, self)._clean_fields(*args, **kwargs)

    def save(self, commit=True):
        if self.instance.personto is None and self.cleaned_data.get("personto_id"):
            self.instance.personto = Person.objects.get(id=int(self.cleaned_data.get("personto_id")))
        if hasattr(self.instance, "person") and self.instance.personto == self.instance.person:
            raise ValueError("person and personto must be different.")

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = PersonRelation
        fields = ["type", "personto_id", "personto_name", "personto", "date_start", "date_end", "info"]
        widgets = {
            "personto": HiddenInput(),
            "info": Textarea(attrs={"rows": 4}),
        }


# TODO: make a generic class
class PersonTagForm(ModelForm):
    tag_id = IntegerField(widget=HiddenInput(), required=False)
    tag_select = IntegerField(label=_("Tag"), widget=Select(attrs={"class": "select_tag"}), required=False)
    tag_name = CharField(widget=HiddenInput(), max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "tag") and self.instance.tag:
            self.initial["tag_id"] = self.instance.tag.id
            self.initial["tag_name"] = self.instance.tag.name
        else:
            self.initial["tag_id"] = None
            self.initial["tag_name"] = ""

    def _clean_fields(self, *args, **kwargs):
        tagId_str = self.data.get(self.add_prefix("tag_select"))
        if tagId_str is not None:
            tagId = int(tagId_str)

            if tagId < 0:
                tag = Tag.objects.create(name=self.data.get(self.add_prefix("tag_name")))
            else:
                tag = Tag.objects.get(id=tagId)

            self.instance.tag = tag
            self.data = self.data.copy()
            self.data[self.add_prefix("tag")] = self.instance.tag.id

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = PersonTag
        fields = ["tag_select", "tag_name", "tag", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class PersonTagFormClassic(ModelForm):
    tag_name = CharField(label=_("Tag name"), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.initial["tag_name"] = ""

        self.fields["tag"].required = False

        try:
            if self.instance.tag:
                self.initial["tag_name"] = self.instance.tag.name
        except:
            pass

    def save(self, commit=True):
        name = self.cleaned_data.get("tag_name").strip()
        if self.initial["tag_name"] != name:
            tag = Tag.objects.get_or_create(name=name)[0]
            self.instance.tag = tag
        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = PersonTag
        fields = ["tag", "tag_name", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class PersonAttributeForm(ModelForm):

    class Meta:
        model = PersonAttribute
        fields = ["name", "value"]


class PersonLinkForm(ModelForm):

    class Meta:
        model = PersonLink
        fields = ["name", "uri", "lang"]


class GroupForm(ModelForm):

    class Meta:
        model = Group
        fields = ["name", "type", "info"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
        }


class GroupAttributeForm(ModelForm):

    class Meta:
        model = GroupAttribute
        fields = ["name", "value"]


class GroupLinkForm(ModelForm):

    class Meta:
        model = GroupLink
        fields = ["name", "uri", "lang"]


class GroupMemberForm(ModelForm):
    person_id = IntegerField(label=_("Name"), widget=Select(attrs={"class": "select_person"}), required=False)
    person_name = CharField(widget=HiddenInput(), max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        self.initial["person_id"] = None
        self.initial["person_name"] = ""

        try:
            self.initial["person_id"] = self.instance.person.id
            self.initial["person_name"] = self.instance.person.name
        except:
            pass

    def _clean_fields(self, *args, **kwargs):
        value = self.data.get(self.add_prefix("person"))
        if value == "-1":
            name = self.data.get(self.add_prefix("person_name"))
            #self.instance.person = Person.objects.get_or_create(name=name)[0]
            self.instance.person = Person.objects.create(name=name)
            self.data = self.data.copy()
            self.data[self.add_prefix("person")] = self.instance.person.id
            self.data[self.add_prefix("person_id")] = self.instance.person.id
        super(ModelForm, self)._clean_fields(*args, **kwargs)

    def save(self, commit=True):
        if self.instance.person is None and self.cleaned_data.get("person_id"):
            self.instance.person = Person.objects.get(id=int(self.cleaned_data.get("person_id")))

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = GroupMember
        fields = ["person_id", "person_name", "person", "date_start", "date_end", "info"]
        widgets = {
            "person": HiddenInput(),
        }


class TagForm(ModelForm):
    parent_id = IntegerField(widget=HiddenInput(), required=False)
    parent_select = IntegerField(label=_("Parent tag"), widget=Select(attrs={"class": "select_tag"}), required=False)
    parent_name = CharField(widget=HiddenInput(), max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "parent") and self.instance.parent:
            self.initial["parent_id"] = self.instance.parent.id
            self.initial["parent_name"] = self.instance.parent.name
        else:
            self.initial["parent_id"] = None
            self.initial["parent_name"] = ""

    def _clean_fields(self, *args, **kwargs):
        try:
            tagId = int(self.data.get(self.add_prefix("parent_select")))

            if tagId < 0:
                tag = Tag.objects.create(name=self.data.get(self.add_prefix("parent_name")))
            else:
                tag = Tag.objects.get(id=tagId)

            self.instance.parent = tag
            self.data = self.data.copy()
            self.data[self.add_prefix("parent")] = self.instance.parent.id
        except:
            self.instance.parent = None
            self.data = self.data.copy()
            self.data[self.add_prefix("parent")] = None

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = Tag
        fields = ["name", "parent_select", "parent_name", "parent", "persons_order", "movies_order", "series_order", "books_order", "games_order", "info"]
        widgets = {
            "parent": HiddenInput(),
            "info": Textarea(attrs={"rows": 4}),
        }


class CountryForm(ModelForm):

    class Meta:
        model = Country
        fields = ["name", "code", "iso"]


class LanguageForm(ModelForm):

    class Meta:
        model = Language
        fields = ["name", "code", "iso1", "iso2", "iso3"]
