from django.urls import re_path

from . import views
from movies import views as views_movies

urlpatterns = [
    re_path(r"^$", views.index, name="base_home"),

    re_path(r"^tags$", views.tagsList, name="tags_list"),
    re_path(r"^tags/api/search$", views.tagsAPISearch, name="tags_api_search"),
    re_path(r"^tags/(?P<id>[0-9]+)$", views.tagInfo, name="tag_info"),
    re_path(r"^tags/(?P<id>[0-9]+)/edit$", views.tagEdit, name="tag_edit"),
    re_path(r"^tags/(?P<id>[0-9]+)/delete$", views.tagDelete, name="tag_delete"),
    re_path(r"^tags/(?P<tag_id>[0-9]+)/export_movies$", views_movies.export_tag_movies, name="tag_export_movies"),

    re_path(r"^persons$", views.personsList, name="persons_list"),
    re_path(r"^persons/create$", views.personCreate, name="person_create"),
    re_path(r"^persons/search$", views.personsSearch, name="persons_search"),
    re_path(r"^persons/api/list$", views.personsAPIList, name="persons_api_list"),
    re_path(r"^persons/api/search$", views.personsAPISearch, name="persons_api_search"),
    re_path(r"^persons/(?P<id>[0-9]+)$", views.personInfo, name="person_info"),
    re_path(r"^persons/(?P<id>[0-9]+)/edit$", views.personEdit, name="person_edit"),
    re_path(r"^persons/(?P<id1>[0-9]+)/merge/(?P<id2>[0-9]+)$", views.personMerge, name="person_merge"),
    re_path(r"^persons/(?P<id>[0-9]+)/delete$", views.personDelete, name="person_delete"),
    re_path(r"^persons/(?P<id>[0-9]+)/add_event$", views.personAddEvent, name="person_add_event"),
    re_path(r"^persons/(?P<id>[0-9]+)/add_relation$", views.personAddRelation, name="person_add_relation"),
    re_path(r"^persons/(?P<id>[0-9]+)/add_tag$", views.personAddTag, name="person_add_tag"),
    re_path(r"^persons/(?P<id>[0-9]+)/add_attribute$", views.personAddAttribute, name="person_add_attribute"),
    re_path(r"^persons/(?P<id>[0-9]+)/add_link$", views.personAddLink, name="person_add_link"),
    #re_path(r"^persons/(?P<id>[0-9]+)/edit_events$", views.personEditEvents, name="person_edit_events"),
    #re_path(r"^persons/(?P<id>[0-9]+)/edit_relations$", views.personEditRelations, name="person_edit_relations"),
    re_path(r"^persons/(?P<id>[0-9]+)/edit_tags$", views.personEditTags, name="person_edit_tags"),
    re_path(r"^persons/(?P<id>[0-9]+)/edit_attributes$", views.personEditAttributes, name="person_edit_attributes"),
    re_path(r"^persons/(?P<id>[0-9]+)/edit_links$", views.personEditLinks, name="person_edit_links"),

    re_path(r"^groups$", views.groupsList, name="groups_list"),
    re_path(r"^groups/create$", views.groupCreate, name="group_create"),
    re_path(r"^groups/api/search$", views.groupsAPISearch, name="groups_api_search"),
    re_path(r"^groups/(?P<id>[0-9]+)$", views.groupInfo, name="group_info"),
    re_path(r"^groups/(?P<id>[0-9]+)/edit$", views.groupEdit, name="group_edit"),
    re_path(r"^groups/(?P<id>[0-9]+)/delete$", views.groupDelete, name="group_delete"),
    re_path(r"^groups/(?P<id>[0-9]+)/add_member$", views.groupAddMember, name="group_add_member"),
    re_path(r"^groups/(?P<id>[0-9]+)/add_attribute$", views.groupAddAttribute, name="group_add_attribute"),
    re_path(r"^groups/(?P<id>[0-9]+)/add_link$", views.groupAddLink, name="group_add_link"),
    re_path(r"^groups/(?P<id>[0-9]+)/edit_members$", views.groupEditMembers, name="group_edit_members"),
    re_path(r"^groups/(?P<id>[0-9]+)/edit_attributes$", views.groupEditAttributes, name="group_edit_attributes"),
    re_path(r"^groups/(?P<id>[0-9]+)/edit_links$", views.groupEditLinks, name="group_edit_links"),

    re_path(r"^countries$", views.countriesList, name="countries_list"),
    re_path(r"^countries/create$", views.countryCreate, name="country_create"),
    re_path(r"^countries/(?P<code>[a-zA-Z0-9]{3})$", views.countryInfo, name="country_info"),
    re_path(r"^countries/(?P<code>[a-zA-Z0-9]{3})/edit$", views.countryEdit, name="country_edit"),

    re_path(r"^languages$", views.languagesList, name="languages_list"),
    re_path(r"^languages/create$", views.languageCreate, name="language_create"),
    re_path(r"^languages/(?P<code>[a-zA-Z0-9]{3})$", views.languageInfo, name="language_info"),
    re_path(r"^languages/(?P<code>[a-zA-Z0-9]{3})/edit$", views.languageEdit, name="language_edit"),

    #re_path(r"^events$", views.eventsList, name="events_list"),
]
