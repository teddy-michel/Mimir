
import os
import shutil
import logging

import PIL
from PIL import Image
from django.conf import settings


def resize_image(filename:str):
    basewidth = settings.IMAGE_THUMB_WIDTH
    baseheight = settings.IMAGE_THUMB_HEIGHT

    folder, filename = os.path.split(filename)
    filename_thumb = os.path.join(settings.MEDIA_ROOT, folder, filename)
    filename_original = os.path.join(settings.MEDIA_ROOT, folder, "original", filename)

    try:
        img = Image.open(filename_thumb)

        if img.size[0] > basewidth or img.size[1] > baseheight:
            logging.info("Resize image '%s'" % filename_thumb)
            logging.info("Original size = %d x %d" % (img.size[0], img.size[1]))

            if float(img.size[0]) / float(img.size[1]) >= float(basewidth) / float(baseheight):
                newwidth = basewidth
                newheight = round(float(basewidth) * (float(img.size[1]) / float(img.size[0])))
            else:
                newwidth = round(float(baseheight) * (float(img.size[0]) / float(img.size[1])))
                newheight = baseheight

            img = img.resize((newwidth, newheight), PIL.Image.Resampling.LANCZOS)
            logging.info("Thumb filename = '%s'" % filename_thumb)
            logging.info("Thumb size = %d x %d" % (newwidth, newheight))

            shutil.copy(filename_thumb, filename_original)
            img.save(filename_thumb)
    except Exception as e:
        logging.error("Resize image error: %s" % str(e))


def remove_old_image(filename_old:str, filename_new:str):
    if filename_old != "" and filename_new != filename_old:
        oldImageFolder, oldImageFilename = os.path.split(filename_old)

        logging.info("Delete file '%s'" % os.path.join(settings.MEDIA_ROOT, oldImageFolder, oldImageFilename))
        try:
            os.remove(os.path.join(settings.MEDIA_ROOT, oldImageFolder, oldImageFilename))
        except:
            pass

        logging.info("Delete file '%s'" % os.path.join(settings.MEDIA_ROOT, oldImageFolder, "original", oldImageFilename))
        try:
            os.remove(os.path.join(settings.MEDIA_ROOT, oldImageFolder, "original", oldImageFilename))
        except:
            pass
