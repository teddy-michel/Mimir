
from django.http import JsonResponse, HttpResponseBadRequest


def generic_APIList(request, columns, query_set, formatter):
    try:
        draw = int(request.GET.get("draw"))
    except:
        return HttpResponseBadRequest("Invalid field 'draw'.")

    try:
        start = int(request.GET.get("start"))
        if start < 0:
            start = 0
    except:
        return HttpResponseBadRequest("Invalid field 'start'.")

    try:
        length = int(request.GET.get("length"))
        if length == 0:
            return HttpResponseBadRequest("Invalid field 'length'.")
    except:
        return HttpResponseBadRequest("Invalid field 'length'.")

    total = query_set.count()

    # Tri
    orders = []
    for i in range(0, 9):
        if ("order[%d][dir]" % i) not in request.GET or ("order[%d][column]" % i) not in request.GET:
            break

        try:
            orderCol = int(request.GET.get("order[%d][column]" % i))
        except:
            orderCol = 1
        if orderCol not in range(0, len(columns)):
            orderCol = 1

        try:
            orderDir = request.GET.get("order[%d][dir]" % i)
        except:
            orderDir = "asc"

        if orderDir == "desc":
            orders.append("-" + columns[orderCol])
        else:
            orders.append(columns[orderCol])

    query_set = query_set.order_by(*orders)

    # Limite
    if length < 0:
        query_set = query_set[start:]
    else:
        query_set = query_set[start:start+length]

    result = {
        "draw": draw,
        "recordsTotal": total,
        "recordsFiltered": total,
        "data": [],
    }

    for item in query_set:
        result["data"].append(formatter(item))

    return JsonResponse(result)


def generic_APISearch(request, query_set, formatter):
    total = query_set.count()

    # Pagination
    itemPerPage = 10

    try:
        page = int(request.GET.get("page", 1))
    except:
        page = 1

    start = (page - 1) * itemPerPage

    try:
        length = int(request.GET.get("length", itemPerPage))
        if length == 0:
            return HttpResponseBadRequest("Invalid field 'length'.")
    except:
        length = itemPerPage

    if length < 0:
        query_set = query_set[start:]
    else:
        query_set = query_set[start:start+length]

    result = {
        "total_count": total,
        "start": start,
        "page": page,
        "incomplete_results": False,
        "items": [],
    }

    for item in query_set:
        result["items"].append(formatter(item))

    result["length"] = len(result["items"])
    if start + result["length"] < total:
        result["incomplete_results"] = True

    return JsonResponse(result)
