from django.contrib import admin

from .models import Person, Group, GroupMember, Tag, PersonTag, PersonRelation, PersonEvent, Language, GroupAttribute, \
    GroupLink, Country, PersonLink, PersonAttribute, Event, EventName


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ("name", "gender")


@admin.register(PersonAttribute)
class PersonAttributeAdmin(admin.ModelAdmin):
    list_display = ("person", "name", "value")


@admin.register(PersonLink)
class PersonLinkAdmin(admin.ModelAdmin):
    list_display = ("person", "name", "uri")


@admin.register(PersonRelation)
class PersonRelationAdmin(admin.ModelAdmin):
    list_display = ("person", "personto", "type")


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ("id", "is_unique", "icon")


@admin.register(EventName)
class EventNameAdmin(admin.ModelAdmin):
    list_display = ("event", "lang", "name")


@admin.register(PersonEvent)
class PersonEventAdmin(admin.ModelAdmin):
    list_display = ("person", "event")


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(GroupAttribute)
class GroupAttributeAdmin(admin.ModelAdmin):
    list_display = ("group", "name", "value")


@admin.register(GroupLink)
class GroupLinkAdmin(admin.ModelAdmin):
    list_display = ("group", "name", "uri")


@admin.register(GroupMember)
class GroupMemberAdmin(admin.ModelAdmin):
    list_display = ("group", "person")


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ("name", "parent")


@admin.register(PersonTag)
class PersonTagAdmin(admin.ModelAdmin):
    list_display = ("person", "tag", "info")


@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):
    list_display = ("name", "code", "iso1", "iso2", "iso3")
    ordering = ["name"]


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ("name", "code", "iso")
    ordering = ["name"]
