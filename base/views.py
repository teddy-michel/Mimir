
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q, Prefetch
from django.forms import modelformset_factory
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _
from django.views.decorators.http import require_safe

from books.models import Reading, EditionText, Translation, TextTag
from movies.models import MovieActor, MovieActorDubbing, MovieCrewMember, MovieView, MovieTag, SerieTag
from games.models import GameTag, GameUser
from .forms import PersonForm, TagForm, PersonEventForm, PersonRelationForm, PersonTagForm, PersonTagFormClassic, PersonAttributeForm, \
    GroupForm, \
    GroupMemberForm, GroupAttributeForm, GroupLinkForm, PersonLinkForm, CountryForm, LanguageForm
from .api import generic_APIList
from .models import Tag, Person, PersonTag, PersonAttribute, Group, GroupMember, Country, GroupAttribute, \
    GroupLink, PersonLink, Language, Event
from .templatetags.mimir import date_format, date_year, get_order, gender_text


@login_required
@require_safe
def home(request):
    return render(request, "home.html")


@login_required
@require_safe
def index(request):
    persons_count = Person.objects.count()
    groups_count = Group.objects.count()
    tags_count = Tag.objects.count()
    countries_count = Country.objects.count()
    languages_count = Language.objects.count()

    return render(request, "base/index.html", {
        "persons_count": persons_count,
        "groups_count": groups_count,
        "tags_count": tags_count,
        "countries_count": countries_count,
        "languages_count": languages_count,
    })


@login_required
@require_safe
def tagsList(request):
    tags = Tag.objects.all()
    return render(request, "base/tags_list.html", {"tags": tags})


@login_required
@require_safe
def tagsAPISearch(request):
    query = request.GET.get("q", "")

    query_set = Tag.objects.filter(name__icontains=query)

    if "exclude" in request.GET:
        excludeTagsId = set()
        for id in request.GET["exclude"].split(","):
            try:
                id = int(id)
                excludeTagsId.add(id)
            except:
                continue
        query_set = query_set.exclude(id__in=excludeTagsId)

    # Tri
    query_set = query_set.order_by("name")

    total = query_set.count()

    # Pagination
    itemPerPage = 10

    try:
        page = int(request.GET.get("page", 1))
    except:
        page = 1

    start = (page - 1) * itemPerPage

    try:
        length = int(request.GET.get("length", itemPerPage))
        if length == 0:
            return HttpResponseBadRequest("Invalid field 'length'.")
    except:
        length = itemPerPage

    result = {
        "start": start,
        "page": page,
        "incomplete_results": False,
        "items": [],
    }

    numItems = 0

    # Add entry for creating tag
    numEntriesCreate = 0
    if query and "create" in request.GET:
        try:
            Tag.objects.get(name=query)
        except Tag.DoesNotExist:
            numEntriesCreate += 1
            if start <= 0:
                try:
                    n = int(request.GET.get("n", 1))
                except:
                    n = 1

                result["items"].append({"id": -n, "name": query, "url": ""})
                numItems += 1

    if length < 0:
        if start > numEntriesCreate:
            query_set = query_set[start - numEntriesCreate:]
    elif length > numItems:
        if start < numEntriesCreate:
            if numEntriesCreate < start + length:
                query_set = query_set[0:length + start - numEntriesCreate]
            else:
                query_set = Tag.objects.none()
        else:
            query_set = query_set[start - numEntriesCreate:start - numEntriesCreate + length - numItems]
    else:
        query_set = Tag.objects.none()

    for tag in query_set:
        result["items"].append({
            "id":    tag.id,
            "name":  tag.name,
            "url":   reverse("tag_info", kwargs={"id": "%06d" % tag.id}),
        })
        numItems += 1

    result["length"] = numItems
    result["total_count"] = total + numEntriesCreate
    if start + numItems < total + numEntriesCreate:
        result["incomplete_results"] = True

    return JsonResponse(result)


@login_required
@require_safe
def tagInfo(request, id):
    readings_queryset = Reading.objects.filter(user=request.user, date_end__isnull=False).order_by()
    collections_queryset = EditionText.objects.select_related("edition").prefetch_related(Prefetch("edition__readings", readings_queryset)).order_by()
    translations_queryset = Translation.objects.prefetch_related(Prefetch("collections", collections_queryset), Prefetch("readings", readings_queryset)).order_by()
    texts_queryset = TextTag.objects.select_related("text", "text__work", "text__translation").prefetch_related(Prefetch("text__work__readings", readings_queryset), Prefetch("text__work__collections", collections_queryset), Prefetch("text__work__translations", translations_queryset), "text__work__authors")
    persons_queryset = PersonTag.objects.select_related("person")
    views_queryset = MovieView.objects.filter(user=request.user)
    movies_queryset = MovieTag.objects.select_related("movie", "movie__episode", "movie__episode__season", "movie__episode__season__serie").prefetch_related(Prefetch("movie__views", views_queryset)).order_by("info", "movie__date")
    series_queryset = SerieTag.objects.select_related("serie")
    games_user = GameUser.objects.filter(user=request.user)
    games_queryset = GameTag.objects.select_related("game").prefetch_related(Prefetch("game__users", games_user))

    try:
        tag = Tag.objects.prefetch_related(Prefetch("movies", movies_queryset), Prefetch("series", series_queryset), Prefetch("persons", persons_queryset), Prefetch("texts", texts_queryset), Prefetch("games", games_queryset)).get(id=id)
    except Tag.DoesNotExist:
        return redirect("tags_list")

    # Movies
    viewed = request.GET.get("viewed", "all")
    if viewed not in ["all", "yes", "no"]:
        viewed = "all"

    movies = []
    for movie_tag in tag.movies.all():
        if request.user.is_authenticated:
            movie_tag.movie.viewed = (movie_tag.movie.views.count() != 0)
            if viewed == "yes" and movie_tag.movie.viewed is True:
                movies.append(movie_tag)
            elif viewed == "no" and movie_tag.movie.viewed is False:
                movies.append(movie_tag)
            elif viewed == "all":
                movies.append(movie_tag)
        else:
            movies.append(movie_tag)

    # Texts
    texts = []
    for work_tag in tag.texts.all():
        work_tag.read = False
        if hasattr(work_tag.text, "work"):
            work_tag.read = work_tag.text.work.read_by_user()
        elif hasattr(work_tag.text, "translation"):
            work_tag.read = work_tag.text.translation.read_by_user()
        texts.append(work_tag)

    # Games
    games = []
    for game_tag in tag.games.all():
        game_tag.finished = False
        game_tag.played = False
        if request.user.is_authenticated:
            for game_user in game_tag.game.users.all():
                if game_user.played:
                    game_tag.played = True
                if game_user.finished:
                    game_tag.finished = True
        games.append(game_tag)

    return render(request, "base/tag_info.html", {"tag": tag, "movies": movies, "texts": texts, "games": games})


@login_required
@permission_required("base.change_tag")
def tagEdit(request, id):
    try:
        tag = Tag.objects.get(id=id)
    except Tag.DoesNotExist:
        return redirect("tags_list")

    if request.method == "POST":
        form = TagForm(request.POST, request.FILES, instance=tag)

        if form.is_valid():
            tag = form.save()
            tag.modification = timezone.now()
            tag.save()
            return redirect("tag_info", "%06d" % tag.id)
    else:
        form = TagForm(instance=tag)

    return render(request, "base/tag_edit.html", {"tag": tag, "form": form})


@login_required
@permission_required("base.delete_tag")
def tagDelete(request, id):
    try:
        tag = Tag.objects.get(id=id)
    except Tag.DoesNotExist:
        return redirect("tags_list")

    if request.method == "POST":
        if "delete" in request.POST:
            tag.delete()
        return redirect("tags_list")

    return render(request, "base/tag_delete.html", {"tag": tag})


@login_required
@require_safe
def personsList(request):
    """ => AJAX
    try:
        page = int(request.GET.get("page", 1))
    except:
        page = 1

    persons = Person.objects.all().order_by("name")
    paginator = Paginator(persons, 100)

    try:
        persons = paginator.page(page)
    except PageNotAnInteger:
        persons = paginator.page(1)
    except EmptyPage:
        persons = paginator.page(paginator.num_pages)
    """

    return render(request, "base/persons_list.html")


@login_required
@permission_required("base.add_person")
def personCreate(request):
    if request.method == "POST":
        form = PersonForm(request.POST)

        if form.is_valid():
            person = form.save()
            return redirect("person_info", "%06d" % person.id)
    else:
        form = PersonForm()

    return render(request, "base/person_create.html", {"form": form})


@login_required
def personsSearch(request):
    query = request.GET.get("q", "").strip()

    if query:
        persons = Person.objects.filter(name__icontains=query)
    else:
        persons = Person.objects.none()

    persons_count = persons.count()

    if query:
        groups = Group.objects.filter(name__icontains=query)
    else:
        groups = Group.objects.none()

    return render(request, "base/persons_search.html", {
            "query": query,
            "persons": persons,
            "persons_count": persons_count,
            "groups": groups,
        })


@login_required
@require_safe
def personsAPIList(request):
    columns = ["name", "gender", "birthdate", "deathdate"]

    query = request.GET.get("q", "").strip()
    if query:
        query_set = Person.objects.filter(name__icontains=query)
    else:
        query_set = Person.objects.all()

    def formatter(person):
        return {
            "id":            person.id,
            "name":          person.name,
            "url":           reverse("person_info", kwargs={"id": "%06d" % person.id}),
            "gender":        person.gender,
            "gender_txt":    gender_text(person.gender),
            "birthdate":     person.birthdate,
            "birthdate_txt": date_format(person.birthdate),
            "birth_year":    date_year(person.birthdate),
            "deathdate":     person.deathdate,
            "deathdate_txt": date_format(person.deathdate),
            "death_year":    date_year(person.deathdate),
        }

    return generic_APIList(request, columns, query_set, formatter)


@login_required
@require_safe
def personsAPISearch(request):
    query = request.GET.get("q", "").strip()

    if "groups" in request.GET:
        groups = Group.objects.filter(name__icontains=query)
    else:
        groups = Group.objects.none()

    numGroups = groups.count()
    persons = Person.objects.filter(name__icontains=query)
    total = persons.count() + numGroups

    persons = persons.order_by("name")
    groups = groups.order_by("name")

    # Pagination
    itemPerPage = 10

    try:
        page = int(request.GET.get("page", 1))
    except:
        page = 1

    start = (page - 1) * itemPerPage

    try:
        length = int(request.GET.get("length", itemPerPage))
        if length == 0:
            return HttpResponseBadRequest("Invalid field 'length'.")
    except:
        length = itemPerPage

    result = {
        "start": start,
        "page": page,
        "incomplete_results": False,
        "items": [],
    }

    numItems = 0

    # Add entry for creating person or group
    numEntriesCreate = 0
    if query and "create" in request.GET:
        try:
            n = int(request.GET.get("n", 1))
        except:
            n = 1

        numEntriesCreate += 1
        if start <= 0:
            """
            try:
                person = Person.objects.get(name=query)
            except:
                result["items"].append({"id": -n, "name": query, "url": ""})
            """
            result["items"].append({"id": -n, "name": query, "url": ""})
            numItems += 1

        if "groups" in request.GET:
            numEntriesCreate += 1
            if start <= 1:
                """
                try:
                    group = Group.objects.get(name=query)
                except:
                    result["items"].append({"id": -n - 1, "name": query, "group": True, "url": ""})
                """
                result["items"].append({"id": -n - 1, "name": query, "group": True, "url": ""})
                numItems += 1

    if length < 0:
        if start > numEntriesCreate:
            groups = groups[start - numEntriesCreate:]
            if start < numGroups + numEntriesCreate:
                persons = persons[0:]
            else:
                persons = persons[start - numEntriesCreate - numGroups:]
    elif length > numItems:
        if start < numGroups + numEntriesCreate:
            if start > numEntriesCreate:
                groups = groups[start - numEntriesCreate:start - numEntriesCreate + length - numItems]
            else:
                groups = groups[0:length - numItems]

            if numGroups + numEntriesCreate < start + length:
                if start > numGroups + numEntriesCreate:
                    persons = persons[start - numEntriesCreate - numGroups:start - numEntriesCreate - numGroups + length - numItems]
                else:
                    persons = persons[0:length + start - numEntriesCreate - numGroups]
            else:
                persons = Person.objects.none()

            #persons = persons[0:length - numItems - numGroups]
            #persons = Person.objects.none()
        else:
            groups = Group.objects.none()
            persons = persons[start - numEntriesCreate - numGroups:start - numEntriesCreate - numGroups + length - numItems]
    else:
        persons = Person.objects.none()
        groups = Group.objects.none()

    for group in groups:
        result["items"].append({
            "id":    group.id,
            "name":  group.name,
            "group": True,
            "url":   reverse("group_info", kwargs={"id": "%06d" % group.id}),
        })
        numItems += 1

    for person in persons:
        result["items"].append({
            "id":         person.id,
            "name":       person.name,
            "birth_year": date_year(person.birthdate),
            "death_year": date_year(person.deathdate),
            "gender":     person.gender,
            "url":        reverse("person_info", kwargs={"id": "%06d" % person.id}),
        })
        numItems += 1

    result["length"] = numItems
    result["total_count"] = total + numEntriesCreate
    if start + numItems < total + numEntriesCreate:
        result["incomplete_results"] = True

    return JsonResponse(result)


@login_required
@require_safe
def personInfo(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    movies_actor = []
    movies_dubbing_actor = []
    movies_dubbed_by = []
    movies_crew = []

    if hasattr(person, "moviename"):
        movies_actor_cursor = person.moviename.movies_as_actor.filter(Q(movie__ismovie=True) | Q(movie__episode__isnull=True)).select_related("movie", "lang")
        movies_dubbing_actor_cursor = person.moviename.dubbing_actor.filter(Q(movie__ismovie=True) | Q(movie__episode__isnull=True)).select_related("movie", "lang", "actor", "actor__person", "actor__group")
        movies_dubbed_by_cursor = person.moviename.dubbed_by.filter(Q(movie__ismovie=True) | Q(movie__episode__isnull=True)).select_related("movie", "lang", "person", "person__person", "person__group")
        movies_crew_cursor = person.moviename.movies_as_crew_member.filter(Q(movie__ismovie=True) | Q(movie__episode__isnull=True)).select_related("movie")
        tmp_episodes_actor = person.moviename.movies_as_actor.filter(movie__episode__isnull=False).select_related("movie", "lang", "movie__episode__season__serie")
        tmp_episodes_dubbing_actor = person.moviename.dubbing_actor.filter(movie__episode__isnull=False).select_related("movie", "lang", "actor", "actor__person", "actor__group", "movie__episode__season__serie")
        tmp_episodes_dubbed_by = person.moviename.dubbed_by.filter(movie__episode__isnull=False).select_related("movie", "lang", "person", "person__person", "person__group", "movie__episode__season__serie")
        tmp_episodes_crew = person.moviename.movies_as_crew_member.filter(movie__episode__isnull=False).select_related("movie", "movie__episode__season__serie")

        for movieActor in movies_actor_cursor:
            if request.user.is_authenticated:
                movieActor.movie.viewed = (movieActor.movie.views.filter(user=request.user).count() != 0)
            movies_actor.append(movieActor)

        for movieActor in movies_dubbing_actor_cursor:
            if request.user.is_authenticated:
                movieActor.movie.viewed = (movieActor.movie.views.filter(user=request.user).count() != 0)
            movies_dubbing_actor.append(movieActor)

        for movieActor in movies_dubbed_by_cursor:
            if request.user.is_authenticated:
                movieActor.movie.viewed = (movieActor.movie.views.filter(user=request.user).count() != 0)
            movies_dubbed_by.append(movieActor)

        for crewMember in movies_crew_cursor:
            if request.user.is_authenticated:
                crewMember.movie.viewed = (crewMember.movie.views.filter(user=request.user).count() != 0)
                movies_crew.append(crewMember)
    else:
        tmp_episodes_actor = MovieActor.objects.none()
        tmp_episodes_dubbing_actor = MovieActorDubbing.objects.none()
        tmp_episodes_dubbed_by = MovieActorDubbing.objects.none()
        tmp_episodes_crew = MovieCrewMember.objects.none()

    episodes_actor = {}
    for actor in tmp_episodes_actor:
        episode = actor.movie.episode
        sid = episode.season.serie.id
        if sid not in episodes_actor:
            episodes_actor[sid] = {"serie": episode.season.serie, "episodes_u": {}, "episodes": []}
        num = "%09d-%09d" % (get_order(episode.season), get_order(episode))
        if num not in episodes_actor[sid]["episodes_u"]:
            episodes_actor[sid]["episodes_u"][num] = {"episode": episode, "actors": []}
        episodes_actor[sid]["episodes_u"][num]["actors"].append(actor)

    for sid in episodes_actor.keys():
        for num in sorted(episodes_actor[sid]["episodes_u"].keys()):
            episodes_actor[sid]["episodes"].append(episodes_actor[sid]["episodes_u"][num])
        del episodes_actor[sid]["episodes_u"]

    episodes_dubbing_actor = {}
    for actor in tmp_episodes_dubbing_actor:
        episode = actor.movie.episode
        sid = episode.season.serie.id
        if sid not in episodes_dubbing_actor:
            episodes_dubbing_actor[sid] = {"serie": episode.season.serie, "episodes_u": {}, "episodes": []}
        num = "%09d-%09d" % (get_order(episode.season), get_order(episode))
        if num not in episodes_dubbing_actor[sid]["episodes_u"]:
            episodes_dubbing_actor[sid]["episodes_u"][num] = {"episode": episode, "actors": []}
        episodes_dubbing_actor[sid]["episodes_u"][num]["actors"].append(actor)

    for sid in episodes_dubbing_actor.keys():
        for num in sorted(episodes_dubbing_actor[sid]["episodes_u"].keys()):
            episodes_dubbing_actor[sid]["episodes"].append(episodes_dubbing_actor[sid]["episodes_u"][num])
        del episodes_dubbing_actor[sid]["episodes_u"]

    episodes_dubbed_by = {}
    for actor in tmp_episodes_dubbed_by:
        episode = actor.movie.episode
        sid = episode.season.serie.id
        if sid not in episodes_dubbed_by:
            episodes_dubbed_by[sid] = {"serie": episode.season.serie, "episodes_u": {}, "episodes": []}
        num = "%09d-%09d" % (get_order(episode.season), get_order(episode))
        if num not in episodes_dubbed_by[sid]["episodes_u"]:
            episodes_dubbed_by[sid]["episodes_u"][num] = {"episode": episode, "actors": []}
        episodes_dubbed_by[sid]["episodes_u"][num]["actors"].append(actor)

    for sid in episodes_dubbed_by.keys():
        for num in sorted(episodes_dubbed_by[sid]["episodes_u"].keys()):
            episodes_dubbed_by[sid]["episodes"].append(episodes_dubbed_by[sid]["episodes_u"][num])
        del episodes_dubbed_by[sid]["episodes_u"]

    episodes_crew = {}
    for member in tmp_episodes_crew:
        episode = member.movie.episode
        sid = episode.season.serie.id
        if sid not in episodes_crew:
            episodes_crew[sid] = {"serie": episode.season.serie, "episodes_u": {}, "episodes": []}
        num = "%09d-%09d" % (get_order(episode.season), get_order(episode))
        if num not in episodes_crew[sid]["episodes_u"]:
            episodes_crew[sid]["episodes_u"][num] = {"episode": episode, "members": []}
        episodes_crew[sid]["episodes_u"][num]["members"].append(member)

    for sid in episodes_crew.keys():
        for num in sorted(episodes_crew[sid]["episodes_u"].keys()):
            episodes_crew[sid]["episodes"].append(episodes_crew[sid]["episodes_u"][num])
        del episodes_crew[sid]["episodes_u"]

    homonyms = Person.objects.filter(name=person.name).exclude(id=person.id)
    tags = person.tags.select_related("tag")
    links = person.links.select_related("lang")

    readings_queryset = Reading.objects.filter(user=request.user, date_end__isnull=False).order_by()
    collectionsQuerySet = EditionText.objects.select_related("edition").prefetch_related(Prefetch("edition__readings", readings_queryset)).order_by()
    translationsQuerySet = Translation.objects.prefetch_related(Prefetch("collections", collectionsQuerySet), Prefetch("readings", readings_queryset)).order_by()

    works = []
    for work in person.works.select_related("lang", "genre").prefetch_related(Prefetch("collections", collectionsQuerySet), Prefetch("translations", translationsQuerySet), Prefetch("readings", queryset=readings_queryset)).all():
        if request.user.is_authenticated:
            work.read = work.read_by_user()
        else:
            work.read = False
        works.append(work)

    translations = []
    for translation in person.translations.select_related("original_work", "lang").prefetch_related("original_work__authors", Prefetch("collections", collectionsQuerySet), Prefetch("readings", queryset=readings_queryset)).all():
        if request.user.is_authenticated:
            translation.read = translation.read_by_user()
        else:
            translation.read = False
        translations.append(translation)

    return render(request, "base/person_info.html", {
        "person":                 person,
        "homonyms":               homonyms,
        "movies_actor":           movies_actor,
        "movies_dubbing_actor":   movies_dubbing_actor,
        "movies_dubbed_by":       movies_dubbed_by,
        "movies_crew":            movies_crew,
        "episodes_actor":         episodes_actor,
        "episodes_dubbing_actor": episodes_dubbing_actor,
        "episodes_dubbed_by":     episodes_dubbed_by,
        "episodes_crew":          episodes_crew,
        "works":                  works,
        "translations":           translations,
        "tags":                   tags,
        "links":                  links,
    })


@login_required
@permission_required("base.change_person")
def personEdit(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    if request.method == "POST":
        form = PersonForm(request.POST, request.FILES, instance=person)

        if form.is_valid():
            person = form.save()
            person.modification = timezone.now()
            person.save()
            return redirect("person_info", "%06d" % person.id)
    else:
        form = PersonForm(instance=person)

    return render(request, "base/person_edit.html", {"person": person, "form": form})


@login_required
@permission_required(["base.change_person", "base.delete_person"])
def personMerge(request, id1, id2):
    if id1 == id2:
        return redirect("person_info", id1)

    try:
        person1 = Person.objects.get(id=id1)
    except:
        return redirect("persons_list")

    try:
        person2 = Person.objects.get(id=id2)
    except:
        return redirect("persons_list")

    diff = []

    if person1.name != person2.name:
        diff.append({"key": "name", "p1": person1.name, "p2": person2.name, "label": _("Name"), "s1": False if person2.name and not person1.name else True})
    if person1.firstname != person2.firstname:
        diff.append({"key": "firstname", "p1": person1.firstname, "p2": person2.firstname, "label": _("First name"), "s1": False if person2.firstname and not person1.firstname else True})
    if person1.lastname != person2.lastname:
        diff.append({"key": "lastname", "p1": person1.lastname, "p2": person2.lastname, "label": _("Last name"), "s1": False if person2.lastname and not person1.lastname else True})
    if person1.gender != person2.gender:
        diff.append({"key": "gender", "p1": gender_text(person1.gender), "p2": gender_text(person2.gender), "label": _("Gender"), "s1": False if person2.gender != 'U' and person1.gender == 'U' else True})
    if person1.birthdate != person2.birthdate:
        diff.append({"key": "birthdate", "p1": person1.birthdate, "p2": person2.birthdate, "label": _("Birth date"), "s1": False if person2.birthdate and not person1.birthdate else True})
    if person1.birthplace != person2.birthplace:
        diff.append({"key": "birthplace", "p1": person1.birthplace, "p2": person2.birthplace, "label": _("Birth place"), "s1": False if person2.birthplace and not person1.birthplace else True})
    if person1.deathdate != person2.deathdate:
        diff.append({"key": "deathdate", "p1": person1.deathdate, "p2": person2.deathdate, "label": _("Death date"), "s1": False if person2.deathdate and not person1.deathdate else True})
    if person1.deathplace != person2.deathplace:
        diff.append({"key": "deathplace", "p1": person1.deathplace, "p2": person2.deathplace, "label": _("Death place"), "s1": False if person2.deathplace and not person1.deathplace else True})
    if person1.info != person2.info:
        diff.append({"key": "info", "p1": person1.info, "p2": person2.info, "label": _("Information"), "s1": False if person2.info and not person1.info else True})

    if request.method == "POST":
        personEdited = False
        for entry in diff:
            if entry["key"] not in request.POST:
                return redirect("person_info", "%06d" % person1.id)
            if request.POST[entry["key"]] == "2":
                setattr(person1, entry["key"], getattr(person2, entry["key"]))
                personEdited = True

        if personEdited:
            person1.save()

        for group in person2.groups.all():
            if person1 in group.persons.all():
                continue
            group.persons.remove(person2)
            group.persons.add(person1)
            group.save()

        for event in person2.events.all():
            event.person = person1
            event.save()

        for relation in person2.relations.all():
            if relation.personto != person1:
                relation.person = person1
                relation.save()

        for relation in person2.relations_to.all():
            if relation.person != person1:
                relation.personto = person1
                relation.save()

        for attribute in person2.attributes.all():
            attribute.person = person1
            attribute.save()

        for link in person2.links.all():
            link.person = person1
            link.save()

        for tag in person2.tags.all():
            tag.person = person1
            tag.save()

        for work in person2.works.all():
            work.authors.remove(person2)
            work.authors.add(person1)
            work.save()

        for translation in person2.translations.all():
            translation.translators.remove(person2)
            translation.translators.add(person1)
            translation.save()

        for ref in person2.ref_by.all():
            ref.ref_person = person1
            ref.save()

        if hasattr(person2, "moviename"):
            movieName2 = person2.moviename
            if hasattr(person1, "moviename"):
                movieName1 = person1.moviename
                for actor in movieName2.movies_as_actor.all():
                    actor.person = movieName1
                    actor.save()
                for actor in movieName2.dubbing_actor.all():
                    actor.person = movieName1
                    actor.save()
                for actor in movieName2.dubbed_by.all():
                    actor.actor = movieName1
                    actor.save()
                for member in movieName2.movies_as_crew_member.all():
                    member.person = movieName1
                    member.save()
                for member in movieName2.series_as_crew_member.all():
                    member.person = movieName1
                    member.save()
            else:
                movieName2.person = person1
                movieName2.save()

        person2.delete()

        return redirect("person_info", "%06d" % person1.id)

    return render(request, "base/person_merge.html", {"person1": person1, "person2": person2, "diff": diff})


@login_required
@permission_required("base.delete_person")
def personDelete(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    if request.method == "POST":
        if "delete" in request.POST:
            person.delete()
        return redirect("persons_list")

    return render(request, "base/person_delete.html", {"person": person})


@login_required
@permission_required("base.add_personevent")
def personAddEvent(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    if request.method == "POST":
        form = PersonEventForm(request.POST)

        if form.is_valid():
            event = form.save(commit=False)
            if event:
                event.person = person
                event.save()
            return redirect("person_info", "%06d" % person.id)
    else:
        form = PersonEventForm()

    exclude = []
    for event in person.events.select_related("event").filter(event__is_unique=True):
        exclude.append(event.event.id)
    form.fields["event"].queryset = Event.objects.exclude(id__in=exclude)

    url = reverse("person_add_event", kwargs={"id": id})
    return render(request, "base/person_add_event.html", {"person": person, "form": form, "form_url": url})


@login_required
@permission_required("base.add_personrelation")
def personAddRelation(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    if request.method == "POST":
        form = PersonRelationForm(request.POST)

        if form.is_valid():
            relation = form.save(commit=False)
            if relation:
                relation.person = person
                relation.save()
            return redirect("person_info", "%06d" % person.id)
    else:
        form = PersonRelationForm()

    url = reverse("person_add_relation", kwargs={"id": id})
    return render(request, "base/person_add_relation.html", {"person": person, "form": form, "form_url": url})


@login_required
@permission_required("base.add_persontag")
def personAddTag(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    if request.method == "POST":
        form = PersonTagForm(request.POST)

        if form.is_valid():
            personTag = form.save(commit=False)
            if personTag:
                personTag.person = person
                personTag.save()
            return redirect("person_info", "%06d" % person.id)
    else:
        form = PersonTagForm()

    url = reverse("person_add_tag", kwargs={"id": "%06d" % person.id})
    return render(request, "base/person_add_tag.html", {"person": person, "form": form, "form_url": url})


@login_required
@permission_required("base.add_personattribute")
def personAddAttribute(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    if request.method == "POST":
        form = PersonAttributeForm(request.POST)

        if form.is_valid():
            attribute = form.save(commit=False)
            if attribute:
                attribute.person = person
                attribute.save()
            return redirect("person_info", "%06d" % person.id)
    else:
        form = PersonAttributeForm()

    url = reverse("person_add_attribute", kwargs={"id": "%06d" % person.id})
    return render(request, "base/person_add_attribute.html", {"person": person, "form": form, "form_url": url})


@login_required
@permission_required("base.add_personlink")
def personAddLink(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    if request.method == "POST":
        form = PersonLinkForm(request.POST)

        if form.is_valid():
            link = form.save(commit=False)
            if link:
                link.person = person
                link.save()
            return redirect("person_info", "%06d" % person.id)
    else:
        form = PersonLinkForm()

    url = reverse("person_add_link", kwargs={"id": "%06d" % person.id})
    return render(request, "base/person_add_link.html", {"person": person, "form": form, "form_url": url})


@login_required
@permission_required("base.change_persontag")
def personEditTags(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    tags = person.tags.all()

    if tags.count() == 0:
        extra = 1
    else:
        extra = 0

    PersonTagFormFormSet = modelformset_factory(PersonTag, form=PersonTagFormClassic, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = PersonTagFormFormSet(request.POST, queryset=tags)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.person = person
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("person_info", "%06d" % person.id)
    else:
        formset = PersonTagFormFormSet(queryset=tags)

    url = reverse("person_edit_tags", kwargs={"id": "%06d" % person.id})
    title = _("Edit tags")
    return render(request, "base/person_edit_data.html", {"person": person, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("base.change_personattribute")
def personEditAttributes(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    attributes = person.attributes.all()

    if attributes.count() == 0:
        extra = 1
    else:
        extra = 0

    PersonAttributeFormSet = modelformset_factory(PersonAttribute, form=PersonAttributeForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = PersonAttributeFormSet(request.POST, queryset=attributes)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.person = person
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("person_info", "%06d" % person.id)
    else:
        formset = PersonAttributeFormSet(queryset=attributes)

    url = reverse("person_edit_attributes", kwargs={"id": "%06d" % person.id})
    title = _("Edit attributes")
    return render(request, "base/person_edit_data.html", {"person": person, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("base.change_personlink")
def personEditLinks(request, id):
    try:
        person = Person.objects.get(id=id)
    except:
        return redirect("persons_list")

    links = person.links.all()

    if links.count() == 0:
        extra = 1
    else:
        extra = 0

    PersonLinkFormSet = modelformset_factory(PersonLink, form=PersonLinkForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = PersonLinkFormSet(request.POST, queryset=links)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.person = person
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("person_info", "%06d" % person.id)
    else:
        formset = PersonLinkFormSet(queryset=links)

    url = reverse("person_edit_links", kwargs={"id": "%06d" % person.id})
    title = _("Edit links")
    return render(request, "base/person_edit_data.html", {"person": person, "title": title, "url_form": url, "formset": formset})


@login_required
@require_safe
def groupsList(request):
    groups = Group.objects.all()
    return render(request, "base/groups_list.html", {"groups": groups})


@login_required
@require_safe
def groupsAPISearch(request):
    query = request.GET.get("q", "")

    groups = Group.objects.filter(name__icontains=query)
    total = groups.count()
    groups = groups.order_by("name")

    # Pagination
    itemPerPage = 10

    try:
        page = int(request.GET.get("page", 1))
    except:
        page = 1

    start = (page - 1) * itemPerPage

    try:
        length = int(request.GET.get("length", itemPerPage))
        if length == 0:
            return HttpResponseBadRequest("Invalid field 'length'.")
    except:
        length = itemPerPage

    result = {
        "start": start,
        "page": page,
        "incomplete_results": False,
        "items": [],
    }

    numItems = 0

    # Add entry for creating group
    if query and "create" in request.GET:
        numEntriesCreate = 1
        if start <= 0:
            try:
                n = int(request.GET.get("n", 1))
            except:
                n = 1

            """
            try:
                group = Group.objects.get(name=query)
            except:
                result["items"].append({"id": -1, "name": query, "group": True, "url": ""})
            """
            result["items"].append({"id": -n, "name": query, "group": True, "url": ""})
            numItems += 1
    else:
        numEntriesCreate = 0

    if length < 0:
        if start > numEntriesCreate:
            groups = groups[start - numEntriesCreate:]
    elif length > numItems:
        if start > numEntriesCreate:
            groups = groups[start - numEntriesCreate:start - numEntriesCreate + length - numItems]
        else:
            groups = groups[0:length - numItems]
    else:
        groups = Group.objects.none()

    for group in groups:
        result["items"].append({
            "id":   group.id,
            "name": group.name,
            "url":  reverse("group_info", kwargs={"id": "%06d" % group.id}),
        })
        numItems += 1

    result["length"] = numItems
    result["total_count"] = total + numEntriesCreate
    if start + numItems < total + numEntriesCreate:
        result["incomplete_results"] = True

    return JsonResponse(result)


@login_required
@require_safe
def groupInfo(request, id):
    try:
        group = Group.objects.get(id=id)
    except:
        return redirect("groups_list")

    if hasattr(group, "moviename"):
        movies_actor = group.moviename.movies_as_actor.filter(movie__ismovie=True).select_related("movie", "lang")
        movies_dubbing_actor = group.moviename.dubbing_actor.filter(movie__ismovie=True).select_related("movie", "lang", "actor", "actor__person", "actor__group")
        movies_dubbed_by = group.moviename.dubbed_by.filter(movie__ismovie=True).select_related("movie", "lang", "person", "person__person", "person__group")
        movies_crew = group.moviename.movies_as_crew_member.filter(movie__ismovie=True).select_related("movie")
        tmp_episodes_actor = group.moviename.movies_as_actor.filter(movie__episode__isnull=False).select_related("movie", "lang", "movie__episode__season__serie")
        tmp_episodes_dubbing_actor = group.moviename.dubbing_actor.filter(movie__episode__isnull=False).select_related("movie", "lang", "actor", "actor__person", "actor__group", "movie__episode__season__serie")
        tmp_episodes_dubbed_by = group.moviename.dubbed_by.filter(movie__episode__isnull=False).select_related("movie", "lang", "person", "person__person", "person__group", "movie__episode__season__serie")
        tmp_episodes_crew = group.moviename.movies_as_crew_member.filter(movie__episode__isnull=False).select_related("movie", "movie__episode__season__serie")
    else:
        movies_actor = MovieActor.objects.none()
        movies_dubbing_actor = MovieActorDubbing.objects.none()
        movies_dubbed_by = MovieActorDubbing.objects.none()
        movies_crew = MovieCrewMember.objects.none()
        tmp_episodes_actor = MovieActor.objects.none()
        tmp_episodes_dubbing_actor = MovieActorDubbing.objects.none()
        tmp_episodes_dubbed_by = MovieActorDubbing.objects.none()
        tmp_episodes_crew = MovieCrewMember.objects.none()

    episodes_actor = {}
    for actor in tmp_episodes_actor:
        episode = actor.movie.episode
        sid = episode.season.serie.id
        if sid not in episodes_actor:
            episodes_actor[sid] = {"serie": episode.season.serie, "episodes_u": {}, "episodes": []}
        num = "%09d-%09d" % (get_order(episode.season), get_order(episode))
        if num not in episodes_actor[sid]["episodes_u"]:
            episodes_actor[sid]["episodes_u"][num] = {"episode": episode, "actors": []}
        episodes_actor[sid]["episodes_u"][num]["actors"].append(actor)

    for sid in episodes_actor.keys():
        for num in sorted(episodes_actor[sid]["episodes_u"].keys()):
            episodes_actor[sid]["episodes"].append(episodes_actor[sid]["episodes_u"][num])
        del episodes_actor[sid]["episodes_u"]

    episodes_dubbing_actor = {}
    for actor in tmp_episodes_dubbing_actor:
        episode = actor.movie.episode
        sid = episode.season.serie.id
        if sid not in episodes_dubbing_actor:
            episodes_dubbing_actor[sid] = {"serie": episode.season.serie, "episodes_u": {}, "episodes": []}
        num = "%09d-%09d" % (get_order(episode.season), get_order(episode))
        if num not in episodes_dubbing_actor[sid]["episodes_u"]:
            episodes_dubbing_actor[sid]["episodes_u"][num] = {"episode": episode, "actors": []}
        episodes_dubbing_actor[sid]["episodes_u"][num]["actors"].append(actor)

    for sid in episodes_dubbing_actor.keys():
        for num in sorted(episodes_dubbing_actor[sid]["episodes_u"].keys()):
            episodes_dubbing_actor[sid]["episodes"].append(episodes_dubbing_actor[sid]["episodes_u"][num])
        del episodes_dubbing_actor[sid]["episodes_u"]

    episodes_dubbed_by = {}
    for actor in tmp_episodes_dubbed_by:
        episode = actor.movie.episode
        sid = episode.season.serie.id
        if sid not in episodes_dubbed_by:
            episodes_dubbed_by[sid] = {"serie": episode.season.serie, "episodes_u": {}, "episodes": []}
        num = "%09d-%09d" % (get_order(episode.season), get_order(episode))
        if num not in episodes_dubbed_by[sid]["episodes_u"]:
            episodes_dubbed_by[sid]["episodes_u"][num] = {"episode": episode, "actors": []}
        episodes_dubbed_by[sid]["episodes_u"][num]["actors"].append(actor)

    for sid in episodes_dubbed_by.keys():
        for num in sorted(episodes_dubbed_by[sid]["episodes_u"].keys()):
            episodes_dubbed_by[sid]["episodes"].append(episodes_dubbed_by[sid]["episodes_u"][num])
        del episodes_dubbed_by[sid]["episodes_u"]

    episodes_crew = {}
    for member in tmp_episodes_crew:
        episode = member.movie.episode
        sid = episode.season.serie.id
        if sid not in episodes_crew:
            episodes_crew[sid] = {"serie": episode.season.serie, "episodes_u": {}, "episodes": []}
        num = "%09d-%09d" % (get_order(episode.season), get_order(episode))
        if num not in episodes_crew[sid]["episodes_u"]:
            episodes_crew[sid]["episodes_u"][num] = {"episode": episode, "members": []}
        episodes_crew[sid]["episodes_u"][num]["members"].append(member)

    for sid in episodes_crew.keys():
        for num in sorted(episodes_crew[sid]["episodes_u"].keys()):
            episodes_crew[sid]["episodes"].append(episodes_crew[sid]["episodes_u"][num])
        del episodes_crew[sid]["episodes_u"]

    members = group.members.select_related("person")
    #tags = group.tags.select_related("tag")
    links = group.links.select_related("lang")

    return render(request, "base/group_info.html", {
        "group":                  group,
        "movies_actor":           movies_actor,
        "movies_dubbing_actor":   movies_dubbing_actor,
        "movies_dubbed_by":       movies_dubbed_by,
        "movies_crew":            movies_crew,
        "episodes_actor":         episodes_actor,
        "episodes_dubbing_actor": episodes_dubbing_actor,
        "episodes_dubbed_by":     episodes_dubbed_by,
        "episodes_crew":          episodes_crew,
        "members":                members,
        #"tags":                   tags,
        "links":                  links,
    })


@login_required
@permission_required("base.add_group")
def groupCreate(request):
    if request.method == "POST":
        form = GroupForm(request.POST, request.FILES)

        if form.is_valid():
            group = form.save()
            return redirect("group_info", "%06d" % group.id)
    else:
        form = GroupForm()

    return render(request, "base/group_create.html", {"form": form})


@login_required
@permission_required("base.change_group")
def groupEdit(request, id):
    try:
        group = Group.objects.get(id=id)
    except:
        return redirect("groups_list")

    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)

        if form.is_valid():
            group = form.save()
            return redirect("group_info", "%06d" % group.id)
    else:
        form = GroupForm(instance=group)

    return render(request, "base/group_edit.html", {"group": group, "form": form})


@login_required
@permission_required("base.delete_group")
def groupDelete(request, id):
    try:
        group = Group.objects.get(id=id)
    except:
        return redirect("groups_list")

    if request.method == "POST":
        if "delete" in request.POST:
            group.delete()
        return redirect("groups_list")

    return render(request, "base/group_delete.html", {"group": group})


@login_required
@permission_required("base.change_group")
def groupAddMember(request, id):
    try:
        group = Group.objects.get(id=id)
    except:
        return redirect("groups_list")

    if request.method == "POST":
        form = GroupMemberForm(request.POST)

        if form.is_valid():
            member = form.save(commit=False)
            if member:
                member.group = group
                member.save()
            return redirect("group_info", "%06d" % group.id)
    else:
        form = GroupMemberForm()

    return render(request, "base/group_add_member.html", {"group": group, "form": form})


@login_required
@permission_required("base.add_groupattribute")
def groupAddAttribute(request, id):
    try:
        group = Group.objects.get(id=id)
    except:
        return redirect("groups_list")

    if request.method == "POST":
        form = GroupAttributeForm(request.POST)

        if form.is_valid():
            attribute = form.save(commit=False)
            if attribute:
                attribute.group = group
                attribute.save()
            return redirect("group_info", "%06d" % group.id)
    else:
        form = GroupAttributeForm()

    url = reverse("group_add_attribute", kwargs={"id": "%06d" % group.id})
    return render(request, "base/group_add_attribute.html", {"group": group, "form": form, "form_url": url})


@login_required
@permission_required("base.add_grouplink")
def groupAddLink(request, id):
    try:
        group = Group.objects.get(id=id)
    except:
        return redirect("groups_list")

    if request.method == "POST":
        form = GroupLinkForm(request.POST)

        if form.is_valid():
            link = form.save(commit=False)
            if link:
                link.group = group
                link.save()
            return redirect("group_info", "%06d" % group.id)
    else:
        form = GroupLinkForm()

    url = reverse("group_add_link", kwargs={"id": "%06d" % group.id})
    return render(request, "base/group_add_link.html", {"group": group, "form": form, "form_url": url})


@login_required
@permission_required("base.change_group")
def groupEditMembers(request, id):
    try:
        group = Group.objects.get(id=id)
    except:
        return redirect("groups_list")

    members = group.members.all()

    if members.count() == 0:
        extra = 1
    else:
        extra = 0

    GroupMemberFormSet = modelformset_factory(GroupMember, form=GroupMemberForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = GroupMemberFormSet(request.POST, queryset=members)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.group = group
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("group_info", "%06d" % group.id)
    else:
        formset = GroupMemberFormSet(queryset=members)

    url = reverse("group_edit_members", kwargs={"id": "%06d" % group.id})
    title = _("Edit members")
    return render(request, "base/group_edit_members.html", {"group": group, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("base.change_groupattribute")
def groupEditAttributes(request, id):
    try:
        group = Group.objects.get(id=id)
    except:
        return redirect("groups_list")

    attributes = group.attributes.all()

    if attributes.count() == 0:
        extra = 1
    else:
        extra = 0

    GroupAttributeFormSet = modelformset_factory(GroupAttribute, form=GroupAttributeForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = GroupAttributeFormSet(request.POST, queryset=attributes)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.group = group
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("group_info", "%06d" % group.id)
    else:
        formset = GroupAttributeFormSet(queryset=attributes)

    url = reverse("group_edit_attributes", kwargs={"id": "%06d" % group.id})
    title = _("Edit attributes")
    return render(request, "base/group_edit_data.html", {"group": group, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("base.change_grouplink")
def groupEditLinks(request, id):
    try:
        group = Group.objects.get(id=id)
    except:
        return redirect("groups_list")

    links = group.links.all()

    if links.count() == 0:
        extra = 1
    else:
        extra = 0

    GroupLinkFormSet = modelformset_factory(GroupLink, form=GroupLinkForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = GroupLinkFormSet(request.POST, queryset=links)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.group = group
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("group_info", "%06d" % group.id)
    else:
        formset = GroupLinkFormSet(queryset=links)

    url = reverse("group_edit_links", kwargs={"id": "%06d" % group.id})
    title = _("Edit links")
    return render(request, "base/group_edit_data.html", {"group": group, "title": title, "url_form": url, "formset": formset})


@login_required
@require_safe
def countriesList(request):
    countries = Country.objects.all()
    return render(request, "base/countries_list.html", {"countries": countries})


@login_required
@permission_required("base.add_country")
def countryCreate(request):
    if request.method == "POST":
        form = CountryForm(request.POST, request.FILES)

        if form.is_valid():
            country = form.save()
            return redirect("country_info", country.code)
    else:
        form = CountryForm()

    return render(request, "base/country_create.html", {"form": form})


@login_required
@require_safe
def countryInfo(request, code):
    try:
        country = Country.objects.get(code=code.lower())
    except:
        return redirect("countries_list")

    movies = []
    for movie in country.movie_set.all():
        if request.user.is_authenticated:
            movie.viewed = (movie.views.filter(user=request.user).count() != 0)
        else:
            movie.viewed = False
        movies.append(movie)

    return render(request, "base/country_info.html", {"country": country, "movies": movies})


@login_required
@permission_required("base.change_country")
def countryEdit(request, code):
    try:
        country = Country.objects.get(code=code.lower())
    except:
        return redirect("countries_list")

    if request.method == "POST":
        form = CountryForm(request.POST, instance=country)

        if form.is_valid():
            country = form.save()
            return redirect("country_info", country.code)
    else:
        form = CountryForm(instance=country)

    return render(request, "base/country_edit.html", {"country": country, "form": form})


@login_required
@require_safe
def languagesList(request):
    languages = Language.objects.all()
    return render(request, "base/languages_list.html", {"languages": languages})


@login_required
@permission_required("base.add_language")
def languageCreate(request):
    if request.method == "POST":
        form = LanguageForm(request.POST, request.FILES)

        if form.is_valid():
            language = form.save()
            #return redirect("language_info", language.code)
            return redirect("languages_list")
    else:
        form = LanguageForm()

    return render(request, "base/language_create.html", {"form": form})


@login_required
@require_safe
def languageInfo(request, code):
    try:
        language = Language.objects.get(code=code.lower())
    except:
        return redirect("languages_list")

    movies = []
    for movie in language.movie_set.all():
        if request.user.is_authenticated:
            movie.viewed = (movie.views.filter(user=request.user).count() != 0)
        else:
            movie.viewed = False
        movies.append(movie)

    works = []
    for text in language.text_set.all():
        if hasattr(text, "work"):
            works.append(text.work)

    return render(request, "base/language_info.html", {"language": language, "movies": movies, "works": works})


@login_required
@permission_required("base.change_language")
def languageEdit(request, code):
    try:
        language = Language.objects.get(code=code.lower())
    except:
        return redirect("countries_list")

    if request.method == "POST":
        form = LanguageForm(request.POST, instance=language)

        if form.is_valid():
            language = form.save()
            return redirect("language_info", language.code)
    else:
        form = LanguageForm(instance=language)

    return render(request, "base/language_edit.html", {"language": language, "form": form})
