from django import template


register = template.Library()


@register.filter
def imdb_link_name(value):
    try:
        return "https://www.imdb.com/name/nm%07d/" % value
    except:
        return value


@register.filter
def imdb_link_title(value):
    try:
        return "https://www.imdb.com/title/tt%07d/" % value
    except:
        return value
