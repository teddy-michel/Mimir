# Generated by Django 4.1.2 on 2022-11-05 16:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='movietag',
            options={'ordering': ['tag__name', 'info'], 'verbose_name': 'Film - Tag', 'verbose_name_plural': 'Films - Tags'},
        ),
        migrations.AlterModelOptions(
            name='serietag',
            options={'ordering': ['tag__name', 'info'], 'verbose_name': 'Série - Tag', 'verbose_name_plural': 'Séries - Tags'},
        ),
        migrations.RenameField(
            model_name='movie',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='movieactor',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='moviecrewmember',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='moviereference',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='movietag',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='movieview',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='saga',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='season',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='serie',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='seriecrewmember',
            old_name='infos',
            new_name='info',
        ),
        migrations.RenameField(
            model_name='serietag',
            old_name='infos',
            new_name='info',
        ),
    ]
