
import requests
import datetime
import dateutil.parser

from django.utils.timezone import make_aware


"""
Subcategories:
- "ACC" : Actualité du cinéma
- "ADS" : ARTE Concert / Arts de la scène
- "AJO" : Info et société / A la une
- "ART" : Culture et pop / Arts
- "ATA" : Voyages et découvertes / A table !
- "AUT" : Culture et pop / Autre
- "AUV" : Info et société / Autrement vu
- "BAR" : ARTE Concert / Musique baroque
- "CHU" : Séries et fictions / Courts humoristiques
- "CIV" : Histoire / Civilisations
- "CLA" : ARTE Concert / Classique
- "CMG" : Cinéma / Courts métrages
- "CMU" : Cinéma / Films muets
- "DCY" : Info et société / Décryptages
- "ENB" : Sciences / En bref
- "ENN" : Sciences / Environnement et nature
- "ENQ" : Info et société / Enquêtes et reportages
- "EVA" : Voyages et découvertes / Evasion
- "FIC" : Séries et fictions / Fictions
- "FLM" : Cinéma / Films
- "HIP" : ARTE Concert / Hip-hop
- "IDE" : Culture et pop / Idées
- "JAZ" : ARTE Concert / Jazz
- "JUN" : Info et société / Junior
- "KUL" : Info et société / Actualités culturelles
- "LGP" : Histoire / Les grands personnages
- "MCL" : Cinéma / Les grands du 7e art
- "MET" : ARTE Concert / Metal
- "MUA" : ARTE Concert / Pop & Rock
- "MUD" : ARTE Concert / Musiques du monde
- "MUE" : ARTE Concert / Musiques électroniques
- "NEA" : Voyages et découvertes / Nature et animaux
- "OPE" : ARTE Concert / Opéra
- "POP" : Culture et pop / Culture pop
- "SAN" : Sciences / Médecine et santé
- "SES" : Séries et fictions / Séries
- "TEC" : Sciences / Technologies et innovations
- "VIA" : Voyages et découvertes / Vies d'ailleurs
- "XXE" : Histoire / XXe siècle
"""


def get_url(page=1, subcategorie=""):
    if page == 0:
        page = 1

    if page > 100:
        print("Error: page must be between 1 and 100")
        return None, None

    limit = 100  # limit max = 100

    # MANUAL_TEASERS/?imageFormats=landscape&authorizedAreas=DE_FR,EUR_DE_FR,SAT,ALL&code=collections_ACT&zone=f4525c0d-45da-4e8f-82bd-0370262b50d4&page=2&limit=6
    # AFP_LISTING/?imageFormats=landscape&authorizedAreas=DE_FR,EUR_DE_FR,SAT,ALL&page=2
    # MOST_RECENT_SUBCATEGORY/?imageFormats=landscape&authorizedAreas=DE_FR,EUR_DE_FR,SAT,ALL&mainZonePage=1&subCategoryCode=FLM&page=2&limit=10
    # COLLECTION_VIDEOS/?imageFormats=landscape&authorizedAreas=DE_FR,EUR_DE_FR,SAT,ALL&page=2&collectionId=RC-014038&limit=12
    # https://www.arte.tv/api/rproxy/emac/v3/fr/web/data/MOST_RECENT_SUBCATEGORY/?imageFormats=landscape&subCategoryCode=FLM&imageWithText=true&page=2&limit=10
    #
    # Changement le 05/09/2023 :
    # https://www.arte.tv/api/rproxy/emac/v4/...
    # https://www.arte.tv/api/rproxy/emac/v4/fr/web/collections/RC-017621
    #
    # Changement le 28/05/2024 :
    # https://www.arte.tv/api/rproxy/emac/v4/fr/web/ => OK
    # https://www.arte.tv/api/rproxy/emac/v4/fr/web/pages/ => OK (liste des catégories)
    # - CATEGORY_CIN
    # - SUBCATEGORY_CMG -> a814f9de-a5ca-4011-865f-7c63f2118b83_CMG
    # - SUBCATEGORY_CMU -> d3b6f9af-47ad-4adb-bcbb-225aa0a70148_CMU
    # - SUBCATEGORY_FIC -> 02895fc9-3b5b-47b7-8dcc-2df56ca97cbc_FIC
    # - SUBCATEGORY_FLM -> f0c3ab02-d878-4dbd-86ed-2c86e7a722b6_FLM
    # https://www.arte.tv/api/rproxy/emac/v4/fr/web/pages/CATEGORY_CIN/ => OK (liste des zones)
    # https://www.arte.tv/api/rproxy/emac/v4/fr/web/zones/f0c3ab02-d878-4dbd-86ed-2c86e7a722b6/content?abv=A&authorizedCountry=FR&page=0&pageId=SUBCATEGORY_FLM&zoneIndexInPage=0

    zones = {
        "CMG": "a814f9de-a5ca-4011-865f-7c63f2118b83",
        "CMU": "d3b6f9af-47ad-4adb-bcbb-225aa0a70148",
        "FIC": "02895fc9-3b5b-47b7-8dcc-2df56ca97cbc",
        "FLM": "f0c3ab02-d878-4dbd-86ed-2c86e7a722b6",
    }

    if subcategorie not in zones:
        print("Error: invalid zone")
        return None

    #return "https://www.arte.tv/api/rproxy/emac/v3/fr/web/data/MOST_RECENT_SUBCATEGORY/?imageFormats=landscape&subCategoryCode=%s&authorizedAreas=DE_FR,EUR_DE_FR,SAT,ALL&page=%d&limit=%d" % (subcategorie, page, limit)
    #return "https://www.arte.tv/api/rproxy/emac/v4/fr/web/data/MOST_RECENT_SUBCATEGORY/?imageFormats=landscape&subCategoryCode=%s&authorizedAreas=DE_FR,EUR_DE_FR,SAT,ALL&page=%d&limit=%d" % (subcategorie, page, limit)
    return "https://www.arte.tv/api/rproxy/emac/v4/fr/web/zones/%s/content?abv=A&authorizedCountry=FR&page=%d&pageId=SUBCATEGORY_%s&zoneIndexInPage=0" % (zones[subcategorie], page, subcategorie)


def download_data(url):
    req = requests.get(url, headers={"User-Agent": "Mozilla/5.0"}, timeout=10)
    try:
        json = req.json()
    except requests.exceptions.JSONDecodeError:
        print("Error with JSON")
        print(req.content)
        raise
    return json


def get_categorie_videos(subcategorie):
    """
    subcategorie: "FLM" for example.
    """

    videos = []

    try:
        for page in range(1, 101):
            url = get_url(page=page, subcategorie=subcategorie)
            if url is None:
                return videos

            response = download_data(url)

            if "data" in response:  # Version 1
                data = response["data"]
                next = response["nextPage"]
            elif "tag" in response and response["tag"] == "Ok" and "value" in response:  # Version 2 et 3
                data = response["value"]["data"]

                if "nextPage" in response["value"]:  # Version 2
                    next = response["value"]["nextPage"]
                elif "pagination" in response["value"]:  # Version 3
                    next = response["value"]["pagination"]
                else:
                    print("Bad response")
                    break
            else:
                print("Bad response")
                break

            for video in data:
                url = video["url"]
                if not url.startswith("https://"):
                    url = "https://www.arte.tv" + url

                videos.append({
                    "program_id": video["programId"],
                    "type": video["kind"]["code"],
                    "url": url,
                    "title": video["title"],
                    "subtitle": video["subtitle"] or "",
                    "duration": video["duration"],
                    "description": video["shortDescription"],
                    "date_start": dateutil.parser.parse(video["availability"]["start"]) if video["availability"] else make_aware(datetime.datetime(1000, 1, 1, 0, 0, 0)),
                    "date_end": dateutil.parser.parse(video["availability"]["end"]) if video["availability"] else make_aware(datetime.datetime(3000, 1, 1, 0, 0, 0)),
                    #"image": video["images"]["landscape"]["resolutions"][0]["url"],
                })

            if next is None:
                break
    except:
        raise
        pass

    return videos


def get_all_videos(subcategories):
    """
    subcategories: "FLM" or "SAN,ENN,TEC" for example.
    """

    subcategories = subcategories.split(",")

    videos = []
    for subcategorie in subcategories:
        videos += get_categorie_videos(subcategorie)
    return videos


if __name__ == "__main__":
    videos = get_all_videos("FLM,CMU")
    for video in videos:
        print(video["url"])
