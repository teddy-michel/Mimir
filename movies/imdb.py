
import logging
import bs4 as BeautifulSoup
import re
import requests


HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate, br",
    #"Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
    "DNT": "1",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:107.0) Gecko/20100101 Firefox/107.0",
}


def getIndividuInfoFromHTML(html):
    info = {'num': '?', 'name': ''}
    soup = BeautifulSoup.BeautifulSoup(html, "html.parser")
    h1 = soup.findAll('h1')

    if not h1:
        return info

    if len(h1) == 1:
        spans = h1[0].findAll('span')
    elif len(h1) == 2:
        spans = h1[1].findAll('span')
    else:
        return info

    if len(spans) >= 2:
        info['name'] = spans[0].get_text().strip()
        info['num'] = spans[1].get_text().replace('(', '').replace(')', '').replace(u'–', 'LALALA').replace('-', 'LALALA').strip()

        if re.match(r'^([0-9]{4})LALALA([0-9]{4})$', info['num']):
            info['num'] = '0'
        elif re.match(r'^LALALA([0-9]{4})$', info['num']):
            info['num'] = '0'
    elif len(spans) == 1:
        info['name'] = spans[0].get_text().strip()
        info['num'] = "0"

    return info


def getIndividuInfo(id):
    #print("getIndividuInfo({:07d})".format(id))
    if id <= 0:
        print("Erreur : invalid id")
        return {'num': '?', 'name': ''}

    try:
        url = "https://www.imdb.com/name/nm%07d/" % id
        request = requests.get(url, headers=HEADERS, timeout=10.0)
        request.raise_for_status()
        html = request.text
        individu = getIndividuInfoFromHTML(html)
        individu["imdb"] = id
        """
        try:
            with open("../cache/imdb/individus/individu_{:07d}.json".format(id), 'w') as file:
                file.write(json.dumps(individu, sort_keys=True, indent=2))
        except Exception as e:
            print("Erreur : " + str(type(e)))
        """
        return individu
    except Exception as e:
        logging.exception("getIndividuInfo")
        print("Erreur : " + str(type(e)))
        return {'num': '?', 'name': ''}


def getMovieIndividusFromHTML(html):
    individus = {}

    soup = BeautifulSoup.BeautifulSoup(html, "html.parser")
    h4 = soup.findAll("h4", {"class": "dataHeaderWithBorder"})

    if not h4:
        return individus

    prog = re.compile(r"^/name/nm(?P<id>[0-9]{1,9})/\?ref_=ttfc_(.+)$")

    for group in h4:
        groupName = group.get_text().replace('\n', ' ').strip()

        if "Cast" in groupName:
            continue

        if 'Writing Credits' in groupName:
            groupName = 'Scénario'

        if groupName == 'Produced by':
            groupName = 'Production'

        table = group.find_next_sibling('table', {"class": "simpleCreditsTable"})

        for tableLine in table.findAll('tr'):
            cases = tableLine.findAll('td')

            if len(cases) == 3:
                fonction = cases[2].get_text().replace('\n', ' ').strip()
            elif len(cases) == 2:
                fonction = cases[1].get_text().replace('\n', ' ').strip()
            else:
                continue

            name = cases[0].get_text().replace('\n', ' ').strip()
            imdbLink = cases[0].find('a').get('href')

            try:
                imdb = int(prog.match(imdbLink).group('id'))
            except:
                imdb = 0

            individu = getIndividuInfo(imdb)
            num = individu['num']

            credit = True
            if '(uncredited)' in fonction:
                fonction = fonction.replace('(uncredited)', '').strip()
                credit = False

            if not groupName in individus:
                individus[groupName] = []

            individus[groupName].append({
                    'name':     name,
                    'num':      num,
                    'imdb':     imdb,
                    'function': fonction,
                    'credited': credit
                })

    return individus


def getMovieIndividus(id):
    #print("getMovieIndividus({:07d})".format(id))

    try:
        url = 'https://www.imdb.com/title/tt%07d/fullcredits' % id
        request = requests.get(url, headers=HEADERS, timeout=10.0)
        request.raise_for_status()
        html = request.text
        return getMovieIndividusFromHTML(html)
    except:
        logging.exception("getMovieIndividus")
        return []


def getSeasonEpisodesFromHTML(html):
    #print("getSeasonEpisodesFromHTML()")
    episodes = {}

    soup = BeautifulSoup.BeautifulSoup(html, "html.parser")

    prog = re.compile(r"^/title/tt(?P<id>[0-9]{1,9})/\?ref_=ttep_ep(?P<lnk>[0-9]{1,3})$")

    """
    # Old format
    lines = soup.findAll("div", {"class": "list_item"})

    if not lines:
        return episodes

    for line in lines:
        episodeNumberTag = line.find("meta", {"itemprop": "episodeNumber"})

        if not episodeNumberTag:
            continue

        episodeNumber = episodeNumberTag.get("content")

        imdbLink = line.find("a").get('href')

        try:
            imdb = int(prog.match(imdbLink).group('id'))
        except:
            imdb = 0

        episodes[episodeNumber] = imdb
    """

    episodeNumber = 1
    for episode in soup.findAll("article", {"class": "episode-item-wrapper"}):
        link = episode.find("h4").find("a")

        imdbLink = link.get('href')

        try:
            imdb = int(prog.match(imdbLink).group('id'))
        except:
            imdb = 0

        episodes[episodeNumber] = {"imdb": imdb, "title": link.get_text()}
        episodeNumber += 1

    return episodes


def getSeasonEpisodes(id, season):
    #print("getSeasonEpisodes({:07d}, {:d})".format(id, season))

    try:
        url = "https://www.imdb.com/title/tt%07d/episodes/?season=%d" % (id, season)
        request = requests.get(url, headers=HEADERS, timeout=10.0)
        request.raise_for_status()
        html = request.text
        return getSeasonEpisodesFromHTML(html)
    except Exception as e:
        logging.exception("getSeasonEpisodes")
        print("Erreur : " + str(type(e)))
        return {}


def getSerieEpisodesFromHTML(id, html):
    #print("getSerieEpisodesFromHTML({:07d})".format(id))
    seasons = {}

    soup = BeautifulSoup.BeautifulSoup(html, "html.parser")

    """
    # Old format
    select = soup.find("select", {"id": "bySeason"})

    if not select:
        return seasons

    for option in select.findAll("option"):
        seasonNumber = int(option.get("value"))

        if seasonNumber == -1:
            continue

        seasons[seasonNumber] = getSeasonEpisodes(id, seasonNumber)
    """

    for season in soup.findAll("li", {"data-testid": "tab-season-entry"}):
        seasonNumber = int(season.get_text())
        seasons[seasonNumber] = getSeasonEpisodes(id, seasonNumber)

    if not seasons:
        for season in soup.findAll("a", {"data-testid": "tab-season-entry"}):
            seasonNumber = int(season.get_text())
            seasons[seasonNumber] = getSeasonEpisodes(id, seasonNumber)

    return seasons


def getSerieEpisodes(id):
    #print("getSerieEpisodes({:07d})".format(id))

    try:
        url = "https://www.imdb.com/title/tt%07d/episodes/" % id
        request = requests.get(url, headers=HEADERS, timeout=10.0)
        request.raise_for_status()
        html = request.text
        return getSerieEpisodesFromHTML(id, html)
    except Exception as e:
        logging.exception("getSerieEpisodes")
        print("Erreur : " + str(type(e)))
        return {}


def getMovieActorsFromHTML(html):
    actors = []

    soup = BeautifulSoup.BeautifulSoup(html, "html.parser")
    table = soup.find("table", {"class": "cast_list"})

    if not table:
        return actors

    prog = re.compile(r"^/name/nm(?P<id>[0-9]{1,9})/\?ref_=ttfc_fc_cl_t(?P<lnk>[0-9]{1,4})$")
    progRole = re.compile(r"^(.*)\(as (.+)\)$")

    for tableLine in table.findAll('tr'):
        if not isinstance(tableLine.get('class'), list):
            continue

        if not ('odd' in tableLine.get('class') or 'even' in tableLine.get('class')):
            continue

        cases = tableLine.findAll('td')

        if len(cases) != 4:
            continue

        name = cases[1].get_text().replace('\n', ' ').strip()
        role = cases[3].get_text().replace('\n', ' ').strip()
        role = re.sub("[ ]{2,}", " ", role)
        info = []
        imdbLink = cases[1].find('a').get('href')

        try:
            imdb = int(prog.match(imdbLink).group('id'))
        except:
            imdb = 0

        individu = getIndividuInfo(imdb)
        num = individu['num']
        actorAs = ""

        credit = True
        if '(uncredited)' in role:
            role = role.replace('(uncredited)', '').strip()
            credit = False

        visible = True
        if '(credit only)' in role:
            role = role.replace('(credit only)', '').strip()
            visible = False

        if '(scenes deleted)' in role:
            role = role.replace('(scenes deleted)', '').strip()
            info.append('scenes deleted')
            visible = False

        if '(archive footage)' in role:
            role = role.replace('(archive footage)', '').strip()
            info.append('archive footage')

        if progRole.match(role):
            actorAs = progRole.match(role).group(2)
            role = progRole.match(role).group(1).strip()
            #info.append('as ' + actorAs)

        if '(voice)' in role:
            role = role.replace('(voice)', '').strip()
            info.append('voice')

        roles = role.split("/")
        for role in roles:
            actors.append({
                    'name':     name,
                    'num':      num,
                    'as':       actorAs,
                    'imdb':     imdb,
                    'role':     role,
                    'credited': credit,
                    'visible':  visible,
                    'info':     info
                })

    return actors


def getMovieActors(id):
    #print("getMovieActors({:07d})".format(id))

    try:
        url = 'https://www.imdb.com/title/tt%07d/fullcredits' % id
        request = requests.get(url, headers=HEADERS, timeout=10.0)
        request.raise_for_status()
        html = request.text
        return getMovieActorsFromHTML(html)
    except:
        logging.exception("getMovieActors")
        return []


def parseDate(text:str) -> str:
    print("parseDate({:s})".format(text))
    months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

    parts = text.strip().replace(",", " ").replace("  ", " ").split(" ")
    print(parts)
    monthName = None
    if len(parts) == 3:
        if parts[0] in months:
            day = int(parts[1])
            year = int(parts[2])
            monthName = parts[0]
        else:
            day = int(parts[0])
            year = int(parts[2])
            monthName = parts[1]
    elif len(parts) == 2:
        day = 0
        monthName = parts[0]
        year = int(parts[1])
    elif len(parts) == 1:
        day = 0
        year = int(parts[0])
    else:
        day = 0
        year = 0

    try:
        month = months.index(monthName) + 1
    except:
        month = 0

    date = "%04d-%02d-%02d" % (year, month, day)
    return date


def getMovieDateFromHTML(html, id:int) -> str:
    #print("getMovieDateFromHTML({:07d})".format(id))
    soup = BeautifulSoup.BeautifulSoup(html, "html.parser")

    minDate = "9999-99-99"

    # Old format
    """
    for releaseDate in soup.findAll("td", {"class": "release-date-item__date"}):
        date = parseDate(releaseDate.get_text())
        if date < minDate:
            minDate = date
    """

    # New format
    for section in soup.findAll("div", {"data-testid": "sub-section-releases"}):
        for releaseDate in section.findAll("span", {"class": "ipc-metadata-list-item__list-content-item"}):
            date = parseDate(releaseDate.get_text())
            if date < minDate:
                minDate = date

    if minDate == "9999-99-99":
        minDate = "0000-00-00"

    return minDate


def getMovieDate(id):
    #print("getMovieDate({:07d})".format(id))

    try:
        url = 'https://www.imdb.com/title/tt%07d/releaseinfo' % id
        request = requests.get(url, headers=HEADERS, timeout=10.0)
        request.raise_for_status()
        html = request.text
        return getMovieDateFromHTML(html, id)
    except:
        logging.exception("getMovieDate")
        return None


countries_codes = {
    "Albania":                        "ALB",
    "Algeria":                        "DZA",
    "Angola":                         "AGO",
    "Argentina":                      "ARG",
    "Australia":                      "AUS",
    "Austria":                        "AUT",
    "Bahamas":                        "BHS",
    "Bangladesh":                     "BGD",
    "Belarus":                        "BLR",
    "Belgium":                        "BEL",
    "Bosnia and Herzegovina":         "BIH",
    "Botswana":                       "BWA",
    "Brazil":                         "BRA",
    "Bulgaria":                       "BGR",
    "Burkina Faso":                   "BFA",
    "Burma":                          "MMR",
    "Canada":                         "CAN",
    "Chile":                          "CHL",
    "China":                          "CHN",
    "Colombia":                       "COL",
    "Costa Rica":                     "CRI",
    "Côte d'Ivoire":                  "CIV",
    "Croatia":                        "HRV",
    "Cuba":                           "CUB",
    "Cyprus":                         "CYP",
    "Czech Republic":                 "CZE",
    "Czechoslovakia":                 "CSK",
    "Denmark":                        "DNK",
    "Dominican Republic":             "DOM",
    "East Germany":                   "DDR",
    "Ecuador":                        "ECU",
    "Egypt":                          "EGY",
    "Estonia":                        "EST",
    "Federal Republic of Yugoslavia": "FRY",
    "Finland":                        "FIN",
    "France":                         "FRA",
    "Georgia":                        "GEO",
    "Germany":                        "DEU",
    "Greece":                         "GRC",
    "Guinea-Bissau":                  "GNB",
    "Hong Kong":                      "HKG",
    "Hungary":                        "HUN",
    "Iceland":                        "ISL",
    "India":                          "IND",
    "Indonesia":                      "IDN",
    "Iraq":                           "IRQ",
    "Iran":                           "IRN",
    "Ireland":                        "IRL",
    "Israel":                         "ISR",
    "Italy":                          "ITA",
    "Jamaica":                        "JAM",
    "Japan":                          "JPN",
    "Jordan":                         "JOR",
    "Kenya":                          "KEN",
    "Kyrgyzstan":                     "KGZ",
    "Latvia":                         "LVA",
    "Lebanon":                        "LBN",
    "Luxembourg":                     "LUX",
    "Madagascar":                     "MDG",
    "Malaysia":                       "MYS",
    "Mali":                           "MLI",
    "Malta":                          "MLT",
    "Morocco":                        "MAR",
    "Mauritania":                     "MRT",
    "Mexico":                         "MEX",
    "Monaco":                         "MCO",
    "Mongolia":                       "MNG",
    "Netherlands":                    "NLD",
    "New Zealand":                    "NZL",
    "Nigeria":                        "NGA",
    "Norway":                         "NOR",
    "Pakistan":                       "PAK",
    "Palestine":                      "PSE",
    "Panama":                         "PAN",
    "Paraguay":                       "PRY",
    "Peru":                           "PER",
    "Philippines":                    "PHL",
    "Poland":                         "POL",
    "Portugal":                       "PRT",
    "Puerto Rico":                    "PRI",
    "Republic of Macedonia":          "MKD",
    "Romania":                        "ROU",
    "Russia":                         "RUS",
    "Serbia":                         "SRB",
    "Serbia and Montenegro":          "SRM",
    "Sierra Leone":                   "SLE",
    "Singapore":                      "SGP",
    "Slovakia":                       "SVK",
    "Slovenia":                       "SVN",
    "Spain":                          "ESP",
    "South Africa":                   "ZAF",
    "South Korea":                    "KOR",
    "Soviet Union":                   "URS",
    "Sweden":                         "SWE",
    "Switzerland":                    "CHE",
    "Taiwan":                         "TWN",
    "Thailand":                       "THA",
    "Tunisia":                        "TUN",
    "Turkey":                         "TUR",
    "Uganda":                         "UGA",
    "Ukraine":                        "UKR",
    "Uruguay":                        "URY",
    "UK":                             "GBR",
    "United Kingdom":                 "GBR",
    "USA":                            "USA",
    "United States":                  "USA",
    "États-Unis":                     "USA",
    "Venezuela":                      "VEN",
    "Vietnam":                        "VNM",
    "West Germany":                   "RFA",
    "Yugoslavia":                     "YUG",
    "Zimbabwe":                       "ZWE",
}

languages_codes = {
    "Aboriginal":               "???", # TODO
    "Afrikaans":                "afr",
    "Albanian":                 "sqi",
    "Algonquin":                "???", # TODO
    "American Sign Language":   "sgn",
    "Arabic":                   "ara",
    "Aramaic":                  "arc",
    "Armenian":                 "arm",
    "Assyrian Neo-Aramaic":     "???", # TODO
    "Azerbaijani":              "???", # TODO
    "Balinese":                 "???", # TODO
    "Bambara":                  "???", # TODO
    "Basque":                   "???", # TODO
    "Belarusian":               "???", # TODO
    "Bengali":                  "ben",
    "Berber languages":         "???", # TODO
    "Bosnian":                  "bos",
    "Breton":                   "???", # TODO
    "British Sign Language":    "???", # TODO
    "Bulgarian":                "bul",
    "Burmese":                  "???", # TODO
    "Cantonese":                "chi",
    "Catalan":                  "???", # TODO
    "Chinese":                  "zho",
    "Corsican":                 "???", # TODO
    "Cree":                     "???", # TODO
    "Croatian":                 "hrv",
    "Czech":                    "cze",
    "Danish":                   "dan",
    "Dari":                     "???", # TODO
    "Dutch":                    "nld",
    "English":                  "eng",
    "Esperanto":                "???", # TODO
    "Estonian":                 "???", # TODO
    "Filipino":                 "???", # TODO
    "Finnish":                  "fin",
    "Flemish":                  "???", # TODO
    "French Sign Language":     "fsl",
    "French":                   "fra",
    "Gallegan":                 "???", # TODO
    "Georgian":                 "???", # TODO
    "German":                   "deu",
    "Greek":                    "gre",
    "Greek, Ancient (to 1453)": "grm",
    "Greenlandic":              "???", # TODO
    "Guarani":                  "???", # TODO
    "Gujarati":                 "???", # TODO
    "Hakka":                    "???", # TODO
    "Hausa":                    "???", # TODO
    "Hawaiian":                 "???", # TODO
    "Hebrew":                   "heb",
    "Hindi":                    "hin",
    "Hmong":                    "???", # TODO
    "Hokkien":                  "???", # TODO
    "Hungarian":                "hun",
    "Icelandic":                "isl",
    "Indonesian":               "???", # TODO
    "Inuktitut":                "???", # TODO
    "Irish":                    "???", # TODO
    "Italian":                  "ita",
    "Japanese Sign Language":   "???", # TODO
    "Japanese":                 "jpn",
    "Kaado":                    "???", # TODO
    "Kannada":                  "???", # TODO
    "Khmer":                    "???", # TODO
    "Kirghiz":                  "kir",
    "Korean Sign Language":     "???", # TODO
    "Korean":                   "kor",
    "Kurdish":                  "???", # TODO
    "Ladino":                   "???", # TODO
    "Lao":                      "???", # TODO
    "Latin":                    "???", # TODO
    "Latvian":                  "???", # TODO
    "Lingala":                  "???", # TODO
    "Macedonian":               "???", # TODO
    "Malay":                    "???", # TODO
    "Malayalam":                "???", # TODO
    "Maltese":                  "???", # TODO
    "Mandarin":                 "cmn",
    "Maori":                    "???", # TODO
    "Marathi":                  "???", # TODO
    "Masai":                    "???", # TODO
    "Mende":                    "???", # TODO
    "Min Nan":                  "???", # TODO
    "Mongolian":                "mon",
    "Navajo":                   "???", # TODO
    "Neapolitan":               "nap", # TODO
    "Nepali":                   "???", # TODO
    "North American Indian":    "???", # TODO
    "Norwegian":                "nor",
    "Occitan":                  "???", # TODO
    "Old English":              "???", # TODO
    "Oriya":                    "???", # TODO
    "Panjabi":                  "???", # TODO
    "Pawnee":                   "???", # TODO
    "Persian":                  "fas",
    "Polish":                   "pol",
    "Polynesian":               "???", # TODO
    "Portuguese":               "por",
    "Pushto":                   "???", # TODO
    "Quechua":                  "???", # TODO
    "Quenya":                   "???", # TODO
    "Romanian":                 "ron",
    "Romany":                   "???", # TODO
    "Russian":                  "rus",
    "Sardinian":                "???", # TODO
    "Scots":                    "???", # TODO
    "Scottish Gaelic":          "???", # TODO
    "Serbian":                  "???", # TODO
    "Serbo-Croatian":           "hrv",
    "Shanghainese":             "???", # TODO
    "Sicilian":                 "???", # TODO
    "Sign Languages":           "???", # TODO
    "Sindarin":                 "???", # TODO
    "Sinhalese":                "???", # TODO
    "Sioux":                    "???", # TODO
    "Slovak":                   "slk",
    "Slovenian":                "???", # TODO
    "Somali":                   "???", # TODO
    "Songhay":                  "???", # TODO
    "Southern Sotho":           "???", # TODO
    "Spanish":                  "spa",
    "Sudan":                    "???", # TODO
    "Swahili":                  "???", # TODO
    "Swedish":                  "swe",
    "Swiss German":             "gsw",
    "Tagalog":                  "tgl",
    "Tamashek":                 "???", # TODO
    "Tamil":                    "tam",
    "Tatar":                    "???", # TODO
    "Telugu":                   "???", # TODO
    "Thai":                     "???", # TODO
    "Tibetan":                  "bod",
    "Tok Pisin":                "???", # TODO
    "Turkish":                  "tur",
    "Ukrainian":                "ukr",
    "Ungwatsi":                 "???", # TODO
    "Urdu":                     "???", # TODO
    "Vietnamese":               "vie",
    "Welsh":                    "???", # TODO
    "Xhosa":                    "???", # TODO
    "Yiddish":                  "yid",
    "Yoruba":                   "???", # TODO
    "Zulu":                     "???", # TODO
}


def getMovieInfoFromHTML_v2(html, id):
    global countries_codes
    global languages_codes

    result = {"imdb_v2": True}

    soup = BeautifulSoup.BeautifulSoup(html, "html.parser")

    # Titre
    title_h1 = soup.find("h1")
    if not title_h1:
        return result

    result["title"] = title_h1.get_text().strip()

    # Handle 4 versions of page:
    # - with or without enclosing div after h1
    # - with or without original title
    next_h1 = title_h1.find_next_sibling()
    if next_h1.name == "div":
        next_next_h1 = next_h1.find()
        if not next_next_h1:
            original_title_element = next_h1
            metadata_list = original_title_element.find_next_sibling("ul")
        elif next_next_h1.name == "div":
            original_title_element = next_next_h1
            metadata_list = original_title_element.find_next_sibling("ul")
        elif next_next_h1.name == "ul":
            original_title_element = None
            metadata_list = next_next_h1
        else:
            original_title_element = next_h1
            metadata_list = original_title_element.find_next_sibling("ul")
    elif next_h1.name == "ul":
        original_title_element = None
        metadata_list = next_h1

    # Titre original
    if original_title_element:
        original_title = original_title_element.get_text().strip()
        if original_title.startswith("Original title: "):
            original_title = original_title[16:]
        if original_title:
            result["title_vo"] = original_title

    # Nom de la série pour un épisode
    serie_link = soup.find("a", {"data-testid": "hero-title-block__series-link"})
    if serie_link:
        serie_name = serie_link.get_text().strip()
        if serie_name:
            result["serie_name"] = serie_name
            result["episode"] = True
            serie_url = serie_link.get("href")
            match = re.match(r"^/title/tt(\d{7,8})/(.+)", serie_url)
            if match:
                result["serie_id"] = int(match.group(1))

    # Série
    serie_episodes = soup.find("a", {"data-testid": "hero-subnav-bar-series-episode-guide-link"})
    if serie_episodes:
        result["serie"] = True

    if metadata_list:
        metadata_elements = metadata_list.find_all("li", recursive=False)

        if len(metadata_elements) > 0:
            # Année
            year_a = metadata_elements[0].find("a")
            if year_a:
                year_url = year_a.get("href")
                if year_url.startswith("/title/tt%07d/releaseinfo" % id):
                    year_text = year_a.get_text()
                    year_text = year_a.get_text()
                    match = re.match(r"^(19|20)\d{2}$", year_text)
                    if match:
                        result["year"] = int(year_text)

            # Durée
            duration = metadata_elements[-1].get_text().strip()
            match = re.match(r"^((\d*)h )?(\d*)m$", duration)
            if match:
                hours = int(match.group(2) or 0)
                minutes = int(match.group(3) or 0)
                duration = hours * 60 + minutes
                result["duration"] = duration * 60
            else:
                match = re.match(r"^(\d*)h$", duration)
                if match:
                    hours = int(match.group(1) or 0)
                    result["duration"] = hours * 3600

    # Genres
    genres_element = soup.find("div", {"data-testid": "genres"})
    if genres_element:
        result["genres"] = []
        for genre in genres_element.find_all("a"):
            result["genres"].append(genre.get_text().strip())

    # Résumé
    plot_element = soup.find("span", {"data-testid": "plot-xl"})
    if plot_element:
        plot_text = plot_element.get_text().strip()
        if plot_text:
            result["plot"] = plot_text

    # Pays
    origin_element = soup.find("li", {"data-testid": "title-details-origin"})
    if origin_element:
        result["countries"] = []
        for country in origin_element.find_all("a"):
            country = country.get_text().strip()
            if country in countries_codes:
                result["countries"].append(countries_codes[country])
            else:
                print("Unknown country (%s)" % country)

    # Langues
    languages_element = soup.find("li", {"data-testid": "title-details-languages"})
    if languages_element:
        result["languages"] = []
        for language in languages_element.find_all("a"):
            language = language.get_text().strip()
            if language in languages_codes:
                result["languages"].append(languages_codes[language])
            else:
                if language != "Aucun" and language != "None":
                    print("Unknown language (%s)" % language)

    return result


def getMovieInfoFromHTML(html, id):
    global countries_codes
    global languages_codes

    result = {}

    soup = BeautifulSoup.BeautifulSoup(html, "html.parser")
    title_div = soup.find("div", {"class": "title_wrapper"})

    if not title_div:
        return getMovieInfoFromHTML_v2(html, id)

    # Titre
    title_h1 = title_div.find("h1")
    if title_h1:
        progTitle = re.compile(r"^\<h1(.*)\>(.+)\<span(.*)\</h1\>$")
        match = progTitle.match(str(title_h1).replace("\n", " "))
        if match:
            result["title"] = match.group(2).strip()
        else:
            result["title"] = title_h1.get_text().strip()

    # Titre original
    title_vo = title_div.find("div", {"class": "originalTitle"})
    if title_vo:
        progTitle = re.compile(r"^\<div(.*)\>(.+)\<span(.*)\</div\>$")
        match = progTitle.match(str(title_vo).replace("\n", " "))
        if match:
            result["title_vo"] = match.group(2).strip()
        else:
            result["title_vo"] = title_vo.get_text().strip()

    # Durée / Pays / Langues
    blocks = soup.find_all("div", {"class": "txt-block"})
    for block in blocks:
        try:
            blockTitle = block.find("h4").get_text()
            if blockTitle == "Runtime:":
                duration = block.find("time").get("datetime")
                progDuration = re.compile(r"^PT(([0-9]*)H)?(([0-9]*)M)$")
                match = progDuration.match(duration)
                if match:
                    hours = int(match.group(2) or 0)
                    minutes = int(match.group(4) or 0)
                    duration = hours * 60 + minutes
                    result["duration"] = duration * 60
            elif blockTitle == "Country:":
                result["countries"] = []
                countries = block.find_all("a")
                for country in countries:
                    country = country.get_text()
                    if country in countries_codes:
                        result["countries"].append(countries_codes[country])
                    else:
                        print("Unknown country (%s)" % country)
            elif blockTitle == "Language:":
                result["languages"] = []
                languages = block.find_all("a")
                for language in languages:
                    language = language.get_text()
                    if language == "None" or language == "Aucun":
                        continue
                    if language in languages_codes:
                        result["languages"].append(languages_codes[language])
                    else:
                        print("Unknown language (%s)" % language)
        except:
            continue

    # Episode
    titles = soup.find_all("h2")
    for title in titles:
        if title.get_text() == "Episodes":
            result["serie"] = True
            break

    return result


def getMovieInfo(id, date=False):
    #print("getMovieInfo({:07d})".format(id))

    try:
        url = 'https://www.imdb.com/title/tt%07d/' % id
        request = requests.get(url, headers=HEADERS, timeout=10.0)
        request.raise_for_status()
        html = request.text
        if not html:
            return {"status": "error", "msg": "No HTML"}

        result = getMovieInfoFromHTML(html, id)
        result["imdb"] = id

        if date:
            movie_date = getMovieDate(id)
            if movie_date == "0000-00-00":
                if "year" in result:
                    result["date"] = "%04d-00-00" % result["year"]
            else:
                result["date"] = movie_date

        result["status"] = "success"
        return result
    except:
        logging.exception("getMovieInfo")
        return {"status": "error"}


def parse_url(url:str) -> int:
    match = re.match(r"^https?://www\.imdb\.com/(|fr/)?title/tt(\d{1,9})/", url)
    if match:
        return int(match.group(2))

    match = re.match(r"^(\d{1,9})$", url)
    if match:
        return int(match.group(1))

    return None
