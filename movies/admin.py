from django.contrib import admin

from .models import MovieName, Movie, MovieAttribute, SerieAttribute, MovieLink, MovieReference, MovieTag, MovieActor, \
    MovieActorDubbing, MovieCrewMember, MovieView, Saga, SagaMovie, Serie, SerieLink, SerieCrewMember, SerieTag, Season, \
    Episode

admin.site.register(MovieCrewMember)
admin.site.register(Saga)
admin.site.register(SagaMovie)
admin.site.register(SerieCrewMember)
admin.site.register(SerieTag)


@admin.register(MovieName)
class MovieNameAdmin(admin.ModelAdmin):
    list_display = ("person", "group", "name", "imdb")


@admin.register(MovieView)
class MovieViewAdmin(admin.ModelAdmin):
    list_display = ("movie", "date", "lang", "sublang", "user")


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ("title", "date")
    filter_horizontal = ["countries"]


@admin.register(MovieAttribute)
class MovieAttributeAdmin(admin.ModelAdmin):
    list_display = ("movie", "name", "value")


@admin.register(MovieReference)
class MovieReferenceAdmin(admin.ModelAdmin):
    list_display = ("movie",)


@admin.register(MovieTag)
class MovieTagAdmin(admin.ModelAdmin):
    list_display = ("movie", "tag")


@admin.register(MovieLink)
class MovieLinkAdmin(admin.ModelAdmin):
    list_display = ("movie", "name", "uri")


@admin.register(MovieActor)
class MovieActorAdmin(admin.ModelAdmin):
    list_display = ("movie", "person", "role", "credit", "visible")


@admin.register(MovieActorDubbing)
class MovieActorDubbingAdmin(admin.ModelAdmin):
    list_display = ("movie", "person", "actor", "lang")


@admin.register(Serie)
class SerieAdmin(admin.ModelAdmin):
    list_display = ("title",)
    filter_horizontal = ["countries"]


@admin.register(SerieAttribute)
class SerieAttributeAdmin(admin.ModelAdmin):
    list_display = ("serie", "name", "value")


@admin.register(SerieLink)
class SerieLinkAdmin(admin.ModelAdmin):
    list_display = ("serie", "name", "uri")


@admin.register(Season)
class SeasonAdmin(admin.ModelAdmin):
    list_display = ("serie", "name")


@admin.register(Episode)
class EpisodeAdmin(admin.ModelAdmin):
    list_display = ("season", "movie")
