import json
import os

from django.utils.translation import gettext as _

from Mimir.settings import BASE_DIR
from base.templatetags.mimir import date_txt, date_year, get_order, date_format
from movies.models import MovieView


def exportViews(user, type, format, order):
    """
    Exporte la liste des visionnages d'un utilisateur.

    :param user: Objet User de l'utilisateur.
    :param type: movies, episodes.
    :param format: text, csv, json.
    :param order: date, title.
    :return: Chaine de caractères.
    """

    if order == "date":
        order_by = "-date"
    elif order == "title":
        if type == "movies":
            order_by = "movie__title"
        elif type == "episodes":
            order_by = "movie__episode__season__serie__title"
        else:
            order_by = "movie__title"
    else:
        order_by = "-date"

    if type == "movies":
        views = user.views.select_related("movie", "lang", "sublang").filter(movie__ismovie=True).order_by(order_by)
    elif type == "episodes":
        views = user.views.select_related("movie", "lang", "sublang", "movie__episode__season__serie").filter(
            movie__episode__isnull=False).order_by(order_by)
    else:
        views = MovieView.objects.none()

    result = ""
    if format == "text":
        lastId = -1
        for view in views:
            if lastId == view.movie.id and order == "title":
                result = result[:-1]
                if view.date:
                    result += _(", %(date)s") % {"date": date_txt(view.date).lower()}
            else:
                if hasattr(view.movie, "episode"):
                    result += "%s - %s - %s %s - " % (
                    view.movie.episode.season.serie.title, view.movie.episode.season.name, _("episode"),
                    get_order(view.movie.episode))
                result += view.movie.title
                if view.movie.date:
                    result += _(" (%(date)s)") % {"date": date_year(view.movie.date)}
                if view.date:
                    result += _(" viewed %(date)s") % {"date": date_txt(view.date).lower()}
                else:
                    result += _(" viewed")
            if view.lang:
                result += _(" in %(lang)s") % {"lang": view.lang.name.lower()}
            if view.sublang:
                result += _(" subtitle in %(lang)s") % {"lang": view.sublang.name.lower()}
            if view.info:
                result += _(" (%(info)s)") % {"info": view.info}
            result += "\n"
            lastId = view.movie.id
    elif format == "csv":
        result += "Title,"
        if type == "episodes":
            result += "Serie,Season,Episode,"
        result += "Year,Date,Lang,Subtitles,Info\r\n"
        for view in views:
            result += "\"%s\"," % view.movie.title
            if hasattr(view.movie, "episode"):
                result += "%s,%s,%s," % (
                view.movie.episode.season.serie.title, view.movie.episode.season.name, get_order(view.movie.episode))
            result += "%s,%s,%s,%s,%s\r\n" % (
            date_year(view.movie.date), date_format(view.date), view.lang.name if view.lang else "",
            view.sublang.name if view.sublang else "", view.info)
    elif format == "json":
        data = []
        for view in views:
            item = {
                "title": view.movie.title,
                "title_vo": view.movie.title_vo,
                "year": date_year(view.movie.date),
                "date": date_format(view.date),
                "lang": view.lang.name if view.lang else "",
                "sublang": view.sublang.name if view.sublang else "",
                "info": view.info,
            }
            if hasattr(view.movie, "episode"):
                item["serie"] = view.movie.episode.season.serie.title
                item["season"] = view.movie.episode.season.name
                item["episode"] = get_order(view.movie.episode)
            data.append(item)
        result += json.dumps(data, indent=2)

    return result


def backupViewsMovies(user):
    result = exportViews(user, "movies", "text", "title")
    with open(os.path.join(BASE_DIR, "backup", "movies-%06d.txt" % user.id), "w") as f:
        f.write(result)


def backupViewsEpisodes(user):
    result = exportViews(user, "episodes", "text", "title")
    with open(os.path.join(BASE_DIR, "backup", "episodes-%06d.txt" % user.id), "w") as f:
        f.write(result)


def backupViews(user):
    backupViewsMovies(user)
    backupViewsEpisodes(user)
