import re

from django.forms import ModelForm, Textarea, TextInput, HiddenInput, CharField, ChoiceField, Select, IntegerField, \
    ValidationError, FileInput
from django.utils.translation import gettext as _, pgettext

from base.models import Tag, Person, Group
from base.templatetags.mimir import date_format, date_year
from books.models import Work
from games.models import Game
from .models import Movie, MovieAttribute, MovieActor, MovieActorDubbing, MovieCrewMember, MovieView, MovieTag, \
    MovieReference, Serie, SerieAttribute, SerieCrewMember, SerieTag, Season, Saga, SagaMovie, Episode, MovieName, \
    MovieLink, SerieLink, List, ListCriterion, ArteVideo


class MovieForm(ModelForm):
    duration_txt = CharField(label=_("Duration"), max_length=20, required=False)
    image_clear = CharField(widget=HiddenInput(), max_length=1, required=False, initial="0")

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        if self.instance.duration:
            self.initial["duration_txt"] = str(self.instance.duration)

    def clean_duration_txt(self):
        duration = self.cleaned_data["duration_txt"].replace(" ", "")
        if duration == "":
            return None
        elif re.match("^[0-9]+$", duration):
            return int(duration)
        else:
            m = re.match("^([0-9]+)h([0-9]{0,2})$", duration)
            if m:
                d = int(m.group(1)) * 3600
                if m.group(2):
                    d += int(m.group(2)) * 60
                return d
            else:
                m = re.match("^([0-9]+)min([0-9]{0,2})$", duration)
                if m:
                    d = int(m.group(1)) * 60
                    if m.group(2):
                        d += int(m.group(2))
                    return d
                else:
                    m = re.match("^([0-9]+)s$", duration)
                    if m:
                        return int(m.group(1))
                    else:
                        raise ValidationError(_("Invalid format."))

    def save(self, commit=True):
        duration = self.cleaned_data.get("duration_txt")
        if duration is None:
            self.instance.duration = None
        else:
            self.instance.duration = duration

        image_clear_value = self.cleaned_data.get("image_clear")
        if image_clear_value == "1":
            self.instance.image = None

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = Movie
        fields = ["title", "title_vo", "ismovie", "imdb", "date", "duration_txt", "lang", "summary", "info", "countries", "image", "image_clear"]
        widgets = {
            "date": TextInput(attrs={"placeholder": "YYYY-MM-DD"}),
            "summary": Textarea(attrs={"rows": 4}),
            "info": Textarea(attrs={"rows": 4}),
            "image": FileInput(),
        }


class MovieAttributeForm(ModelForm):

    class Meta:
        model = MovieAttribute
        fields = ["name", "value"]


class MovieLinkForm(ModelForm):

    class Meta:
        model = MovieLink
        fields = ["name", "uri", "lang"]


class MovieViewForm(ModelForm):

    class Meta:
        model = MovieView
        fields = ["date", "lang", "sublang", "info"]
        widgets = {
            "date": TextInput(attrs={"placeholder": "YYYY-MM-DD"}),
        }


class MovieActorForm(ModelForm):
    person_id = IntegerField(widget=HiddenInput(), required=False)
    person_select = IntegerField(label=_("Person") + " (*)", widget=Select(attrs={"class": "select_person"}), required=False)
    person_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    group_id = IntegerField(widget=HiddenInput(), required=False)
    group_select = IntegerField(label=pgettext("The group", "Group") + " (*)", widget=Select(attrs={"class": "select_group"}), required=False)
    group_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    other_name = CharField(label=_("Other") + " (*)", max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        self.initial["person_id"] = ""
        self.initial["person_name"] = ""
        self.initial["group_id"] = ""
        self.initial["group_name"] = ""
        self.initial["other_name"] = ""

        if hasattr(self.instance, "person"):
            if self.instance.person.person:
                self.initial["person_id"] = self.instance.person.person.id
                self.initial["person_name"] = self.instance.person.person.name
            elif self.instance.person.group:
                self.initial["group_id"] = self.instance.person.group.id
                self.initial["group_name"] = self.instance.person.group.name
            else:
                self.initial["other_name"] = self.instance.person.name

    def _clean_fields(self, *args, **kwargs):
        personId = self.data.get(self.add_prefix("person_select"))
        groupId = self.data.get(self.add_prefix("group_select"))

        if personId:
            personId = int(personId)
            if personId < 0:
                person = Person.objects.create(name=self.data.get(self.add_prefix("person_name")))
            else:
                person = Person.objects.get(id=personId)
            movieName = MovieName.objects.get_or_create(person=person)[0]
        elif groupId:
            groupId = int(groupId)
            if groupId < 0:
                group = Group.objects.create(name=self.data.get(self.add_prefix("group_name")))
            else:
                group = Group.objects.get(id=groupId)
            movieName = MovieName.objects.get_or_create(group=group)[0]
        else:
            creditName = self.data.get(self.add_prefix("other_name"))
            movieName = MovieName.objects.get_or_create(name=creditName)[0]

        self.instance.person = movieName
        self.data = self.data.copy()
        self.data[self.add_prefix("person")] = self.instance.person.id

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = MovieActor
        fields = ["person_select", "person_name", "group_select", "group_name", "other_name", "person",
                  "role", "lang", "info", "name", "credit", "visible"]
        widgets = {
            "person": HiddenInput(),
        }


class MovieActorReadOnlyForm(ModelForm):
    person_name = CharField(label=_("Actor"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        if hasattr(self.instance, "person"):
            if self.instance.person.person:
                self.initial["person_name"] = self.instance.person.person.name
            elif self.instance.person.group:
                self.initial["person_name"] = self.instance.person.group.name
            else:
                self.initial["person_name"] = self.instance.person.name
        else:
            self.initial["person_name"] = ""

    class Meta:
        model = MovieActor
        fields = ["person_name", "role", "lang", "info", "name", "credit", "visible"]


class MovieActorDubbingForm(ModelForm):
    person_person_id = IntegerField(widget=HiddenInput(), required=False)
    person_person_select = IntegerField(label=_("Dubbing actor") + " (" + _("Person") + ") (*)", widget=Select(attrs={"class": "select_person"}), required=False)
    person_person_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    person_group_id = IntegerField(widget=HiddenInput(), required=False)
    person_group_select = IntegerField(label=_("Dubbing actor") + " (" + pgettext("person group", "Group") + ") (*)", widget=Select(attrs={"class": "select_group"}), required=False)
    person_group_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    person_other_name = CharField(label=_("Dubbing actor") + " (" + _("Other") + ") (*)", max_length=100, required=False)
    actor_person_id = IntegerField(widget=HiddenInput(), required=False)
    actor_person_select = IntegerField(label=_("Actor") + " (" + _("Person") + ") (*)", widget=Select(attrs={"class": "select_person"}), required=False)
    actor_person_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    actor_group_id = IntegerField(widget=HiddenInput(), required=False)
    actor_group_select = IntegerField(label=_("Actor") + " (" + pgettext("person group", "Group") + ") (*)", widget=Select(attrs={"class": "select_group"}), required=False)
    actor_group_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    actor_other_name = CharField(label=_("Actor") + " (" + _("Other") + ") (*)", max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        self.initial["person_person_id"] = ""
        self.initial["person_person_name"] = ""
        self.initial["person_group_id"] = ""
        self.initial["person_group_name"] = ""
        self.initial["person_other_name"] = ""

        if hasattr(self.instance, "person"):
            if self.instance.person.person:
                self.initial["person_person_id"] = self.instance.person.person.id
                self.initial["person_person_name"] = self.instance.person.person.name
            elif self.instance.person.group:
                self.initial["person_group_id"] = self.instance.person.group.id
                self.initial["person_group_name"] = self.instance.person.group.name
            else:
                self.initial["person_other_name"] = self.instance.person.name

        self.initial["actor_person_id"] = ""
        self.initial["actor_person_name"] = ""
        self.initial["actor_group_id"] = ""
        self.initial["actor_group_name"] = ""
        self.initial["actor_other_name"] = ""

        if hasattr(self.instance, "actor"):
            if self.instance.person.person:
                self.initial["actor_person_id"] = self.instance.actor.person.id
                self.initial["actor_person_name"] = self.instance.actor.person.name
            elif self.instance.person.group:
                self.initial["actor_group_id"] = self.instance.actor.group.id
                self.initial["actor_group_name"] = self.instance.actor.group.name
            else:
                self.initial["actor_other_name"] = self.instance.actor.name

    def _clean_fields(self, *args, **kwargs):
        person_personId = self.data.get(self.add_prefix("person_person_select"))
        person_groupId = self.data.get(self.add_prefix("person_group_select"))

        if person_personId:
            person_personId = int(person_personId)
            if person_personId < 0:
                person_person = Person.objects.create(name=self.data.get(self.add_prefix("person_person_name")))
            else:
                person_person = Person.objects.get(id=person_personId)
            person_movieName = MovieName.objects.get_or_create(person=person_person)[0]
        elif person_groupId:
            person_groupId = int(person_groupId)
            if person_groupId < 0:
                person_group = Group.objects.create(name=self.data.get(self.add_prefix("person_group_name")))
            else:
                person_group = Group.objects.get(id=person_groupId)
            person_movieName = MovieName.objects.get_or_create(group=person_group)[0]
        else:
            person_creditName = self.data.get(self.add_prefix("person_other_name"))
            person_movieName = MovieName.objects.get_or_create(name=person_creditName)[0]

        self.instance.person = person_movieName
        self.data = self.data.copy()
        self.data[self.add_prefix("person")] = self.instance.person.id

        actor_personId = self.data.get(self.add_prefix("actor_person_select"))
        actor_groupId = self.data.get(self.add_prefix("actor_group_select"))

        if actor_personId:
            actor_personId = int(actor_personId)
            if actor_personId < 0:
                actor_person = Person.objects.create(name=self.data.get(self.add_prefix("actor_person_name")))
            else:
                actor_person = Person.objects.get(id=actor_personId)
            actor_movieName = MovieName.objects.get_or_create(person=actor_person)[0]
        elif actor_groupId:
            actor_groupId = int(actor_groupId)
            if actor_groupId < 0:
                actor_group = Group.objects.create(name=self.data.get(self.add_prefix("actor_group_name")))
            else:
                actor_group = Group.objects.get(id=actor_groupId)
            actor_movieName = MovieName.objects.get_or_create(group=actor_group)[0]
        else:
            actor_creditName = self.data.get(self.add_prefix("actor_other_name"))
            actor_movieName = MovieName.objects.get_or_create(name=actor_creditName)[0]

        self.instance.actor = actor_movieName
        self.data = self.data.copy()
        self.data[self.add_prefix("actor")] = self.instance.actor.id

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = MovieActorDubbing
        fields = ["person_person_select", "person_person_name", "person_group_select", "person_group_name",
                  "person_other_name", "person", "actor_person_select", "actor_person_name", "actor_group_select",
                  "actor_group_name", "actor_other_name", "actor", "lang"]
        widgets = {
            "person": HiddenInput(),
            "actor": HiddenInput(),
        }


class MovieActorDubbingReadOnlyForm(ModelForm):
    person_name = CharField(label=_("Dubbing actor"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=100)
    actor_name = CharField(label=_("Actor"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        if hasattr(self.instance, "person"):
            if self.instance.person.person:
                self.initial["person_name"] = self.instance.person.person.name
            elif self.instance.person.group:
                self.initial["person_name"] = self.instance.person.group.name
            else:
                self.initial["person_name"] = self.instance.person.name
        else:
            self.initial["person_name"] = ""

        if hasattr(self.instance, "actor"):
            if self.instance.actor.person:
                self.initial["actor_name"] = self.instance.actor.person.name
            elif self.instance.actor.group:
                self.initial["actor_name"] = self.instance.actor.group.name
            else:
                self.initial["actor_name"] = self.instance.actor.name
        else:
            self.initial["actor_name"] = ""

    class Meta:
        model = MovieActorDubbing
        fields = ["person_name", "actor_name", "lang"]


class MovieCrewMemberReadOnlyForm(ModelForm):
    person_name = CharField(label=_("Person"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        if hasattr(self.instance, "person"):
            if self.instance.person.person:
                self.initial["person_name"] = self.instance.person.person.name
            elif self.instance.person.group:
                self.initial["person_name"] = self.instance.person.group.name
            else:
                self.initial["person_name"] = self.instance.person.name
        else:
            self.initial["person_name"] = ""

    class Meta:
        model = MovieCrewMember
        fields = ["person_name", "function", "info", "name", "credit"]


class MovieCrewMemberForm(ModelForm):
    person_id = IntegerField(widget=HiddenInput(), required=False)
    person_select = IntegerField(label=_("Person") + " (*)", widget=Select(attrs={"class": "select_person"}), required=False)
    person_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    group_id = IntegerField(widget=HiddenInput(), required=False)
    group_select = IntegerField(label=pgettext("The group", "Group") + " (*)", widget=Select(attrs={"class": "select_group"}), required=False)
    group_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    other_name = CharField(label=_("Other") + " (*)", max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        self.initial["person_id"] = ""
        self.initial["person_name"] = ""
        self.initial["group_id"] = ""
        self.initial["group_name"] = ""
        self.initial["other_name"] = ""

        if hasattr(self.instance, "person"):
            if self.instance.person.person:
                self.initial["person_id"] = self.instance.person.person.id
                self.initial["person_name"] = self.instance.person.person.name
            elif self.instance.person.group:
                self.initial["group_id"] = self.instance.person.group.id
                self.initial["group_name"] = self.instance.person.group.name
            else:
                self.initial["other_name"] = self.instance.person.name

    def _clean_fields(self, *args, **kwargs):
        personId = self.data.get(self.add_prefix("person_select"))
        groupId = self.data.get(self.add_prefix("group_select"))

        if personId:
            personId = int(personId)
            if personId < 0:
                person = Person.objects.create(name=self.data.get(self.add_prefix("person_name")))
            else:
                person = Person.objects.get(id=personId)
            movieName = MovieName.objects.get_or_create(person=person)[0]
        elif groupId:
            groupId = int(groupId)
            if groupId < 0:
                group = Group.objects.create(name=self.data.get(self.add_prefix("group_name")))
            else:
                group = Group.objects.get(id=groupId)
            movieName = MovieName.objects.get_or_create(group=group)[0]
        else:
            creditName = self.data.get(self.add_prefix("other_name"))
            movieName = MovieName.objects.get_or_create(name=creditName)[0]

        self.instance.person = movieName
        self.data = self.data.copy()
        self.data[self.add_prefix("person")] = self.instance.person.id

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = MovieCrewMember
        fields = ["person_select", "person_name", "group_select", "group_name", "other_name", "person", "function", "info", "name", "credit"]
        widgets = {
            "person": HiddenInput(),
        }


class MovieTagForm(ModelForm):
    tag_id = IntegerField(widget=HiddenInput(), required=False)
    tag_select = IntegerField(label=_("Tag"), widget=Select(attrs={"class": "select_tag"}), required=False)
    tag_name = CharField(widget=HiddenInput(), max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "tag") and self.instance.tag:
            self.initial["tag_id"] = self.instance.tag.id
            self.initial["tag_name"] = self.instance.tag.name
        else:
            self.initial["tag_id"] = None
            self.initial["tag_name"] = ""

    def _clean_fields(self, *args, **kwargs):
        tagId_str = self.data.get(self.add_prefix("tag_select"))
        if tagId_str is not None:
            tagId = int(tagId_str)

            if tagId < 0:
                tag = Tag.objects.create(name=self.data.get(self.add_prefix("tag_name")))
            else:
                tag = Tag.objects.get(id=tagId)

            self.instance.tag = tag
            self.data = self.data.copy()
            self.data[self.add_prefix("tag")] = self.instance.tag.id

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = MovieTag
        fields = ["tag_select", "tag_name", "tag", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class MovieTagFormClassic(ModelForm):
    tag_name = CharField(label=_("Tag name"), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields["tag"].required = False
        if hasattr(self.instance, "tag"):
            self.initial["tag_name"] = self.instance.tag.name
        else:
            self.initial["tag_name"] = ""

    def save(self, commit=True):
        name = self.cleaned_data.get("tag_name").strip()
        if self.initial["tag_name"] != name:
            tag = Tag.objects.get_or_create(name=name)[0]
            self.instance.tag = tag
        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = MovieTag
        fields = ["tag", "tag_name", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class MovieReferenceForm(ModelForm):
    object_type = ChoiceField(label=_("Type"), widget=Select(attrs={"class": "object_type"}),
                              choices=(("text", _("Text")), ("movie", _("Movie")), ("serie", _("Serie")), ("person", _("Person")), ("book", _("Book")), ("game", _("Game"))))
    movie_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_movie"}), required=False)
    serie_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_serie"}), required=False)
    person_id = IntegerField(label=_("Name"), widget=Select(attrs={"class": "select_person"}), required=False)
    book_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_book"}), required=False)
    game_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_game"}), required=False)
    movie_title = CharField(widget=HiddenInput(), max_length=150, required=False)
    serie_title = CharField(widget=HiddenInput(), max_length=100, required=False)
    person_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    book_title = CharField(widget=HiddenInput(), max_length=100, required=False)
    game_title = CharField(widget=HiddenInput(), max_length=150, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        self.initial["object_type"] = "text"
        self.initial["movie_id"] = None
        self.initial["serie_id"] = None
        self.initial["person_id"] = None
        self.initial["book_id"] = None
        self.initial["game_id"] = None

        self.initial["movie_title"] = ""
        self.initial["serie_title"] = ""
        self.initial["person_name"] = ""
        self.initial["book_title"] = ""
        self.initial["game_title"] = ""

        try:
            if self.instance.ref_movie:
                self.initial["object_type"] = "movie"
                self.initial["movie_id"] = self.instance.ref_movie.id
                self.initial["movie_title"] = self.instance.ref_movie.title
            elif self.instance.ref_serie:
                self.initial["object_type"] = "serie"
                self.initial["serie_id"] = self.instance.ref_serie.id
                self.initial["serie_title"] = self.instance.ref_serie.title
            elif self.instance.ref_person:
                self.initial["object_type"] = "person"
                self.initial["person_id"] = self.instance.ref_person.id
                self.initial["person_name"] = self.instance.ref_person.name
            elif self.instance.ref_book:
                self.initial["object_type"] = "book"
                self.initial["book_id"] = self.instance.ref_book.id
                self.initial["book_title"] = self.instance.ref_book.title
            elif self.instance.ref_game:
                self.initial["object_type"] = "game"
                self.initial["game_id"] = self.instance.ref_game.id
                self.initial["game_title"] = self.instance.ref_game.title
        except:
            pass

    def save(self, commit=True):
        if self.instance.ref_movie is None and self.cleaned_data.get("movie_id"):
            self.instance.ref_movie = Movie.objects.get(id=int(self.cleaned_data.get("movie_id")))
        if self.instance.ref_serie is None and self.cleaned_data.get("serie_id"):
            self.instance.ref_serie = Serie.objects.get(id=int(self.cleaned_data.get("serie_id")))
        if self.instance.ref_person is None and self.cleaned_data.get("person_id"):
            self.instance.ref_person = Person.objects.get(id=int(self.cleaned_data.get("person_id")))
        if self.instance.ref_book is None and self.cleaned_data.get("book_id"):
            self.instance.ref_book = Work.objects.get(id=int(self.cleaned_data.get("book_id")))
        if self.instance.ref_game is None and self.cleaned_data.get("game_id"):
            self.instance.ref_game = Game.objects.get(id=int(self.cleaned_data.get("game_id")))

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = MovieReference
        fields = ["object_type",
                  "movie_id", "movie_title", "ref_movie",
                  "serie_id", "serie_title", "ref_serie",
                  "person_id", "person_name", "ref_person",
                  "book_id", "book_title", "ref_book",
                  "game_id", "game_title", "ref_game",
                  "info"]
        widgets = {
            "ref_movie": HiddenInput(),
            "ref_serie": HiddenInput(),
            "ref_person": HiddenInput(),
            "ref_book": HiddenInput(),
            "ref_game": HiddenInput(),
        }


class MovieReferenceReadOnlyForm(ModelForm):
    text_type = CharField(label=_("Type"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=100)
    text_title = CharField(label=_("Title or name"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=150)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if self.instance.ref_movie:
            self.initial["text_type"] = _("Movie")
            self.initial["text_title"] = self.instance.ref_movie.title
        elif self.instance.ref_serie:
            self.initial["text_type"] = _("Serie")
            self.initial["text_title"] = self.instance.ref_serie.title
        elif self.instance.ref_person:
            self.initial["text_type"] = _("Person")
            self.initial["text_title"] = self.instance.ref_person.name
        elif self.instance.ref_book:
            self.initial["text_type"] = _("Book")
            if self.instance.ref_book.genre:
                self.initial["text_type"] += " (" + str(self.instance.ref_book.genre) + ")"
            self.initial["text_title"] = str(self.instance.ref_book)
        elif self.instance.ref_game:
            self.initial["text_type"] = _("Game")
            self.initial["text_title"] = self.instance.ref_game.title
        else:
            self.initial["text_type"] = _("Text")
            self.initial["text_title"] = ""

    class Meta:
        model = MovieReference
        fields = ["text_type", "text_title", "info"]


class SerieForm(ModelForm):

    class Meta:
        model = Serie
        fields = ["title", "title_vo", "imdb", "duration", "lang", "summary", "countries", "info"]
        widgets = {
            "summary": Textarea(attrs={"rows": 4}),
            "info": Textarea(attrs={"rows": 4}),
        }


class SerieAttributeForm(ModelForm):

    class Meta:
        model = SerieAttribute
        fields = ["name", "value"]


class SerieLinkForm(ModelForm):

    class Meta:
        model = SerieLink
        fields = ["name", "uri", "lang"]


class SerieCrewMemberReadOnlyForm(ModelForm):
    person_name = CharField(label=_("Person"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        if self.instance.person:
            if self.instance.person.person:
                self.initial["person_name"] = self.instance.person.person.name
            elif self.instance.person.group:
                self.initial["person_name"] = self.instance.person.group.name
            else:
                self.initial["person_name"] = self.instance.person.name

    class Meta:
        model = SerieCrewMember
        fields = ["person_name", "function", "info", "name"]


class SerieCrewMemberForm(ModelForm):
    person_id = IntegerField(widget=HiddenInput(), required=False)
    person_select = IntegerField(label=_("Person") + " (*)", widget=Select(attrs={"class": "select_person"}), required=False)
    person_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    group_id = IntegerField(widget=HiddenInput(), required=False)
    group_select = IntegerField(label=pgettext("The group", "Group") + " (*)", widget=Select(attrs={"class": "select_group"}), required=False)
    group_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    other_name = CharField(label=_("Other") + " (*)", max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        self.initial["person_id"] = ""
        self.initial["person_name"] = ""
        self.initial["group_id"] = ""
        self.initial["group_name"] = ""
        self.initial["other_name"] = ""

        if hasattr(self.instance, "person"):
            if self.instance.person.person:
                self.initial["person_id"] = self.instance.person.person.id
                self.initial["person_name"] = self.instance.person.person.name
            elif self.instance.person.group:
                self.initial["group_id"] = self.instance.person.group.id
                self.initial["group_name"] = self.instance.person.group.name
            else:
                self.initial["other_name"] = self.instance.person.name

    def _clean_fields(self, *args, **kwargs):
        personId = self.data.get(self.add_prefix("person_select"))
        groupId = self.data.get(self.add_prefix("group_select"))

        if personId:
            personId = int(personId)
            if personId < 0:
                person = Person.objects.create(name=self.data.get(self.add_prefix("person_name")))
            else:
                person = Person.objects.get(id=personId)
            movieName = MovieName.objects.get_or_create(person=person)[0]
        elif groupId:
            groupId = int(groupId)
            if groupId < 0:
                group = Group.objects.create(name=self.data.get(self.add_prefix("group_name")))
            else:
                group = Group.objects.get(id=groupId)
            movieName = MovieName.objects.get_or_create(group=group)[0]
        else:
            creditName = self.data.get(self.add_prefix("other_name"))
            movieName = MovieName.objects.get_or_create(name=creditName)[0]

        self.instance.person = movieName
        self.data = self.data.copy()
        self.data[self.add_prefix("person")] = self.instance.person.id

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = SerieCrewMember
        fields = ["person_select", "person_name", "group_select", "group_name", "other_name", "person", "function", "info", "name"]
        widgets = {
            "person": HiddenInput(),
        }


class SerieTagForm(ModelForm):
    tag_id = IntegerField(widget=HiddenInput(), required=False)
    tag_select = IntegerField(label=_("Tag"), widget=Select(attrs={"class": "select_tag"}), required=False)
    tag_name = CharField(widget=HiddenInput(), max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "tag") and self.instance.tag:
            self.initial["tag_id"] = self.instance.tag.id
            self.initial["tag_name"] = self.instance.tag.name
        else:
            self.initial["tag_id"] = None
            self.initial["tag_name"] = ""

    def _clean_fields(self, *args, **kwargs):
        tagId = int(self.data.get(self.add_prefix("tag_select")))

        if tagId < 0:
            tag = Tag.objects.create(name=self.data.get(self.add_prefix("tag_name")))
        else:
            tag = Tag.objects.get(id=tagId)

        self.instance.tag = tag
        self.data = self.data.copy()
        self.data[self.add_prefix("tag")] = self.instance.tag.id

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = SerieTag
        fields = ["tag_select", "tag_name", "tag", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class SerieTagFormClassic(ModelForm):
    tag_name = CharField(label=_("Tag name"), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields["tag"].required = False
        if hasattr(self.instance, "tag"):
            self.initial["tag_name"] = self.instance.tag.name
        else:
            self.initial["tag_name"] = ""

    def save(self, commit=True):
        name = self.cleaned_data.get("tag_name").strip()
        if self.initial["tag_name"] != name:
            tag = Tag.objects.get_or_create(name=name)[0]
            self.instance.tag = tag
        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = SerieTag
        fields = ["tag", "tag_name", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class SeasonForm(ModelForm):

    class Meta:
        model = Season
        fields = ["name", "info"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
        }


class EpisodeForm(ModelForm):
    movie_title = CharField(label=_("Movie"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=250)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        if self.instance.movie:
            self.initial["movie_title"] = self.instance.movie.title
            if self.instance.movie.title_vo:
                self.initial["movie_title"] += " (" + self.instance.movie.title_vo + ")"
            if self.instance.movie.date:
                self.initial["movie_title"] += " (" + date_format(self.instance.movie.date) + ")"

    class Meta:
        model = Episode
        fields = ["movie_title", "movie"]
        widgets = {
            "movie": HiddenInput(),
        }


class SagaForm(ModelForm):

    class Meta:
        model = Saga
        fields = ["title", "title_vo", "info"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
        }


class SagaMovieForm(ModelForm):
    movie_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_movie"}), required=False)
    movie_title = CharField(widget=HiddenInput(), max_length=150, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "movie") and self.instance.movie:
            self.initial["movie_id"] = self.instance.movie.id
            self.initial["movie_title"] = self.instance.movie.title
            year = date_year(self.instance.movie.date)
            if year:
                self.initial["movie_title"] += " (%s)" % year
        else:
            self.initial["movie_id"] = None
            self.initial["movie_title"] = ""

    def save(self, commit=True):
        if self.instance.movie is None and self.cleaned_data.get("movie_id"):
            self.instance.movie = Movie.objects.get(id=int(self.cleaned_data.get("movie_id")))

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = SagaMovie
        fields = ["movie_id", "movie_title", "movie"]
        widgets = {
            "movie": HiddenInput(),
        }


class ListForm(ModelForm):

    class Meta:
        model = List
        fields = ["name", "union"]


class ListCriterionForm(ModelForm):

    class Meta:
        model = ListCriterion
        fields = ["type", "condition", "value1", "value2"]


class ArteVideoForm(ModelForm):
    movie_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_movie"}), required=False)
    movie_title = CharField(widget=HiddenInput(), max_length=150, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "movie") and self.instance.movie:
            self.initial["movie_id"] = self.instance.movie.id
            self.initial["movie_title"] = self.instance.movie.title
            year = date_year(self.instance.movie.date)
            if year:
                self.initial["movie_title"] += " (%s)" % year
        else:
            self.initial["movie_id"] = None
            self.initial["movie_title"] = ""

    def save(self, commit=True):
        if self.instance.movie is None and self.cleaned_data.get("movie_id"):
            self.instance.movie = Movie.objects.get(id=int(self.cleaned_data.get("movie_id")))

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = ArteVideo
        fields = ["movie_id", "movie_title", "movie"]
        widgets = {
            "movie": HiddenInput(),
        }
