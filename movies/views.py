
import logging
import os

import pymysql
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q, Count, Prefetch
from django.forms import modelformset_factory, HiddenInput
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone, dateformat
from django.utils.translation import gettext as _
from django.views.decorators.http import require_safe

from base.images import resize_image, remove_old_image
from base.api import generic_APIList, generic_APISearch
from base.models import Person, Group, Tag, PersonTag, Language, Country, PersonAttribute, GroupAttribute, PersonLink, \
    GroupLink
from base.templatetags.mimir import date_format, date_year, duration_txt, get_order
from .export import exportViews, backupViews, backupViewsMovies, backupViewsEpisodes
from .forms import MovieForm, MovieAttributeForm, MovieViewForm, MovieActorReadOnlyForm, MovieActorDubbingReadOnlyForm, \
    MovieCrewMemberReadOnlyForm, MovieReferenceForm, MovieReferenceReadOnlyForm, MovieTagForm, MovieTagFormClassic, SerieForm, SerieAttributeForm, \
    SerieCrewMemberReadOnlyForm, SerieTagForm, SerieTagFormClassic, SeasonForm, SagaForm, SagaMovieForm, EpisodeForm, MovieActorForm, MovieCrewMemberForm, \
    SerieCrewMemberForm, MovieActorDubbingForm, SerieLinkForm, MovieLinkForm, ListForm, ListCriterionForm, ArteVideoForm
from .imdb import getMovieActors, getMovieInfo, getSerieEpisodes, getMovieDate, parse_url
from .models import MovieName, Movie, MovieAttribute, MovieReference, MovieTag, MovieActor, MovieActorDubbing, \
    MovieCrewMember, MovieView, Serie, SerieAttribute, SerieCrewMember, SerieTag, Season, Episode, Saga, SagaMovie, \
    SerieLink, MovieLink, List, ListCriterion, ArteVideo
from .arte import get_all_videos


@require_safe
@login_required
def index(request):
    movies_cursor = Movie.objects.filter(ismovie=True).order_by("-date")[0:10]
    movies_cursor = movies_cursor.prefetch_related(Prefetch("views", queryset=MovieView.objects.filter(user=request.user)))

    movies = []
    for movie in movies_cursor:
        if request.user.is_authenticated:
            movie.viewed = (movie.views.count() != 0)
        else:
            movie.viewed = False
        movies.append(movie)

    episodes_cursor = Movie.objects.filter(episode__isnull=False).select_related("episode", "episode__season", "episode__season__serie").order_by("-date", "-episode___order")[0:10]
    episodes_cursor = episodes_cursor.prefetch_related(Prefetch("views", queryset=MovieView.objects.filter(user=request.user)))

    episodes = []
    for movie in episodes_cursor:
        if request.user.is_authenticated:
            movie.viewed = (movie.views.count() != 0)
        else:
            movie.viewed = False
        episodes.append(movie)

    # Videos from Arte.tv
    arte_categories = request.GET.get("arte", "FLM,CMU,FIC")
    arte = sorted(get_all_videos(arte_categories), key=lambda v: v["date_end"])

    known_arte_videos = ArteVideo.objects.all().select_related("movie")
    known_arte_videos = known_arte_videos.prefetch_related(Prefetch("movie__views", queryset=MovieView.objects.filter(user=request.user)))

    if arte:
        for video in arte:
            video["movie"] = None
            for k in known_arte_videos:
                if video["program_id"] == k.program_id and video["date_start"] == k.date_start and video["date_end"] == k.date_end:
                    video["id"] = k.id
                    if k.movie is not None:
                        video["movie"] = k.movie
                        if request.user.is_authenticated:
                            video["movie"].viewed = (k.movie.views.count() != 0)
                        else:
                            video["movie"].viewed = False
                    break
            else:
                k = ArteVideo.objects.create(
                    program_id=video["program_id"],
                    title=video["title"],
                    subtitle=video["subtitle"],
                    url=video["url"],
                    description=video["description"],
                    duration=video["duration"],
                    date_start=video["date_start"],
                    date_end=video["date_end"],
                )
                video["id"] = k.id

    """
    # TODO: filter by date
    else:
        for k in known_arte_videos:
            video = {
                "id": k.id,
                "program_id": k.program_id,
                "title": k.title,
                "subtitle": k.subtitle,
                "url": k.url,
                "description": k.description,
                "duration": k.duration,
                "date_start": k.date_start,
                "date_end": k.date_end,
            }

            if k.movie is not None:
                video["movie"] = k.movie
                if request.user.is_authenticated:
                    video["movie"].viewed = (k.movie.views.count() != 0)
                else:
                    video["movie"].viewed = False

            arte.append(video)
    """

    return render(request, "movies/index.html", {"movies": movies, "episodes": episodes, "arte": arte})


@require_safe
@login_required
def stats(request):
    movies = Movie.objects.filter(ismovie=True)

    data = {
        "movies_count": movies.count(),
        "series_count": Serie.objects.count(),
        "episodes_count": Episode.objects.count(),
        "sagas_count": Saga.objects.count(),
        "movies_long_count": movies.filter(duration__gte=3600).count(),
        "movies_medium_count": movies.filter(duration__lt=3600).filter(duration__gte=1800).count(),
        "movies_short_count": movies.filter(duration__lt=1800).count(),
        "years": [],
        "countries": [],
    }

    countries = {}

    years = {}
    minYear = 9999
    maxYear = 0

    for movie in Movie.objects.all().prefetch_related("countries"):
        if movie.ismovie:
            for country in movie.countries.all():
                if country not in countries:
                    countries[country] = 1
                else:
                    countries[country] += 1

        try:
            year = int(date_year(movie.date))

            if year < minYear:
                minYear = year
            if year > maxYear:
                maxYear = year

            if year not in years:
                years[year] = {"movies": 0, "episodes": 0}

            if movie.ismovie:
                years[year]["movies"] += 1
            else:
                years[year]["episodes"] += 1
        except:
            pass

    for year in range(minYear, maxYear):
        if year not in years:
            years[year] = {"movies": 0, "episodes": 0}

    for year in sorted(years.keys()):
        data["years"].append({"year": year, "movies": years[year]["movies"], "episodes": years[year]["episodes"]})

    for country in sorted(countries, key=countries.get, reverse=True):
        data["countries"].append({"country": country, "movies": countries[country]})

    return render(request, "movies/stats.html", {"stats": data})


@login_required
def viewsStats(request):
    views_movies_req = request.user.views.select_related("movie", "lang", "sublang").filter(movie__ismovie=True)
    views_episodes_req = request.user.views.select_related("movie", "lang", "sublang", "movie__episode__season__serie").filter(movie__episode__isnull=False)

    movies = []
    episodes = []
    views_movies = []
    views_episodes = []
    movies_long_count = 0
    countries = {}

    years = {}
    minYear = 9999
    maxYear = 0

    for view in views_movies_req:
        try:
            year = int(date_year(view.movie.date))

            if year < minYear:
                minYear = year
            if year > maxYear:
                maxYear = year

            if year not in years:
                years[year] = {"movies": 0, "episodes": 0}

            years[year]["movies"] += 1
        except:
            pass

        for country in view.movie.countries.all():
            if country not in countries:
                countries[country] = 1
            else:
                countries[country] += 1

        if view.movie.id not in movies:
            movies.append(view.movie.id)
            if view.movie.duration >= 3600:
                movies_long_count += 1

        views_movies.append(view)

    for view in views_episodes_req:
        try:
            year = int(date_year(view.movie.date))

            if year < minYear:
                minYear = year
            if year > maxYear:
                maxYear = year

            if year not in years:
                years[year] = {"movies": 0, "episodes": 0}

            years[year]["episodes"] += 1
        except:
            pass

        if view.movie.id not in episodes:
            episodes.append(view.movie.id)

        views_episodes.append(view)

    for year in range(minYear, maxYear):
        if year not in years:
            years[year] = {"movies": 0, "episodes": 0}

    years2 = []
    for year in sorted(years.keys()):
        years2.append({"year": year, "movies": years[year]["movies"], "episodes": years[year]["episodes"]})

    movies_count = len(movies)
    episodes_count = len(episodes)
    views_count = len(views_movies) + len(views_episodes)

    countries_sortes = []
    for country in sorted(countries, key=countries.get, reverse=True):
        countries_sortes.append({"country": country, "movies": countries[country]})

    return render(request, "movies/stats_views.html", {"views_count": views_count, "views_movies": views_movies, "views_episodes": views_episodes, "movies_count": movies_count, "episodes_count": episodes_count, "movies_long_count": movies_long_count, "countries": countries_sortes, "years": years2})


@login_required
def viewsExport(request):
    if request.method == "POST":
        type = request.POST.get("type", "movies")
        format = request.POST.get("format", "text")
        order = request.POST.get("order", "date")

        result = exportViews(request.user, type, format, order)

        if format == "json":
            return HttpResponse(result, "application/json")
        else:
            return HttpResponse(result, "text/plain")

    return render(request, "movies/views_export.html")


@require_safe
@login_required
def moviesList(request):
    try:
        page = int(request.GET.get("page", 1))
    except:
        page = 1

    display_thumb = ("thumb" in request.GET)

    order = request.GET.get("order", "title")
    if order not in ["title", "-title", "date", "-date"]:
        order = "title"

    movies = Movie.objects.filter(ismovie=True).order_by(order)
    paginator = Paginator(movies, 70)
    movies = paginator.get_page(page)

    return render(request, "movies/movies_list.html", {"movies": movies, "display_thumb": display_thumb})


@require_safe
@login_required
def moviesAPIList(request):
    columns = ["title", "date", "duration"]

    query = request.GET.get("q", "").strip()
    query_set = Movie.objects.filter(ismovie=True)
    if query:
        query_set = query_set.filter(Q(title__icontains=query) | Q(title_vo__icontains=query))

    def formatter(movie):
        data = {
            "id":           movie.id,
            "title":        movie.title,
            "title_vo":     movie.title_vo,
            "url":          reverse("movie_info", kwargs={"id": "%06d" % movie.id}),
            "date":         movie.date,
            "date_txt":     date_format(movie.date),
            "year":         date_year(movie.date),
            "duration":     movie.duration,
            "duration_txt": duration_txt(movie.duration),
            "imdb":         movie.imdb,
            "lang":         movie.lang.code if movie.lang else "",
            "viewed":       False,
        }

        if request.user.is_authenticated:
            data["viewed"] = (movie.views.filter(user=request.user).count() != 0)

        return data

    return generic_APIList(request, columns, query_set, formatter)


@login_required
def moviesSearch(request):
    query = request.GET.get("q", "").strip()

    if query:
        movies_req = Movie.objects.filter(Q(ismovie=True) | Q(episode__isnull=True)).filter(Q(title__icontains=query) | Q(title_vo__icontains=query))
        series = Serie.objects.filter(Q(title__icontains=query) | Q(title_vo__icontains=query))
        episodes = Movie.objects.filter(episode__isnull=False).filter(Q(title__icontains=query) | Q(title_vo__icontains=query))
        sagas = Saga.objects.filter(Q(title__icontains=query) | Q(title_vo__icontains=query))

        movies = []
        for movie in movies_req:
            if request.user.is_authenticated:
                movie.viewed = (movie.views.filter(user=request.user).count() != 0)
            else:
                movie.viewed = False
            movies.append(movie)
    else:
        movies = Movie.objects.none()
        series = Serie.objects.none()
        episodes = Movie.objects.none()
        sagas = Saga.objects.none()

    return render(request, "movies/search.html", {"query": query, "movies": movies, "series": series, "episodes": episodes, "sagas": sagas})


def _checkMovieImages():
    # List images used by movies and resize them if necessary
    images = []
    for movie in Movie.objects.all():
        if movie.image:
            resize_image(str(movie.image))
            images.append(str(movie.image))

    # Remove unused images
    for file in os.listdir(os.path.join(settings.MEDIA_ROOT, "movies")):
        filename = os.path.join(settings.MEDIA_ROOT, "movies", file)
        if not os.path.isfile(filename):
            continue
        if "movies/" + file not in images:
            os.remove(filename)

    # Remove unused images
    for file in os.listdir(os.path.join(settings.MEDIA_ROOT, "movies", "original")):
        filename = os.path.join(settings.MEDIA_ROOT, "movies", "original", file)
        if not os.path.isfile(filename):
            continue
        if "movies/" + file not in images:
            os.remove(filename)


@login_required
@permission_required("movies.add_movie")
def movieCreate(request):
    if request.method == "POST":
        form = MovieForm(request.POST, request.FILES)

        if form.is_valid():
            movie = form.save()

            if movie.image is not None:
                #_checkMovieImages()
                resize_image(str(movie.image))

            return redirect("movie_info", "%06d" % movie.id)
    else:
        form = MovieForm()

    return render(request, "movies/movie_create.html", {"form": form})


@login_required
@permission_required("movies.add_movie")
def movieImport(request):
    data = {}
    imdb = None
    query = ""
    movies = Movie.objects.none()
    serie = Serie.objects.none()

    if request.method == "GET":
        if "query" in request.GET:
            query = request.GET.get("query", "").strip()
            imdb = parse_url(query)
    elif request.method == "POST":
        if "imdb" in request.POST:
            try:
                imdb = int(request.POST.get("imdb", ""))
            except:
                imdb = None
        elif "query" in request.POST:
            query = request.POST.get("query", "").strip()
            imdb = parse_url(query)

    if imdb:
        movies = Movie.objects.filter(imdb=imdb)
        data = getMovieInfo(imdb, date=True)

        if "countries" in data and data["countries"]:
            countries = []
            for code in data["countries"]:
                try:
                    country = Country.objects.get(code=code)
                    countries.append(country)
                except:
                    logging.warning("Unknown country (%s)", code)
                    print("Unknown country (%s)" % code)
            data["countries"] = countries

        if "languages" in data and data["languages"]:
            languages = []
            for code in data["languages"]:
                try:
                    language = Language.objects.get(code=code)
                    languages.append(language)
                except:
                    logging.warning("Unknown language (%s)", code)
                    print("Unknown language (%s)" % code)
            data["languages"] = languages

        if "serie_id" in data:
            try:
                serie = Serie.objects.get(imdb=data["serie_id"])
            except Serie.DoesNotExist:
                pass

        if request.method == "POST":
            if data and "import" in request.POST and "title" in data:
                movie = Movie.objects.create(title=data["title"], imdb=imdb)
                if movie:
                    if "date" in data:
                        movie.date = data["date"]
                    if "episode" in data and data["episode"]:
                        movie.ismovie = False
                    if "duration" in data:
                        movie.duration = data["duration"]
                    if "title_vo" in data:
                        movie.title_vo = data["title_vo"]
                    if "countries" in data:
                        for country in data["countries"]:
                            movie.countries.add(country)
                    if "languages" in data and len(data["languages"]) > 0:
                        movie.lang = data["languages"][0]
                    movie.save()

                    MovieLink.objects.create(movie=movie, name="IMDb", uri="https://www.imdb.com/title/tt%07d/" % imdb, lang=Language.objects.get(code="eng"))
                    MovieAttribute.objects.create(movie=movie, name="imdb-id", value=imdb)

                    actors = getMovieActors(imdb)
                    _movie_import_actors(movie, actors)

                    return redirect("movie_info", "%06d" % movie.id)

    # Recherche sur IMDb : "https://www.imdb.com/find?q={query}&s=tt&ref_=fn_al_tt_mr"
    """
    <table class="findList">
        <tr class="findResult odd">
            <td class="primary_photo">
                <a href="/title/tt0137523/?ref_=fn_tt_tt_1">
                    <img src="https://images-na.ssl-images-amazon.com/images/M/MV5BZGY5jU0OTQ0OTY@._V1_UX32_CR0,0,32,44_AL_.jpg">
                </a>
            </td>
            <td class="result_text">
                <a href="/title/tt0137523/?ref_=fn_tt_tt_1">Fight Club</a> (1999)
            </td>
        </tr>
    """
    return render(request, "movies/movie_import.html", {"query": query, "data": data, "movies": movies, "serie": serie})


@login_required
@require_safe
def moviesAPISearch(request):
    query = request.GET.get("q", "").strip()

    try:
        episodes = bool(request.GET.get("episodes", False))
    except:
        episodes = False

    query_set = Movie.objects.filter(Q(title__icontains=query) | Q(title_vo__icontains=query))

    if not episodes:
        query_set = query_set.filter(ismovie=True)

    if "exclude" in request.GET:
        excludeMoviesId = set()
        for id in request.GET["exclude"].split(","):
            try:
                id = int(id)
                excludeMoviesId.add(id)
            except:
                continue
        query_set = query_set.exclude(id__in=excludeMoviesId)

    # Tri
    query_set = query_set.order_by("title")

    def formatter(movie):
        return {
            "id":           movie.id,
            "title":        movie.title,
            "title_vo":     movie.title_vo,
            "url":          reverse("movie_info", kwargs={"id": "%06d" % movie.id}),
            "date":         movie.date,
            "date_txt":     date_format(movie.date),
            "year":         date_year(movie.date),
            "duration":     movie.duration,
            "duration_txt": duration_txt(movie.duration),
            "imdb":         movie.imdb,
            "lang":         movie.lang.code if movie.lang else "",
            "creation":     movie.creation,
        }

    return generic_APISearch(request, query_set, formatter)


@login_required
@require_safe
def moviesAPIImport(request, imdb):
    try:
        imdb = int(imdb)
    except:
        return HttpResponse("")

    result = getMovieInfo(imdb)

    date = getMovieDate(imdb)
    if date:
        result["date"] = date

    return JsonResponse(result)


@login_required
@require_safe
def movieInfo(request, id):
    actors_queryset = MovieActor.objects.select_related("person", "person__person", "person__group", "lang")
    actors_dubbing_queryset = MovieActorDubbing.objects.select_related("person", "person__person", "person__group", "actor", "actor__person", "actor__group", "lang")
    crew_queryset = MovieCrewMember.objects.select_related("person", "person__person", "person__group")
    references_queryset = MovieReference.objects.select_related("ref_movie", "ref_serie", "ref_person", "ref_book", "ref_book__genre", "ref_game").prefetch_related("ref_book__authors")
    tags_queryset = MovieTag.objects.select_related("tag")
    links_queryset = MovieLink.objects.select_related("lang")

    try:
        movie = Movie.objects.select_related("lang", "episode").prefetch_related("sagas", "countries", "attributes", Prefetch("actors", actors_queryset), Prefetch("dubbing_actors", actors_dubbing_queryset), Prefetch("crew_members", crew_queryset), Prefetch("refs", references_queryset), Prefetch("ref_by", references_queryset), Prefetch("tags", tags_queryset), Prefetch("links", links_queryset)).get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.user.is_authenticated:
        views = request.user.views.select_related("lang", "sublang").filter(movie=movie)
    else:
        views = []

    episode_previous = None
    episode_next = None
    if hasattr(movie, "episode") and movie.episode:
        episodes = Episode.objects.filter(season__serie=movie.episode.season.serie).select_related("movie", "season")
        episodes_sorted = {}
        for episode in episodes:
            episodes_sorted["%09d-%09d" % (get_order(episode.season), get_order(episode))] = episode
        episodes_sorted = [value for (key, value) in sorted(episodes_sorted.items())]
        for i in range(len(episodes_sorted)):
            if episodes_sorted[i] == movie.episode:
                if i > 0:
                    episode_previous = episodes_sorted[i - 1]
                if i < len(episodes_sorted) - 1:
                    episode_next = episodes_sorted[i + 1]
                break

    return render(request, "movies/movie_info.html", {"movie": movie, "episode_previous": episode_previous, "episode_next": episode_next, "views": views})


@login_required
@permission_required("movies.change_movie")
def movieEdit(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        oldImage = str(movie.image) if movie.image is not None else ""
        form = MovieForm(request.POST, request.FILES, instance=movie)

        if form.is_valid():
            if form.has_changed():
                movie = form.save()
                movie.modification = timezone.now()
                movie.save()

                # Resize image
                if movie.image is not None and str(movie.image) != "" and str(movie.image) != oldImage:
                    #_checkMovieImages()
                    resize_image(str(movie.image))

                # Remove old image
                remove_old_image(oldImage, str(movie.image))

            return redirect("movie_info", "%06d" % movie.id)
    else:
        form = MovieForm(instance=movie)

    return render(request, "movies/movie_edit.html", {"movie": movie, "form": form})


@login_required
@permission_required("movies.delete_movie")
def movieDelete(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        if "delete" in request.POST:
            movie.delete()
        return redirect("movies_list")

    return render(request, "movies/movie_delete.html", {"movie": movie})


@login_required
@permission_required("movies.add_movieview")
def movieAddView(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        form = MovieViewForm(request.POST)

        if form.is_valid():
            view = form.save(commit=False)
            if view:
                view.movie = movie
                view.user = request.user
                view.save()

                if movie.ismovie:
                    backupViewsMovies(request.user)
                else:
                    backupViewsEpisodes(request.user)

            return redirect("movie_info", "%06d" % movie.id)
    else:
        try:
            lang = Language.objects.get(iso1=request.LANGUAGE_CODE)
        except:
            lang = None
        form = MovieViewForm(initial={"date": dateformat.format(timezone.now(), "Y-m-d"), "lang": lang})

    url = reverse("movie_add_view", kwargs={"id": "%06d" % movie.id})
    return render(request, "movies/movie_add_view.html", {"movie": movie, "form": form, "form_url": url})


@login_required
@permission_required("movies.add_movieactor")
def movieAddActor(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        form = MovieActorForm(request.POST)

        if form.is_valid():
            actor = form.save(commit=False)
            if actor:
                actor.movie = movie
                actor.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        form = MovieActorForm()

    url = reverse("movie_add_actor", kwargs={"id": "%06d" % movie.id})
    return render(request, "movies/movie_add_actor.html", {"movie": movie, "form": form, "form_url": url})


@login_required
@permission_required("movies.add_movieactordubbing")
def movieAddActorDubbing(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        form = MovieActorDubbingForm(request.POST)

        if form.is_valid():
            actor = form.save(commit=False)
            if actor:
                actor.movie = movie
                actor.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        form = MovieActorDubbingForm()

    url = reverse("movie_add_actordubbing", kwargs={"id": "%06d" % movie.id})
    return render(request, "movies/movie_add_actordubbing.html", {"movie": movie, "form": form, "form_url": url})


@login_required
@permission_required("movies.add_moviecrewmember")
def movieAddCrewMember(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        form = MovieCrewMemberForm(request.POST)

        if form.is_valid():
            crewmember = form.save(commit=False)
            if crewmember:
                crewmember.movie = movie
                crewmember.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        form = MovieCrewMemberForm()

    url = reverse("movie_add_crewmember", kwargs={"id": "%06d" % movie.id})
    return render(request, "movies/movie_add_crewmember.html", {"movie": movie, "form": form, "form_url": url})


@login_required
@permission_required("movies.add_moviereference")
def movieAddRef(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        form = MovieReferenceForm(request.POST)

        if form.is_valid():
            ref = form.save(commit=False)
            if ref:
                ref.movie = movie
                ref.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        form = MovieReferenceForm()

    return render(request, "movies/movie_add_ref.html", {"movie": movie, "form": form})


@login_required
@permission_required("movies.add_movietag")
def movieAddTag(request, id):
    try:
        movietag_queryset = MovieTag.objects.select_related("tag")
        movie = Movie.objects.prefetch_related(Prefetch("tags", movietag_queryset)).get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        form = MovieTagForm(request.POST)

        if form.is_valid():
            movieTag = form.save(commit=False)
            if movieTag:
                movieTag.movie = movie
                movieTag.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        form = MovieTagForm()

    url = reverse("movie_add_tag", kwargs={"id": "%06d" % movie.id})
    return render(request, "movies/movie_add_tag.html", {"movie": movie, "form": form, "form_url": url})


@login_required
@permission_required("movies.add_movieattribute")
def movieAddAttribute(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        form = MovieAttributeForm(request.POST)

        if form.is_valid():
            attribute = form.save(commit=False)
            if attribute:
                attribute.movie = movie
                attribute.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        form = MovieAttributeForm()

    url = reverse("movie_add_attribute", kwargs={"id": "%06d" % movie.id})
    return render(request, "movies/movie_add_attribute.html", {"movie": movie, "form": form, "form_url": url})


@login_required
@permission_required("movies.add_movielink")
def movieAddLink(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if request.method == "POST":
        form = MovieLinkForm(request.POST)

        if form.is_valid():
            link = form.save(commit=False)
            if link:
                link.movie = movie
                link.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        form = MovieLinkForm()

    url = reverse("movie_add_link", kwargs={"id": "%06d" % movie.id})
    return render(request, "movies/movie_add_link.html", {"movie": movie, "form": form, "form_url": url})


@login_required
@permission_required("movies.change_movieview")
def movieEditViews(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    views = movie.views.filter(user=request.user)
    MovieViewFormSet = modelformset_factory(MovieView, form=MovieViewForm, extra=0, can_delete=True)
    # ATTENTION: MovieViewFormSet n'est pas optimisé. La liste des langues est requêtée pour chaque entrée !

    if request.method == "POST":
        formset = MovieViewFormSet(request.POST, queryset=views)

        if formset.is_valid():
            formset.save_existing_objects()
            instances = formset.save_new_objects(commit=False)
            for instance in instances:
                instance.movie = movie
                instance.user = request.user
                instance.save()
            backupViews(request.user)
            return redirect("movie_info", "%06d" % movie.id)
    else:
        formset = MovieViewFormSet(queryset=views)

    url = reverse("movie_edit_views", kwargs={"id": "%06d" % movie.id})
    title = _("Edit views")
    return render(request, "movies/movie_edit_data.html", {"movie": movie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_movieactor")
def movieEditActors(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    actors = movie.actors.select_related("person", "person__person", "person__group", "lang").all()
    # TODO: optimize languages queryset
    MovieActorFormSet = modelformset_factory(MovieActor, form=MovieActorReadOnlyForm, extra=0, can_delete=True, can_order=True)

    if request.method == "POST":
        formset = MovieActorFormSet(request.POST, queryset=actors)

        if formset.is_valid():
            for form in formset.ordered_forms:
                form.instance._order = form.cleaned_data["ORDER"] - 1
            formset.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        formset = MovieActorFormSet(queryset=actors)

    url = reverse("movie_edit_actors", kwargs={"id": "%06d" % movie.id})
    title = _("Edit actors")
    return render(request, "movies/movie_edit_data.html", {"movie": movie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_movieactor")
def movieImportActors(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    if not movie.imdb or movie.imdb == 0:
        return redirect("movie_info", "%06d" % movie.id)

    if request.method == "POST":
        try:
            movie.actors.all().delete()
            actors = []

            count = int(request.POST.get("form-TOTAL", 0))
            for i in range(count):
                name = request.POST.get("form-%d-name" % i, "").strip()
                if not name:
                    continue

                role = request.POST.get("form-%d-role" % i, "").strip()
                info = request.POST.get("form-%d-info" % i, "").strip()
                actorAs = request.POST.get("form-%d-as" % i, "").strip()
                credit = request.POST.get("form-%d-credit" % i, False)
                visible = request.POST.get("form-%d-visible" % i, False)

                try:
                    imdb = int(request.POST.get("form-%d-imdb" % i))
                except:
                    imdb = None

                actors.append({
                    "name": name,
                    "role": role,
                    "info": info,
                    "as": actorAs,
                    "credited": credit,
                    "visible": visible,
                    "imdb": imdb,
                })

            _movie_import_actors(movie, actors)
            """
            count = int(request.POST.get("form-TOTAL", 0))
            for i in range(count):
                name = request.POST.get("form-%d-name" % i, "").strip()
                if not name:
                    continue

                role = request.POST.get("form-%d-role" % i, "").strip()
                info = request.POST.get("form-%d-info" % i, "").strip()
                actorAs = request.POST.get("form-%d-as" % i, "").strip()
                credit = request.POST.get("form-%d-credit" % i, False)
                visible = request.POST.get("form-%d-visible" % i, False)

                try:
                    imdb = int(request.POST.get("form-%d-imdb" % i))
                except:
                    imdb = None

                movieName = None
                try:
                    if imdb:
                        movieName = MovieName.objects.get(imdb=imdb)
                    else:
                        raise Exception()
                except:
                    try:
                        person = Person.objects.get(name=name)
                    except:
                        person = Person.objects.create(name=name)
                    movieNameGet = MovieName.objects.get_or_create(person=person)[0]
                    if not movieNameGet.imdb:
                        movieName = movieNameGet
                        if imdb:
                            movieName.imdb = imdb
                            movieName.save()

                            PersonLink.objects.create(person=person, name="IMDb",
                                                      uri="https://www.imdb.com/name/nm%07d/" % imdb,
                                                      lang=Language.objects.get(code="eng"))
                            PersonAttribute.objects.create(person=person, name="imdb-id", value=imdb)
                    elif movieNameGet.imdb == imdb:
                        movieName = movieNameGet

                    if not movieName:
                        movieName = MovieName.objects.create(person=person, imdb=imdb)

                MovieActor.objects.create(movie=movie, person=movieName, name=actorAs, role=role, info=info, credit=credit, visible=visible)
            """

            return redirect("movie_info", "%06d" % movie.id)
        except:
            return redirect("movie_info", "%06d" % movie.id)

    actors = getMovieActors(movie.imdb)

    return render(request, "movies/movie_import_actors.html", {"movie": movie, "actors": actors})


@login_required
@permission_required("movies.change_movieactordubbing")
def movieEditActorsDubbing(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except:
        return redirect("movies_list")

    actors = movie.dubbing_actors.select_related("person", "person__person", "person__group", "actor", "actor__person", "actor__group", "lang").all()
    # TODO: optimize languages queryset
    MovieActorDubbingFormSet = modelformset_factory(MovieActorDubbing, form=MovieActorDubbingReadOnlyForm, extra=0, can_delete=True)

    if request.method == "POST":
        formset = MovieActorDubbingFormSet(request.POST, queryset=actors)

        if formset.is_valid():
            formset.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        formset = MovieActorDubbingFormSet(queryset=actors)

    url = reverse("movie_edit_actorsdubbing", kwargs={"id": "%06d" % movie.id})
    title = _("Edit dubbing actors")
    return render(request, "movies/movie_edit_data.html", {"movie": movie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_moviecrewmember")
def movieEditCrew(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except:
        return redirect("movies_list")

    crew = movie.crew_members.select_related("person", "person__person", "person__group").all()
    MovieCrewMemberFormSet = modelformset_factory(MovieCrewMember, form=MovieCrewMemberReadOnlyForm, extra=0, can_delete=True)

    if request.method == "POST":
        formset = MovieCrewMemberFormSet(request.POST, queryset=crew)

        if formset.is_valid():
            formset.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        formset = MovieCrewMemberFormSet(queryset=crew)

    url = reverse("movie_edit_crew", kwargs={"id": "%06d" % movie.id})
    title = _("Edit crew")
    return render(request, "movies/movie_edit_data.html", {"movie": movie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_moviereference")
def movieEditRefs(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    refs = movie.refs.select_related("ref_movie", "ref_serie", "ref_person", "ref_book", "ref_book__genre", "ref_game").prefetch_related("ref_book__authors").all()
    MovieReferenceFormSet = modelformset_factory(MovieReference, form=MovieReferenceReadOnlyForm, extra=0, can_delete=True)

    if request.method == "POST":
        formset = MovieReferenceFormSet(request.POST, queryset=refs)

        if formset.is_valid():
            formset.save()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        formset = MovieReferenceFormSet(queryset=refs)

    url = reverse("movie_edit_refs", kwargs={"id": "%06d" % movie.id})
    title = _("Edit references")
    return render(request, "movies/movie_edit_data.html", {"movie": movie, "title": title, "url_form": url, "formset": formset, "can_add": False})


@login_required
@permission_required("movies.change_movietag")
def movieEditTags(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except Movie.DoesNotExist:
        return redirect("movies_list")

    tags = movie.tags.all()

    if tags.count() == 0:
        extra = 1
    else:
        extra = 0

    MovieTagFormSet = modelformset_factory(MovieTag, form=MovieTagFormClassic, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = MovieTagFormSet(request.POST, queryset=tags)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.movie = movie
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        formset = MovieTagFormSet(queryset=tags)

    url = reverse("movie_edit_tags", kwargs={"id": "%06d" % movie.id})
    title = _("Edit tags")
    return render(request, "movies/movie_edit_data.html", {"movie": movie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_movieattribute")
def movieEditAttributes(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except:
        return redirect("movies_list")

    attributes = movie.attributes.all()

    if attributes.count() == 0:
        extra = 1
    else:
        extra = 0

    MovieAttributeFormSet = modelformset_factory(MovieAttribute, form=MovieAttributeForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = MovieAttributeFormSet(request.POST, queryset=attributes)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.movie = movie
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        formset = MovieAttributeFormSet(queryset=attributes)

    url = reverse("movie_edit_attributes", kwargs={"id": "%06d" % movie.id})
    title = _("Edit attributes")
    return render(request, "movies/movie_edit_data.html", {"movie": movie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_movielink")
def movieEditLinks(request, id):
    try:
        movie = Movie.objects.get(id=id)
    except:
        return redirect("movies_list")

    links = movie.links.all()

    if links.count() == 0:
        extra = 1
    else:
        extra = 0

    MovieLinkFormSet = modelformset_factory(MovieLink, form=MovieLinkForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = MovieLinkFormSet(request.POST, queryset=links)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.movie = movie
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("movie_info", "%06d" % movie.id)
    else:
        formset = MovieLinkFormSet(queryset=links)

    url = reverse("movie_edit_links", kwargs={"id": "%06d" % movie.id})
    title = _("Edit links")
    return render(request, "movies/movie_edit_data.html", {"movie": movie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@require_safe
def seriesList(request):
    try:
        page = int(request.GET.get("page", 1))
    except:
        page = 1

    order = request.GET.get("order", "title")
    if order not in ["title", "-title"]:
        order = "title"

    series = Serie.objects.all().annotate(num_seasons=Count("seasons")).order_by(order)
    paginator = Paginator(series, 10)

    try:
        series = paginator.page(page)
    except PageNotAnInteger:
        series = paginator.page(1)
    except EmptyPage:
        series = paginator.page(paginator.num_pages)

    return render(request, "movies/series_list.html", {"series": series})


@login_required
@permission_required("movies.add_serie")
def serieCreate(request):
    if request.method == "POST":
        form = SerieForm(request.POST)

        if form.is_valid():
            serie = form.save()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        form = SerieForm()

    return render(request, "movies/serie_create.html", {"form": form})


@login_required
@permission_required("movies.add_serie")
@permission_required("movies.add_movie")
def serieImport(request):
    data = {}
    imdb = None
    query = ""
    series = Serie.objects.none()

    if request.method == "GET":
        if "query" in request.GET:
            query = request.GET.get("query", "").strip()
            imdb = parse_url(query)
    elif request.method == "POST":
        if "imdb" in request.POST:
            try:
                imdb = int(request.POST.get("imdb", ""))
            except:
                imdb = None
        elif "query" in request.POST:
            query = request.POST.get("query", "").strip()
            imdb = parse_url(query)

    if imdb:
        series = Serie.objects.filter(imdb=imdb)
        data = getMovieInfo(imdb)

        if "countries" in data and data["countries"]:
            countries = []
            for code in data["countries"]:
                try:
                    country = Country.objects.get(code=code)
                    countries.append(country)
                except:
                    logging.warning("Unknown country (%s)", code)
                    print("Unknown country (%s)" % code)
            data["countries"] = countries

        if "languages" in data and data["languages"]:
            languages = []
            for code in data["languages"]:
                try:
                    language = Language.objects.get(code=code)
                    languages.append(language)
                except:
                    logging.warning("Unknown language (%s)", code)
                    print("Unknown language (%s)" % code)
            data["languages"] = languages

        if request.method == "POST":
            if data and "import" in request.POST:
                serie = Serie.objects.create(title=data["title"], imdb=imdb)
                if serie:
                    if "duration" in data:
                        serie.duration = data["duration"]
                    if "title_vo" in data:
                        serie.title_vo = data["title_vo"]
                    if "countries" in data:
                        for country in data["countries"]:
                            serie.countries.add(country)
                    if "languages" in data and len(data["languages"]) > 0:
                        serie.lang = data["languages"][0]
                    serie.save()

                    SerieLink.objects.create(serie=serie, name="IMDb", uri="https://www.imdb.com/title/tt%07d/" % imdb, lang=Language.objects.get(code="eng"))
                    SerieAttribute.objects.create(serie=serie, name="imdb-id", value=imdb)

                    seasons = getSerieEpisodes(imdb)
                    for seasonNum, episodes in seasons.items():
                        season = Season.objects.create(serie=serie, name=_("Season %(num)s") % {"num": seasonNum})
                        for episodeNum, episodeInfo in episodes.items():
                            episodeImdb = episodeInfo["imdb"]
                            data = getMovieInfo(episodeImdb)
                            if data:
                                if "countries" in data and data["countries"]:
                                    countries = []
                                    for code in data["countries"]:
                                        try:
                                            country = Country.objects.get(code=code)
                                            countries.append(country)
                                        except:
                                            logging.warning("Unknown country (%s)", code)
                                            print("Unknown country (%s)" % code)
                                    data["countries"] = countries

                                if "languages" in data and data["languages"]:
                                    languages = []
                                    for code in data["languages"]:
                                        try:
                                            language = Language.objects.get(code=code)
                                            languages.append(language)
                                        except:
                                            logging.warning("Unknown language (%s)", code)
                                            print("Unknown language (%s)" % code)
                                    data["languages"] = languages

                                movie = Movie.objects.create(title=data["title"], imdb=episodeImdb, ismovie=False)
                                if movie:
                                    date = getMovieDate(episodeImdb)
                                    if date:
                                        movie.date = date
                                    if "duration" in data:
                                        movie.duration = data["duration"]
                                    if "title_vo" in data:
                                        movie.title_vo = data["title_vo"]
                                    if "countries" in data:
                                        for country in data["countries"]:
                                            movie.countries.add(country)
                                    if "languages" in data and len(data["languages"]) > 0:
                                        movie.lang = data["languages"][0]
                                    movie.save()

                                    MovieLink.objects.create(movie=movie, name="IMDb", uri="https://www.imdb.com/title/tt%07d/" % episodeImdb, lang=Language.objects.get(code="eng"))
                                    MovieAttribute.objects.create(movie=movie, name="imdb-id", value=episodeImdb)
                                    Episode.objects.create(season=season, movie=movie)

                    return redirect("serie_info", "%06d" % serie.id)

    # Recherche sur IMDb : "https://www.imdb.com/find?q={query}&s=tt&ref_=fn_al_tt_mr"

    return render(request, "movies/serie_import.html", {"query": query, "data": data, "series": series})


@login_required
@permission_required("movies.change_serie")
@permission_required("movies.add_movie")
def serieImportEpisodes(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    if not serie.imdb:
        return redirect("serie_info", "%06d" % serie.id)

    seasons = getSerieEpisodes(serie.imdb)

    seasons2 = {}
    for seasonNum, episodes in seasons.items():
        season_name = _("Season %(num)s") % {"num": seasonNum}
        episodes2 = []
        for episodeNum, episodeInfo in episodes.items():
            episodes2.append({"imdb": episodeInfo["imdb"], "title": episodeInfo["title"], "object": None})
        seasons2[season_name] = {"episodes": episodes2, "object": None, "exists": True}
    seasons = seasons2

    def _season_get_episode(episodes:list, imdb:int) -> dict:
        for episode in episodes:
            if episode["imdb"] == imdb:
                return episode
        return None

    for season in serie.seasons.all():
        if season.name in seasons:
            seasons[season.name]["object"] = season

            for episode in season.episodes.all():
                episodeInfo = _season_get_episode(seasons[season.name]["episodes"], episode.movie.imdb)
                if episodeInfo is None:
                    seasons[season.name]["episodes"].append({"imdb": episode.movie.imdb, "title": None, "object": episode})
                else:
                    episodeInfo["object"] = episode
        else:
            seasons[season.name] = {"episodes": [], "object": season, "exists": False}

            for episode in season.episodes.all():
                seasons[season.name]["episodes"].append({"imdb": episode.movie.imdb, "title": None, "object": episode})

    if request.method == "POST" and "import" in request.POST:
        # Création des nouvelles saisons et liste des épisodes à supprimer ou déplacer
        bad_episodes = {}
        for seasonName, seasonInfo in seasons.items():
            if seasonInfo["object"] is None:
                seasonInfo["object"] = Season.objects.create(serie=serie, name=seasonName)

            for episodeInfo in seasonInfo["episodes"]:
                if episodeInfo["title"] is None and episodeInfo["object"] is not None and episodeInfo["object"].imdb:
                    bad_episodes[episodeInfo["object"].imdb] = episodeInfo["object"]

        for seasonName, seasonInfo in seasons.items():
            for episodeInfo in seasonInfo["episodes"]:
                if episodeInfo["object"] is not None:
                    continue

                try:
                    movie = Movie.objects.get(imdb=episodeInfo["imdb"])
                    continue  # Le film existe déjà (dans une autre série ou sans série) : on ne fait rien pour l'instant
                except Movie.DoesNotExist:
                    pass

                data = getMovieInfo(episodeInfo["imdb"])
                if data:
                    if "countries" in data and data["countries"]:
                        countries = []
                        for code in data["countries"]:
                            try:
                                country = Country.objects.get(code=code)
                                countries.append(country)
                            except:
                                logging.warning("Unknown country (%s)", code)
                                print("Unknown country (%s)" % code)
                        data["countries"] = countries

                    if "languages" in data and data["languages"]:
                        languages = []
                        for code in data["languages"]:
                            try:
                                language = Language.objects.get(code=code)
                                languages.append(language)
                            except:
                                logging.warning("Unknown language (%s)", code)
                                print("Unknown language (%s)" % code)
                        data["languages"] = languages

                    movie = Movie.objects.create(title=data["title"], imdb=episodeInfo["imdb"], ismovie=False)
                    if movie:
                        date = getMovieDate(episodeInfo["imdb"])
                        if date:
                            movie.date = date
                        if "duration" in data:
                            movie.duration = data["duration"]
                        if "title_vo" in data:
                            movie.title_vo = data["title_vo"]
                        if "countries" in data:
                            for country in data["countries"]:
                                movie.countries.add(country)
                        if "languages" in data and len(data["languages"]) > 0:
                            movie.lang = data["languages"][0]
                        movie.save()

                        MovieLink.objects.create(movie=movie, name="IMDb", uri="https://www.imdb.com/title/tt%07d/" % episodeInfo["imdb"], lang=Language.objects.get(code="eng"))
                        MovieAttribute.objects.create(movie=movie, name="imdb-id", value=episodeInfo["imdb"])
                        Episode.objects.create(season=seasonInfo["object"], movie=movie)

        return redirect("serie_info", "%06d" % serie.id)

    return render(request, "movies/serie_import_episodes.html", {"serie": serie, "seasons": seasons})


@login_required
@require_safe
def seriesAPIList(request):
    try:
        draw = int(request.GET.get("draw"))
    except:
        return HttpResponseBadRequest("Invalid field 'draw'.")

    try:
        start = int(request.GET.get("start"))
        if start < 0:
            start = 0
    except:
        return HttpResponseBadRequest("Invalid field 'start'.")

    try:
        length = int(request.GET.get("length"))
        if length == 0:
            return HttpResponseBadRequest("Invalid field 'length'.")
    except:
        return HttpResponseBadRequest("Invalid field 'length'.")

    query_set = Serie.objects.all()
    total = query_set.count()

    # Search
    if "search[value]" in request.GET:
        q = request.GET.get("search[value]").strip()
        if q:
            query_set = query_set.filter(Q(title__icontains=q) | Q(title_vo__icontains=q))

    totalFiltered = query_set.count()

    try:
        orderDir = request.GET.get("order[0][dir]")
    except:
        orderDir = "asc"

    # Tri
    if "order[0][column]" in request.GET and request.GET.get("order[0][column]") == "0":
        if orderDir == "desc":
            query_set = query_set.order_by("-title")
        else:
            query_set = query_set.order_by("title")

        # Limite
        if length < 0:
            query_set = query_set[start:]
        else:
            query_set = query_set[start:start+length]

    query_set = query_set.annotate(Count("seasons"))

    result = {
        "draw": draw,
        "recordsTotal": total,
        "recordsFiltered": totalFiltered,
        "data": [],
    }

    for serie in query_set:
        try:
            lastEpisode = serie.last_episode

            lastEpisodeJSON = {
                "id": lastEpisode.id,
                "title": lastEpisode.title,
                "title_vo": lastEpisode.title_vo,
                "date": lastEpisode.date,
                "date_txt": date_format(lastEpisode.date),
                "year": date_year(lastEpisode.date),
                "duration": lastEpisode.duration,
                "duration_txt": duration_txt(lastEpisode.duration),
                "imdb": lastEpisode.imdb,
                "url": reverse("movie_info", kwargs={"id": "%06d" % lastEpisode.id}),
            }
        except:
            lastEpisodeJSON = None

        result["data"].append({
            "id": serie.id,
            "title": serie.title,
            "title_vo": serie.title_vo,
            "url": reverse("serie_info", kwargs={"id": "%06d" % serie.id}),
            "imdb": serie.imdb,
            "lang": serie.lang.code if serie.lang else "",
            "seasons_count": int(serie.seasons__count),
            "episodes_count": int(serie.episodes_count),
            "last_episode": lastEpisodeJSON,
        })

    if "order[0][column]" in request.GET:
        if request.GET.get("order[0][column]") == "1":
            if orderDir == "desc":
                result["data"] = sorted(result["data"], key=lambda k: k["seasons_count"], reverse=True)
            else:
                result["data"] = sorted(result["data"], key=lambda k: k["seasons_count"])
        elif request.GET.get("order[0][column]") == "2":
            if orderDir == "desc":
                result["data"] = sorted(result["data"], key=lambda k: k["episodes_count"], reverse=True)
            else:
                result["data"] = sorted(result["data"], key=lambda k: k["episodes_count"])
        elif request.GET.get("order[0][column]") == "3":
            if orderDir == "desc":
                result["data"] = sorted(result["data"], key=lambda k: k["last_episode"]["date"] if k["last_episode"] and k["last_episode"]["date"] else "", reverse=True)
            else:
                result["data"] = sorted(result["data"], key=lambda k: k["last_episode"]["date"] if k["last_episode"] and k["last_episode"]["date"] else "")

        if request.GET.get("order[0][column]") != "0":
            # Limite
            if length < 0:
                result["data"] = result["data"][start:]
            else:
                result["data"] = result["data"][start:start+length]

    return JsonResponse(result)


@login_required
@require_safe
def seriesAPISearch(request):
    query = request.GET.get("q", "")

    query_set = Serie.objects.filter(Q(title__icontains=query) | Q(title_vo__icontains=query))

    if "exclude" in request.GET:
        excludeSeriesId = set()
        for id in request.GET["exclude"].split(","):
            try:
                id = int(id)
                excludeSeriesId.add(id)
            except:
                continue
        query_set = query_set.exclude(id__in=excludeSeriesId)

    total = query_set.count()

    # Tri
    query_set = query_set.order_by("title")

    # Pagination
    itemPerPage = 10

    try:
        page = int(request.GET.get("page", 1))
    except:
        page = 1

    start = (page - 1) * itemPerPage

    try:
        length = int(request.GET.get("length", itemPerPage))
        if length == 0:
            return HttpResponseBadRequest("Invalid field 'length'.")
    except:
        length = itemPerPage

    if length < 0:
        query_set = query_set[start:]
    else:
        query_set = query_set[start:start+length]

    result = {
        "total_count": total,
        "start": start,
        "length": len(query_set),
        "page": page,
        "incomplete_results": False,
        "items": [],
    }

    for serie in query_set:
        result["items"].append({
            "id":       serie.id,
            "title":    serie.title,
            "title_vo": serie.title_vo,
            "url":      reverse("serie_info", kwargs={"id": "%06d" % serie.id}),
            "imdb":     serie.imdb,
            "lang":     serie.lang.code if serie.lang else "",
        })

    if start + result["length"] < total:
        result["incomplete_results"] = True

    return JsonResponse(result)


@login_required
@require_safe
def serieInfo(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect('series_list')

    seasons = {}
    episodes = Episode.objects.filter(season__serie=serie).select_related("movie", "season")
    for episode in episodes:
        if episode.season.id not in seasons:
            seasonOrder = "%09d" % get_order(episode.season)
            seasons[episode.season.id] = {"order": seasonOrder, "season": episode.season, "episodes_d": {}}
        episodeOrder = "%09d" % get_order(episode)
        if request.user.is_authenticated:
            episode.viewed = (episode.movie.views.filter(user=request.user).count() != 0)
        else:
            episode.viewed = False
        seasons[episode.season.id]["episodes_d"][episode.id] = {"order": episodeOrder, "episode": episode}

    # Saisons sans épisodes
    seasonsReq = Season.objects.filter(serie=serie)
    for season in seasonsReq:
        if season.id not in seasons:
            seasonOrder = "%09d" % get_order(season)
            seasons[season.id] = {"order": seasonOrder, "season": season, "episodes_d": {}}

    seasons = sorted(seasons.values(), key=lambda k: k["order"])

    for season in seasons:
        season["episodes"] = [k["episode"] for k in sorted(season["episodes_d"].values(), key=lambda k: k["order"])]
        del season["order"]
        del season["episodes_d"]

    countries = serie.countries
    crew = serie.crew_members.select_related("person", "person__person", "person__group")
    tags = serie.tags.select_related("tag")
    links = serie.links.select_related("lang")

    actorsQuerySet = MovieActor.objects.filter(movie__episode__season__serie=serie).select_related("person", "person__person", "person__group", "movie")
    actors = {}
    for actor in actorsQuerySet:
        if actor.person.id not in actors:
            actors[actor.person.id] = {"actor": actor.person, "date_first": actor.movie.date, "date_last": actor.movie.date, "count": 1, "roles": {actor.role: 1}}
        else:
            actors[actor.person.id]["count"] += 1
            if actor.movie.date:
                if actors[actor.person.id]["date_first"] is None or actor.movie.date < actors[actor.person.id]["date_first"]:
                    actors[actor.person.id]["date_first"] = actor.movie.date
                if actors[actor.person.id]["date_last"] is None or actor.movie.date > actors[actor.person.id]["date_last"]:
                    actors[actor.person.id]["date_last"] = actor.movie.date
            if actor.role in actors[actor.person.id]["roles"]:
                actors[actor.person.id]["roles"][actor.role] += 1
            else:
                actors[actor.person.id]["roles"][actor.role] = 1
    actors = sorted(actors.values(), key=lambda k: k["count"], reverse=True)

    for actor in actors:
        items = [(v, k) for k, v in actor["roles"].items()]
        items.sort()
        items.reverse()
        actor["roles"] = [(k, v) for v, k in items]

    #if request.user.is_authenticated:
    #    data["viewed"] = (movie.views.filter(user=request.user).count() != 0)

    return render(request, "movies/serie_info.html", {"serie": serie, "countries": countries, "seasons": seasons, "actors": actors, "crew": crew, "tags": tags, "links": links})


@login_required
@permission_required("movies.change_serie")
def serieEdit(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    if request.method == "POST":
        form = SerieForm(request.POST, instance=serie)

        if form.is_valid():
            serie = form.save()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        form = SerieForm(instance=serie)

    return render(request, "movies/serie_edit.html", {"serie": serie, "form": form})


@login_required
@permission_required("movies.delete_serie")
def serieDelete(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    if request.method == "POST":
        if "delete" in request.POST:
            serie.delete()
        return redirect("series_list")

    return render(request, "movies/serie_delete.html", {"serie": serie})


@login_required
@permission_required("movies.add_seriecrewmember")
def serieAddCrewMember(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    if request.method == "POST":
        form = SerieCrewMemberForm(request.POST)

        if form.is_valid():
            crewmember = form.save(commit=False)
            if crewmember:
                crewmember.serie = serie
                crewmember.save()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        form = SerieCrewMemberForm()

    url = reverse("serie_add_crewmember", kwargs={"id": id})
    return render(request, "movies/serie_add_crewmember.html", {"serie": serie, "form": form, "form_url": url})


@login_required
@permission_required("movies.add_serietag")
def serieAddTag(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    if request.method == "POST":
        form = SerieTagForm(request.POST)

        if form.is_valid():
            serieTag = form.save(commit=False)
            if serieTag:
                serieTag.serie = serie
                serieTag.save()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        form = SerieTagForm()

    return render(request, "movies/serie_add_tag.html", {"serie": serie, "form": form})


@login_required
@permission_required("movies.add_serieattribute")
def serieAddAttribute(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    if request.method == "POST":
        form = SerieAttributeForm(request.POST)

        if form.is_valid():
            attribute = form.save(commit=False)
            if attribute:
                attribute.serie = serie
                attribute.save()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        form = SerieAttributeForm()

    url = reverse("serie_add_attribute", kwargs={"id": "%06d" % serie.id})
    return render(request, "movies/serie_add_attribute.html", {"serie": serie, "form": form, "form_url": url})


@login_required
@permission_required("movies.add_serielink")
def serieAddLink(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    if request.method == "POST":
        form = SerieLinkForm(request.POST)

        if form.is_valid():
            link = form.save(commit=False)
            if link:
                link.serie = serie
                link.save()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        form = SerieLinkForm()

    return render(request, "movies/serie_add_link.html", {"serie": serie, "form": form})


@login_required
@permission_required("movies.add_season")
def serieAddSeason(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    if request.method == "POST":
        form = SeasonForm(request.POST)

        if form.is_valid():
            season = form.save(commit=False)
            if season:
                season.serie = serie
                season.save()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        form = SeasonForm()

    return render(request, "movies/serie_add_season.html", {"serie": serie, "form": form})


@login_required
@permission_required("movies.change_seriecrewmember")
def serieEditCrew(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    crew = serie.crew_members.all()
    SerieCrewMemberFormSet = modelformset_factory(SerieCrewMember, form=SerieCrewMemberReadOnlyForm, extra=0, can_delete=True)

    if request.method == "POST":
        formset = SerieCrewMemberFormSet(request.POST, queryset=crew)

        if formset.is_valid():
            formset.save()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        formset = SerieCrewMemberFormSet(queryset=crew)

    url = reverse("serie_edit_crew", kwargs={"id": "%06d" % serie.id})
    title = _("Edit crew")
    return render(request, "movies/serie_edit_data.html", {"serie": serie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_serietag")
def serieEditTags(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    tags = serie.tags.all()

    if tags.count() == 0:
        extra = 1
    else:
        extra = 0

    SerieTagFormSet = modelformset_factory(SerieTag, form=SerieTagFormClassic, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = SerieTagFormSet(request.POST, queryset=tags)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.serie = serie
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        formset = SerieTagFormSet(queryset=tags)

    url = reverse("serie_edit_tags", kwargs={"id": "%06d" % serie.id})
    title = _("Edit tags")
    return render(request, "movies/serie_edit_data.html", {"serie": serie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_serieattribute")
def serieEditAttributes(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    attributes = serie.attributes.all()

    if attributes.count() == 0:
        extra = 1
    else:
        extra = 0

    SerieAttributeFormSet = modelformset_factory(SerieAttribute, form=SerieAttributeForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = SerieAttributeFormSet(request.POST, queryset=attributes)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.serie = serie
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        formset = SerieAttributeFormSet(queryset=attributes)

    url = reverse("serie_edit_attributes", kwargs={"id": "%06d" % serie.id})
    title = _("Edit attributes")
    return render(request, "movies/serie_edit_data.html", {"serie": serie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_serielink")
def serieEditLinks(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    links = serie.links.all()

    if links.count() == 0:
        extra = 1
    else:
        extra = 0

    SerieLinkFormSet = modelformset_factory(SerieLink, form=SerieLinkForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = SerieLinkFormSet(request.POST, queryset=links)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.serie = serie
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        formset = SerieLinkFormSet(queryset=links)

    url = reverse("serie_edit_links", kwargs={"id": "%06d" % serie.id})
    title = _("Edit links")
    return render(request, "movies/serie_edit_data.html", {"serie": serie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.change_season")
def serieEditSeasons(request, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    seasons = serie.seasons.all()

    if seasons.count() == 0:
        extra = 1
    else:
        extra = 0

    SerieSeasonsFormSet = modelformset_factory(Season, form=SeasonForm, extra=extra, can_order=True, can_delete=True)

    if request.method == "POST":
        formset = SerieSeasonsFormSet(request.POST, queryset=seasons)

        if formset.is_valid():
            for form in formset.ordered_forms:
                form.instance._order = form.cleaned_data['ORDER'] - 1
            instances = formset.save(commit=False)
            for instance in instances:
                wantedOrder = instance._order
                instance.serie = serie
                instance.save()
                if instance._order != wantedOrder:  # new instance have automatic order
                    instance._order = wantedOrder
                    instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        formset = SerieSeasonsFormSet(queryset=seasons)

    url = reverse("serie_edit_seasons", kwargs={"id": "%06d" % serie.id})
    title = _("Edit seasons")
    return render(request, "movies/serie_edit_data.html", {"serie": serie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("movies.add_episode")
def seasonAddEpisode(request, id, sid):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    try:
        season = Season.objects.get(id=sid)
    except Season.DoesNotExist:
        return redirect("serie_info", "%06d" % serie.id)

    if season.serie != serie:
        return redirect("serie_info", "%06d" % serie.id)

    if request.method == "POST":
        try:
            movie_id = int(request.POST.get("movie"))
            movie = Movie.objects.get(id=movie_id)
            if movie:
                Episode.objects.create(season=season, movie=movie)
                return redirect("serie_info", "%06d" % serie.id)
        except:
            pass

    return render(request, "movies/season_add_episode.html", {"serie": serie, "season": season})


@login_required
@permission_required(["movies.add_episode", "movies.add_movie"])
def seasonCreateEpisode(request, sid, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    try:
        season = Season.objects.get(id=sid)
    except Season.DoesNotExist:
        return redirect("serie_info", "%06d" % serie.id)

    if season.serie != serie:
        return redirect("serie_info", id)

    if request.method == "POST":
        form = MovieForm(request.POST, request.FILES, initial={"duration": serie.duration, "ismovie": False})

        if form.is_valid():
            movie = form.save()
            if movie:
                Episode.objects.create(season=season, movie=movie)
                return redirect("movie_info", "%06d" % movie.id)
            return redirect("serie_info", "%06d" % serie.id)
    else:
        form = MovieForm(initial={"duration": serie.duration, "ismovie": False, "lang": serie.lang})

    form.fields["ismovie"].widget = HiddenInput()

    return render(request, "movies/season_create_episode.html", {"serie": serie, "season": season, "form": form})


@login_required
@permission_required("movies.change_episode")
def seasonEditEpisodes(request, sid, id):
    try:
        serie = Serie.objects.get(id=id)
    except Serie.DoesNotExist:
        return redirect("series_list")

    try:
        season = Season.objects.get(id=sid)
    except Season.DoesNotExist:
        return redirect("serie_info", "%06d" % serie.id)

    if season.serie != serie:
        return redirect("serie_info", "%06d" % serie.id)

    episodes = season.episodes.all()
    EpisodeFormSet = modelformset_factory(Episode, form=EpisodeForm, extra=0, can_order=True, can_delete=True)

    if request.method == "POST":
        formset = EpisodeFormSet(request.POST, queryset=episodes)

        if formset.is_valid():
            for form in formset.ordered_forms:
                form.instance._order = form.cleaned_data['ORDER'] - 1
            formset.save()
            return redirect("serie_info", "%06d" % serie.id)
    else:
        formset = EpisodeFormSet(queryset=episodes)

    url = reverse("season_edit_episodes", kwargs={"id": "%06d" % serie.id, "sid": "%06d" % season.id})
    title = season.name + " - " + _("Edit episodes")
    return render(request, "movies/serie_edit_data.html", {"serie": serie, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@require_safe
def sagasList(request):
    sagas = Saga.objects.all()
    return render(request, "movies/sagas_list.html", {"sagas": sagas})


@login_required
@permission_required("movies.add_saga")
def sagaCreate(request):
    if request.method == "POST":
        form = SagaForm(request.POST)

        if form.is_valid():
            saga = form.save()
            return redirect("saga_info", "%06d" % saga.id)
    else:
        form = SagaForm()

    return render(request, "movies/saga_create.html", {"form": form})


@login_required
@require_safe
def sagaInfo(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("saga_list")

    movies = []
    for sagaMovie in saga.movies.all():
        if request.user.is_authenticated:
            sagaMovie.viewed = (sagaMovie.movie.views.filter(user=request.user).count() != 0)
        else:
            sagaMovie.viewed = False
        movies.append(sagaMovie)

    return render(request, "movies/saga_info.html", {"saga": saga, "movies": movies})


@login_required
@permission_required("movies.change_saga")
def sagaEdit(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("sagas_list")

    if request.method == "POST":
        form = SagaForm(request.POST, instance=saga)

        if form.is_valid():
            saga = form.save()
            return redirect("saga_info", "%06d" % saga.id)
    else:
        form = SagaForm(instance=saga)

    return render(request, "movies/saga_edit.html", {"saga": saga, "form": form})


@login_required
@permission_required("movies.delete_saga")
def sagaDelete(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("sagas_list")

    if request.method == "POST":
        if "delete" in request.POST:
            saga.delete()
        return redirect("sagas_list")

    return render(request, "movies/saga_delete.html", {"saga": saga})


@login_required
@permission_required("movies.add_sagamovie")
def sagaAddMovie(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("sagas_list")

    if request.method == "POST":
        form = SagaMovieForm(request.POST)

        if form.is_valid():
            sagaMovie = form.save(commit=False)
            if sagaMovie:
                sagaMovie.saga = saga
                sagaMovie.save()
            return redirect("saga_info", "%06d" % saga.id)
    else:
        form = SagaMovieForm()

    return render(request, "movies/saga_add_movie.html", {"saga": saga, "form": form})


@login_required
@permission_required("movies.change_sagamovie")
def sagaEditMovies(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("sagas_list")

    movies = saga.movies.all()

    if movies.count() == 0:
        extra = 1
    else:
        extra = 0

    SagaMovieFormSet = modelformset_factory(SagaMovie, form=SagaMovieForm, extra=extra, can_order=True, can_delete=True)

    if request.method == "POST":
        formset = SagaMovieFormSet(request.POST, queryset=movies)

        if formset.is_valid():
            for form in formset.ordered_forms:
                form.instance._order = form.cleaned_data["ORDER"] - 1
            instances = formset.save(commit=False)
            for instance in instances:
                wantedOrder = instance._order
                instance.saga = saga
                instance.save()
                if instance._order != wantedOrder:  # new instance have automatic order
                    instance._order = wantedOrder
                    instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("saga_info", "%06d" % saga.id)
    else:
        formset = SagaMovieFormSet(queryset=movies)

    return render(request, "movies/saga_edit_movies.html", {"saga": saga, "formset": formset})


@login_required
@require_safe
def moviesYear(request, year):
    try:
        year = int(year)
    except:
        return redirect("movies_list")

    movies = []
    episodes = []

    movies_cursor = Movie.objects.filter(Q(ismovie=True) | Q(episode__isnull=True)).filter(date__startswith=str(year))
    for movie in movies_cursor.all():
        if request.user.is_authenticated:
            movie.viewed = (movie.views.filter(user=request.user).count() != 0)
        else:
            movie.viewed = False
        movies.append(movie)

    episodes_cursor = Movie.objects.filter(episode__isnull=False).filter(date__startswith=str(year))
    for movie in episodes_cursor.all():
        if request.user.is_authenticated:
            movie.viewed = (movie.views.filter(user=request.user).count() != 0)
        else:
            movie.viewed = False
        episodes.append(movie)

    return render(request, "movies/year.html", {"year": year, "movies": movies, "episodes": episodes})


@login_required
@require_safe
def listList(request):
    lists = List.objects.filter(user=request.user)
    return render(request, "movies/lists_list.html", {"lists": lists})


@login_required
@permission_required("movies.add_list")
def listCreate(request):
    if request.method == "POST":
        form = ListForm(request.POST)

        if form.is_valid():
            list = form.save(commit=False)
            if list:
                list.user = request.user
                list.save()
                return redirect("list_info", "%06d" % list.id)
    else:
        form = ListForm()

    return render(request, "movies/list_create.html", {"form": form})


def _list_get_movies(list, user):
    movies = set()
    firstCriterion = True

    for criterion in list.criteria.all():
        # Titre
        if criterion.type == 0x0101:
            if criterion.condition == 1:
                critMovies = set(Movie.objects.filter(Q(title__iexact=criterion.value1) | Q(title__iexact=criterion.value1)))
            elif criterion.condition == 2:
                critMovies = set(Movie.objects.all().exclude(Q(title__iexact=criterion.value1) | Q(title__iexact=criterion.value1)))
            elif criterion.condition == 3:
                critMovies = set(Movie.objects.filter(Q(title__icontains=criterion.value1) | Q(title_vo__icontains=criterion.value1)))
            elif criterion.condition == 4:
                critMovies = set(Movie.objects.all().exclude(Q(title__icontains=criterion.value1) | Q(title_vo__icontains=criterion.value1)))
            elif criterion.condition == 5:
                critMovies = set(Movie.objects.filter(Q(title__istartswith=criterion.value1) | Q(title_vo__istartswith=criterion.value1)))
            elif criterion.condition == 6:
                critMovies = set(Movie.objects.filter(Q(title__iendswith=criterion.value1) | Q(title_vo__iendswith=criterion.value1)))
            else:
                print("Error")
                critMovies = set()
        # Résumé
        elif criterion.type == 0x0102:
            if criterion.condition == 1:
                critMovies = set(Movie.objects.filter(summary__iexact=criterion.value1))
            elif criterion.condition == 2:
                critMovies = set(Movie.objects.all().exclude(summary__iexact=criterion.value1))
            elif criterion.condition == 3:
                critMovies = set(Movie.objects.filter(summary__icontains=criterion.value1))
            elif criterion.condition == 4:
                critMovies = set(Movie.objects.all().exclude(summary__icontains=criterion.value1))
            elif criterion.condition == 5:
                critMovies = set(Movie.objects.filter(summary__istartswith=criterion.value1))
            elif criterion.condition == 6:
                critMovies = set(Movie.objects.filter(summary__iendswith=criterion.value1))
            else:
                print("Error")
                critMovies = set()
        # Informations
        elif criterion.type == 0x0103:
            if criterion.condition == 1:
                critMovies = set(Movie.objects.filter(info__iexact=criterion.value1))
            elif criterion.condition == 2:
                critMovies = set(Movie.objects.all().exclude(info__iexact=criterion.value1))
            elif criterion.condition == 3:
                critMovies = set(Movie.objects.filter(info__icontains=criterion.value1))
            elif criterion.condition == 4:
                critMovies = set(Movie.objects.all().exclude(info__icontains=criterion.value1))
            elif criterion.condition == 5:
                critMovies = set(Movie.objects.filter(info__istartswith=criterion.value1))
            elif criterion.condition == 6:
                critMovies = set(Movie.objects.filter(info__iendswith=criterion.value1))
            else:
                print("Error")
                critMovies = set()
        # Tag
        elif criterion.type == 0x0104:
            if criterion.condition == 1:
                tags = Tag.objects.filter(name__iexact=criterion.value1)
                critMovies = set(Movie.objects.filter(tags__in=MovieTag.objects.filter(tag__in=tags)))
            elif criterion.condition == 2:
                tags = Tag.objects.filter(name__iexact=criterion.value1)
                critMovies = Movie.objects.all().exclude(tags__in=MovieTag.objects.filter(tag__in=tags))
            elif criterion.condition == 5:
                tags = Tag.objects.filter(name__istartswith=criterion.value1)
                critMovies = set(Movie.objects.filter(tags__in=MovieTag.objects.filter(tag__in=tags)))
            else:
                print("Error")
                critMovies = set()
        # List
        elif criterion.type == 0x0105:
            if criterion.condition == 1:
                lists = List.objects.filter(user=user, name__iexact=criterion.value1)
                critMovies = set()
                for slist in lists:
                    critMovies = critMovies.union(_list_get_movies(slist, user))
            elif criterion.condition == 2:
                lists = List.objects.filter(user=user, name__iexact=criterion.value1)
                critMovies = set()
                for slist in lists:
                    critMovies = critMovies.union(_list_get_movies(slist, user))
                critMovies = set(Movie.objects.all().exclude(id__in=critMovies))
            else:
                print("Error")
                critMovies = set()
        # Duration
        elif criterion.type == 0x0201:
            if criterion.condition == 1:
                critMovies = set(Movie.objects.filter(duration=int(criterion.value1)))
            elif criterion.condition == 2:
                critMovies = set(Movie.objects.all().exclude(duration=int(criterion.value1)))
            elif criterion.condition == 7:
                critMovies = set(Movie.objects.filter(duration__lt=int(criterion.value1)))
            elif criterion.condition == 8:
                critMovies = set(Movie.objects.filter(duration__gt=int(criterion.value1)))
            elif criterion.condition == 9:
                critMovies = set(Movie.objects.filter(duration__gt=int(criterion.value1)).filter(duration__lt=int(criterion.value2)))
            else:
                print("Error")
                critMovies = set()
        # Views (TODO)
        elif criterion.type == 0x0202:
            if criterion.condition == 1:
                critMovies = set()
            elif criterion.condition == 2:
                critMovies = set()
            elif criterion.condition == 7:
                critMovies = set()
            elif criterion.condition == 8:
                critMovies = set()
            elif criterion.condition == 9:
                critMovies = set()
            else:
                print("Error")
                critMovies = set()
        # Year
        elif criterion.type == 0x0203:
            if criterion.condition == 1:
                critMovies = set(Movie.objects.filter(date__startswith=criterion.value1))
            elif criterion.condition == 2:
                critMovies = set(Movie.objects.all().exclude(date__startswith=criterion.value1))
            elif criterion.condition == 7:
                critMovies = set(Movie.objects.filter(date__lt=criterion.value1))
            elif criterion.condition == 8:
                critMovies = set(Movie.objects.filter(date__gt=criterion.value1))
            elif criterion.condition == 9:
                critMovies = set(Movie.objects.filter(date__gt=criterion.value1).filter(date__lt=criterion.value2))
            else:
                print("Error")
                critMovies = set()
        # Episode
        elif criterion.type == 0x0301:
            if criterion.condition == 1:
                critMovies = set(Movie.objects.filter(episode__isnull=False))
            elif criterion.condition == 2:
                critMovies = set(Movie.objects.filter(episode__isnull=True))
            else:
                print("Error")
                critMovies = set()
        # Viewed
        elif criterion.type == 0x0302:
            if criterion.condition == 1:
                critMovies = set(Movie.objects.filter(views__in=MovieView.objects.filter(user=user)))
            elif criterion.condition == 2:
                critMovies = set(Movie.objects.all().exclude(views__in=MovieView.objects.filter(user=user)))
            else:
                print("Error")
                critMovies = set()
        # Date (TODO)
        elif criterion.type == 0x0401:
            if criterion.condition == 1:
                critMovies = set()
            elif criterion.condition == 2:
                critMovies = set()
            elif criterion.condition == 7:
                critMovies = set()
            elif criterion.condition == 8:
                critMovies = set()
            elif criterion.condition == 9:
                critMovies = set()
            else:
                print("Error")
                critMovies = set()
        else:
            print("Error")
            critMovies = set()

        if list.union:
            movies = movies.union(critMovies)
        else:
            if firstCriterion:
                movies = critMovies
            else:
                movies = movies.intersection(critMovies)
        firstCriterion = False

    return movies


@login_required
@require_safe
def listInfo(request, id):
    try:
        list = List.objects.get(id=id)
    except List.DoesNotExist:
        return redirect("lists_list")

    movies = _list_get_movies(list, request.user)

    for movie in movies:
        movie.viewed = (movie.views.filter(user=request.user).count() != 0)

    return render(request, "movies/list_info.html", {"list": list, "movies": movies})


@login_required
@permission_required("movies.change_list")
def listEdit(request, id):
    try:
        list = List.objects.get(id=id)
    except List.DoesNotExist:
        return redirect("lists_list")

    criteria = list.criteria.all()

    if criteria.count() == 0:
        extra = 1
    else:
        extra = 0

    ListCriterionFormSet = modelformset_factory(ListCriterion, form=ListCriterionForm, extra=extra, can_order=False, can_delete=True)

    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        formset = ListCriterionFormSet(request.POST, queryset=criteria)

        if form.is_valid() and formset.is_valid():
            list = form.save()
            instances = formset.save(commit=False)
            for instance in instances:
                instance.list = list
                instance.save()
            for sform in formset.deleted_forms:
                sform.instance.delete()
            return redirect("list_info", "%06d" % list.id)
    else:
        form = ListForm(instance=list)
        formset = ListCriterionFormSet(queryset=criteria)

    return render(request, "movies/list_edit.html", {"list": list, "form": form, "formset": formset})


@login_required
@permission_required("movies.delete_list")
def listDelete(request, id):
    try:
        list = List.objects.get(id=id)
    except List.DoesNotExist:
        return redirect("lists_list")

    if request.method == "POST":
        if "delete" in request.POST:
            list.delete()
        return redirect("lists_list")

    return render(request, "movies/list_delete.html", {"list": list})


def _movie_import_actors(movie, actors):
    for actor in actors:
        movieName = None
        person = None

        try:
            if actor["imdb"]:
                movieName = MovieName.objects.get(imdb=actor["imdb"])
            else:
                raise Exception()
        except:
            persons = Person.objects.filter(name=actor["name"])
            if persons:
                personUsable = None
                for person in persons:
                    try:
                        movieName = MovieName.objects.get(person=person)
                        if movieName.imdb:
                            movieName = None
                            continue
                        else:
                            personUsable = person
                    except:
                        personUsable = person
                if not movieName:
                    if personUsable:
                        person = personUsable
                    else:
                        person = None
            if not person:
                person = Person.objects.create(name=actor["name"])

            if actor["imdb"]:
                personHasAttribute = False
                for attribute in person.attributes.all():
                    if attribute.name == "imdb-id" and attribute.value == str(actor["imdb"]):
                        personHasAttribute = True
                        break
                if not personHasAttribute:
                    PersonAttribute.objects.create(person=person, name="imdb-id", value=actor["imdb"])

            movieNameGet = MovieName.objects.get_or_create(person=person)[0]
            if not movieNameGet.imdb:
                movieName = movieNameGet
                if actor["imdb"]:
                    movieName.imdb = actor["imdb"]
                    movieName.save()

                    PersonLink.objects.create(person=person, name="IMDb",
                                              uri="https://www.imdb.com/name/nm%07d/" % actor["imdb"],
                                              lang=Language.objects.get(code="eng"))
            elif movieNameGet.imdb == actor["imdb"]:
                movieName = movieNameGet
            else:
                print("Import actors / IMDb diff : %s / %s" % (str(movieNameGet.imdb), str(actor["imdb"])))
                movieName = movieNameGet

            #if not movieName:
            #    movieName = MovieName.objects.create(person=person, imdb=actor["imdb"])

        if isinstance(actor["info"], list):
            actor["info"] = ", ".join(actor["info"])

        MovieActor.objects.create(movie=movie, person=movieName, name=actor["as"], role=actor["role"], info=actor["info"], credit=actor["credited"], visible=actor["visible"])


@login_required
def editArtevideo(request, id):
    try:
        video = ArteVideo.objects.get(id=id)
    except ArteVideo.DoesNotExist:
        return redirect("movies_home")

    if request.method == "POST":
        form = ArteVideoForm(request.POST)

        if form.is_valid():
            video.movie = Movie.objects.get(id=form.cleaned_data["movie_id"])
        else:
            video.movie = None
        video.save()
        return redirect("movies_home")
    else:
        form = ArteVideoForm()

    return render(request, "movies/edit_arte_video.html", {"video": video, "form": form})


@require_safe
@login_required
def export_tag_movies(request, tag_id:int):
    persons = [
        "Al Pacino",
        "Alain Delon",
        "Albert Dupontel",
        "André Dussollier",
        "Annie Girardot",
        "Anémone",
        "Bourvil",
        "Brad Pitt",
        "Bruce Willis",
        "Christian Bale",
        "Christian Clavier",
        "Claude Rich",
        "Coluche",
        "Cécile de France",
        "Danielle Darrieux",
        "Denis Podalydès",
        "Dominique Lavanant",
        "Eddy Mitchell",
        "Fabrice Luchini",
        "Fernandel",
        "Francis Blanche",
        "Gad Elmaleh",
        "George Clooney",
        "Gérard Darmon",
        "Gérard Depardieu",
        "Gérard Jugnot",
        "Gérard Lanvin",
        "Harrison Ford",
        "Isabelle Nanty",
        "Jack Nicholson",
        "Jackie Chan",
        "Jacques Brel",
        "Jacques Villeret",
        "Jane Birkin",
        "Jean Carmet",
        "Jean Gabin",
        "Jean Marais",
        "Jean Reno",
        "Jean Rochefort",
        "Jean-Louis Trintignant",
        "Jean-Paul Belmondo",
        "Jean-Pierre Marielle",
        "Johnny Depp",
        "Josiane Balasko",
        "Julie Depardieu",
        "Leonardo DiCaprio",
        "Lino Ventura",
        "Louis de Funès",
        "Ludivine Sagnier",
        "Marion Cotillard",
        "Mathieu Amalric",
        "Mel Gibson",
        "Meryl Streep",
        "Michael Lonsdale",
        "Michel Blanc",
        "Michel Galabru",
        "Michel Piccoli",
        "Michel Serrault",
        "Morgan Freeman",
        "Natalie Portman",
        "Nicole Kidman",
        "Patrick Bruel",
        "Patrick Dewaere",
        "Philippe Noiret",
        "Pierre Tornade",
        "Robert De Niro",
        "Robin Williams",
        "Romane Bohringer",
        "Romy Schneider",
        "Sandrine Kiberlain",
        "Simone Signoret",
        "Sophie Marceau",
        "Thierry Lhermitte",
        "Tom Cruise",
        "Valérie Lemercier",
        "Vincent Cassel",
        "Vincent Lindon",
        "Yvan Attal",
        "Yves Montand",
        "Élie Semoun",
        #"Pierre Richard",
    ]
    persons_dict = {
        223: "Pierre Richard",
    }

    for person in Person.objects.filter(name__in=persons):
        persons_dict[person.id] = person.name

    actors_qs = MovieActor.objects.select_related("person", "person__person")
    crew_qs = MovieCrewMember.objects.filter(function="Réalisation").select_related("person", "person__person")
    movietags = MovieTag.objects.filter(tag=tag_id, movie__ismovie=True).select_related("movie", "movie__episode").prefetch_related(Prefetch("movie__crew_members", queryset=crew_qs), Prefetch("movie__actors", queryset=actors_qs), "movie__sagas", "movie__countries").order_by("movie__date")
    movies = {}

    for movietag in movietags:
        if movietag.movie.id in movies:
            movies[movietag.movie.id]["info"] += ", " + movietag.info
        else:
            actors = []
            for actor in movietag.movie.actors.all():
                if actor.person.person is not None and actor.person.person.id in persons_dict:
                    if persons_dict[actor.person.person.id] not in actors:
                        actors.append(persons_dict[actor.person.person.id])

            movies[movietag.movie.id] = {
                "movie": movietag.movie,
                "info": movietag.info,
                "realisation": movietag.movie.crew_members.all(),
                "acteurs": actors,
                "sagas": movietag.movie.sagas.all(),
            }

    return render(request, "movies/export_tag_movies.html", {"movies": movies})
