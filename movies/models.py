import os
import uuid

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _, pgettext

from base.models import Attribute, Person, Group, Tag, Language, Country, Link
from books.models import Work
from games.models import Game


def random_filename(instance, filename):
    ext = os.path.splitext(filename)[1].lower()
    if ext == ".jpeg":
        ext = ".jpg"
    filename = "%s%s" % (uuid.uuid4(), ext)
    return os.path.join("movies", filename)


class MovieName(models.Model):
    person = models.OneToOneField(Person, related_name="moviename", on_delete=models.SET_NULL, null=True, blank=True)
    group = models.OneToOneField(Group, related_name="moviename", on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False, null=False)
    imdb = models.IntegerField(verbose_name=_("IMDb number"), null=True, blank=True, unique=True)

    def save(self, *args, **kwargs):
        if (self.person is not None and self.group is not None):
            raise ValueError("MovieName can't have both person and group.")
        super(MovieName, self).save(*args, **kwargs)

    def __str__(self):
        if self.person is not None:
            return self.person.name
        elif self.group is not None:
            return self.group.name
        else:
            return self.name


class Movie(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=150, blank=False)
    title_vo = models.CharField(verbose_name=_("Original title"), max_length=150, blank=True)
    ismovie = models.BooleanField(verbose_name=_("Movie"), help_text=_("If this value is false, it's an episode of a serie, else it's a movie."), default=True)
    #date = models.DateField(verbose_name=_("Release date"), null=True, blank=True)
    date = models.CharField(verbose_name=_("Release date"), max_length=10, null=True, blank=True)
    imdb = models.IntegerField(verbose_name=_("IMDb number"), null=True, blank=True)
    duration = models.IntegerField(verbose_name=_("Duration"), null=True, blank=True)
    summary = models.TextField(verbose_name=_("Summary"), blank=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    lang = models.ForeignKey(Language, verbose_name=pgettext("language", "Language"), on_delete=models.SET_NULL, null=True, blank=True)
    countries = models.ManyToManyField(Country, verbose_name=_("Countries"), blank=True)
    image = models.ImageField(verbose_name=_("Image"), upload_to=random_filename, null=True, blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), null=True, blank=True, default=timezone.now)
    modification = models.DateTimeField(verbose_name=_("Modification date"), null=True, blank=True, default=timezone.now)
    #creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    #modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Movie")
        verbose_name_plural = _("Movies")
        ordering = ["title"]


class MovieAttribute(Attribute):
    movie = models.ForeignKey(Movie, related_name="attributes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Movie") + " - " + _("Attribute")
        verbose_name_plural = _("Movies") + " - " + _("Attributes")
        #ordering = ["name"]
        db_table = "movies_" + "movie_attributes"


class MovieLink(Link):
    movie = models.ForeignKey(Movie, related_name="links", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Movie") + " - " + _("Link")
        verbose_name_plural = _("Movies") + " - " + _("Links")
        #ordering = ["name"]
        db_table = "movies_" + "movie_links"


class MovieReference(models.Model):
    movie = models.ForeignKey(Movie, verbose_name=_("Movie"), related_name="refs", on_delete=models.CASCADE)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)
    ref_movie = models.ForeignKey(Movie, verbose_name=_("Referenced movie"), related_name="ref_by", on_delete=models.SET_NULL, null=True, blank=True)
    ref_serie = models.ForeignKey("Serie", verbose_name=_("Referenced serie"), related_name="ref_by", on_delete=models.SET_NULL, null=True, blank=True)
    ref_person = models.ForeignKey(Person, verbose_name=_("Referenced person"), related_name="ref_by", on_delete=models.SET_NULL, null=True, blank=True)
    ref_book = models.ForeignKey(Work, verbose_name=_("Referenced book"), related_name="ref_by", on_delete=models.SET_NULL, null=True, blank=True)
    ref_game = models.ForeignKey(Game, verbose_name=_("Referenced game"), related_name="ref_by", on_delete=models.SET_NULL, null=True, blank=True)

    def save(self, *args, **kwargs):
        numNotNone = 0
        if self.ref_movie is not None:
            numNotNone += 1
        if self.ref_serie is not None:
            numNotNone += 1
        if self.ref_person is not None:
            numNotNone += 1
        if self.ref_book is not None:
            numNotNone += 1
        if self.ref_game is not None:
            numNotNone += 1
        if numNotNone > 1:
            raise ValueError("Movie, serie, person, book or game can't be defined at the same time for a movie reference.")

        super(MovieReference, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("Movie") + " - " + _("Reference")
        verbose_name_plural = _("Movies") + " - " + _("References")
        ordering = ["ref_movie", "ref_serie", "ref_person", "ref_book", "ref_game"]
        db_table = "movies_" + "movie_references"


class MovieTag(models.Model):
    movie = models.ForeignKey(Movie, verbose_name=_("Movie"), related_name="tags", on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, verbose_name=_("Tag"), related_name="movies", on_delete=models.CASCADE)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)

    class Meta:
        verbose_name = _("Movie") + " - " + _("Tag")
        verbose_name_plural = _("Movies") + " - " + _("Tags")
        ordering = ["tag__name", "info"]
        db_table = "movies_" + "movie_tags"


class MovieActor(models.Model):
    movie = models.ForeignKey(Movie, verbose_name=_("Movie"), related_name="actors", on_delete=models.CASCADE)
    person = models.ForeignKey(MovieName, verbose_name=_("Actor"), related_name="movies_as_actor", on_delete=models.CASCADE)
    name = models.CharField(verbose_name=_("As"), max_length=100, blank=True)
    role = models.CharField(verbose_name=_("Role"), max_length=250, blank=True)
    lang = models.ForeignKey(Language, verbose_name=pgettext("language", "Language"), on_delete=models.SET_NULL, null=True, blank=True)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)
    credit = models.BooleanField(verbose_name=_("Credited"), default=True)
    visible = models.BooleanField(verbose_name=_("Visible"), default=True)

    class Meta:
        verbose_name = _("Movie") + " - " + _("Actor")
        verbose_name_plural = _("Movies") + " - " + _("Actors")
        order_with_respect_to = "movie"
        db_table = "movies_" + "movie_actors"


class MovieActorDubbing(models.Model):
    movie = models.ForeignKey(Movie, verbose_name=_("Movie"), related_name="dubbing_actors", on_delete=models.CASCADE)
    person = models.ForeignKey(MovieName, verbose_name=_("Dubbing actor"), related_name="dubbing_actor", on_delete=models.CASCADE)  # TODO: utiliser Person à la place de MovieName
    actor = models.ForeignKey(MovieName, verbose_name=_("Actor"), related_name="dubbed_by", on_delete=models.CASCADE)
    lang = models.ForeignKey(Language, verbose_name=pgettext("language", "Language"), on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        verbose_name = _("Movie") + " - " + _("Dubbing actor")
        verbose_name_plural = _("Movies") + " - " + _("Dubbing actors")
        db_table = "movies_" + "movie_actorsdubbing"


class MovieCrewMember(models.Model):
    movie = models.ForeignKey(Movie, verbose_name=_("Movie"), related_name="crew_members", on_delete=models.CASCADE)
    person = models.ForeignKey(MovieName, verbose_name=_("Person"), related_name="movies_as_crew_member", on_delete=models.CASCADE)
    name = models.CharField(verbose_name=_("As"), max_length=100, blank=True)
    function = models.CharField(verbose_name=_("Function"), max_length=100, blank=True)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)
    credit = models.BooleanField(verbose_name=_("Credited"), default=True)

    class Meta:
        verbose_name = _("Movie") + " - " + _("Crew member")
        verbose_name_plural = _("Movies") + " - " + _("Crew members")
        ordering = ["movie", "function", "person"]
        db_table = "movies_" + "movie_crewmembers"


class MovieView(models.Model):
    movie = models.ForeignKey(Movie, verbose_name=_("Movie"), related_name="views", on_delete=models.CASCADE)
    #date = models.DateField(verbose_name=_("Date"), null=True, blank=True)
    date = models.CharField(verbose_name=_("Date"), max_length=10, null=True, blank=True)
    lang = models.ForeignKey(Language, verbose_name=pgettext("language", "Language"), related_name="views_as_lang", on_delete=models.SET_NULL, null=True, blank=True)
    sublang = models.ForeignKey(Language, verbose_name=_("Subtitles"), related_name="views_as_sublang", on_delete=models.SET_NULL, null=True, blank=True)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)
    user = models.ForeignKey(User, verbose_name=_("User"), related_name="views", on_delete=models.CASCADE)

    def __str__(self):
        return "%s, %s by %s" % (str(self.movie), str(self.date), str(self.user))

    class Meta:
        verbose_name = _("View")
        verbose_name_plural = _("Views")
        ordering = ["movie", "-date"]
        db_table = "movies_" + "movie_views"


class Saga(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=100, blank=False)
    title_vo = models.CharField(verbose_name=_("Original title"), max_length=100, blank=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = pgettext("The saga", "Saga")
        verbose_name_plural = _("Sagas")
        ordering = ["title"]


class SagaMovie(models.Model):
    saga = models.ForeignKey(Saga, related_name="movies", on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, related_name="sagas", verbose_name=_("Movie"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = pgettext("The saga", "Saga") + " - " + _("Movie")
        verbose_name_plural = _("Sagas") + " - " + _("Movies")
        order_with_respect_to = "saga"
        unique_together = ("saga", "movie")
        db_table = "movies_" + "saga_movies"


class Serie(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=100, blank=False)
    title_vo = models.CharField(verbose_name=_("Original title"), max_length=100, blank=True)
    imdb = models.IntegerField(verbose_name=_("IMDb number"), null=True, blank=True)
    duration = models.IntegerField(verbose_name=_("Episode duration"), null=True, blank=True)
    summary = models.TextField(verbose_name=_("Summary"), blank=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    lang = models.ForeignKey(Language, verbose_name=pgettext("The language", "Language"), on_delete=models.SET_NULL, null=True, blank=True)
    countries = models.ManyToManyField(Country, verbose_name=_("Countries"), blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return self.title

    @property
    def episodes_count(self):
        return Episode.objects.filter(season__serie=self).count()

    @property
    def last_episode(self):
        return Episode.objects.filter(season__serie=self).select_related("movie").latest("movie__date").movie

    class Meta:
        verbose_name = _("Serie")
        verbose_name_plural = _("Series")
        ordering = ["title"]


class SerieAttribute(Attribute):
    serie = models.ForeignKey(Serie, related_name="attributes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Serie") + " - " + _("Attribute")
        verbose_name_plural = _("Series") + " - " + _("Attributes")
        #ordering = ["name"]
        db_table = "movies_" + "serie_attributes"


class SerieLink(Link):
    serie = models.ForeignKey(Serie, related_name="links", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Serie") + " - " + _("Link")
        verbose_name_plural = _("Series") + " - " + _("Links")
        #ordering = ["name"]
        db_table = "movies_" + "serie_links"


class SerieCrewMember(models.Model):
    serie = models.ForeignKey(Serie, verbose_name=_("Serie"), related_name="crew_members", on_delete=models.CASCADE)
    person = models.ForeignKey(MovieName, verbose_name=_("Person"), related_name="series_as_crew_member", on_delete=models.CASCADE)
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=True)
    function = models.CharField(verbose_name=_("Function"), max_length=100, blank=True)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)

    class Meta:
        verbose_name = _("Serie") + " - " + _("Crew member")
        verbose_name_plural = _("Series") + " - " + _("Crew members")
        db_table = "movies_" + "serie_crewmembers"


class SerieTag(models.Model):
    serie = models.ForeignKey(Serie, related_name="tags", on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, related_name="series", on_delete=models.CASCADE)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)

    class Meta:
        verbose_name = _("Serie") + " - " + _("Tag")
        verbose_name_plural = _("Series") + " - " + _("Tags")
        ordering = ["tag__name", "info"]
        db_table = "movies_" + "serie_tags"


class Season(models.Model):
    serie = models.ForeignKey(Serie, verbose_name=_("Serie"), related_name="seasons", on_delete=models.CASCADE)
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False)
    info = models.TextField(verbose_name=_("Information"), blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Season")
        verbose_name_plural = _("Seasons")
        order_with_respect_to = "serie"


class Episode(models.Model):
    season = models.ForeignKey(Season, related_name="episodes", on_delete=models.CASCADE)
    movie = models.OneToOneField(Movie, related_name="episode", on_delete=models.CASCADE)
    #movie = models.ForeignKey(Movie, related_name="episodes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Episode")
        verbose_name_plural = _("Episodes")
        order_with_respect_to = "season"


class List(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=200, blank=False)
    union = models.BooleanField(verbose_name=_("Match any conditions"), default=True)
    user = models.ForeignKey(User, verbose_name=_("User"), related_name="lists", on_delete=models.CASCADE)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return str(self.name) + _(" by ") + str(self.user)

    class Meta:
        verbose_name = _("List")
        verbose_name_plural = _("Lists")
        db_table = "lists"


class ListCriterion(models.Model):
    list = models.ForeignKey(List, verbose_name=_("List"), related_name="criteria", on_delete=models.CASCADE)
    #parent = models.ForeignKey("ListCriterion", verbose_name=_("Parent criterion"), related_name="children", on_delete=models.SET_NULL, null=True, blank=True)
    #type = models.IntegerField(verbose_name=_("Criterion type"), choices=[(0, "Invalid"), (1, "Union"), (2, "Intersection"), (0x101, _("Title")), (0x102, _("Summary")), (0x103, _("Information")), (0x104, _("Tag")), (0x201, _("Seen")), (0x301, _("Duration")), (0x302, _("Views"))], default=0)
    type = models.IntegerField(verbose_name=_("Criterion type"), default=0,
                               choices=[(0x0000, "Invalid"),
                                        (0x0101, _("Title")),
                                        (0x0102, _("Summary")),
                                        (0x0103, _("Information")),
                                        (0x0104, _("Tag")),
                                        (0x0105, _("List")),
                                        (0x0201, _("Duration")),
                                        #(0x0202, _("Views")),
                                        (0x0203, _("Year")),
                                        (0x0301, _("Episode")),
                                        (0x0302, _("Viewed")),
                                        #(0x0401, _("Date")),
                                       ])
    condition = models.IntegerField(verbose_name=_("Condition"), choices=[(0, "Invalid"), (1, _("is")), (2, _("is not")), (3, _("contains")), (4, _("does not contain")), (5, _("starts with")), (6, _("ends with")), (7, _("is less than")), (8, _("is greater than")), (9, _("is between"))], default=0)
    value1 = models.CharField(verbose_name=_("First value"), max_length=100, null=True, blank=True)
    value2 = models.CharField(verbose_name=_("Second value"), max_length=100, null=True, blank=True)

    # Types : actor, country, language, realisator
    # TODO: sorting

    def __str__(self):
        return str(self.list) + " - " + _("Criterion #") + str(self.id)

    class Meta:
        verbose_name = _("List criterion")
        verbose_name_plural = _("List criteria")
        db_table = "list_criteria"


class ArteVideo(models.Model):
    program_id = models.CharField(verbose_name=_("Program ID"), max_length=50, blank=False)
    title = models.CharField(verbose_name=_("Title"), max_length=200, blank=False)
    subtitle = models.CharField(verbose_name=_("Subtitle"), max_length=200, null=True, blank=True)
    description = models.TextField(verbose_name=_("Description"), null=True, blank=True)
    url = models.CharField(verbose_name=_("URL"), max_length=200, blank=False)
    duration = models.IntegerField(verbose_name=_("Duration"), null=True, blank=True)
    date_start = models.DateTimeField(verbose_name=_("Start date"), null=True, blank=True)
    date_end = models.DateTimeField(verbose_name=_("End date"), null=True, blank=True)
    movie = models.ForeignKey(Movie, related_name="arte", verbose_name=_("Movie"), on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _("Arte video")
        verbose_name_plural = _("Arte videos")
        db_table = "arte"
