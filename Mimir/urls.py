from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.urls import path, include, re_path

from base import views


urlpatterns = [
    re_path(r"^admin/", admin.site.urls),
    re_path(r"^auth/", include('django.contrib.auth.urls')),

    re_path(r"^$", views.home, name="home"),

    # Applications
    re_path(r"^base/", include('base.urls')),
    re_path(r"^movies/", include('movies.urls')),
    re_path(r"^books/", include('books.urls')),
    re_path(r"^games/", include('games.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG and settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
