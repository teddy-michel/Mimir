import json
import os

from django.utils.translation import pgettext
from django.db.models import Prefetch

from Mimir.settings import BASE_DIR
from base.templatetags.mimir import get_order
from .models import SagaWork, EditionText


def _get_reading_texts(user, order):
    sagaQuerySet = SagaWork.objects.select_related("saga")
    editionTextQuerySet = EditionText.objects.select_related("text", "text__work", "text__work__genre", "text__lang", "text__translation", "text__translation__original_work", "text__translation__original_work__genre").prefetch_related("text__work__authors", "text__translation__translators", "text__translation__original_work__authors", Prefetch("text__work__sagas", queryset=sagaQuerySet), Prefetch("text__translation__original_work__sagas", queryset=sagaQuerySet))

    texts = {}
    for reading in user.readings.all().select_related("text", "text__work", "text__work__genre", "text__lang", "text__translation", "text__translation__original_work", "text__translation__original_work__genre", "edition").prefetch_related("text__work__authors", "text__translation__translators", "text__translation__original_work__authors", Prefetch("edition__texts", queryset=editionTextQuerySet), Prefetch("text__work__sagas", queryset=sagaQuerySet), Prefetch("text__translation__original_work__sagas", queryset=sagaQuerySet)):
        if reading.text:
            if reading.text.id not in texts:
                texts[reading.text.id] = {"text": reading.text, "titles": []}
            if hasattr(reading.text, "work") and reading.text.work:
                if reading.text.work.title not in texts[reading.text.id]["titles"]:
                    texts[reading.text.id]["titles"].append(reading.text.work.title)
            elif hasattr(reading.text, "translation") and reading.text.translation:
                if reading.text.translation.original_work.title not in texts[reading.text.id]["titles"]:
                    texts[reading.text.id]["titles"].append(reading.text.translation.original_work.title)
            if reading.edition:
                if reading.edition.title not in texts[reading.text.id]["titles"]:
                    texts[reading.text.id]["titles"].append(reading.edition.title)
        elif reading.edition:
            for edition_text in reading.edition.texts.all():
                if edition_text.text.id not in texts:
                    texts[edition_text.text.id] = {"text": edition_text.text, "titles": []}
                if edition_text.title not in texts[edition_text.text.id]["titles"]:
                    texts[edition_text.text.id]["titles"].append(edition_text.title)

    texts_json = []
    for text in texts.values():
        title = ""
        original_title = ""
        genre = None
        author_credited = ""
        authors = []
        translators = []
        year = text["text"].year
        original_year = text["text"].year
        language = text["text"].lang.iso1 if text["text"].lang else ""
        sagas = []

        if hasattr(text["text"], "work") and text["text"].work:
            original_title = text["text"].work.title_vo or text["text"].work.title
            author_credited = text["text"].work.author_credit
            genre = text["text"].work.genre
            for author in text["text"].work.authors.all():
                authors.append(author.name)
            for saga_work in text["text"].work.sagas.all():
                sagas.append({"title": saga_work.saga.title, "order": get_order(saga_work)})
        elif hasattr(text["text"], "translation") and text["text"].translation:
            original_title = text["text"].translation.original_work.title_vo or text["text"].translation.original_work.title
            author_credited = text["text"].translation.original_work.author_credit
            original_year = text["text"].translation.original_work.year
            genre = text["text"].translation.original_work.genre
            if not text["text"].year:
                year = text["text"].translation.original_work.year
            for author in text["text"].translation.original_work.authors.all():
                authors.append(author.name)
            for translator in text["text"].translation.translators.all():
                translators.append(translator.name)
            for saga_work in text["text"].translation.original_work.sagas.all():
                sagas.append({"title": saga_work.saga.title, "order": get_order(saga_work)})

        if len(text["titles"]) > 1 and original_title in text["titles"]:
            text["titles"].remove(original_title)

        data = {
            "original_title": original_title,
            "title": " / ".join(text["titles"]),
            "genre": str(genre) if genre else pgettext("Unknown genre", "Unknown"),
            "year": str(year) if year else "",
            "original_year": str(original_year) if original_year else "",
            "authors": authors,
            "language": language,
        }

        if author_credited:
            data["author_credited"] = author_credited

        if translators:
            data["translators"] = translators

        if sagas:
            data["sagas"] = sagas

        texts_json.append(data)

    if order == "title":
        texts_json = sorted(texts_json, key=lambda k: k["title"])
    elif order == "year":
        texts_json = sorted(texts_json, key=lambda k: k["year"])

    return texts_json

def _get_reading_works(user, order):
    sagaQuerySet = SagaWork.objects.select_related("saga")
    editionTextQuerySet = EditionText.objects.select_related("text", "text__work", "text__work__genre", "text__lang", "text__translation", "text__translation__original_work", "text__translation__original_work__lang").prefetch_related("text__work__authors", "text__translation__translators", "text__translation__original_work__authors", Prefetch("text__work__sagas", queryset=sagaQuerySet), Prefetch("text__translation__original_work__sagas", queryset=sagaQuerySet))

    works = []
    for reading in user.readings.all().select_related("text", "text__work", "text__work__genre", "text__lang", "text__translation", "text__translation__original_work", "text__translation__original_work__lang", "edition").prefetch_related("text__work__authors", "text__translation__original_work__authors", Prefetch("edition__texts", queryset=editionTextQuerySet), Prefetch("text__work__sagas", queryset=sagaQuerySet), Prefetch("text__translation__original_work__sagas", queryset=sagaQuerySet)):
        if reading.text:
            if hasattr(reading.text, "work") and reading.text.work:
                if reading.text.work not in works:
                    works.append(reading.text.work)
            elif hasattr(reading.text, "translation") and reading.text.translation:
                if reading.text.translation.original_work not in works:
                    works.append(reading.text.translation.original_work)
        elif reading.edition:
            for edition_text in reading.edition.texts.all():
                if hasattr(edition_text.text, "work") and edition_text.text.work:
                    if edition_text.text.work not in works:
                        works.append(edition_text.text.work)
                elif hasattr(edition_text.text, "translation") and edition_text.text.translation:
                    if edition_text.text.translation.original_work not in works:
                        works.append(edition_text.text.translation.original_work)

    works_json = []
    for work in works:
        sagas = []
        for saga_work in work.sagas.all():
            sagas.append({"title": saga_work.saga.title, "order": get_order(saga_work)})

        authors = []
        for author in work.authors.all():
            authors.append(author.name)

        genre = work.genre

        data = {
            "title": work.title,
            "original_title": work.title_vo,
            "genre": str(genre) if genre else pgettext("Unknown genre", "Unknown"),
            "year": str(work.year) if work.year else "",
            "authors": authors,
            "language": work.lang.iso1 if work.lang else "",
        }

        if work.author_credit:
            data["author_credited"] = work.author_credit

        if sagas:
            data["sagas"] = sagas

        works_json.append(data)

    if order == "title":
        works_json = sorted(works_json, key=lambda k: k["title"])
    elif order == "year":
        works_json = sorted(works_json, key=lambda k: k["year"])

    return works_json

def _get_reading_readings(user, order):
    editionTextQuerySet = EditionText.objects.select_related("text__work", "text__work__genre", "text__translation__original_work", "text__translation__original_work__genre").prefetch_related("text__work__authors", "text__translation__original_work__authors")

    readings = []
    for reading in user.readings.all().select_related("volume", "volume__edition", "edition", "text", "text__lang", "text__work", "text__work__genre", "text__translation", "text__translation__original_work", "text__translation__original_work__genre").prefetch_related("text__work__authors", "text__translation__original_work__authors", "text__translation__translators", Prefetch("edition__texts", queryset=editionTextQuerySet)):
        title = ""
        original_title = ""
        genre = None
        author_credited = ""
        year = None
        original_year = None
        authors = []
        language = ""
        translators = []

        if reading.volume:
            title = reading.volume.title
            year = reading.volume.edition.year
            original_year = reading.volume.edition.year
        elif reading.edition:
            title = reading.edition.title
            year = reading.edition.year
            original_year = reading.edition.year

        if reading.edition and reading.edition.texts.count() > 0:
            genre = pgettext("collection of books", "Collection")

        if reading.text:
            year = reading.text.year
            original_year = reading.text.year
            language = reading.text.lang.iso1 if reading.text.lang else ""
            if hasattr(reading.text, "work") and reading.text.work:
                if not title:
                    title = reading.text.work.title
                original_title = reading.text.work.title_vo or reading.text.work.title
                author_credited = reading.text.work.author_credit
                genre = reading.text.work.genre
                for author in reading.text.work.authors.all():
                    authors.append(author.name)
            elif hasattr(reading.text, "translation") and reading.text.translation:
                if not title:
                    title = reading.text.translation.original_work.title
                original_title = reading.text.translation.original_work.title_vo or reading.text.translation.original_work.title
                author_credited = reading.text.translation.original_work.author_credit
                genre = reading.text.translation.original_work.genre
                original_year = reading.text.translation.original_work.year
                if not reading.text.year:
                    year = reading.text.translation.original_work.year
                for author in reading.text.translation.original_work.authors.all():
                    authors.append(author.name)
                for translator in reading.text.translation.translators.all():
                    translators.append(translator.name)
        elif reading.edition:
            for author in reading.edition.authors:
                authors.append(author.name)

        data = {
            "title": title,
            "original_title": original_title,
            "genre": str(genre) if genre else pgettext("Unknown genre", "Unknown"),
            "year": str(year) if year else "",
            "original_year": str(original_year) if original_year else "",
            "authors": authors,
            "language": language,
            "translators": translators,
            "date_start": reading.date_start or "",
            "date_end": reading.date_end or "",
        }

        if author_credited:
            data["author_credited"] = author_credited

        readings.append(data)

    if order == "title":
        readings = sorted(readings, key=lambda k: k["title"])
    elif order == "year":
        readings = sorted(readings, key=lambda k: k["original_year"])
    elif order == "date":
        readings = sorted(readings, key=lambda k: k["date_end"])

    return readings

def _get_books(user, order):
    sagaQuerySet = SagaWork.objects.select_related("saga")
    editionTextQuerySet = EditionText.objects.select_related("text", "text__work", "text__work__genre", "text__lang", "text__translation", "text__translation__original_work", "text__translation__original_work__genre", "text__translation__original_work__lang").prefetch_related("text__work__authors", "text__translation__translators", "text__translation__original_work__authors", Prefetch("text__work__sagas", queryset=sagaQuerySet), Prefetch("text__translation__original_work__sagas", queryset=sagaQuerySet))

    books = []
    for book in user.books.all().select_related("volume", "volume__edition", "volume__edition__text", "volume__edition__text__work", "volume__edition__text__lang", "volume__edition__text__translation", "volume__edition__text__translation__original_work", "volume__edition__text__translation__original_work__lang").prefetch_related("volume__edition__text__work__authors", "volume__edition__text__translation__translators", "volume__edition__text__translation__original_work__authors", Prefetch("volume__edition__texts", queryset=editionTextQuerySet), Prefetch("volume__edition__text__work__sagas", queryset=sagaQuerySet), Prefetch("volume__edition__text__translation__original_work__sagas", queryset=sagaQuerySet)):
        genre = None
        author_credited = ""
        original_title = ""
        original_language = None
        sagas = {}

        if book.volume.edition.text:
            original_year = book.volume.edition.text.year
            if hasattr(book.volume.edition.text, "work") and book.volume.edition.text.work:
                original_title = book.volume.edition.text.work.title_vo or book.volume.edition.text.work.title
                author_credited = book.volume.edition.text.work.author_credit
                genre = book.volume.edition.text.work.genre
                original_language = book.volume.edition.text.lang

                for saga_work in book.volume.edition.text.work.sagas.all():
                    if saga_work.saga.title not in sagas:
                        sagas[saga_work.saga.title] = []
                    sagas[saga_work.saga.title].append(get_order(saga_work))
                    sagas[saga_work.saga.title].sort()
            elif hasattr(book.volume.edition.text, "translation") and book.volume.edition.text.translation:
                original_title = book.volume.edition.text.translation.original_work.title_vo or book.volume.edition.text.translation.original_work.title
                author_credited = book.volume.edition.text.translation.original_work.author_credit
                genre = book.volume.edition.text.translation.original_work.genre
                original_year = book.volume.edition.text.translation.original_work.year
                original_language = book.volume.edition.text.translation.original_work.lang

                for saga_work in book.volume.edition.text.translation.original_work.sagas.all():
                    if saga_work.saga.title not in sagas:
                        sagas[saga_work.saga.title] = []
                    sagas[saga_work.saga.title].append(get_order(saga_work))
                    sagas[saga_work.saga.title].sort()
        else:
            original_year = book.volume.edition.year
            if book.volume.edition.texts.count() > 0:
                genre = pgettext("collection of books", "Collection")

                for edition_text in book.volume.edition.texts.all():
                    if hasattr(edition_text.text, "work") and edition_text.text.work:
                        for saga_work in edition_text.text.work.sagas.all():
                            if saga_work.saga.title not in sagas:
                                sagas[saga_work.saga.title] = []
                            sagas[saga_work.saga.title].append(get_order(saga_work))
                            sagas[saga_work.saga.title].sort()
                    elif hasattr(edition_text.text, "translation") and edition_text.text.translation:
                        for saga_work in edition_text.text.translation.original_work.sagas.all():
                            if saga_work.saga.title not in sagas:
                                sagas[saga_work.saga.title] = []
                            sagas[saga_work.saga.title].append(get_order(saga_work))
                            sagas[saga_work.saga.title].sort()

        data = {
            "title": book.volume.title or book.volume.edition.title,
            "original_title": original_title,
            "original_year": str(original_year) if original_year else "",
            "genre": str(genre) if genre else pgettext("Unknown genre", "Unknown"),
            "authors": [author.name for author in book.volume.edition.authors],
            "language": book.volume.edition.language.iso1 if book.volume.edition.language else "",
            "original_language": original_language.iso1 if original_language else "",
            "translators": [translator.name for translator in book.volume.edition.translators],
            "isbn": book.volume.isbn if book.volume.isbn is not None else "",
            "year": str(book.volume.edition.year) if book.volume.edition.year else "",
            "publisher": book.volume.edition.publisher,
            "date": book.date or "",
        }

        if author_credited:
            data["author_credited"] = author_credited

        if sagas:
            data["sagas"] = sagas

        books.append(data)

    if order == "title":
        books = sorted(books, key=lambda k: k["title"])
    elif order == "year":
        books = sorted(books, key=lambda k: k["year"])

    return books

def _format_reading_texts(texts, format):
    result = ""

    if format == "csv":
        result += "Title,Original title,Year,Authors,Genre,Language,Translators,Translation year,Sagas\n"
        for text in texts:
            if "sagas" in text:
                sagas = []
                for saga in text["sagas"]:
                    sagas.append("%s (%d)" % (saga["title"], saga["order"]))
                sagas = " / ".join(sagas)
            else:
                sagas = ""

            if "translators" in text:
                translators = " / ".join(text["translators"])
            else:
                translators = ""

            authors = " / ".join(text["authors"])
            if "author_credited" in text and text["author_credited"]:
                if authors:
                    authors = text["author_credited"] + " (" + authors + ")"
                else:
                    authors = text["author_credited"]

            result += "\"%s\",\"%s\",%s,\"%s\",%s,%s,\"%s\",%s,\"%s\"\n" % (text["title"].replace("\"", "\\\""), text["original_title"].replace("\"", "\\\""), text["original_year"], authors, text["genre"], text["language"], translators, text["year"], sagas)
    elif format == "json":
        result += json.dumps(texts, indent=2)

    return result

def _format_reading_works(works, format):
    result = ""

    if format == "csv":
        result += "Title,Original title,Year,Authors,Genre,Language,Sagas\n"
        for work in works:
            if "sagas" in work:
                sagas = []
                for saga in work["sagas"]:
                    sagas.append("%s (%d)" % (saga["title"], saga["order"]))
                sagas = " / ".join(sagas)
            else:
                sagas = ""

            authors = " / ".join(work["authors"])
            if "author_credited" in work and work["author_credited"]:
                if authors:
                    authors = work["author_credited"] + " (" + authors + ")"
                else:
                    authors = work["author_credited"]

            result += "\"%s\",\"%s\",%s,\"%s\",%s,%s,\"%s\"\n" % (work["title"].replace("\"", "\\\""), work["original_title"].replace("\"", "\\\""), work["year"], authors, work["genre"], work["language"], sagas)
    elif format == "json":
        result += json.dumps(works, indent=2)

    return result

def _format_reading_readings(readings, format):
    result = ""

    if format == "csv":
        result += "Title,Original title,Year,Authors,Genre,Language,Translators,Translation year,Reading date start,Reading date end\n"
        for reading in readings:
            if "translators" in reading:
                translators = " / ".join(reading["translators"])
            else:
                translators = ""

            authors = " / ".join(reading["authors"])
            if "author_credited" in reading and reading["author_credited"]:
                if authors:
                    authors = reading["author_credited"] + " (" + authors + ")"
                else:
                    authors = reading["author_credited"]

            result += "\"%s\",\"%s\",%s,\"%s\",%s,%s,\"%s\",%s,%s,%s\n" % (reading["title"].replace("\"", "\\\""), reading["original_title"].replace("\"", "\\\""), reading["original_year"], authors, reading["genre"], reading["language"], translators, reading["year"], reading["date_start"], reading["date_end"])
    elif format == "json":
        result += json.dumps(readings, indent=2)

    return result

def _format_books(books, format):
    result = ""

    if format == "csv":
        result += "Title,Original title,Authors,Translators,Publisher,ISBN,Sagas,Date\n"
        for book in books:
            translators = " / ".join(book["translators"])
            authors = " / ".join(book["authors"])

            if "author_credited" in book and book["author_credited"]:
                if authors:
                    authors = book["author_credited"] + " (" + authors + ")"
                else:
                    authors = book["author_credited"]

            if "sagas" in book:
                sagas = []
                for saga_title, saga_orders in book["sagas"].items():
                    sagas.append("%s (%s)" % (saga_title, ", ".join("%d" % o for o in saga_orders)))
                sagas = " / ".join(sagas)
            else:
                sagas = ""

            result += "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n" % (book["title"].replace("\"", "\\\""), book["original_title"].replace("\"", "\\\""), authors, translators, book["publisher"], book["isbn"], sagas, book["date"])
    elif format == "json":
        result += json.dumps(books, indent=2)

    return result


def exportReadings(user, type, format, order):
    """
    Exporte la liste des lectures d'un utilisateur.

    :param user: Objet User de l'utilisateur.
    :param type: readings, texts, works.
    :param format: csv, json.
    :param order: title, year, date.
    :return: Chaine de caractères.
    """

    if type == "texts":
        texts = _get_reading_texts(user, order)
        return _format_reading_texts(texts, format)
    elif type == "works":
        works = _get_reading_works(user, order)
        return _format_reading_works(works, format)
    elif type == "readings":
        readings = _get_reading_readings(user, order)
        return _format_reading_readings(readings, format)

def backupReadings(user):
    texts = _get_reading_texts(user, "title")
    with open(os.path.join(BASE_DIR, "backup", "readings_texts-%06d.json" % user.id), "w", encoding="utf-8") as f:
        f.write(_format_reading_texts(texts, "json"))
    with open(os.path.join(BASE_DIR, "backup", "readings_texts-%06d.csv" % user.id), "w", encoding="utf-8") as f:
        f.write(_format_reading_texts(texts, "csv"))

    works = _get_reading_works(user, "title")
    with open(os.path.join(BASE_DIR, "backup", "readings_works-%06d.json" % user.id), "w", encoding="utf-8") as f:
        f.write(_format_reading_works(works, "json"))
    with open(os.path.join(BASE_DIR, "backup", "readings_works-%06d.csv" % user.id), "w", encoding="utf-8") as f:
        f.write(_format_reading_works(works, "csv"))

    readings = _get_reading_readings(user, "date")
    with open(os.path.join(BASE_DIR, "backup", "readings-%06d.json" % user.id), "w", encoding="utf-8") as f:
        f.write(_format_reading_readings(readings, "json"))
    with open(os.path.join(BASE_DIR, "backup", "readings-%06d.csv" % user.id), "w", encoding="utf-8") as f:
        f.write(_format_reading_readings(readings, "csv"))

def backupBooks(user):
    books = _get_books(user, "title")
    with open(os.path.join(BASE_DIR, "backup", "books-%06d.json" % user.id), "w", encoding="utf-8") as f:
        f.write(_format_books(books, "json"))
    with open(os.path.join(BASE_DIR, "backup", "books-%06d.csv" % user.id), "w", encoding="utf-8") as f:
        f.write(_format_books(books, "csv"))
