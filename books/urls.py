from django.urls import re_path

from . import views


urlpatterns = [
    re_path(r"^$", views.index, name="books_home"),

    re_path(r"^works$", views.worksList, name="works_list"),
    re_path(r"^works/create$", views.workCreate, name="work_create"),
    re_path(r"^works/api/list$", views.worksAPIList, name="works_api_list"),
    re_path(r"^works/api/search$", views.worksAPISearch, name="works_api_search"),
    re_path(r"^works/(?P<id>[0-9]+)$", views.workInfo, name="work_info"),
    re_path(r"^works/(?P<id>[0-9]+)/edit$", views.workEdit, name="work_edit"),
    re_path(r"^works/(?P<id>[0-9]+)/delete$", views.workDelete, name="work_delete"),
    re_path(r"^works/(?P<id>[0-9]+)/add_reading$", views.workAddReading, name="work_add_reading"),
    re_path(r"^works/(?P<id>[0-9]+)/add_translation$", views.workAddTranslation, name="work_add_translation"),
    re_path(r"^works/(?P<id>[0-9]+)/add_edition$", views.workAddEdition, name="work_add_edition"),
    re_path(r"^works/(?P<id>[0-9]+)/add_attribute$", views.workAddAttribute, name="work_add_attribute"),
    re_path(r"^works/(?P<id>[0-9]+)/add_ref$", views.workAddRef, name="work_add_ref"),
    re_path(r"^works/(?P<id>[0-9]+)/add_tag$", views.workAddTag, name="work_add_tag"),
    re_path(r"^works/(?P<id>[0-9]+)/add_link$", views.workAddLink, name="work_add_link"),
    re_path(r"^works/(?P<id>[0-9]+)/edit_readings$", views.workEditReadings, name="work_edit_readings"),
    re_path(r"^works/(?P<id>[0-9]+)/edit_attributes$", views.workEditAttributes, name="work_edit_attributes"),
    re_path(r"^works/(?P<id>[0-9]+)/edit_refs$", views.workEditRefs, name="work_edit_refs"),
    re_path(r"^works/(?P<id>[0-9]+)/edit_tags$", views.workEditTags, name="work_edit_tags"),
    re_path(r"^works/(?P<id>[0-9]+)/edit_links$", views.workEditLinks, name="work_edit_links"),

    re_path(r"^translations/(?P<id>[0-9]+)$", views.translationInfo, name="translation_info"),
    re_path(r"^translations/(?P<id>[0-9]+)/edit$", views.translationEdit, name="translation_edit"),
    re_path(r"^translations/(?P<id>[0-9]+)/delete$", views.translationDelete, name="translation_delete"),
    re_path(r"^translations/(?P<id>[0-9]+)/add_reading$", views.translationAddReading, name="translation_add_reading"),
    re_path(r"^translations/(?P<id>[0-9]+)/add_edition$", views.translationAddEdition, name="translation_add_edition"),
    re_path(r"^translations/(?P<id>[0-9]+)/add_attribute$", views.translationAddAttribute, name="translation_add_attribute"),
    re_path(r"^translations/(?P<id>[0-9]+)/add_tag$", views.translationAddTag, name="translation_add_tag"),
    re_path(r"^translations/(?P<id>[0-9]+)/add_link$", views.translationAddLink, name="translation_add_link"),
    re_path(r"^translations/(?P<id>[0-9]+)/edit_readings$", views.translationEditReadings, name="translation_edit_readings"),
    re_path(r"^translations/(?P<id>[0-9]+)/edit_attributes$", views.translationEditAttributes, name="translation_edit_attributes"),
    re_path(r"^translations/(?P<id>[0-9]+)/edit_tags$", views.translationEditTags, name="translation_edit_tags"),
    re_path(r"^translations/(?P<id>[0-9]+)/edit_links$", views.translationEditLinks, name="translation_edit_links"),

    re_path(r"^texts/api/search$", views.textsAPISearch, name="texts_api_search"),
    re_path(r"^texts/(?P<id>[0-9]+)$", views.textInfo, name="text_info"),

    re_path(r"^editions/create$", views.editionCreateCollection, name="edition_collection_create"),
    re_path(r"^editions/api/list$", views.editionAPIList, name="editions_api_list"),
    re_path(r"^editions/(?P<id>[0-9]+)$", views.editionInfo, name="edition_info"),
    re_path(r"^editions/(?P<id>[0-9]+)/edit$", views.editionEdit, name="edition_edit"),
    re_path(r"^editions/(?P<id>[0-9]+)/delete$", views.editionDelete, name="edition_delete"),
    re_path(r"^editions/(?P<id>[0-9]+)/duplicate$", views.editionDuplicate, name="edition_duplicate"),
    re_path(r"^editions/(?P<id>[0-9]+)/to_collection$", views.editionTransformToCollection, name="edition_to_collection"),
    re_path(r"^editions/(?P<id>[0-9]+)/to_edition$", views.editionTransformToEdition, name="edition_to_edition"),
    re_path(r"^editions/(?P<id>[0-9]+)/add_text$", views.editionAddText, name="edition_add_text"),
    re_path(r"^editions/(?P<id>[0-9]+)/add_volume$", views.editionAddVolume, name="edition_add_volume"),
    re_path(r"^editions/(?P<id>[0-9]+)/add_reading$", views.editionAddReading, name="edition_add_reading"),
    re_path(r"^editions/(?P<id>[0-9]+)/add_attribute$", views.editionAddAttribute, name="edition_add_attribute"),
    re_path(r"^editions/(?P<id>[0-9]+)/add_link$", views.editionAddLink, name="edition_add_link"),
    re_path(r"^editions/(?P<id>[0-9]+)/edit_texts$", views.editionEditTexts, name="edition_edit_texts"),
    re_path(r"^editions/(?P<id>[0-9]+)/edit_readings$", views.editionEditReadings, name="edition_edit_readings"),
    re_path(r"^editions/(?P<id>[0-9]+)/edit_attributes$", views.editionEditAttributes, name="edition_edit_attributes"),
    re_path(r"^editions/(?P<id>[0-9]+)/edit_links$", views.editionEditLinks, name="edition_edit_links"),
    re_path(r"^editions/(?P<id>[0-9]+)/(?P<vid>[0-9]+)$", views.volumeInfo, name="volume_info"),
    re_path(r"^editions/(?P<id>[0-9]+)/(?P<vid>[0-9]+)/edit$", views.volumeEdit, name="volume_edit"),
    re_path(r"^editions/(?P<id>[0-9]+)/(?P<vid>[0-9]+)/delete$", views.volumeDelete, name="volume_delete"),
    re_path(r"^editions/(?P<id>[0-9]+)/(?P<vid>[0-9]+)/add_book$", views.volumeAddBook, name="volume_add_book"),
    re_path(r"^editions/(?P<id>[0-9]+)/(?P<vid>[0-9]+)/add_reading$", views.volumeAddReading, name="volume_add_reading"),
    re_path(r"^editions/(?P<id>[0-9]+)/(?P<vid>[0-9]+)/edit_readings$", views.volumeEditReadings, name="volume_edit_readings"),

    re_path(r"^books$", views.booksList, name="books_list"),
    re_path(r"^books/(?P<id>[0-9]+)$", views.bookInfo, name="book_info"),
    re_path(r"^books/(?P<id>[0-9]+)/edit$", views.bookEdit, name="book_edit"),
    re_path(r"^books/(?P<id>[0-9]+)/delete$", views.bookDelete, name="book_delete"),
    re_path(r"^books/(?P<id>[0-9]+)/add_reading$", views.bookAddReading, name="book_add_reading"),
    re_path(r"^books/(?P<id>[0-9]+)/edit_readings$", views.bookEditReadings, name="book_edit_readings"),

    re_path(r"^sagas$", views.sagasList, name="books_sagas_list"),
    re_path(r"^sagas/create$", views.sagaCreate, name="books_saga_create"),
    re_path(r"^sagas/(?P<id>[0-9]+)$", views.sagaInfo, name="books_saga_info"),
    re_path(r"^sagas/(?P<id>[0-9]+)/edit$", views.sagaEdit, name="books_saga_edit"),
    re_path(r"^sagas/(?P<id>[0-9]+)/delete$", views.sagaDelete, name="books_saga_delete"),
    re_path(r"^sagas/(?P<id>[0-9]+)/add_work$", views.sagaAddWork, name="books_saga_add_work"),
    re_path(r"^sagas/(?P<id>[0-9]+)/edit_works$", views.sagaEditWorks, name="books_saga_edit_works"),

    re_path(r"^lists$", views.listList, name="booklists_list"),
    re_path(r"^lists/create$", views.listCreate, name="booklist_create"),
    re_path(r"^lists/(?P<id>[0-9]+)$", views.listInfo, name="booklist_info"),
    re_path(r"^lists/(?P<id>[0-9]+)/edit$", views.listEdit, name="booklist_edit"),
    re_path(r"^lists/(?P<id>[0-9]+)/delete$", views.listDelete, name="booklist_delete"),
    re_path(r"^lists/(?P<id>[0-9]+)/add/(?P<vid>[0-9]+)$", views.listAddVolume, name="booklist_add_volume"),
    re_path(r"^lists/(?P<id>[0-9]+)/remove/(?P<vid>[0-9]+)$", views.listRemoveVolume, name="booklist_remove_volume"),

    re_path(r"^search$", views.worksSearch, name="works_search"),
    re_path(r"^readings$", views.readings, name="books_readings"),
    re_path(r"^readings/export$", views.readingsExport, name="books_readings_export"),
]
