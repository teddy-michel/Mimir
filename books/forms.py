
from django.forms import ModelForm, Textarea, HiddenInput, IntegerField, CharField, Select, TextInput, \
    MultipleChoiceField, ChoiceField
from django.utils.translation import gettext as _

from base.models import Tag
from .models import Work, Reading, TextAttribute, TextTag, SagaWork, Saga, Edition, Translation, EditionAttribute, EditionLink, Book, \
    TextLink, Volume, EditionText, Text, WorkReference, List


class WorkForm(ModelForm):
    authors_select = MultipleChoiceField(label=_("Authors"), required=False)

    class Meta:
        model = Work
        fields = ["title", "title_vo", "year", "authors_select", "author_credit", "lang", "genre", "summary", "info"]
        widgets = {
            "summary": Textarea(attrs={"rows": 4}),
            "info": Textarea(attrs={"rows": 4}),
        }


class ReadingForm(ModelForm):

    class Meta:
        model = Reading
        fields = ["date_start", "date_end", "info"]
        widgets = {
            "date_start": TextInput(attrs={"placeholder": "YYYY-MM-DD"}),
            "date_end": TextInput(attrs={"placeholder": "YYYY-MM-DD"}),
        }


class TranslationForm(ModelForm):
    translators_select = MultipleChoiceField(label=_("Translators"), required=False)

    class Meta:
        model = Translation
        fields = ["year", "lang", "translators_select", "info"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
        }


class TextAttributeForm(ModelForm):

    class Meta:
        model = TextAttribute
        fields = ["name", "value"]


class TextLinkForm(ModelForm):

    class Meta:
        model = TextLink
        fields = ["name", "uri", "lang"]


# TODO: make a generic class
class TextTagForm(ModelForm):
    tag_id = IntegerField(widget=HiddenInput(), required=False)
    tag_select = IntegerField(label=_("Tag"), widget=Select(attrs={"class": "select_tag"}), required=False)
    tag_name = CharField(widget=HiddenInput(), max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "tag") and self.instance.tag:
            self.initial["tag_id"] = self.instance.tag.id
            self.initial["tag_name"] = self.instance.tag.name
        else:
            self.initial["tag_id"] = None
            self.initial["tag_name"] = ""

    def _clean_fields(self, *args, **kwargs):
        tagId = int(self.data.get(self.add_prefix("tag_select")))

        if tagId < 0:
            tag = Tag.objects.create(name=self.data.get(self.add_prefix("tag_name")))
        else:
            tag = Tag.objects.get(id=tagId)

        self.instance.tag = tag
        self.data = self.data.copy()
        self.data[self.add_prefix("tag")] = self.instance.tag.id

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = TextTag
        fields = ["tag_select", "tag_name", "tag", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class TextTagFormClassic(ModelForm):
    tag_name = CharField(label=_("Tag name"), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        self.fields["tag"].required = False

        if hasattr(self.instance, "tag"):
            self.initial["tag_name"] = self.instance.tag.name
        else:
            self.initial["tag_name"] = ""

    def save(self, commit=True):
        name = self.cleaned_data.get("tag_name").strip()
        if self.initial["tag_name"] != name:
            tag = Tag.objects.get_or_create(name=name)[0]
            self.instance.tag = tag
        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = TextTag
        fields = ["tag", "tag_name", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class EditionForm(ModelForm):

    def __init__(self, *args, **kwargs):
        if "work" in kwargs:
            work = kwargs["work"]
            del kwargs["work"]
        else:
            work = None

        super(ModelForm, self).__init__(*args, **kwargs)

        if not work and self.instance is not None and self.instance.text is not None:
            if hasattr(self.instance.text, "work") and self.instance.text.work is not None:
                work = self.instance.text.work
            elif hasattr(self.instance.text, "translation") and self.instance.text.translation is not None:
                work = self.instance.text.translation.original_work

        #if work:
        #    self.fields["translation"].queryset = Translation.objects.filter(original_work=work)

    class Meta:
        model = Edition
        fields = ["title", "year", "publisher", "notreadable", "info"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
        }


class EditionTextForm(ModelForm):
    text_id = IntegerField(label=_("Text"), widget=Select(attrs={"class": "select_text"}), required=False)
    text_title = CharField(widget=HiddenInput(), max_length=150, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "text"):
            self.initial["text_id"] = self.instance.text.id
            self.initial["text_title"] = self.instance.text.title
            if self.instance.text.year:
                self.initial["text_title"] += " (%s)" % self.instance.text.year
        else:
            self.initial["text_id"] = None
            self.initial["text_title"] = ""

    def save(self, commit=True):
        if self.instance.text is None and self.cleaned_data.get("text_id"):
            self.instance.text = Text.objects.get(id=int(self.cleaned_data.get("text_id")))

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = EditionText
        fields = ["text_id", "text_title", "text", "title", "role"]
        widgets = {
            "text": HiddenInput(),
        }


class EditionTextReadOnlyForm(ModelForm):
    text_title = CharField(label=_("Text"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        if hasattr(self.instance.text, "work") and self.instance.text.work:
            self.initial["text_title"] = self.instance.text.work.title
        elif hasattr(self.instance.text, "translation") and self.instance.text.translation:
            self.initial["text_title"] = self.instance.text.translation.original_work.title
        else:
            self.initial["text_title"] = ""

    class Meta:
        model = EditionText
        fields = ["text_title", "title", "role"]


class EditionAttributeForm(ModelForm):

    class Meta:
        model = EditionAttribute
        fields = ["name", "value"]


class EditionLinkForm(ModelForm):

    class Meta:
        model = EditionLink
        fields = ["name", "uri", "lang"]


class VolumeForm(ModelForm):
    image_clear = CharField(widget=HiddenInput(), max_length=1, required=False, initial="0")

    def save(self, commit=True):
        image_clear_value = self.cleaned_data.get("image_clear")
        if image_clear_value == "1":
            self.instance.image = None

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = Volume
        fields = ["title", "isbn", "number", "pages", "info", "image", "image_clear"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
        }


class BookForm(ModelForm):

    class Meta:
        model = Book
        fields = ["date", "printingdate", "origin", "info"]
        widgets = {
            "date": TextInput(attrs={"placeholder": "YYYY-MM-DD"}),
            "printingdate": TextInput(attrs={"placeholder": "YYYY-MM-DD"}),
            "info": Textarea(attrs={"rows": 4}),
        }


class SagaForm(ModelForm):

    class Meta:
        model = Saga
        fields = ["title", "title_vo", "info"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
        }


class SagaWorkForm(ModelForm):
    work_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_work"}), required=False)
    work_title = CharField(widget=HiddenInput(), max_length=150, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "work"):
            self.initial["work_id"] = self.instance.work.id
            self.initial["work_title"] = self.instance.work.title
            if self.instance.work.year:
                self.initial["work_title"] += " (%s)" % self.instance.work.year
        else:
            self.initial["work_id"] = None
            self.initial["work_title"] = ""

    def save(self, commit=True):
        if self.instance.work is None and self.cleaned_data.get("work_id"):
            self.instance.work = Work.objects.get(id=int(self.cleaned_data.get("work_id")))

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = SagaWork
        fields = ["work_id", "work_title", "work"]
        widgets = {
            "work": HiddenInput(),
        }


class WorkReferenceForm(ModelForm):
    object_type = ChoiceField(label=_("Type"), widget=Select(attrs={"class": "object_type"}),
                              choices=(("text", _("Text")), ("movie", _("Movie")), ("serie", _("Serie")), ("person", _("Person")), ("book", _("Book")), ("game", _("Game"))))
    movie_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_movie"}), required=False)
    serie_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_serie"}), required=False)
    person_id = IntegerField(label=_("Name"), widget=Select(attrs={"class": "select_person"}), required=False)
    book_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_book"}), required=False)
    game_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_game"}), required=False)
    movie_title = CharField(widget=HiddenInput(), max_length=150, required=False)
    serie_title = CharField(widget=HiddenInput(), max_length=100, required=False)
    person_name = CharField(widget=HiddenInput(), max_length=100, required=False)
    book_title = CharField(widget=HiddenInput(), max_length=100, required=False)
    game_title = CharField(widget=HiddenInput(), max_length=150, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        self.initial["object_type"] = "text"
        self.initial["movie_id"] = None
        self.initial["serie_id"] = None
        self.initial["person_id"] = None
        self.initial["book_id"] = None
        self.initial["game_id"] = None

        self.initial["movie_title"] = ""
        self.initial["serie_title"] = ""
        self.initial["person_name"] = ""
        self.initial["book_title"] = ""
        self.initial["game_title"] = ""

        try:
            if self.instance.ref_movie:
                self.initial["object_type"] = "movie"
                self.initial["movie_id"] = self.instance.ref_movie.id
                self.initial["movie_title"] = self.instance.ref_movie.title
            elif self.instance.ref_serie:
                self.initial["object_type"] = "serie"
                self.initial["serie_id"] = self.instance.ref_serie.id
                self.initial["serie_title"] = self.instance.ref_serie.title
            elif self.instance.ref_person:
                self.initial["object_type"] = "person"
                self.initial["person_id"] = self.instance.ref_person.id
                self.initial["person_name"] = self.instance.ref_person.name
            elif self.instance.ref_book:
                self.initial["object_type"] = "book"
                self.initial["book_id"] = self.instance.ref_book.id
                self.initial["book_title"] = self.instance.ref_book.title
            elif self.instance.ref_game:
                self.initial["object_type"] = "game"
                self.initial["game_id"] = self.instance.ref_game.id
                self.initial["game_title"] = self.instance.ref_game.title
        except:
            pass

    def save(self, commit=True):
        if self.instance.ref_movie is None and self.cleaned_data.get("movie_id"):
            self.instance.ref_movie = Movie.objects.get(id=int(self.cleaned_data.get("movie_id")))
        if self.instance.ref_serie is None and self.cleaned_data.get("serie_id"):
            self.instance.ref_serie = Serie.objects.get(id=int(self.cleaned_data.get("serie_id")))
        if self.instance.ref_person is None and self.cleaned_data.get("person_id"):
            self.instance.ref_person = Person.objects.get(id=int(self.cleaned_data.get("person_id")))
        if self.instance.ref_book is None and self.cleaned_data.get("book_id"):
            self.instance.ref_book = Work.objects.get(id=int(self.cleaned_data.get("book_id")))
        if self.instance.ref_game is None and self.cleaned_data.get("game_id"):
            self.instance.ref_game = Game.objects.get(id=int(self.cleaned_data.get("game_id")))

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = WorkReference
        fields = ["object_type",
                  "movie_id", "movie_title", "ref_movie",
                  "serie_id", "serie_title", "ref_serie",
                  "person_id", "person_name", "ref_person",
                  "book_id", "book_title", "ref_book",
                  "game_id", "game_title", "ref_game",
                  "info"]
        widgets = {
            "ref_movie": HiddenInput(),
            "ref_serie": HiddenInput(),
            "ref_person": HiddenInput(),
            "ref_book": HiddenInput(),
            "ref_game": HiddenInput(),
        }


class WorkReferenceReadOnlyForm(ModelForm):
    text_type = CharField(label=_("Type"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=100)
    text_title = CharField(label=_("Title or name"), widget=TextInput(attrs={"readonly": "readonly"}), max_length=150)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if self.instance.ref_movie:
            self.initial["text_type"] = _("Movie")
            self.initial["text_title"] = self.instance.ref_movie.title
        elif self.instance.ref_serie:
            self.initial["text_type"] = _("Serie")
            self.initial["text_title"] = self.instance.ref_serie.title
        elif self.instance.ref_person:
            self.initial["text_type"] = _("Person")
            self.initial["text_title"] = self.instance.ref_person.name
        elif self.instance.ref_book:
            self.initial["text_type"] = _("Book")
            if self.instance.ref_book.genre:
                self.initial["text_type"] += " (" + str(self.instance.ref_book.genre) + ")"
            self.initial["text_title"] = str(self.instance.ref_book)
        elif self.instance.ref_game:
            self.initial["text_type"] = _("Game")
            self.initial["text_title"] = self.instance.ref_game.title
        else:
            self.initial["text_type"] = _("Text")
            self.initial["text_title"] = ""

    class Meta:
        model = WorkReference
        fields = ["text_type", "text_title", "info"]


class ListForm(ModelForm):

    class Meta:
        model = List
        fields = ["name", "info"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
        }
