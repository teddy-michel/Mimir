from django.contrib import admin

from .models import Work, WorkGenre, Translation, Edition, Book, Reading, TextTag, Saga, SagaWork, EditionAttribute, TextAttribute, \
    TextLink, Volume, EditionText


@admin.register(TextAttribute)
class TextAttributeAdmin(admin.ModelAdmin):
    list_display = ("text", "name", "value")


@admin.register(TextLink)
class TextLinkAdmin(admin.ModelAdmin):
    list_display = ("text", "name", "uri")


@admin.register(TextTag)
class TextTagAdmin(admin.ModelAdmin):
    list_display = ("text", "tag")


@admin.register(Work)
class WorkAdmin(admin.ModelAdmin):
    list_display = ("title", "year")


@admin.register(WorkGenre)
class WorkGenreAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Translation)
class TranslationAdmin(admin.ModelAdmin):
    list_display = ("original_work", "year")


@admin.register(Edition)
class EditionAdmin(admin.ModelAdmin):
    list_display = ("title", "year", "publisher")


@admin.register(EditionText)
class EditionTextAdmin(admin.ModelAdmin):
    list_display = ("title", "text", "edition", "role")


@admin.register(EditionAttribute)
class EditionAttributeAdmin(admin.ModelAdmin):
    list_display = ("edition", "name", "value")


@admin.register(Volume)
class VolumeAdmin(admin.ModelAdmin):
    list_display = ("title", "isbn", "edition")


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ("volume", "user")


@admin.register(Reading)
class ReadingAdmin(admin.ModelAdmin):
    list_display = ("text", "date_start", "date_end", "user")


@admin.register(Saga)
class SagaAdmin(admin.ModelAdmin):
    list_display = ("title",)


@admin.register(SagaWork)
class SagaWorkAdmin(admin.ModelAdmin):
    list_display = ("saga", "work")
