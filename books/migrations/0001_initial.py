# Generated by Django 4.0 on 2021-12-25 16:26

import books.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('base', '0001_initial'),
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.CharField(blank=True, max_length=10, null=True, verbose_name="Date d'achat")),
                ('printingdate', models.DateField(blank=True, null=True, verbose_name="Date d'impression")),
                ('origin', models.CharField(choices=[('U', 'Inconnu'), ('B', 'Acheté'), ('G', 'Donné'), ('F', 'Trouvé')], default='U', max_length=1, verbose_name='Origine')),
                ('infos', models.TextField(blank=True, verbose_name='Informations')),
                ('creation', models.DateTimeField(auto_now_add=True, verbose_name='Date de création')),
                ('modification', models.DateTimeField(auto_now=True, verbose_name='Date de modification')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='books', to='auth.user', verbose_name='Utilisateur')),
            ],
            options={
                'verbose_name': 'Livre',
                'verbose_name_plural': 'Livres',
                'permissions': (('delete_any_book', 'Delete book of any user'), ('view_all_books', 'View books of all users')),
            },
        ),
        migrations.CreateModel(
            name='Edition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, verbose_name='Titre')),
                ('year', models.IntegerField(blank=True, null=True, verbose_name='Année')),
                ('publisher', models.CharField(blank=True, max_length=100, verbose_name='Éditeur')),
                ('infos', models.TextField(blank=True, verbose_name='Informations')),
                ('creation', models.DateTimeField(auto_now_add=True, verbose_name='Date de création')),
                ('modification', models.DateTimeField(auto_now=True, verbose_name='Date de modification')),
            ],
            options={
                'verbose_name': 'Édition',
                'verbose_name_plural': 'Éditions',
            },
        ),
        migrations.CreateModel(
            name='Saga',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='Titre')),
                ('title_vo', models.CharField(blank=True, max_length=100, verbose_name='Titre original')),
                ('infos', models.TextField(blank=True, verbose_name='Informations')),
                ('creation', models.DateTimeField(auto_now_add=True, verbose_name='Date de création')),
                ('modification', models.DateTimeField(auto_now=True, verbose_name='Date de modification')),
            ],
            options={
                'verbose_name': 'Saga',
                'verbose_name_plural': 'Sagas',
                'ordering': ['title'],
            },
        ),
        migrations.CreateModel(
            name='Text',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.IntegerField(blank=True, null=True, verbose_name='Année')),
                ('infos', models.TextField(blank=True, verbose_name='Informations')),
                ('creation', models.DateTimeField(auto_now_add=True, verbose_name='Date de création')),
                ('modification', models.DateTimeField(auto_now=True, verbose_name='Date de modification')),
                ('lang', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='base.language', verbose_name='Langue')),
            ],
        ),
        migrations.CreateModel(
            name='WorkGenre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
            ],
            options={
                'verbose_name': 'Genre',
                'verbose_name_plural': 'Genres',
                'db_table': 'books_genres',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Volume',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('isbn', models.CharField(blank=True, max_length=20, null=True, unique=True, verbose_name='ISBN')),
                ('title', models.CharField(max_length=150, verbose_name='Titre')),
                ('infos', models.TextField(blank=True, verbose_name='Informations')),
                ('image', models.ImageField(blank=True, null=True, upload_to=books.models.random_filename, verbose_name='Image')),
                ('creation', models.DateTimeField(auto_now_add=True, verbose_name='Date de création')),
                ('modification', models.DateTimeField(auto_now=True, verbose_name='Date de modification')),
                ('edition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='volumes', to='books.edition')),
            ],
            options={
                'verbose_name': 'Volume',
                'verbose_name_plural': 'Volumes',
            },
        ),
        migrations.CreateModel(
            name='TextTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('infos', models.CharField(blank=True, max_length=100, verbose_name='Informations')),
                ('tag', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='texts', to='base.tag', verbose_name='Tag')),
                ('text', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tags', to='books.text')),
            ],
            options={
                'verbose_name': 'Texte - Tag',
                'verbose_name_plural': 'Textes - Tags',
                'db_table': 'books_text_tags',
                'ordering': ['tag__name'],
            },
        ),
        migrations.CreateModel(
            name='TextLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('uri', models.CharField(max_length=200, verbose_name='URI')),
                ('lang', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='base.language', verbose_name='Langue')),
                ('text', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='links', to='books.text')),
            ],
            options={
                'verbose_name': 'Texte - Lien',
                'verbose_name_plural': 'Textes - Liens',
                'db_table': 'books_text_links',
            },
        ),
        migrations.CreateModel(
            name='TextAttribute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('value', models.CharField(max_length=200, verbose_name='Valeur')),
                ('text', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='attributes', to='books.text')),
            ],
            options={
                'verbose_name': 'Texte - Attribut',
                'verbose_name_plural': 'Textes - Attributs',
                'db_table': 'books_text_attributes',
            },
        ),
        migrations.CreateModel(
            name='Reading',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_start', models.CharField(blank=True, max_length=10, null=True, verbose_name='Date de début')),
                ('date_end', models.CharField(blank=True, max_length=10, null=True, verbose_name='Date de fin')),
                ('infos', models.CharField(blank=True, max_length=100, verbose_name='Informations')),
                ('book', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='readings', to='books.book')),
                ('edition', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='readings', to='books.edition')),
                ('text', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='readings', to='books.text', verbose_name='Texte')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='readings', to='auth.user', verbose_name='Utilisateur')),
                ('volume', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='readings', to='books.volume')),
            ],
            options={
                'verbose_name': 'Lecture',
                'verbose_name_plural': 'Lectures',
                'db_table': 'books_book_readings',
                'ordering': ['-date_start'],
            },
        ),
        migrations.CreateModel(
            name='EditionAttribute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('value', models.CharField(max_length=200, verbose_name='Valeur')),
                ('edition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='attributes', to='books.edition')),
            ],
            options={
                'verbose_name': 'Édition - Attribut',
                'verbose_name_plural': 'Éditions - Attributs',
                'db_table': 'books_edition_attributes',
            },
        ),
        migrations.AddField(
            model_name='edition',
            name='text',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='editions', to='books.text'),
        ),
        migrations.AddField(
            model_name='book',
            name='volume',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='books', to='books.volume'),
        ),
        migrations.CreateModel(
            name='Work',
            fields=[
                ('text', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='work', serialize=False, to='books.text')),
                ('title', models.CharField(max_length=150, verbose_name='Titre')),
                ('title_vo', models.CharField(blank=True, max_length=150, verbose_name='Titre original')),
                ('author_credit', models.CharField(blank=True, max_length=100, null=True, verbose_name='Auteur crédité')),
                ('summary', models.TextField(blank=True, verbose_name='Résumé')),
                ('search', models.CharField(blank=True, max_length=500, verbose_name='Rechercher')),
                ('authors', models.ManyToManyField(blank=True, related_name='works', to='base.Person', verbose_name='Auteurs')),
                ('genre', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='works', to='books.workgenre', verbose_name='Genre')),
            ],
            options={
                'verbose_name': 'Œuvre',
                'verbose_name_plural': 'Œuvres',
                'ordering': ['title'],
            },
            bases=('books.text',),
        ),
        migrations.CreateModel(
            name='Translation',
            fields=[
                ('text', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='translation', serialize=False, to='books.text')),
                ('original_work', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='books.work')),
                ('translators', models.ManyToManyField(blank=True, related_name='translations', to='base.Person', verbose_name='Traducteurs')),
            ],
            options={
                'verbose_name': 'Traduction',
                'verbose_name_plural': 'Traductions',
                'ordering': ['original_work', 'year'],
            },
            bases=('books.text',),
        ),
        migrations.CreateModel(
            name='EditionText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, verbose_name='Titre')),
                ('role', models.CharField(choices=[('M', 'Texte principal'), ('B', 'Préface'), ('A', 'Postface'), ('N', 'Notes'), ('O', 'Autre')], default='M', max_length=1, verbose_name='Rôle')),
                ('edition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='texts', to='books.edition')),
                ('text', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='collections', to='books.text', verbose_name='Texte')),
            ],
            options={
                'verbose_name': 'Édition - Texte',
                'verbose_name_plural': 'Éditions - Textes',
                'db_table': 'books_edition_texts',
                'order_with_respect_to': 'edition',
                'unique_together': {('edition', 'text')},
            },
        ),
        migrations.AddField(
            model_name='edition',
            name='work',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='all_editions', to='books.work'),
        ),
        migrations.CreateModel(
            name='SagaWork',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('saga', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='works', to='books.saga')),
                ('work', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sagas', to='books.work', verbose_name='Œuvre')),
            ],
            options={
                'verbose_name': 'Saga - Œuvre',
                'verbose_name_plural': 'Sagas - Œuvres',
                'db_table': 'books_saga_works',
                'order_with_respect_to': 'saga',
                'unique_together': {('saga', 'work')},
            },
        ),
    ]
