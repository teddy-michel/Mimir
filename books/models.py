import os
import uuid

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext as _, pgettext

from base.models import Attribute, Person, Tag, Language, Link
from games.models import Game


def random_filename(instance, filename):
    ext = os.path.splitext(filename)[1].lower()
    if ext == ".jpeg":
        ext = ".jpg"
    filename = "%s%s" % (uuid.uuid4(), ext)
    return os.path.join("books", filename)


class Text(models.Model):
    lang = models.ForeignKey(Language, verbose_name=pgettext("language", "Language"), on_delete=models.SET_NULL, null=True, blank=True)
    year = models.IntegerField(verbose_name=_("Year"), blank=True, null=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        if hasattr(self, "work"):
            return str(self.work)
        elif hasattr(self, "translation"):
            result = self.translation.original_work.title

            if self.translation.original_work.year:
                result += " (" + str(self.translation.original_work.year) + ")"

            if self.translation.original_work.authors:
                authors = ""
                for author in self.translation.original_work.authors.all():
                    if authors:
                        authors += ", "
                    authors += author.name
                result += _(" by %(authors)s") % {"authors": authors}

            result += " "

            translators = ""
            for translator in self.translation.translators.all():
                if translators:
                    if len(translators) > 40:
                        translators += " [...]"
                        break
                    translators += ", "
                translators += translator.name

            if self.lang:
                if self.translation.translators:
                    result += _("translated in %(language)s by %(translators)s") % {"language": str(self.lang), "translators": translators}
                else:
                    result += _("translated in %(language)s") % {"language": str(self.lang)}
            else:
                if self.translation.translators:
                    result += _("translated by %(translators)s") % {"translators": translators}
                else:
                    result += _("translated")

            return result
        else:
            return _("Unknown text")

    def read_by_user(self):
        raise Exception("You shouldn't be there!")

        if self.readings.count() > 0:
            return True

        return False


class TextAttribute(Attribute):
    text = models.ForeignKey(Text, related_name="attributes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Text") + " - " + _("Attribute")
        verbose_name_plural = _("Texts") + " - " + _("Attributes")
        db_table = "books_" + "text_attributes"


class TextLink(Link):
    text = models.ForeignKey(Text, related_name="links", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Text") + " - " + _("Link")
        verbose_name_plural = _("Texts") + " - " + _("Links")
        db_table = "books_" + "text_links"


class TextTag(models.Model):
    text = models.ForeignKey(Text, related_name="tags", on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, verbose_name=_("Tag"), related_name="texts", on_delete=models.CASCADE)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)

    class Meta:
        verbose_name = _("Text") + " - " + _("Tag")
        verbose_name_plural = _("Texts") + " - " + _("Tags")
        ordering = ["tag__name"]
        db_table = "books_" + "text_tags"


class WorkGenre(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = _("Genre")
        verbose_name_plural = _("Genres")
        ordering = ["name"]
        db_table = "books_" + "genres"


class Work(Text):
    text = models.OneToOneField(Text, related_name="work", on_delete=models.CASCADE, parent_link=True)
    title = models.CharField(verbose_name=_("Title"), max_length=150, blank=False)
    title_vo = models.CharField(verbose_name=_("Original title"), max_length=150, blank=True)
    authors = models.ManyToManyField(Person, verbose_name=_("Authors"), related_name="works", blank=True)
    author_credit = models.CharField(verbose_name=_("Author credited"), max_length=100, null=True, blank=True)
    summary = models.TextField(verbose_name=_("Summary"), blank=True)
    search = models.CharField(verbose_name=_("Search"), max_length=500, blank=True)
    genre = models.ForeignKey(WorkGenre, verbose_name=_("Genre"), related_name="works", on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        result = str(self.title)
        if self.year:
            result += " (" + str(self.year) + ")"
        if self.authors:
            authors = ""
            for author in self.authors.all():
                if authors:
                    authors += ", "
                authors += author.name
            result += _(" by %(authors)s") % {"authors": authors}
        return result

    def read_by_user(self):
        if self.readings.count() > 0:
            return True

        for editionText in self.collections.all():
            if editionText.edition.readings.count() > 0:
                return True

        for translation in self.translations.all():
            if translation.read_by_user():
                return True

        return False

    class Meta:
        verbose_name = _("Work")
        verbose_name_plural = _("Works")
        ordering = ["title"]


class WorkReference(models.Model):
    work = models.ForeignKey(Work, verbose_name=_("Work"), related_name="refs", on_delete=models.CASCADE)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)
    ref_movie = models.ForeignKey("movies.Movie", verbose_name=_("Referenced movie"), related_name="ref_by_work", on_delete=models.SET_NULL, null=True, blank=True)
    ref_serie = models.ForeignKey("movies.Serie", verbose_name=_("Referenced serie"), related_name="ref_by_work", on_delete=models.SET_NULL, null=True, blank=True)
    ref_person = models.ForeignKey(Person, verbose_name=_("Referenced person"), related_name="ref_by_work", on_delete=models.SET_NULL, null=True, blank=True)
    ref_book = models.ForeignKey(Work, verbose_name=_("Referenced book"), related_name="ref_by_work", on_delete=models.SET_NULL, null=True, blank=True)
    ref_game = models.ForeignKey(Game, verbose_name=_("Referenced game"), related_name="ref_by_work", on_delete=models.SET_NULL, null=True, blank=True)

    def save(self, *args, **kwargs):
        numNotNone = 0
        if self.ref_movie is not None:
            numNotNone += 1
        if self.ref_serie is not None:
            numNotNone += 1
        if self.ref_person is not None:
            numNotNone += 1
        if self.ref_book is not None:
            numNotNone += 1
        if self.ref_game is not None:
            numNotNone += 1
        if numNotNone > 1:
            raise ValueError("Movie, serie, person, book or game can't be defined at the same time for a work reference.")

        super(WorkReference, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("Work") + " - " + _("Reference")
        verbose_name_plural = _("Works") + " - " + _("References")
        ordering = ["ref_movie", "ref_serie", "ref_person", "ref_book", "ref_game"]
        db_table = "books_" + "work_references"


class Translation(Text):
    text = models.OneToOneField(Text, related_name="translation", on_delete=models.CASCADE, parent_link=True)
    original_work = models.ForeignKey(Work, related_name="translations", on_delete=models.CASCADE)
    translators = models.ManyToManyField(Person, verbose_name=_("Translators"), related_name="translations", blank=True)

    def __str__(self):
        translators = ""
        for translator in self.translators.all():
            if translators:
                if len(translators) > 40:
                    translators += " [...]"
                    break
                translators += ", "
            translators += translator.name

        if self.lang:
            if translators:
                result = _("In %(language)s by %(translators)s") % {"language": str(self.lang), "translators": translators}
            else:
                result = _("In %(language)s") % {"language": str(self.lang)}
        else:
            if translators:
                result = _("By %(translators)s") % {"translators": translators}
            else:
                result = "?"

        if self.year:
            result += " (" + str(self.year) + ")"

        return result

    def read_by_user(self):
        if self.readings.count() > 0:
            return True

        for editionText in self.collections.all():
            if editionText.edition.readings.count() > 0:
                return True

        return False

    class Meta:
        verbose_name = _("Translation")
        verbose_name_plural = _("Translations")
        ordering = ["original_work", "year"]


"""
class Collection(Text):
    title = models.CharField(verbose_name=_("Title"), max_length=150, blank=False)

    class Meta:
        verbose_name = _("Collection")
        verbose_name_plural = _("Collections")


class CollectionText(models.Model):
    collection = models.ForeignKey(Collection, related_name="texts", on_delete=models.CASCADE)
    text = models.ForeignKey(Text, verbose_name=_("Text"), related_name="collections", on_delete=models.CASCADE)
    title = models.CharField(verbose_name=_("Title"), max_length=150, blank=False)
    role = models.CharField(verbose_name=_("Role"), max_length=1, choices=(("M", _("Main text")), ("B", _("Preface")), ("A", _("Postface")), ("N", _("Notes")), ("O", _("Other"))), default="M")

    def __str__(self):
        result = self.title
        result += _(" in %(collection)s") % {"collection": self.collection.title}
        return result

    class Meta:
        verbose_name = _("Collection") + " - " + _("Text")
        verbose_name_plural = _("Collections") + " - " + _("Texts")
        order_with_respect_to = "collection"
        unique_together = ("collection", "text")
        db_table = "books_" + "collection_texts"
"""


class Edition(models.Model):
    work = models.ForeignKey(Work, related_name="all_editions", on_delete=models.CASCADE, null=True, blank=True)
    text = models.ForeignKey(Text, related_name="editions", on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(verbose_name=_("Title"), max_length=150, blank=False)
    year = models.IntegerField(verbose_name=_("Year"), blank=True, null=True)  # TODO: change to date
    publisher = models.CharField(verbose_name=_("Publisher"), max_length=100, blank=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)
    notreadable = models.BooleanField(verbose_name=_("Not readable"), default=False)
    # TODO: auteur credit

    @property
    def language(self):
        if hasattr(self, "text") and self.text:
            return self.text.lang
        else:
            lang = None
            for editionText in self.texts.all():
                if lang is None:
                    lang = editionText.text.lang
                elif lang != editionText.text.lang:
                    return None
            return lang

    @property
    def authors(self):
        if not hasattr(self, "authors_saved"):
            self.authors_saved = []
            if hasattr(self, "text") and self.text:
                if hasattr(self.text, "work") and self.text.work:
                    return self.text.work.authors.all()
                elif hasattr(self.text, "translation") and self.text.translation:
                    return self.text.translation.original_work.authors.all()
            else:
                for editionText in self.texts.filter(role="M").select_related("text", "text__work", "text__translation", "text__translation__original_work").prefetch_related("text__work__authors", "text__translation__original_work__authors").all():
                    if hasattr(editionText.text, "work") and editionText.text.work:
                        for author in editionText.text.work.authors.all():
                            if author not in self.authors_saved:
                                self.authors_saved.append(author)
                    elif hasattr(editionText.text, "translation") and editionText.text.translation:
                        for author in editionText.text.translation.original_work.authors.all():
                            if author not in self.authors_saved:
                                self.authors_saved.append(author)

        return self.authors_saved

    @property
    def authors_prefetched(self):
        if not hasattr(self, "authors_saved"):
            self.authors_saved = []
            if hasattr(self, "text") and self.text:
                if hasattr(self.text, "work") and self.text.work:
                    return self.text.work.authors.all()
                elif hasattr(self.text, "translation") and self.text.translation:
                    return self.text.translation.original_work.authors.all()
            else:
                for editionText in self.texts.all():
                    if editionText.role != "M":
                        continue
                    if hasattr(editionText.text, "work") and editionText.text.work:
                        for author in editionText.text.work.authors.all():
                            if author not in self.authors_saved:
                                self.authors_saved.append(author)
                    elif hasattr(editionText.text, "translation") and editionText.text.translation:
                        for author in editionText.text.translation.original_work.authors.all():
                            if author not in self.authors_saved:
                                self.authors_saved.append(author)

        return self.authors_saved

    @property
    def translators(self):
        if not hasattr(self, "translators_saved"):
            self.translators_saved = []
            if hasattr(self, "text") and self.text:
                if hasattr(self.text, "translation") and self.text.translation:
                    return self.text.translation.translators.all()
            else:
                for editionText in self.texts.select_related("text", "text__translation").prefetch_related("text__translation__translators").all():
                    if hasattr(editionText.text, "translation") and editionText.text.translation:
                        for translator in editionText.text.translation.translators.all():
                            if translator not in self.translators_saved:
                                self.translators_saved.append(translator)

        return self.translators_saved

    def __str__(self):
        result = self.title
        if self.year:
            result += " (" + str(self.year) + ")"
        return result

    class Meta:
        verbose_name = _("Edition")
        verbose_name_plural = _("Editions")
        ordering = ["year"]


class EditionText(models.Model):  # TODO => CollectionText
    edition = models.ForeignKey(Edition, related_name="texts", on_delete=models.CASCADE)
    text = models.ForeignKey(Text, verbose_name=_("Text"), related_name="collections", on_delete=models.CASCADE)
    title = models.CharField(verbose_name=_("Title"), max_length=150, blank=False)
    role = models.CharField(verbose_name=_("Role"), max_length=1, choices=(("M", _("Main text")), ("B", _("Preface")), ("A", _("Postface")), ("N", _("Notes")), ("O", _("Other")), ("I", _("Introduction"))), default="M")

    def __str__(self):
        result = self.title
        result += _(" in %(edition)s") % {"edition": self.edition.title}
        return result

    class Meta:
        verbose_name = _("Edition") + " - " + _("Text")
        verbose_name_plural = _("Editions") + " - " + _("Texts")
        order_with_respect_to = "edition"
        unique_together = ("edition", "text")
        db_table = "books_" + "edition_texts"


class EditionAttribute(Attribute):
    edition = models.ForeignKey(Edition, related_name="attributes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Edition") + " - " + _("Attribute")
        verbose_name_plural = _("Editions") + " - " + _("Attributes")
        db_table = "books_" + "edition_attributes"


class EditionLink(Link):
    edition = models.ForeignKey(Edition, related_name="links", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Edition") + " - " + _("Link")
        verbose_name_plural = _("Editions") + " - " + _("Links")
        db_table = "books_" + "edition_links"


class Volume(models.Model):
    edition = models.ForeignKey(Edition, related_name="volumes", on_delete=models.CASCADE)
    isbn = models.CharField(verbose_name=_("ISBN"), max_length=20, null=True, blank=True)
    number = models.CharField(verbose_name=_("Number"), max_length=10, null=True, blank=True)
    pages = models.IntegerField(verbose_name=_("Pages"), blank=True, null=True)
    title = models.CharField(verbose_name=_("Title"), max_length=150, blank=False)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    image = models.ImageField(verbose_name=_("Image"), upload_to=random_filename, null=True, blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return str(self.edition)

    class Meta:
        verbose_name = _("Volume")
        verbose_name_plural = _("Volumes")


class Book(models.Model):
    volume = models.ForeignKey(Volume, related_name="books", on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name=_("User"), related_name="books", on_delete=models.CASCADE)
    date = models.CharField(verbose_name=_("Added date"), max_length=10, null=True, blank=True)
    printingdate = models.DateField(verbose_name=_("Printing date"), null=True, blank=True)
    origin = models.CharField(verbose_name=_("Origin"), max_length=1, choices=(("U", _("Unknown")), ("B", _("Bought")), ("G", _("Given")), ("F", _("Found"))), default="U")
    info = models.TextField(verbose_name=_("Information"), blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return str(self.volume) + _(" of ") + str(self.user)

    class Meta:
        verbose_name = _("Book")
        verbose_name_plural = _("Books")
        permissions = (
            ("delete_any_book", "Delete book of any user"),
            ("view_all_books", "View books of all users"),
        )


class Reading(models.Model):
    text = models.ForeignKey(Text, verbose_name=_("Text"), related_name="readings", on_delete=models.SET_NULL, null=True, blank=True)
    edition = models.ForeignKey(Edition, related_name="readings", on_delete=models.SET_NULL, null=True, blank=True)
    volume = models.ForeignKey(Volume, related_name="readings", on_delete=models.SET_NULL, null=True, blank=True)
    book = models.ForeignKey(Book, related_name="readings", on_delete=models.SET_NULL, null=True, blank=True) # TODO: remove
    #date_start = models.DateField(verbose_name=_("Date start"), null=True, blank=True)
    #date_end = models.DateField(verbose_name=_("Date start"), null=True, blank=True)
    date_start = models.CharField(verbose_name=_("Date start"), max_length=10, null=True, blank=True)
    date_end = models.CharField(verbose_name=_("Date end"), max_length=10, null=True, blank=True)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)
    user = models.ForeignKey(User, verbose_name=_("User"), related_name="readings", on_delete=models.CASCADE)

    def __str__(self):
        if self.book:
            return "%s, %s by %s" % (str(self.book), str(self.date_start), str(self.user))
        elif self.volume:
            return "%s, %s by %s" % (str(self.volume), str(self.date_start), str(self.user))
        elif self.edition:
            return "%s, %s by %s" % (str(self.edition), str(self.date_start), str(self.user))
        else:
            return "%s, %s by %s" % (str(self.text), str(self.date_start), str(self.user))

    class Meta:
        verbose_name = _("Reading")
        verbose_name_plural = _("Readings")
        ordering = ["-date_start"]
        db_table = "books_" + "book_readings"


class Saga(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=100, blank=False)
    title_vo = models.CharField(verbose_name=_("Original title"), max_length=100, blank=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = pgettext("The saga", "Saga")
        verbose_name_plural = _("Sagas")
        ordering = ["title"]


class SagaWork(models.Model):
    saga = models.ForeignKey(Saga, related_name="works", on_delete=models.CASCADE)
    work = models.ForeignKey(Work, verbose_name=_("Work"), related_name="sagas", on_delete=models.CASCADE)

    class Meta:
        verbose_name = pgettext("The saga", "Saga") + " - " + _("Work")
        verbose_name_plural = pgettext("The saga", "Saga") + " - " + _("Works")
        order_with_respect_to = "saga"
        unique_together = ("saga", "work")
        db_table = "books_" + "saga_works"


class List(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=100, blank=False)
    user = models.ForeignKey(User, verbose_name=_("User"), related_name="booklists", on_delete=models.CASCADE)
    info = models.TextField(verbose_name=_("Information"), blank=True)

    def __str__(self):
        return str(self.name) + _(" by ") + str(self.user)

    class Meta:
        verbose_name = _("List")
        verbose_name_plural = _("Lists")
        db_table = "books_" + "list"


class ListVolume(models.Model):
    list = models.ForeignKey(List, related_name="volumes", on_delete=models.CASCADE)
    volume = models.ForeignKey(Volume, verbose_name=_("Volume"), related_name="lists", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("List") + " - " + _("Volume")
        verbose_name_plural = _("List") + " - " + _("Volumes")
        unique_together = ("list", "volume")
        db_table = "books_" + "list_volume"
