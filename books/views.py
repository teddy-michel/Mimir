
from django.contrib.auth.decorators import login_required, permission_required
from django.db import IntegrityError
from django.db.models import Q, Prefetch
from django.forms import modelformset_factory
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone, dateformat
from django.utils.translation import gettext as _, pgettext
from django.views.decorators.http import require_safe

from base.images import resize_image, remove_old_image
from base.api import generic_APIList, generic_APISearch
from base.models import Person
from books.export import exportReadings, backupReadings, backupBooks
from .forms import WorkForm, ReadingForm, TextTagForm, TextAttributeForm, SagaWorkForm, SagaForm, EditionForm, TextTagFormClassic, \
    TranslationForm, EditionAttributeForm, BookForm, TextLinkForm, EditionLinkForm, VolumeForm, EditionTextForm, EditionTextReadOnlyForm, WorkReferenceForm, WorkReferenceReadOnlyForm, ListForm
from .models import Work, Reading, Saga, SagaWork, Edition, Translation, EditionAttribute, Book, Volume, Text, \
    EditionText, TextAttribute, TextTag, TextLink, EditionLink, WorkReference, List, ListVolume


@login_required
@require_safe
def index(request):
    works = Work.objects.prefetch_related("authors").all().order_by("-modification")[0:10]
    books = []
    readings = []

    if request.user.is_authenticated:
        readings_in_progress_queryset = request.user.readings.select_related("edition", "text__work", "text__translation", "text__translation__original_work", "book").prefetch_related("text__work__authors", "text__translation__original_work__authors").filter(date_end__isnull=True).order_by("-date_start")
        for reading in readings_in_progress_queryset:
            if len(readings) < 10:
                readings.append(reading)

        last_readings_queryset = request.user.readings.select_related("edition", "text__work", "text__translation", "text__translation__original_work", "book").prefetch_related("text__work__authors", "text__translation__original_work__authors").order_by("-date_end")[0:10]
        for reading in last_readings_queryset:
            if len(readings) < 10:
                readings.append(reading)

        edition_texts_queryset = EditionText.objects.select_related("text", "text__work", "text__translation", "text__translation__original_work").prefetch_related("text__work__authors", "text__translation__original_work__authors")
        books_queryset = request.user.books.select_related("volume", "volume__edition", "volume__edition__text", "volume__edition__text__work", "volume__edition__text__translation", "volume__edition__text__translation__original_work").prefetch_related(Prefetch("volume__edition__texts", edition_texts_queryset), "volume__edition__text__work__authors", "volume__edition__text__translation__original_work__authors").order_by("-date")[0:9]
        for book in books_queryset:
            books.append(book)

    return render(request, "books/index.html", {"works": works, "readings": readings, "books": books})


@login_required
@require_safe
def worksList(request):
    #collections = Edition.objects.filter(text__isnull=True)
    #return render(request, "books/works_list.html", {"collections": collections})
    return render(request, "books/works_list.html")


@login_required
@require_safe
def worksAPIList(request):
    columns = ["title", "year", "authors", "genre"]

    query = request.GET.get("q", "").strip()
    if query:
        query_set = Work.objects.select_related("lang", "genre").filter(title__icontains=query)
    else:
        query_set = Work.objects.select_related("lang", "genre").all()

    # TODO: filter by genre

    def formatter(work):
        authors = []
        for author in work.authors.all():
            authors.append({
                "id":     author.id,
                "name":   author.name,
                "gender": author.gender,
                "url":    reverse("person_info", kwargs={"id": "%06d" % author.id}),
            })

        return {
            "id":      work.id,
            "title":   work.title,
            "url":     reverse("work_info", kwargs={"id": "%06d" % work.id}),
            "year":    work.year,
            "lang":    work.lang.code if work.lang else "",
            "authors": authors,
            "genre":   str(work.genre) if work.genre else _("Unknown"),
        }

    return generic_APIList(request, columns, query_set, formatter)


@login_required
@require_safe
def editionAPIList(request):
    columns = ["title", "year", "authors"]

    query = request.GET.get("q", "").strip()
    if query:
        query_set = Edition.objects.filter(title__icontains=query)
    else:
        query_set = Edition.objects.all()

    if "only_collections" in request.GET:
        query_set = query_set.filter(text__isnull=True)

    def formatter(edition):
        authors = []
        for author in edition.authors:
            authors.append({
                "id":     author.id,
                "name":   author.name,
                "gender": author.gender,
                "url":    reverse("person_info", kwargs={"id": "%06d" % author.id}),
            })

        return {
            "id":      edition.id,
            "title":   edition.title,
            "url":     reverse("edition_info", kwargs={"id": "%06d" % edition.id}),
            "year":    edition.year,
            "authors": authors,
        }

    return generic_APIList(request, columns, query_set, formatter)


@login_required
@permission_required("books.add_work")
def workCreate(request):
    if request.method == "POST":
        form = WorkForm(request.POST, request.FILES)

        if "authors_select" in request.POST:
            authors = {author: None for author in request.POST.getlist("authors_select")}
            for author in authors.keys():
                person = Person.objects.get(id=author)
                authors[author] = person
            form.fields["authors_select"].choices = [(id, name) for id, name in authors.items()]
        else:
            authors = {}

        if form.is_valid():
            work = form.save()

            work.search = work.title
            if work.title_vo:
                work.search += "#" + work.title_vo
            if work.author_credit:
                work.search += "#" + work.author_credit

            for author in authors.values():
                work.authors.add(author)
                work.search += "#" + str(author)

            work.save()

            return redirect("work_info", "%06d" % work.id)
    else:
        form = WorkForm()

    return render(request, "books/work_create.html", {"form": form})


@login_required
@require_safe
def worksAPISearch(request):
    query = request.GET.get("q", "").strip()

    works = Work.objects.filter(search__icontains=query).select_related("lang")

    excludeWorksId = set()
    if "exclude" in request.GET:
        for id in request.GET["exclude"].split(","):
            try:
                id = int(id)
                excludeWorksId.add(id)
            except:
                continue
        #works = works.exclude(id__in=excludeWorksId)

    total = works.count()

    # Tri
    works = works.order_by("title")

    # Pagination
    itemPerPage = 10

    try:
        page = int(request.GET.get("page", 1))
    except:
        page = 1

    start = (page - 1) * itemPerPage

    try:
        length = int(request.GET.get("length", itemPerPage))
        if length == 0:
            return HttpResponseBadRequest("Invalid field 'length'.")
    except:
        length = itemPerPage

    if length < 0:
        works = works[start:]
    else:
        works = works[start:start+length]

    result = {
        "total_count": total,
        "start": start,
        "page": page,
        "incomplete_results": False,
        "items": [],
    }

    for work in works:
        authors = []
        for author in work.authors.all():
            authors.append({
                "id":     author.id,
                "name":   author.name,
                "gender": author.gender,
                "url":    reverse("person_info", kwargs={"id": "%06d" % author.id}),
            })

        data = {
            "id":      work.id,
            "title":   work.title,
            "url":     reverse("work_info", kwargs={"id": "%06d" % work.id}),
            "year":    work.year,
            "lang":    work.lang.code if work.lang else "",
            "authors": authors,
        }

        if work.id in excludeWorksId:
            data["disabled"] = True

        result["items"].append(data)

    result["length"] = len(result["items"])
    if start + result["length"] < total:
        result["incomplete_results"] = True

    return JsonResponse(result)


@login_required
@require_safe
def workInfo(request, id):
    persons_queryset = Person.objects.only("id", "name", "lastname", "firstname", "gender").order_by()
    collection_queryset = EditionText.objects.select_related("edition", "text", "text__translation", "text__translation__lang").prefetch_related(Prefetch("text__translation__translators", persons_queryset)).order_by("edition__year")
    translation_queryset = Translation.objects.only("id", "original_work", "lang", "year").select_related("lang").prefetch_related(Prefetch("collections", collection_queryset), Prefetch("translators", persons_queryset)).order_by("year")
    edition_queryset = Edition.objects.defer("info", "creation", "modification").select_related("text", "text__translation", "text__translation__lang").prefetch_related(Prefetch("text__translation__translators", persons_queryset))

    try:
        work = Work.objects.select_related("lang").prefetch_related(Prefetch("all_editions", edition_queryset), Prefetch("collections", collection_queryset), Prefetch("translations", translation_queryset), Prefetch("authors", persons_queryset), "refs").get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    if request.user.is_authenticated:
        readings = request.user.readings.select_related("user", "text").filter(Q(text=work) | Q(text__translation__isnull=False, text__translation__original_work=work))
    else:
        readings = Reading.objects.none()

    collections = []
    collections_translation = []

    for editionText in work.collections.all():
        collections.append(editionText)
        if request.user.is_authenticated:
            readings2 = editionText.edition.readings.select_related("user", "text").filter(user=request.user)
            readings |= readings2

    for translation in work.translations.all():
        for editionText in translation.collections.all():
            collections_translation.append(editionText)
            if request.user.is_authenticated:
                readings2 = editionText.edition.readings.select_related("user", "text").filter(user=request.user)
                readings |= readings2

    tags = work.tags.select_related("tag")
    links = work.links.select_related("lang")

    return render(request, "books/work_info.html", {"work": work, "readings": readings, "collections": collections, "collections_translation": collections_translation, "tags": tags, "links": links})


@login_required
@permission_required("books.change_work")
def workEdit(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    authors_initial = {author.id: author.name for author in work.authors.all()}

    if request.method == "POST":
        form = WorkForm(request.POST, request.FILES, instance=work, initial={"authors_select": list(authors_initial)})

        if "authors_select" in request.POST:
            authors = {author: None for author in request.POST.getlist("authors_select")}
            for author in authors.keys():
                person = Person.objects.get(id=author)
                authors[author] = person
            form.fields["authors_select"].choices = [(id, name) for id, name in authors.items()]
        else:
            authors = {}

        if form.is_valid():
            if form.has_changed():
                work = form.save()

                work.search = work.title
                if work.title_vo:
                    work.search += "#" + work.title_vo
                if work.author_credit:
                    work.search += "#" + work.author_credit

                work.authors.through.objects.filter(work=work).delete()
                for author in authors.values():
                    work.authors.add(author)
                    work.search += "#" + str(author)

                work.save()

            return redirect("work_info", "%06d" % work.id)
    else:
        form = WorkForm(instance=work, initial={"authors_select": list(authors_initial)})
        form.fields["authors_select"].choices = [(id, name) for id, name in authors_initial.items()]

    return render(request, "books/work_edit.html", {"work": work, "form": form})


@login_required
@permission_required("books.delete_work")
def workDelete(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        if "delete" in request.POST:
            work.delete()
        return redirect("works_list")

    return render(request, "books/work_delete.html", {"work": work})


@login_required
@permission_required("books.add_reading")
def workAddReading(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = ReadingForm(request.POST)

        if form.is_valid():
            reading = form.save(commit=False)
            if reading:
                reading.text = work
                reading.user = request.user
                reading.save()
                backupReadings(request.user)
            return redirect("work_info", "%06d" % work.id)
    else:
        form = ReadingForm(initial={"date": dateformat.format(timezone.now(), "Y-m-d")})

    url = reverse("work_add_reading", kwargs={"id": "%06d" % work.id})
    return render(request, "books/work_add_reading.html", {"work": work, "form": form, "form_url": url})


@login_required
@permission_required("books.add_translation")
def workAddTranslation(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = TranslationForm(request.POST)

        if "translators_select" in request.POST:
            translators = {translator: None for translator in request.POST.getlist("translators_select")}
            for translator in translators.keys():
                person = Person.objects.get(id=translator)
                translators[translator] = person
            form.fields["translators_select"].choices = [(id, name) for id, name in translators.items()]
        else:
            translators = {}

        if form.is_valid():
            translation = form.save(commit=False)
            if translation:
                translation.original_work = work
                translation.save()
                for translator in translators.values():
                    translation.translators.add(translator)
            return redirect("translation_info", "%06d" % translation.id)
    else:
        form = TranslationForm()

    url = reverse("work_add_translation", kwargs={"id": "%06d" % work.id})
    return render(request, "books/work_add_translation.html", {"work": work, "form": form, "form_url": url})


@login_required
@permission_required("books.add_edition")
def workAddEdition(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = EditionForm(request.POST, work=work)

        if form.is_valid():
            edition = form.save(commit=False)
            if edition:
                edition.text = work
                edition.work = work
                edition.save()
                volume = Volume.objects.create(edition=edition, title=edition.title)
                return redirect("edition_info", "%06d" % edition.id)
            return redirect("work_info", "%06d" % work.id)
    else:
        form = EditionForm(initial={"title": work.title}, work=work)

    return render(request, "books/work_add_edition.html", {"work": work, "form": form})


@login_required
@permission_required("books.add_textattribute")
def workAddAttribute(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = TextAttributeForm(request.POST)

        if form.is_valid():
            attribute = form.save(commit=False)
            if attribute:
                attribute.text = work
                attribute.save()
            return redirect("work_info", "%06d" % work.id)
    else:
        form = TextAttributeForm()

    url = reverse("work_add_attribute", kwargs={"id": "%06d" % work.id})
    return render(request, "books/work_add_attribute.html", {"work": work, "form": form, "form_url": url})


@login_required
@permission_required("books.add_workreference")
def workAddRef(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = WorkReferenceForm(request.POST)

        if form.is_valid():
            ref = form.save(commit=False)
            if ref:
                ref.work = work
                ref.save()
            return redirect("work_info", "%06d" % work.id)
    else:
        form = WorkReferenceForm()

    return render(request, "books/work_add_ref.html", {"work": work, "form": form})


@login_required
@permission_required("books.add_texttag")
def workAddTag(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = TextTagForm(request.POST)

        if form.is_valid():
            text_tag = form.save(commit=False)
            if text_tag:
                text_tag.text = work
                text_tag.save()
            return redirect("work_info", "%06d" % work.id)
    else:
        form = TextTagForm()

    url = reverse("work_add_tag", kwargs={"id": "%06d" % work.id})
    return render(request, "books/work_add_tag.html", {"work": work, "form": form, "form_url": url})


@login_required
@permission_required("books.add_textlink")
def workAddLink(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = TextLinkForm(request.POST)

        if form.is_valid():
            link = form.save(commit=False)
            if link:
                link.text = work
                link.save()
            return redirect("work_info", "%06d" % work.id)
    else:
        form = TextLinkForm()

    url = reverse("work_add_link", kwargs={"id": "%06d" % work.id})
    return render(request, "books/work_add_link.html", {"work": work, "form": form, "form_url": url})


@login_required
@permission_required("books.change_reading")
def workEditReadings(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    readings = work.readings.filter(user=request.user)

    if readings.count() == 0:
        extra = 1
    else:
        extra = 0

    ReadingFormSet = modelformset_factory(Reading, form=ReadingForm, extra=extra, can_delete=True)
    # ATTENTION: ReadingFormSet n'est pas optimisé. La liste des langues est requêtée pour chaque entrée !

    if request.method == "POST":
        formset = ReadingFormSet(request.POST, queryset=readings)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.text = work
                instance.user = request.user
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            backupReadings(request.user)
            return redirect("work_info", "%06d" % work.id)
    else:
        formset = ReadingFormSet(queryset=readings)

    url = reverse("work_edit_readings", kwargs={"id": "%06d" % work.id})
    title = _("Edit readings")
    return render(request, "books/work_edit_data.html", {"work": work, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("books.change_textattribute")
def workEditAttributes(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    attributes = work.attributes.all()

    if attributes.count() == 0:
        extra = 1
    else:
        extra = 0

    TextAttributeFormSet = modelformset_factory(TextAttribute, form=TextAttributeForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = TextAttributeFormSet(request.POST, queryset=attributes)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.text = work
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            return redirect("work_info", "%06d" % work.id)
    else:
        formset = TextAttributeFormSet(queryset=attributes)

    url = reverse("work_edit_attributes", kwargs={"id": "%06d" % work.id})
    title = _("Edit attributes")
    return render(request, "books/work_edit_data.html", {"work": work, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("books.change_workreference")
def workEditRefs(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("movies_list")

    refs = work.refs.select_related("ref_movie", "ref_serie", "ref_person", "ref_book", "ref_book__genre", "ref_game").prefetch_related("ref_book__authors").all()
    WorkReferenceFormSet = modelformset_factory(WorkReference, form=WorkReferenceReadOnlyForm, extra=0, can_delete=True)

    if request.method == "POST":
        formset = WorkReferenceFormSet(request.POST, queryset=refs)

        if formset.is_valid():
            formset.save()
            return redirect("work_info", "%06d" % work.id)
    else:
        formset = WorkReferenceFormSet(queryset=refs)

    url = reverse("work_edit_refs", kwargs={"id": "%06d" % work.id})
    title = _("Edit references")
    return render(request, "books/work_edit_data.html", {"work": work, "title": title, "url_form": url, "formset": formset, "can_add": False})


@login_required
@permission_required("books.change_texttag")
def workEditTags(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    tags = work.tags.all()

    if tags.count() == 0:
        extra = 1
    else:
        extra = 0

    TextTagFormSet = modelformset_factory(TextTag, form=TextTagFormClassic, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = TextTagFormSet(request.POST, queryset=tags)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.text = work
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            return redirect("work_info", "%06d" % work.id)
    else:
        formset = TextTagFormSet(queryset=tags)

    url = reverse("work_edit_tags", kwargs={"id": "%06d" % work.id})
    title = _("Edit tags")
    return render(request, "books/work_edit_data.html", {"work": work, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("books.change_textlink")
def workEditLinks(request, id):
    try:
        work = Work.objects.get(id=id)
    except Work.DoesNotExist:
        return redirect("works_list")

    links = work.links.all()

    if links.count() == 0:
        extra = 1
    else:
        extra = 0

    TextLinkFormSet = modelformset_factory(TextLink, form=TextLinkForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = TextLinkFormSet(request.POST, queryset=links)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.text = work
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            return redirect("work_info", "%06d" % work.id)
    else:
        formset = TextLinkFormSet(queryset=links)

    url = reverse("work_edit_links", kwargs={"id": "%06d" % work.id})
    title = _("Edit links")
    return render(request, "books/work_edit_data.html", {"work": work, "title": title, "url_form": url, "formset": formset, "can_add": True})


@login_required
@permission_required("books.add_edition")
def editionCreateCollection(request):
    if request.method == "POST":
        form = EditionForm(request.POST)

        if form.is_valid():
            edition = form.save()
            volume = Volume.objects.create(edition=edition, title=edition.title)
            return redirect("edition_info", "%06d" % edition.id)
    else:
        form = EditionForm()

    return render(request, "books/edition_collection_create.html", {"form": form})


@login_required
@require_safe
def editionInfo(request, id):
    try:
        edition = Edition.objects.select_related("text", "text__work", "text__translation", "text__translation__original_work").prefetch_related("text__work__authors", "text__translation__translators", "text__translation__original_work__authors").get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    volumes = edition.volumes.all()
    volume = Volume.objects.none()
    if len(volumes) == 1:
        volume = volumes[0]

    readings_queryset = Reading.objects.filter(user=request.user, date_end__isnull=False).order_by()
    collections_queryset = EditionText.objects.select_related("edition").prefetch_related(Prefetch("edition__readings", readings_queryset))
    texts_queryset = edition.texts.select_related("text", "text__work", "text__translation", "text__translation__lang", "text__translation__original_work").prefetch_related("text__work__authors", "text__translation__translators", "text__translation__original_work__authors", Prefetch("text__work__readings", readings_queryset), Prefetch("text__work__collections", collections_queryset), Prefetch("text__translation__readings", readings_queryset), Prefetch("text__translation__collections", collections_queryset)).all()

    texts = []
    for edition_text in texts_queryset:
        edition_text.text.read = False
        if request.user.is_authenticated:
            if hasattr(edition_text.text, "work") and edition_text.text.work:
                edition_text.text.work.read = edition_text.text.work.read_by_user()
                edition_text.text.read = edition_text.text.work.read
            elif hasattr(edition_text.text, "translation") and edition_text.text.translation:
                edition_text.text.translation.read = edition_text.text.translation.read_by_user()
                edition_text.text.read = edition_text.text.translation.read
        texts.append(edition_text)

    readings = request.user.readings.filter(edition=edition)

    if volume:
        volume_lists = list(volume.lists.filter(list__user=request.user).values_list("list__id", flat=True))
        lists = []
        for booklist in request.user.booklists.all():
            booklist.disabled = (booklist.id in volume_lists)
            lists.append(booklist)
    else:
        lists = []

    if volume:
        if request.user.has_perm("books.view_all_books"):
            books = volume.books.select_related("user")
        else:
            books = request.user.books.select_related("user").filter(volume=volume)
    else:
        books = Book.objects.none()

    return render(request, "books/edition_info.html", {"edition": edition, "volumes": volumes, "volume": volume, "texts": texts, "readings": readings, "books": books, "lists": lists})


@login_required
@permission_required("books.change_edition")
def editionEdit(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = EditionForm(request.POST, instance=edition)

        if form.is_valid():
            if form.has_changed():
                edition = form.save()
            return redirect("edition_info", "%06d" % edition.id)
    else:
        form = EditionForm(instance=edition)

    return render(request, "books/edition_edit.html", {"edition": edition, "form": form})


@login_required
@permission_required("books.delete_edition")
def editionDelete(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        work = edition.work
        if "delete" in request.POST:
            edition.delete()
        if work:
            return redirect("work_info", "%06d" % work.id)
        else:
            return redirect("works_list")

    return render(request, "books/edition_delete.html", {"edition": edition})


@login_required
@permission_required("books.add_edition")
def editionDuplicate(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST" and request.POST.get("duplicate", "1") == "1":
        new_edition = Edition.objects.create(work=edition.work, text=edition.text, title=edition.title, year=edition.year, publisher=edition.publisher, info=edition.info)

        # Duplicate texts
        for etext in edition.texts.all():
            new_edition.texts.create(edition=new_edition, text=etext.text, title=etext.title, role=etext.role)

        # Duplicate volumes
        for volume in edition.volumes.all():
            new_edition.volumes.create(edition=new_edition, isbn=volume.isbn, title=volume.title, info=volume.info, pages=volume.pages, number=volume.number)
            # TODO: duplicate image file?

        # Duplicate attributes
        for attribute in edition.attributes.all():
            new_edition.attributes.create(edition=new_edition, name=attribute.name, value=attribute.value)

        # Duplicate links
        for link in edition.links.all():
            new_edition.links.create(edition=new_edition, name=link.name, uri=link.uri, lang=link.lang)

        return redirect("edition_info", "%06d" % new_edition.id)

    return render(request, "books/edition_duplicate.html", {"edition": edition})


@login_required
@permission_required("books.change_edition")
def editionTransformToCollection(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if edition.work is None:
        # This edition is already a collection
        return redirect("edition_info", "%06d" % edition.id)

    if request.method == "POST" and request.POST.get("transform", "1") == "1":
        edition.texts.create(edition=edition, text=edition.text, title=edition.title)
        edition.work = None
        edition.text = None
        edition.save()

        # Update readings (TODO: test)
        for reading in edition.readings.all():
            reading.text = None
            reading.save()

        return redirect("edition_info", "%06d" % edition.id)

    return render(request, "books/edition_to_collection.html", {"edition": edition})


@login_required
@permission_required("books.change_edition")
def editionTransformToEdition(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if edition.work is not None:
        # This edition is not a collection
        return redirect("edition_info", "%06d" % edition.id)

    if edition.texts.count() != 1:
        # This collection has more than one text
        return redirect("edition_info", "%06d" % edition.id)

    if request.method == "POST" and request.POST.get("transform", "1") == "1":
        text = edition.texts.all()[0]
        if hasattr(text, "translation"):  # TODO: test
            work = text.translation.original_work
        else:
            work = text

        edition.text = text
        edition.work = work
        edition.save()
        edition.texts.all().delete()  # TODO: test

        # Update readings (TODO: test)
        for reading in edition.readings.all():
            reading.text = text
            reading.save()

        return redirect("edition_info", "%06d" % edition.id)

    return render(request, "books/edition_to_edition.html", {"edition": edition})


@login_required
@permission_required("books.add_editionwork")
def editionAddText(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = EditionTextForm(request.POST)

        if form.is_valid():
            edition_work = form.save(commit=False)
            if edition_work:
                edition_work.edition = edition

                try:
                    edition_work.save()
                except IntegrityError:
                    pass  # Duplicated text

            return redirect("edition_info", "%06d" % edition.id)
    else:
        form = EditionTextForm()

    url = reverse("edition_add_text", kwargs={"id": "%06d" % edition.id})
    return render(request, "books/edition_add_text.html", {"edition": edition, "form": form, "form_url": url})


def _checkVolumeImages():
    # List images used by volumes and resize them if necessary
    images = []
    for volume in Volume.objects.all():
        if volume.image:
            resize_image(str(volume.image))
            images.append(str(volume.image))

    # Remove unused images
    for file in os.listdir(os.path.join(settings.MEDIA_ROOT, "books")):
        filename = os.path.join(settings.MEDIA_ROOT, "books", file)
        if not os.path.isfile(filename):
            continue
        if "books/" + file not in images:
            os.remove(filename)

    # Remove unused images
    for file in os.listdir(os.path.join(settings.MEDIA_ROOT, "books", "original")):
        filename = os.path.join(settings.MEDIA_ROOT, "books", "original", file)
        if not os.path.isfile(filename):
            continue
        if "books/" + file not in images:
            os.remove(filename)


@login_required
@permission_required("books.add_volume")
def editionAddVolume(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = VolumeForm(request.POST)

        if form.is_valid():
            volume = form.save(commit=False)
            if volume:
                volume.edition = edition
                volume.save()

                if volume.image is not None:
                    #_checkVolumeImages()
                    resize_image(str(volume.image))

                return redirect("volume_info", "%06d" % edition.id, "%06d" % volume.id)
            else:
                return redirect("edition_info", "%06d" % edition.id)
    else:
        form = VolumeForm(initial={"title": edition.title})

    url = reverse("edition_add_volume", kwargs={"id": "%06d" % edition.id})
    return render(request, "books/edition_add_volume.html", {"edition": edition, "form": form, "form_url": url})


@login_required
@permission_required("books.add_reading")
def editionAddReading(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = ReadingForm(request.POST)

        if form.is_valid():
            reading = form.save(commit=False)
            if reading:
                reading.text = edition.text
                reading.edition = edition
                reading.user = request.user
                reading.save()
                backupReadings(request.user)
            return redirect("edition_info", "%06d" % edition.id)
    else:
        form = ReadingForm(initial={"date": dateformat.format(timezone.now(), "Y-m-d")})

    url = reverse("edition_add_reading", kwargs={"id": "%06d" % edition.id})
    return render(request, "books/edition_add_reading.html", {"edition": edition, "form": form, "form_url": url})


@login_required
@permission_required("books.add_editionattribute")
def editionAddAttribute(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = EditionAttributeForm(request.POST)

        if form.is_valid():
            attribute = form.save(commit=False)
            if attribute:
                attribute.edition = edition
                attribute.save()
            return redirect("edition_info", "%06d" % edition.id)
    else:
        form = EditionAttributeForm()

    url = reverse("edition_add_attribute", kwargs={"id": "%06d" % edition.id})
    return render(request, "books/edition_add_attribute.html", {"edition": edition, "form": form, "form_url": url})


@login_required
@permission_required("books.add_editionlink")
def editionAddLink(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = EditionLinkForm(request.POST)

        if form.is_valid():
            link = form.save(commit=False)
            if link:
                link.edition = edition
                link.save()
            return redirect("edition_info", "%06d" % edition.id)
    else:
        form = EditionLinkForm()

    url = reverse("edition_add_link", kwargs={"id": "%06d" % edition.id})
    return render(request, "books/edition_add_link.html", {"edition": edition, "form": form, "form_url": url})


@login_required
@permission_required("books.change_editionwork")
def editionEditTexts(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    texts = edition.texts.all()
    EditionTextFormSet = modelformset_factory(EditionText, form=EditionTextReadOnlyForm, extra=0, can_delete=True, can_order=True)

    if request.method == "POST":
        formset = EditionTextFormSet(request.POST, queryset=texts)

        if formset.is_valid():
            for form in formset.ordered_forms:
                form.instance._order = form.cleaned_data["ORDER"] - 1
            formset.save()

            # Reordering texts
            order = 0
            edition = Edition.objects.get(id=id)
            for text in edition.texts.all():
                text._order = order
                order += 1
                text.save()

            return redirect("edition_info", "%06d" % edition.id)
    else:
        formset = EditionTextFormSet(queryset=texts)

    url = reverse("edition_edit_texts", kwargs={"id": "%06d" % edition.id})
    title = _("Edit texts")
    return render(request, "books/edition_edit_data.html", {"edition": edition, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("books.change_reading")
def editionEditReadings(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    readings = edition.readings.filter(user=request.user)

    if readings.count() == 0:
        extra = 1
    else:
        extra = 0

    ReadingFormSet = modelformset_factory(Reading, form=ReadingForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = ReadingFormSet(request.POST, queryset=readings)

        if formset.is_valid():
            formset.save_existing_objects()
            instances = formset.save_new_objects(commit=False)
            for instance in instances:
                instance.text = edition.text
                instance.edition = edition
                instance.user = request.user
                instance.save()
            backupReadings(request.user)
            return redirect("edition_info", "%06d" % edition.id)
    else:
        formset = ReadingFormSet(queryset=readings)

    url = reverse("edition_edit_readings", kwargs={"id": "%06d" % edition.id})
    title = _("Edit readings")
    return render(request, "books/edition_edit_data.html", {"edition": edition, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("books.change_editionattribute")
def editionEditAttributes(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    attributes = edition.attributes.all()

    if attributes.count() == 0:
        extra = 1
    else:
        extra = 0

    EditionAttributeFormSet = modelformset_factory(EditionAttribute, form=EditionAttributeForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = EditionAttributeFormSet(request.POST, queryset=attributes)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.edition = edition
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            return redirect("edition_info", "%06d" % edition.id)
    else:
        formset = EditionAttributeFormSet(queryset=attributes)

    url = reverse("edition_edit_attributes", kwargs={"id": "%06d" % edition.id})
    title = _("Edit attributes")
    return render(request, "books/edition_edit_data.html", {"edition": edition, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("books.change_editionlink")
def editionEditLinks(request, id):
    try:
        edition = Edition.objects.get(id=id)
    except Edition.DoesNotExist:
        return redirect("works_list")

    links = edition.links.all()

    if links.count() == 0:
        extra = 1
    else:
        extra = 0

    EditionLinkFormSet = modelformset_factory(EditionLink, form=EditionLinkForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = EditionLinkFormSet(request.POST, queryset=links)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.edition = edition
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            return redirect("edition_info", "%06d" % edition.id)
    else:
        formset = EditionLinkFormSet(queryset=links)

    url = reverse("edition_edit_links", kwargs={"id": "%06d" % edition.id})
    title = _("Edit links")
    return render(request, "books/edition_edit_data.html", {"edition": edition, "title": title, "url_form": url, "formset": formset})


@login_required
@require_safe
def volumeInfo(request, id, vid):
    persons_queryset = Person.objects.only("id", "name", "lastname", "firstname", "gender").order_by()

    try:
        volume = Volume.objects.select_related("edition", "edition__text", "edition__text__work", "edition__text__translation", "edition__text__translation__original_work").prefetch_related(Prefetch("edition__text__work__authors", persons_queryset), Prefetch("edition__text__translation__original_work__authors", persons_queryset), Prefetch("edition__text__translation__translators", persons_queryset)).defer("edition__info", "edition__creation", "edition__modification").get(id=vid)
    except Volume.DoesNotExist:
        return redirect("works_list")

    try:
        id = int(id)
    except:
        return redirect("works_list")

    if volume.edition.id != id:
        return redirect("edition_info", "%06d" % id)

    readings = request.user.readings.filter(volume=volume)
    volume_lists = list(volume.lists.filter(list__user=request.user).values_list("list__id", flat=True))
    lists = []
    for booklist in request.user.booklists.all():
        booklist.disabled = (booklist.id in volume_lists)
        lists.append(booklist)

    if request.user.has_perm("books.view_all_books"):
        books = volume.books
    else:
        books = request.user.books.filter(volume=volume)

    return render(request, "books/volume_info.html", {"volume": volume, "books": books, "lists": lists, "readings": readings})


@login_required
@permission_required("books.change_volume")
def volumeEdit(request, id, vid):
    try:
        volume = Volume.objects.get(id=vid)
    except Volume.DoesNotExist:
        return redirect("works_list")

    try:
        id = int(id)
    except:
        return redirect("works_list")

    if volume.edition.id != id:
        return redirect("edition_info", "%06d" % id)

    if request.method == "POST":
        oldImage = str(volume.image) if volume.image is not None else ""
        form = VolumeForm(request.POST, request.FILES, instance=volume)

        if form.is_valid():
            if form.has_changed():
                volume = form.save()
                volume.modification = timezone.now()
                volume.save()

                # Resize image
                if volume.image is not None and str(volume.image) != "" and str(volume.image) != oldImage:
                    #_checkVolumeImages()
                    resize_image(str(volume.image))

                # Remove old image
                remove_old_image(oldImage, str(volume.image))

            return redirect("volume_info", "%06d" % volume.edition.id, "%06d" % volume.id)
    else:
        form = VolumeForm(instance=volume)

    return render(request, "books/volume_edit.html", {"volume": volume, "form": form})


@login_required
@permission_required("books.delete_volume")
def volumeDelete(request, id, vid):
    try:
        volume = Volume.objects.get(id=vid)
    except Volume.DoesNotExist:
        return redirect("works_list")

    try:
        id = int(id)
    except:
        return redirect("works_list")

    if volume.edition.id != id:
        return redirect("edition_info", "%06d" % id)

    # An edition must have a least one volume
    if volume.edition.volumes.count() <= 1:
        return redirect("volume_info", "%06d" % volume.edition.id, "%06d" % volume.id)

    if request.method == "POST":
        if "delete" in request.POST:
            volume.delete()
        return redirect("edition_info", "%06d" % volume.edition.id)

    return render(request, "books/volume_delete.html", {"volume": volume})


@login_required
@permission_required("books.add_book")
def volumeAddBook(request, id, vid):
    try:
        volume = Volume.objects.get(id=vid)
    except Volume.DoesNotExist:
        return redirect("works_list")

    try:
        id = int(id)
    except:
        return redirect("works_list")

    if volume.edition.id != id:
        return redirect("edition_info", "%06d" % id)

    if request.method == "POST":
        form = BookForm(request.POST)

        if form.is_valid():
            book = form.save(commit=False)
            if book:
                book.volume = volume
                book.user = request.user
                book.save()
                backupBooks(request.user)
            return redirect("book_info", "%06d" % book.id)
    else:
        form = BookForm()

    url = reverse("volume_add_book", kwargs={"id": "%06d" % volume.edition.id, "vid": "%06d" % volume.id})
    return render(request, "books/volume_add_book.html", {"volume": volume, "form": form, "form_url": url})


@login_required
@permission_required("books.add_reading")
def volumeAddReading(request, id, vid):
    try:
        volume = Volume.objects.get(id=vid)
    except Volume.DoesNotExist:
        return redirect("works_list")

    try:
        id = int(id)
    except:
        return redirect("works_list")

    if volume.edition.id != id:
        return redirect("edition_info", "%06d" % id)

    if request.method == "POST":
        form = ReadingForm(request.POST)

        if form.is_valid():
            reading = form.save(commit=False)
            if reading:
                reading.text = volume.edition.text
                reading.edition = volume.edition
                reading.volume = volume
                reading.user = request.user
                reading.save()
                backupReadings(request.user)
            return redirect("volume_info", "%06d" % volume.edition.id, "%06d" % volume.id)
    else:
        form = ReadingForm(initial={"date": dateformat.format(timezone.now(), "Y-m-d")})

    url = reverse("volume_add_reading", kwargs={"id": "%06d" % volume.edition.id, "vid": "%06d" % volume.id})
    return render(request, "books/volume_add_reading.html", {"volume": volume, "form": form, "form_url": url})


@login_required
@permission_required("books.change_reading")
def volumeEditReadings(request, id, vid):
    try:
        volume = Volume.objects.get(id=vid)
    except Volume.DoesNotExist:
        return redirect("works_list")

    try:
        id = int(id)
    except:
        return redirect("works_list")

    if volume.edition.id != id:
        return redirect("edition_info", "%06d" % id)

    readings = volume.readings.filter(user=request.user)

    if readings.count() == 0:
        extra = 1
    else:
        extra = 0

    ReadingFormSet = modelformset_factory(Reading, form=ReadingForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = ReadingFormSet(request.POST, queryset=readings)

        if formset.is_valid():
            formset.save_existing_objects()
            instances = formset.save_new_objects(commit=False)
            for instance in instances:
                instance.text = volume.edition.text
                instance.edition = volume.edition
                instance.volume = volume
                instance.user = request.user
                instance.save()
            backupReadings(request.user)
            return redirect("volume_info", "%06d" % volume.edition.id, "%06d" % volume.id)
    else:
        formset = ReadingFormSet(queryset=readings)

    url = reverse("volume_edit_readings", kwargs={"id": "%06d" % volume.edition.id, "vid": "%06d" % volume.id})
    title = _("Edit readings")
    return render(request, "books/volume_edit_data.html", {"volume": volume, "title": title, "url_form": url, "formset": formset})


@login_required
@require_safe
def booksList(request):
    #if request.user.has_perm("books.view_all_books"):
    #    books = Book.objects.select_related("volume", "volume__edition", "volume__edition__work", "user").all()

    readings_queryset = Reading.objects.select_related("book", "volume", "edition", "text").filter(user=request.user, date_end__isnull=False).order_by()
    edition_texts_queryset = EditionText.objects.select_related("text", "text__work", "text__translation", "text__translation__original_work").prefetch_related("text__work__authors", "text__translation__original_work__authors")

    books_queryset = request.user.books.select_related("volume", "volume__edition", "volume__edition__text", "volume__edition__text__work", "volume__edition__text__translation", "volume__edition__text__translation__lang", "volume__edition__text__translation__original_work", "user").prefetch_related(Prefetch("volume__edition__texts", edition_texts_queryset), "volume__edition__text__work__authors", "volume__edition__text__translation__original_work__authors", "volume__edition__text__translation__translators", Prefetch("readings", readings_queryset), Prefetch("volume__readings", readings_queryset), Prefetch("volume__edition__readings", readings_queryset)).order_by("volume__title")

    books = []
    for book in books_queryset.all():
        book.read = False
        book.authors = ""
        book.year = ""

        if book.volume.edition.text:
            book.title = book.volume.title
            if hasattr(book.volume.edition.text, "work") and book.volume.edition.text.work:
                if book.volume.edition.text.work.title != book.volume.title:
                    book.title += " (" + book.volume.edition.text.work.title + ")"

                for author in book.volume.edition.text.work.authors.all():
                    if book.authors:
                        book.authors += "|"
                    if author.lastname:
                        book.authors += author.lastname + ", " + author.firstname
                    else:
                        book.authors += author.name
                if book.volume.edition.text.work.author_credit:
                    if book.authors:
                        book.authors += " (" + book.volume.edition.text.work.author_credit + ")"
                    else:
                        book.authors = book.volume.edition.text.work.author_credit

                if book.volume.edition.text.work.year:
                    book.year = book.volume.edition.text.work.year
            elif hasattr(book.volume.edition.text, "translation") and book.volume.edition.text.translation:
                if book.volume.edition.text.translation.original_work.title != book.volume.title:
                    book.title += " (" + book.volume.edition.text.translation.original_work.title + ")"

                for author in book.volume.edition.text.translation.original_work.authors.all():
                    if book.authors:
                        book.authors += "|"
                    if author.lastname:
                        book.authors += author.lastname + ", " + author.firstname
                    else:
                        book.authors += author.name
                if book.volume.edition.text.translation.original_work.author_credit:
                    if book.authors:
                        book.authors += " (" + book.volume.edition.text.translation.original_work.author_credit + ")"
                    else:
                        book.authors = book.volume.edition.text.translation.original_work.author_credit

                if book.volume.edition.text.translation.original_work.year:
                    book.year = book.volume.edition.text.translation.original_work.year
        else:
            book.title = book.volume.edition.title + " (" + pgettext("collection of books", "collection") + ")"

            for author in book.volume.edition.authors_prefetched:
                if book.authors:
                    book.authors += "|"
                if author.lastname:
                    book.authors += author.lastname + ", " + author.firstname
                else:
                    book.authors += author.name

            if book.volume.edition.year:
                book.year = book.volume.edition.year

        if book.readings.count() > 0 or book.volume.readings.count() > 0 or book.volume.edition.readings.count() > 0:
            book.read = True
        elif hasattr(book.volume.edition, "text") and book.volume.edition.text:
            if hasattr(book.volume.edition.text, "work") and book.volume.edition.text.work:
                book.read = book.volume.edition.text.work.read_by_user()
            elif hasattr(book.volume.edition.text, "translation") and book.volume.edition.text.translation:
                book.read = book.volume.edition.text.translation.read_by_user()

        if book.volume.edition.notreadable:
            book.unread = False
        else:
            book.unread = not book.read

        books.append(book)

    if request.GET.get("mode", "table") == "images":
        return render(request, "books/books_list_images.html", {"books": books})
    else:
        return render(request, "books/books_list.html", {"books": books})


@login_required
@require_safe
def bookInfo(request, id):
    try:
        book = Book.objects.select_related("volume", "volume__edition").get(id=id)
    except Book.DoesNotExist:
        return redirect("works_list")

    if book.user != request.user and not request.user.has_perm("books.view_all_books"):
        return redirect("book_info", book.id)

    if request.user.is_authenticated:
        readings = request.user.readings.filter(book=book)
    else:
        readings = Reading.objects.none()

    volume_lists = list(book.volume.lists.filter(list__user=request.user).values_list("list__id", flat=True))
    lists = []
    for booklist in request.user.booklists.all():
        booklist.disabled = (booklist.id in volume_lists)
        lists.append(booklist)

    return render(request, "books/book_info.html", {"book": book, "readings": readings, "lists": lists})


@login_required
@permission_required("books.change_book")
def bookEdit(request, id):
    try:
        book = Book.objects.get(id=id)
    except Book.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = BookForm(request.POST, instance=book)

        if form.is_valid():
            book = form.save()
            backupBooks(request.user)
            return redirect("book_info", "%06d" % book.id)
    else:
        form = BookForm(instance=book)

    return render(request, "books/book_edit.html", {"book": book, "form": form})


@login_required
@permission_required("books.delete_book")
def bookDelete(request, id):
    try:
        book = Book.objects.get(id=id)
    except Book.DoesNotExist:
        return redirect("works_list")

    if book.user != request.user and not request.user.has_perm("books.delete_any_book"):
        return redirect("book_info", book.id)

    if request.method == "POST":
        edition = book.volume.edition
        if "delete" in request.POST:
            book.delete()
            backupBooks(request.user)
        return redirect("edition_info", "%06d" % edition.id)

    return render(request, "books/book_delete.html", {"book": book})


@login_required
@permission_required("books.add_reading")
def bookAddReading(request, id):
    try:
        book = Book.objects.get(id=id)
    except Book.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = ReadingForm(request.POST)

        if form.is_valid():
            reading = form.save(commit=False)
            if reading:
                reading.text = book.volume.edition.text
                reading.edition = book.volume.edition
                reading.volume = book.volume
                reading.book = book
                reading.user = request.user
                reading.save()
                backupReadings(request.user)
            return redirect("book_info", "%06d" % book.id)
    else:
        form = ReadingForm(initial={"date": dateformat.format(timezone.now(), "Y-m-d")})

    url = reverse("book_add_reading", kwargs={"id": "%06d" % book.id})
    return render(request, "books/book_add_reading.html", {"book": book, "form": form, "form_url": url})


@login_required
@permission_required("books.change_reading")
def bookEditReadings(request, id):
    try:
        book = Book.objects.get(id=id)
    except Book.DoesNotExist:
        return redirect("works_list")

    readings = book.readings.filter(user=request.user)

    if readings.count() == 0:
        extra = 1
    else:
        extra = 0

    ReadingFormSet = modelformset_factory(Reading, form=ReadingForm, extra=extra, can_delete=True)
    # ATTENTION: ReadingFormSet n'est pas optimisé. La liste des langues est requêtée pour chaque entrée !

    if request.method == "POST":
        formset = ReadingFormSet(request.POST, queryset=readings)

        if formset.is_valid():
            formset.save_existing_objects()
            instances = formset.save_new_objects(commit=False)
            for instance in instances:
                instance.text = book.edition.text
                instance.edition = book.edition
                instance.book = book
                instance.user = request.user
                instance.save()
            backupReadings(request.user)
            return redirect("book_info", "%06d" % book.id)
    else:
        formset = ReadingFormSet(queryset=readings)

    url = reverse("book_edit_readings", kwargs={"id": "%06d" % book.id})
    title = _("Edit readings")
    return render(request, "books/book_edit_data.html", {"book": book, "title": title, "url_form": url, "formset": formset})


@login_required
@require_safe
def translationInfo(request, id):
    persons_queryset = Person.objects.only("id", "name", "lastname", "firstname", "gender").order_by()
    readings_queryset = Reading.objects.filter(user=request.user)
    tags_queryset = TextTag.objects.select_related("tag").only("info", "tag__id", "tag__name")
    links_queryset = TextLink.objects.select_related("lang")
    editions_queryset = Edition.objects.only("id", "text_id", "title", "year", "publisher")

    try:
        translation = Translation.objects.select_related("text", "original_work", "lang").prefetch_related(Prefetch("translators", persons_queryset), Prefetch("original_work__authors", persons_queryset), Prefetch("editions", editions_queryset), "collections", Prefetch("readings", readings_queryset), "attributes", Prefetch("tags", tags_queryset), Prefetch("links", links_queryset)).defer("original_work__summary", "original_work__search").get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    if request.user.is_authenticated:
        readings = translation.readings.all()
        for editionText in translation.collections.all():
            readings |= editionText.edition.readings.filter(user=request.user)
    else:
        readings = Reading.objects.none()

    return render(request, "books/translation_info.html", {"translation": translation, "readings": readings})


@login_required
@permission_required("books.change_translation")
def translationEdit(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    translators_initial = {translator.id: translator.name for translator in translation.translators.all()}

    if request.method == "POST":
        form = TranslationForm(request.POST, instance=translation, initial={"translators_select": list(translators_initial)})

        if "translators_select" in request.POST:
            translators = {translator: None for translator in request.POST.getlist("translators_select")}
            for translator in translators.keys():
                person = Person.objects.get(id=translator)
                translators[translator] = person
            form.fields["translators_select"].choices = [(id, name) for id, name in translators.items()]
        else:
            translators = {}

        if form.is_valid():
            if form.has_changed():
                translation = form.save()
                translation.translators.through.objects.filter(translation=translation).delete()
                for translator in translators.values():
                    translation.translators.add(translator)

                return redirect("translation_info", "%06d" % translation.id)
    else:
        form = TranslationForm(instance=translation, initial={"translators_select": list(translators_initial)})
        form.fields["translators_select"].choices = [(id, name) for id, name in translators_initial.items()]

    return render(request, "books/translation_edit.html", {"translation": translation, "form": form})


@login_required
@permission_required("books.delete_translation")
def translationDelete(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        work = translation.original_work
        if "delete" in request.POST:
            translation.delete()
        return redirect("work_info", "%06d" % work.id)

    return render(request, "books/translation_delete.html", {"translation": translation})


@login_required
@permission_required("books.add_reading")
def translationAddReading(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = ReadingForm(request.POST)

        if form.is_valid():
            reading = form.save(commit=False)
            if reading:
                reading.text = translation
                reading.user = request.user
                reading.save()
                backupReadings(request.user)
            return redirect("translation_info", "%06d" % translation.id)
    else:
        form = ReadingForm(initial={"date": dateformat.format(timezone.now(), "Y-m-d")})

    url = reverse("translation_add_reading", kwargs={"id": "%06d" % translation.id})
    return render(request, "books/translation_add_reading.html", {"translation": translation, "form": form, "form_url": url})


@login_required
@permission_required("books.add_edition")
def translationAddEdition(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = EditionForm(request.POST, work=translation.original_work, initial={"translation": translation})

        if form.is_valid():
            edition = form.save(commit=False)
            if edition:
                edition.text = translation
                edition.work = translation.original_work
                edition.save()
                Volume.objects.create(edition=edition, title=edition.title)
                return redirect("edition_info", "%06d" % edition.id)
            return redirect("translation_info", "%06d" % translation.id)
    else:
        form = EditionForm(initial={"title": translation.original_work.title, "translation": translation}, work=translation.original_work)

    #form.fields["translation"].widget = HiddenInput()

    return render(request, "books/translation_add_edition.html", {"translation": translation, "form": form})


@login_required
@permission_required("books.add_textattribute")
def translationAddAttribute(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = TextAttributeForm(request.POST)

        if form.is_valid():
            attribute = form.save(commit=False)
            if attribute:
                attribute.text = translation
                attribute.save()
            return redirect("translation_info", "%06d" % translation.id)
    else:
        form = TextAttributeForm()

    url = reverse("translation_add_attribute", kwargs={"id": "%06d" % translation.id})
    return render(request, "books/translation_add_attribute.html", {"translation": translation, "form": form, "form_url": url})


@login_required
@permission_required("books.add_texttag")
def translationAddTag(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = TextTagForm(request.POST)

        if form.is_valid():
            text_tag = form.save(commit=False)
            if text_tag:
                text_tag.text = translation
                text_tag.save()
            return redirect("translation_info", "%06d" % translation.id)
    else:
        form = TextTagForm()

    url = reverse("translation_add_tag", kwargs={"id": "%06d" % translation.id})
    return render(request, "books/translation_add_tag.html", {"translation": translation, "form": form, "form_url": url})


@login_required
@permission_required("books.add_textlink")
def translationAddLink(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    if request.method == "POST":
        form = TextLinkForm(request.POST)

        if form.is_valid():
            link = form.save(commit=False)
            if link:
                link.text = translation
                link.save()
            return redirect("translation_info", "%06d" % translation.id)
    else:
        form = TextLinkForm()

    url = reverse("translation_add_link", kwargs={"id": "%06d" % translation.id})
    return render(request, "books/translation_add_link.html", {"translation": translation, "form": form, "form_url": url})


@login_required
@permission_required("books.change_reading")
def translationEditReadings(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    readings = translation.readings.filter(user=request.user)

    if readings.count() == 0:
        extra = 1
    else:
        extra = 0

    ReadingFormSet = modelformset_factory(Reading, form=ReadingForm, extra=extra, can_delete=True)
    # ATTENTION: ReadingFormSet n'est pas optimisé. La liste des langues est requêtée pour chaque entrée !

    if request.method == "POST":
        formset = ReadingFormSet(request.POST, queryset=readings)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.text = translation
                instance.user = request.user
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            backupReadings(request.user)
            return redirect("translation_info", "%06d" % translation.id)
    else:
        formset = ReadingFormSet(queryset=readings)

    url = reverse("translation_edit_readings", kwargs={"id": "%06d" % translation.id})
    title = _("Edit readings")
    return render(request, "books/translation_edit_data.html", {"translation": translation, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("books.change_textattribute")
def translationEditAttributes(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    attributes = translation.attributes.all()

    if attributes.count() == 0:
        extra = 1
    else:
        extra = 0

    TextAttributeFormSet = modelformset_factory(TextAttribute, form=TextAttributeForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = TextAttributeFormSet(request.POST, queryset=attributes)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.text = translation
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            return redirect("translation_info", "%06d" % translation.id)
    else:
        formset = TextAttributeFormSet(queryset=attributes)

    url = reverse("translation_edit_attributes", kwargs={"id": "%06d" % translation.id})
    title = _("Edit attributes")
    return render(request, "books/translation_edit_data.html", {"translation": translation, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("books.change_texttag")
def translationEditTags(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    tags = translation.tags.all()

    if tags.count() == 0:
        extra = 1
    else:
        extra = 0

    TextTagFormSet = modelformset_factory(TextTag, form=TextTagForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = TextTagFormSet(request.POST, queryset=tags)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.text = translation
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            return redirect("translation_info", "%06d" % translation.id)
    else:
        formset = TextTagFormSet(queryset=tags)

    url = reverse("translation_edit_tags", kwargs={"id": "%06d" % translation.id})
    title = _("Edit tags")
    return render(request, "books/translation_edit_data.html", {"translation": translation, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("books.change_textlink")
def translationEditLinks(request, id):
    try:
        translation = Translation.objects.get(id=id)
    except Translation.DoesNotExist:
        return redirect("works_list")

    links = translation.links.all()

    if links.count() == 0:
        extra = 1
    else:
        extra = 0

    TextLinkFormSet = modelformset_factory(TextLink, form=TextLinkForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = TextLinkFormSet(request.POST, queryset=links)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.text = translation
                instance.save()
            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()
            return redirect("translation_info", "%06d" % translation.id)
    else:
        formset = TextLinkFormSet(queryset=links)

    url = reverse("translation_edit_links", kwargs={"id": "%06d" % translation.id})
    title = _("Edit links")
    return render(request, "books/translation_edit_data.html", {"translation": translation, "title": title, "url_form": url, "formset": formset})


@login_required
@require_safe
def textsAPISearch(request):
    query = request.GET.get("q", "").strip()

    query_set = Text.objects.all().filter(Q(work__isnull=False, work__title__icontains=query) | Q(work__isnull=False, work__title_vo__icontains=query) | Q(translation__isnull=False, translation__original_work__title__icontains=query) | Q(translation__isnull=False, translation__original_work__title_vo__icontains=query)).select_related("lang")

    if "exclude" in request.GET:
        excludeTextsId = set()
        for id in request.GET["exclude"].split(","):
            try:
                id = int(id)
                excludeTextsId.add(id)
            except:
                continue
        query_set = query_set.exclude(id__in=excludeTextsId)

    # Tri
    query_set = query_set.order_by("year")

    def formatter(text):
        if hasattr(text, "work") and text.work:
            authors = []
            for author in text.work.authors.all():
                authors.append({
                    "id":     author.id,
                    "name":   author.name,
                    "gender": author.gender,
                    "url":    reverse("person_info", kwargs={"id": author.id}),
                })

            return {
                "id":        text.id,
                "title":     text.work.title,
                "title_vo":  text.work.title_vo,
                "url":       reverse("work_info", kwargs={"id": text.work.id}),
                "year":      text.year if text.year else "",
                "work_year": text.year if text.year else "",
                "lang":      text.lang.code if text.lang else "",
                "lang_name": text.lang.name if text.lang else "",
                "authors":   authors,
            }
        elif hasattr(text, "translation") and text.translation:
            authors = []
            for author in text.translation.original_work.authors.all():
                authors.append({
                    "id":     author.id,
                    "name":   author.name,
                    "gender": author.gender,
                    "url":    reverse("person_info", kwargs={"id": author.id}),
                })

            translators = []
            for translator in text.translation.translators.all():
                translators.append({
                    "id":     translator.id,
                    "name":   translator.name,
                    "gender": translator.gender,
                    "url":    reverse("person_info", kwargs={"id": translator.id}),
                })

            return {
                "id":          text.id,
                "title":       text.translation.original_work.title,
                "title_vo":    text.translation.original_work.title_vo,
                "url":         reverse("translation_info", kwargs={"id": text.translation.id}),
                "year":        text.year if text.year else "",
                "work_year":   text.translation.original_work.year if text.translation.original_work.year else "",
                "lang":        text.lang.code if text.lang else "",
                "lang_name":   text.lang.name if text.lang else "",
                "authors":     authors,
                "translators": translators,
            }

    return generic_APISearch(request, query_set, formatter)


@login_required
@require_safe
def textInfo(request, id):
    try:
        text = Text.objects.get(id=id)
    except Text.DoesNotExist:
        return redirect("books_home")

    if hasattr(text, "work"):
        return workInfo(request, id)
    elif hasattr(text, "translation"):
        return translationInfo(request, id)
    else:
        return redirect("books_home")


@login_required
@require_safe
def sagasList(request):
    sagas = Saga.objects.all()
    return render(request, "books/sagas_list.html", {"sagas": sagas})


@login_required
@permission_required("books.add_saga")
def sagaCreate(request):
    if request.method == "POST":
        form = SagaForm(request.POST)

        if form.is_valid():
            saga = form.save()
            return redirect("books_saga_info", "%06d" % saga.id)
    else:
        form = SagaForm()

    return render(request, "books/saga_create.html", {"form": form})


@login_required
@require_safe
def sagaInfo(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("books_saga_list")

    readings_queryset = Reading.objects.filter(user=request.user, date_end__isnull=False).order_by()
    collections_queryset = EditionText.objects.select_related("edition").prefetch_related(Prefetch("edition__readings", readings_queryset)).order_by()
    translations_queryset = Translation.objects.prefetch_related(Prefetch("collections", collections_queryset), Prefetch("readings", readings_queryset)).order_by()

    works = []
    for sagaWork in saga.works.select_related("work", "work__lang", "work__genre").prefetch_related(Prefetch("work__collections", collections_queryset), Prefetch("work__translations", translations_queryset), Prefetch("work__readings", queryset=readings_queryset), "work__authors").all():
        if request.user.is_authenticated:
            sagaWork.read = sagaWork.work.read_by_user()
        else:
            sagaWork.read = False
        works.append(sagaWork)

    return render(request, "books/saga_info.html", {"saga": saga, "works": works})


@login_required
@permission_required("books.change_saga")
def sagaEdit(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("books_sagas_list")

    if request.method == "POST":
        form = SagaForm(request.POST, instance=saga)

        if form.is_valid():
            saga = form.save()
            return redirect("books_saga_info", "%06d" % saga.id)
    else:
        form = SagaForm(instance=saga)

    return render(request, "books/saga_edit.html", {"saga": saga, "form": form})


@login_required
@permission_required("books.delete_saga")
def sagaDelete(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("books_sagas_list")

    if request.method == "POST":
        if "delete" in request.POST:
            saga.delete()
        return redirect("books_sagas_list")

    return render(request, "books/saga_delete.html", {"saga": saga})


@login_required
@permission_required("books.add_sagawork")
def sagaAddWork(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("books_sagas_list")

    if request.method == "POST":
        form = SagaWorkForm(request.POST)

        if form.is_valid():
            sagaWork = form.save(commit=False)
            if sagaWork:
                try:
                    SagaWork.objects.get(saga=saga, work=sagaWork.work)
                    # Duplicate
                    return redirect("books_saga_info", "%06d" % saga.id)
                except:
                    pass
                sagaWork.saga = saga
                sagaWork.save()
            return redirect("books_saga_info", "%06d" % saga.id)
    else:
        form = SagaWorkForm()

    return render(request, "books/saga_add_work.html", {"saga": saga, "form": form})


@login_required
@permission_required("books.change_sagawork")
def sagaEditWorks(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("books_sagas_list")

    works = saga.works.all()

    if works.count() == 0:
        extra = 1
    else:
        extra = 0

    SagaWorkFormSet = modelformset_factory(SagaWork, form=SagaWorkForm, extra=extra, can_order=True, can_delete=True)

    if request.method == "POST":
        formset = SagaWorkFormSet(request.POST, queryset=works)

        if formset.is_valid():
            for form in formset.ordered_forms:
                form.instance._order = form.cleaned_data["ORDER"] - 1

            instances = formset.save(commit=False)
            for instance in instances:
                wantedOrder = instance._order
                instance.saga = saga
                instance.save()
                if instance._order != wantedOrder:  # new instance have automatic order
                    instance._order = wantedOrder
                    instance.save()

            for form in formset.deleted_forms:
                if form.instance.id:
                    form.instance.delete()

            # Reordering works
            order = 0
            saga = Saga.objects.get(id=id)
            for saga_work in saga.works.all():
                saga_work._order = order
                order += 1
                saga_work.save()

            return redirect("books_saga_info", "%06d" % saga.id)
    else:
        formset = SagaWorkFormSet(queryset=works)

    return render(request, "books/saga_edit_works.html", {"saga": saga, "formset": formset})


@login_required
def worksSearch(request):
    query = request.GET.get("q", "").strip()

    readings_queryset = Reading.objects.filter(user=request.user, date_end__isnull=False).order_by()

    works = []
    editions = Edition.objects.none()
    collections_texts = EditionText.objects.none()
    sagas = Saga.objects.none()

    if query:
        for work in Work.objects.select_related("genre").prefetch_related(Prefetch("readings", queryset=readings_queryset), "authors").filter(search__icontains=query):
            if request.user.is_authenticated:
                work.read = work.read_by_user()
            else:
                work.read = False
            works.append(work)

        editions = Edition.objects.prefetch_related("work__authors").filter(Q(title__icontains=query) | Q(publisher__icontains=query)).select_related("work")
        collections_texts = EditionText.objects.filter(title__icontains=query).select_related("text", "edition")
        sagas = Saga.objects.prefetch_related("works").filter(Q(title__icontains=query) | Q(title_vo__icontains=query))

    return render(request, "books/search.html", {"query": query, "works": works, "editions": editions, "collections_texts": collections_texts, "sagas": sagas})


@login_required
def readings(request):
    editionTextQuerySet = EditionText.objects.select_related("text", "text__work", "text__work__genre", "text__translation", "text__translation__original_work", "text__translation__original_work__genre").prefetch_related("text__work__authors", "text__translation__translators", "text__translation__original_work__authors")
    readings_req = request.user.readings.select_related("book", "edition", "text", "text__work", "text__work__genre", "text__translation", "text__translation__original_work", "text__translation__original_work__genre").prefetch_related("text__work__authors", "text__translation__translators", "text__translation__original_work__authors", Prefetch("edition__texts", queryset=editionTextQuerySet))

    works = []
    readings = []

    for reading in readings_req:
        work = None

        if reading.text:
            if hasattr(reading.text, "work") and reading.text.work is not None:
                work = reading.text.work
            elif hasattr(reading.text, "translation") and reading.text.translation is not None:
                work = reading.text.translation.original_work
            else:
                work = None
            if work is not None and work not in works:
                works.append(work)
        elif reading.edition:
            for edition_text in reading.edition.texts.all():
                if hasattr(edition_text.text, "work") and edition_text.text.work is not None:
                    work = edition_text.text.work
                elif hasattr(edition_text.text, "translation") and edition_text.text.translation is not None:
                    work = edition_text.text.translation.original_work
                else:
                    work = None
                if work is not None and work not in works:
                    works.append(work)

        readings.append(reading)

    # Count works for each genre
    unk_key = pgettext("Unknown genre", "Unknown")
    genres = {}
    for work in works:
        if work.genre is None:
            genre_str = unk_key
        else:
            genre_str = str(work.genre)
        if genre_str not in genres:
            genres[genre_str] = 0
        genres[genre_str] += 1

    genres = dict(sorted(genres.items()))

    return render(request, "books/readings.html", {"readings": readings, "works": works, "genres": genres})


@login_required
def readingsExport(request):
    if request.method == "POST":
        type = request.POST.get("type", "readings")
        format = request.POST.get("format", "csv")
        order = request.POST.get("order", "title")

        result = exportReadings(request.user, type, format, order)

        if format == "json":
            return HttpResponse(result, "application/json")
        else:
            return HttpResponse(result, "text/plain")

    return render(request, "books/readings_export.html")


@login_required
@require_safe
def listList(request):
    lists = List.objects.filter(user=request.user).prefetch_related("volumes")
    return render(request, "books/lists_list.html", {"lists": lists})


@login_required
@permission_required("books.add_list")
def listCreate(request):
    if request.method == "POST":
        form = ListForm(request.POST)

        if form.is_valid():
            booklist = form.save(commit=False)
            if booklist:
                booklist.user = request.user
                booklist.save()
                return redirect("booklist_info", "%06d" % booklist.id)
    else:
        form = ListForm()

    return render(request, "books/list_create.html", {"form": form})


@login_required
@require_safe
def listInfo(request, id):
    persons_queryset = Person.objects.only("id", "name", "lastname", "firstname", "gender").order_by()
    volumes_queryset = ListVolume.objects.select_related("volume", "volume__edition", "volume__edition__text", "volume__edition__text__work", "volume__edition__text__translation", "volume__edition__text__translation__lang", "volume__edition__text__translation__original_work").prefetch_related(Prefetch("volume__edition__text__work__authors", persons_queryset), Prefetch("volume__edition__text__translation__original_work__authors", persons_queryset), Prefetch("volume__edition__text__translation__translators", persons_queryset)).defer("volume__info", "volume__creation", "volume__modification", "volume__edition__info", "volume__edition__creation", "volume__edition__modification", "volume__edition__text__info", "volume__edition__text__creation", "volume__edition__text__modification", "volume__edition__text__work__summary", "volume__edition__text__work__search", "volume__edition__text__translation__original_work__summary", "volume__edition__text__translation__original_work__search")

    try:
        booklist = List.objects.prefetch_related(Prefetch("volumes", volumes_queryset)).get(id=id, user=request.user)
    except List.DoesNotExist:
        return redirect("booklists_list")

    books = []
    for list_volume in booklist.volumes.all():
        volume = list_volume.volume

        book = {
            "id": volume.id,
            "volume": volume,
            "authors": "",
            "read": False,
            "year": "",
        }

        if volume.edition.text:
            book["title"] = volume.title
            if hasattr(volume.edition.text, "work") and volume.edition.text.work:
                if volume.edition.text.work.title != volume.title:
                    book["title"] += " (" + volume.edition.text.work.title + ")"

                for author in volume.edition.text.work.authors.all():
                    if book["authors"]:
                        book["authors"] += "|"
                    if author.lastname:
                        book["authors"] += author.lastname + ", " + author.firstname
                    else:
                        book["authors"] += author.name
                if volume.edition.text.work.author_credit:
                    if book["authors"]:
                        book["authors"] += " (" + volume.edition.text.work.author_credit + ")"
                    else:
                        book["authors"] = volume.edition.text.work.author_credit

                if volume.edition.text.work.year:
                    book["year"] = volume.edition.text.work.year
            elif hasattr(volume.edition.text, "translation") and volume.edition.text.translation:
                if volume.edition.text.translation.original_work.title != volume.title:
                    book["title"] += " (" + volume.edition.text.translation.original_work.title + ")"

                for author in volume.edition.text.translation.original_work.authors.all():
                    if book["authors"]:
                        book["authors"] += "|"
                    if author.lastname:
                        book["authors"] += author.lastname + ", " + author.firstname
                    else:
                        book["authors"] += author.name
                if volume.edition.text.translation.original_work.author_credit:
                    if book["authors"]:
                        book["authors"] += " (" + volume.edition.text.translation.original_work.author_credit + ")"
                    else:
                        book["authors"] = volume.edition.text.translation.original_work.author_credit

                if volume.edition.text.translation.original_work.year:
                    book["year"] = volume.edition.text.translation.original_work.year
        else:
            book["title"] = volume.edition.title + " (" + pgettext("collection of books", "collection") + ")"

            for author in volume.edition.authors_prefetched:
                if book["authors"]:
                    book["authors"] += "|"
                if author.lastname:
                    book["authors"] += author.lastname + ", " + author.firstname
                else:
                    book["authors"] += author.name

            if volume.edition.year:
                book["year"] = volume.edition.year

        if volume.readings.count() > 0 or volume.edition.readings.count() > 0:
            book["read"] = True
        elif hasattr(volume.edition, "text") and volume.edition.text:
            if hasattr(volume.edition.text, "work") and volume.edition.text.work:
                book["read"] = volume.edition.text.work.read_by_user()
            elif hasattr(volume.edition.text, "translation") and volume.edition.text.translation:
                book["read"] = volume.edition.text.translation.read_by_user()

        if volume.edition.notreadable:
            book["unread"] = False
        else:
            book["unread"] = not book["read"]

        books.append(book)

    if request.GET.get("mode", "table") == "images":
        return render(request, "books/books_list_images.html", {"list": booklist, "books": books})
    else:
        return render(request, "books/books_list.html", {"list": booklist, "books": books})


@login_required
@permission_required("books.change_list")
def listEdit(request, id):
    try:
        booklist = List.objects.get(id=id, user=request.user)
    except List.DoesNotExist:
        return redirect("booklists_list")

    if request.method == "POST":
        form = ListForm(request.POST, instance=booklist)

        if form.is_valid():
            booklist = form.save()
            return redirect("booklist_info", "%06d" % booklist.id)
    else:
        form = ListForm(instance=booklist)

    return render(request, "books/list_edit.html", {"list": booklist, "form": form})


@login_required
@permission_required("books.change_list")
def listAddVolume(request, id, vid):
    try:
        volume = Volume.objects.get(id=vid)
    except Volume.DoesNotExist:
        redirect("works_list")

    try:
        list = List.objects.get(id=id, user=request.user)
    except List.DoesNotExist:
        return redirect("volume_info", "%06d" % volume.id)

    ListVolume.objects.get_or_create(list=list, volume=volume)

    return redirect("booklist_info", "%06d" % list.id)


@login_required
@permission_required("books.change_list")
def listRemoveVolume(request, id, vid):
    try:
        volume = Volume.objects.get(id=vid)
    except Volume.DoesNotExist:
        redirect("works_list")

    try:
        list = List.objects.get(id=id, user=request.user)
    except List.DoesNotExist:
        return redirect("volume_info", "%06d" % volume.id)

    try:
        item = ListVolume.objects.get(list=list, volume=volume)
    except ListVolume.DoesNotExist:
        return redirect("booklist_info", "%06d" % list.id)

    item.delete()

    return redirect("booklist_info", "%06d" % list.id)


@login_required
@permission_required("books.delete_list")
def listDelete(request, id):
    try:
        list = List.objects.get(id=id, user=request.user)
    except List.DoesNotExist:
        return redirect("booklists_list")

    if request.method == "POST":
        if "delete" in request.POST:
            list.delete()
        return redirect("booklists_list")

    return render(request, "books/list_delete.html", {"list": list})
