
from django.forms import ModelForm, Textarea, HiddenInput, IntegerField, CharField, Select, FileInput
from django.utils.translation import gettext as _

from base.models import Tag
from .models import Game, Saga, SagaGame, GameAttribute, GameLink, GameTag, GameUser


class GameForm(ModelForm):
    image_clear = CharField(widget=HiddenInput(), max_length=1, required=False, initial="0")

    def save(self, commit=True):
        image_clear_value = self.cleaned_data.get("image_clear")
        if image_clear_value == "1":
            self.instance.image = None

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = Game
        fields = ["title", "title_vo", "year", "editor", "developper", "platform", "info", "image", "image_clear"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
            "image": FileInput(),
        }


class GameAttributeForm(ModelForm):

    class Meta:
        model = GameAttribute
        fields = ["name", "value"]


class GameLinkForm(ModelForm):

    class Meta:
        model = GameLink
        fields = ["name", "uri", "lang"]


# TODO: make a generic class
class GameTagForm(ModelForm):
    tag_id = IntegerField(widget=HiddenInput(), required=False)
    tag_select = IntegerField(label=_("Tag"), widget=Select(attrs={"class": "select_tag"}), required=False)
    tag_name = CharField(widget=HiddenInput(), max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "tag") and self.instance.tag:
            self.initial["tag_id"] = self.instance.tag.id
            self.initial["tag_name"] = self.instance.tag.name
        else:
            self.initial["tag_id"] = None
            self.initial["tag_name"] = ""

    def _clean_fields(self, *args, **kwargs):
        tagId = int(self.data.get(self.add_prefix("tag_select")))

        if tagId < 0:
            tag = Tag.objects.create(name=self.data.get(self.add_prefix("tag_name")))
        else:
            tag = Tag.objects.get(id=tagId)

        self.instance.tag = tag
        self.data = self.data.copy()
        self.data[self.add_prefix("tag")] = self.instance.tag.id

        super(ModelForm, self)._clean_fields(*args, **kwargs)

    class Meta:
        model = GameTag
        fields = ["tag_select", "tag_name", "tag", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class GameTagFormClassic(ModelForm):
    tag_name = CharField(label=_("Tag name"), max_length=100)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields["tag"].required = False
        if hasattr(self.instance, "tag"):
            self.initial["tag_name"] = self.instance.tag.name
        else:
            self.initial["tag_name"] = ""

    def save(self, commit=True):
        name = self.cleaned_data.get("tag_name").strip()
        if self.initial["tag_name"] != name:
            tag = Tag.objects.get_or_create(name=name)[0]
            self.instance.tag = tag
        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = GameTag
        fields = ["tag", "tag_name", "info"]
        widgets = {
            "tag": HiddenInput(),
        }


class SagaForm(ModelForm):

    class Meta:
        model = Saga
        fields = ["title", "title_vo", "info"]
        widgets = {
            "info": Textarea(attrs={"rows": 4}),
        }


class SagaGameForm(ModelForm):
    game_id = IntegerField(label=_("Title"), widget=Select(attrs={"class": "select_game"}), required=False)
    game_title = CharField(widget=HiddenInput(), max_length=150, required=False)

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)

        if hasattr(self.instance, "game"):
            self.initial["game_id"] = self.instance.game.id
            self.initial["game_title"] = self.instance.game.title
            if self.instance.game.year:
                self.initial["game_title"] += " (%s)" % self.instance.game.year
        else:
            self.initial["game_id"] = None
            self.initial["game_title"] = ""

    def save(self, commit=True):
        if self.instance.game is None and self.cleaned_data.get("game_id"):
            self.instance.game = Game.objects.get(id=int(self.cleaned_data.get("game_id")))

        return super(ModelForm, self).save(commit=commit)

    class Meta:
        model = SagaGame
        fields = ["game_id", "game_title", "game"]
        widgets = {
            "game": HiddenInput(),
        }


class GameUserForm(ModelForm):

    class Meta:
        model = GameUser
        fields = ["bought", "location", "played", "finished", "notes"]
        widgets = {
            "notes": Textarea(attrs={"rows": 4}),
        }
