import os
import uuid

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext as _, pgettext

from base.models import Attribute, Link, Tag


def random_filename(instance, filename):
    ext = os.path.splitext(filename)[1].lower()
    if ext == ".jpeg":
        ext = ".jpg"
    filename = "%s%s" % (uuid.uuid4(), ext)
    return os.path.join("games", filename)


class Game(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=150, blank=False)
    title_vo = models.CharField(verbose_name=_("Original title"), max_length=150, blank=True)
    year = models.IntegerField(verbose_name=_("Year"), blank=True, null=True)
    editor = models.CharField(verbose_name=_("Editor"), max_length=150, blank=True)
    developper = models.CharField(verbose_name=_("Developper"), max_length=150, blank=True)
    platform = models.CharField(verbose_name=_("Platform"), max_length=150, blank=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    image = models.ImageField(verbose_name=_("Image"), upload_to=random_filename, null=True, blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Game")
        verbose_name_plural = _("Games")
        ordering = ["title"]


class GameAttribute(Attribute):
    game = models.ForeignKey(Game, related_name="attributes", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Game") + " - " + _("Attribute")
        verbose_name_plural = _("Games") + " - " + _("Attributes")
        #ordering = ["name"]
        db_table = "games_" + "game_attributes"


class GameLink(Link):
    game = models.ForeignKey(Game, related_name="links", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Game") + " - " + _("Link")
        verbose_name_plural = _("Games") + " - " + _("Links")
        #ordering = ["name"]
        db_table = "games_" + "game_links"


class GameTag(models.Model):
    game = models.ForeignKey(Game, related_name="tags", on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, verbose_name=_("Tag"), related_name="games", on_delete=models.CASCADE)
    info = models.CharField(verbose_name=_("Information"), max_length=100, blank=True)

    class Meta:
        verbose_name = _("Game") + " - " + _("Tag")
        verbose_name_plural = _("Games") + " - " + _("Tags")
        ordering = ["tag__name"]
        db_table = "games_" + "game_tags"


class Saga(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=100, blank=False)
    title_vo = models.CharField(verbose_name=_("Original title"), max_length=100, blank=True)
    info = models.TextField(verbose_name=_("Information"), blank=True)
    creation = models.DateTimeField(verbose_name=_("Creation date"), auto_now_add=True)
    modification = models.DateTimeField(verbose_name=_("Modification date"), auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = pgettext("The saga", "Saga")
        verbose_name_plural = _("Sagas")
        ordering = ["title"]


class SagaGame(models.Model):
    saga = models.ForeignKey(Saga, related_name="games", on_delete=models.CASCADE)
    game = models.ForeignKey(Game, verbose_name=_("Game"), related_name="sagas", on_delete=models.CASCADE)

    class Meta:
        verbose_name = pgettext("The saga", "Saga") + " - " + _("Game")
        verbose_name_plural = _("Sagas") + " - " + _("Games")
        order_with_respect_to = "saga"
        unique_together = ("saga", "game")
        db_table = "games_" + "saga_games"


class GameUser(models.Model):
    game = models.ForeignKey(Game, verbose_name=_("Game"), related_name="users", on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name=_("User"), related_name="games", on_delete=models.CASCADE)
    bought = models.BooleanField(verbose_name=_("Bought"), default=False)
    location = models.CharField(verbose_name=_("Location"), max_length=100, blank=True)
    played = models.BooleanField(verbose_name=_("Played"), default=False)
    finished = models.BooleanField(verbose_name=_("Finished"), default=False)
    notes = models.TextField(verbose_name=_("Notes"), blank=True)

    class Meta:
        verbose_name = _("Game") + " - " + _("User")
        verbose_name_plural = _("Games") + " - " + _("Users")
        unique_together = ("game", "user")
        db_table = "games_" + "game_users"
