from django.contrib import admin

from games.models import SagaGame, Saga, Game, GameAttribute, GameLink, GameTag, GameUser


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ("title", "year")


@admin.register(GameAttribute)
class GameAttributeAdmin(admin.ModelAdmin):
    list_display = ("game", "name", "value")


@admin.register(GameLink)
class GameLinkAdmin(admin.ModelAdmin):
    list_display = ("game", "name", "uri")


@admin.register(GameTag)
class GameTagAdmin(admin.ModelAdmin):
    list_display = ("game", "tag")


@admin.register(Saga)
class SagaAdmin(admin.ModelAdmin):
    list_display = ("title",)


@admin.register(SagaGame)
class SagaGameAdmin(admin.ModelAdmin):
    list_display = ("saga", "game")


@admin.register(GameUser)
class GameUserAdmin(admin.ModelAdmin):
    list_display = ("game", "user")
