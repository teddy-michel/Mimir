
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q
from django.forms import modelformset_factory
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.translation import gettext as _
from django.utils import timezone
from django.views.decorators.http import require_safe

from base.images import resize_image, remove_old_image
from base.api import generic_APIList, generic_APISearch
from .forms import GameForm, SagaForm, SagaGameForm, GameLinkForm, GameTagForm, GameTagFormClassic, GameAttributeForm, GameUserForm
from .models import Game, Saga, SagaGame, GameTag, GameLink, GameAttribute, GameUser


@login_required
@require_safe
def index(request):
    last_modified_games_sq = Game.objects.all().only("title", "title_vo", "year", "modification").order_by("-modification")[0:10]
    last_created_games_qs = Game.objects.all().only("title", "title_vo", "year", "creation").order_by("-creation")[0:10]

    last_modified_games = []
    for game in last_modified_games_sq:
        last_modified_games.append(game)

    last_created_games = []
    for game in last_created_games_qs:
        last_created_games.append(game)

    return render(request, "games/index.html", {"last_modified_games": last_modified_games, "last_created_games": last_created_games})


@login_required
@require_safe
def gamesList(request):
    return render(request, "games/games_list.html", {})


@login_required
@require_safe
def gamesAPIList(request):
    columns = ["title", "year", "editor", "platform"]

    query = request.GET.get("q", "").strip()
    if query:
        query_set = Game.objects.filter(title__icontains=query)
    else:
        query_set = Game.objects.all()

    def formatter(game):
        data = {
            "id":       game.id,
            "title":    game.title,
            "url":      reverse("game_info", kwargs={"id": "%06d" % game.id}),
            "year":     game.year,
            "editor":   game.editor,
            "platform": game.platform,
            "played":   False,
            "finished": False,
        }

        if request.user.is_authenticated:
            try:
                gameuser = GameUser.objects.get(game=game, user=request.user)
                if gameuser.played:
                    data["played"] = True
                if gameuser.finished:
                    data["finished"] = True
            except:
                pass

        return data

    return generic_APIList(request, columns, query_set, formatter)


def _checkGameImages():
    # List images used by games and resize them if necessary
    images = []
    for game in Game.objects.all():
        if game.image:
            resize_image(str(game.image))
            images.append(str(game.image))

    # Remove unused images
    for file in os.listdir(os.path.join(settings.MEDIA_ROOT, "games")):
        filename = os.path.join(settings.MEDIA_ROOT, "games", file)
        if not os.path.isfile(filename):
            continue
        if "games/" + file not in images:
            os.remove(filename)

    # Remove unused images
    for file in os.listdir(os.path.join(settings.MEDIA_ROOT, "games", "original")):
        filename = os.path.join(settings.MEDIA_ROOT, "games", "original", file)
        if not os.path.isfile(filename):
            continue
        if "games/" + file not in images:
            os.remove(filename)


@login_required
@permission_required("games.add_game")
def gameCreate(request):
    if request.method == "POST":
        form = GameForm(request.POST, request.FILES)
        if form.is_valid():
            game = form.save()

            if game.image is not None:
                #_checkGameImages()
                resize_image(str(game.image))

            return redirect("game_info", "%06d" % game.id)
    else:
        form = GameForm()

    return render(request, "games/game_create.html", {"form": form})


@login_required
@require_safe
def gamesAPISearch(request):
    query = request.GET.get("q", "").strip()

    query_set = Game.objects.all().filter(title__icontains=query)

    if "exclude" in request.GET:
        excludeGamesId = set()
        for id in request.GET["exclude"].split(","):
            try:
                id = int(id)
                excludeGamesId.add(id)
            except:
                continue
        query_set = query_set.exclude(id__in=excludeGamesId)

    # Tri
    query_set = query_set.order_by("title")

    def formatter(game):
        return {
            "id":       game.id,
            "title":    game.title,
            "title_vo": game.title_vo,
            "url":      reverse("game_info", kwargs={"id": "%06d" % game.id}),
            "year":     game.year,
        }

    return generic_APISearch(request, query_set, formatter)


@login_required
@require_safe
def gameInfo(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    if request.user.is_authenticated:
        try:
            gameuser = GameUser.objects.get(game=game, user=request.user)
        except GameUser.DoesNotExist:
            gameuser = None
    else:
        gameuser = None

    tags = game.tags.select_related("tag")
    links = game.links.select_related("lang")

    return render(request, "games/game_info.html", {"game": game, "tags": tags, "links": links, "gameuser": gameuser})


@login_required
@permission_required("games.change_game")
def gameEdit(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    if request.method == "POST":
        oldImage = str(game.image) if game.image is not None else ""
        form = GameForm(request.POST, request.FILES, instance=game)

        if form.is_valid():
            if form.has_changed():
                game = form.save()
                game.modification = timezone.now()
                game.save()

                # Resize image
                if game.image is not None and str(game.image) != "" and str(game.image) != oldImage:
                    #_checkGameImages()
                    resize_image(str(game.image))

                # Remove old image
                remove_old_image(oldImage, str(game.image))

            return redirect("game_info", "%06d" % game.id)
    else:
        form = GameForm(instance=game)

    return render(request, "games/game_edit.html", {"game": game, "form": form})


@login_required
@permission_required("games.delete_game")
def gameDelete(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    if request.method == "POST":
        if "delete" in request.POST:
            game.delete()
        return redirect("games_list")

    return render(request, "games/game_delete.html", {"game": game})


@login_required
@permission_required("games.add_gameattribute")
def gameAddAttribute(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    if request.method == "POST":
        form = GameAttributeForm(request.POST)

        if form.is_valid():
            attribute = form.save(commit=False)
            if attribute:
                attribute.game = game
                attribute.save()
            return redirect("game_info", "%06d" % game.id)
    else:
        form = GameAttributeForm()

    url = reverse("game_add_attribute", kwargs={"id": "%06d" % game.id})
    return render(request, "games/game_add_attribute.html", {"game": game, "form": form, "form_url": url})


@login_required
@permission_required("games.add_gametag")
def gameAddTag(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    if request.method == "POST":
        form = GameTagForm(request.POST)

        if form.is_valid():
            gameTag = form.save(commit=False)
            if gameTag:
                gameTag.game = game
                gameTag.save()
            return redirect("game_info", "%06d" % game.id)
    else:
        form = GameTagForm()

    url = reverse("game_add_tag", kwargs={"id": "%06d" % game.id})
    return render(request, "games/game_add_tag.html", {"game": game, "form": form, "form_url": url})


@login_required
@permission_required("games.add_gamelink")
def gameAddLink(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    if request.method == "POST":
        form = GameLinkForm(request.POST)

        if form.is_valid():
            link = form.save(commit=False)
            if link:
                link.game = game
                link.save()
            return redirect("game_info", "%06d" % game.id)
    else:
        form = GameLinkForm()

    url = reverse("game_add_link", kwargs={"id": "%06d" % game.id})
    return render(request, "games/game_add_link.html", {"game": game, "form": form, "form_url": url})

@login_required
@permission_required("games.change_gameattribute")
def gameEditAttributes(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    attributes = game.attributes.all()

    if attributes.count() == 0:
        extra = 1
    else:
        extra = 0

    GameAttributeFormSet = modelformset_factory(GameAttribute, form=GameAttributeForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = GameAttributeFormSet(request.POST, queryset=attributes)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.game = game
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("game_info", "%06d" % game.id)
    else:
        formset = GameAttributeFormSet(queryset=attributes)

    url = reverse("game_edit_attributes", kwargs={"id": "%06d" % game.id})
    title = _("Edit attributes")
    return render(request, "games/game_edit_data.html", {"game": game, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("games.change_gametag")
def gameEditTags(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    tags = game.tags.all()

    if tags.count() == 0:
        extra = 1
    else:
        extra = 0

    GameTagFormSet = modelformset_factory(GameTag, form=GameTagFormClassic, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = GameTagFormSet(request.POST, queryset=tags)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.game = game
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("game_info", "%06d" % game.id)
    else:
        formset = GameTagFormSet(queryset=tags)

    url = reverse("game_edit_tags", kwargs={"id": "%06d" % game.id})
    title = _("Edit tags")
    return render(request, "games/game_edit_data.html", {"game": game, "title": title, "url_form": url, "formset": formset})


@login_required
@permission_required("games.change_gamelink")
def gameEditLinks(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    links = game.links.all()

    if links.count() == 0:
        extra = 1
    else:
        extra = 0

    GameLinkFormSet = modelformset_factory(GameLink, form=GameLinkForm, extra=extra, can_delete=True)

    if request.method == "POST":
        formset = GameLinkFormSet(request.POST, queryset=links)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for instance in instances:
                instance.game = game
                instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("game_info", "%06d" % game.id)
    else:
        formset = GameLinkFormSet(queryset=links)

    url = reverse("game_edit_links", kwargs={"id": "%06d" % game.id})
    title = _("Edit links")
    return render(request, "games/game_edit_data.html", {"game": game, "title": title, "url_form": url, "formset": formset})


@login_required
def gameEditUserInfo(request, id):
    try:
        game = Game.objects.get(id=id)
    except Game.DoesNotExist:
        return redirect("games_list")

    gameuser = GameUser.objects.get_or_create(game=game, user=request.user)[0]

    if request.method == "POST":
        form = GameUserForm(request.POST, instance=gameuser)
        if form.is_valid():
            if form.has_changed():
                gameuser = form.save()
            return redirect("game_info", "%06d" % game.id)
    else:
        form = GameUserForm(instance=gameuser)

    return render(request, "games/game_edit_user.html", {"game": game, "gameuser": gameuser, "form": form})


@login_required
@require_safe
def sagasList(request):
    sagas = Saga.objects.all()
    return render(request, "games/sagas_list.html", {"sagas": sagas})


@login_required
@permission_required("games.add_saga")
def sagaCreate(request):
    if request.method == "POST":
        form = SagaForm(request.POST)

        if form.is_valid():
            saga = form.save()
            return redirect("games_saga_info", "%06d" % saga.id)
    else:
        form = SagaForm()

    return render(request, "games/saga_create.html", {"form": form})


@login_required
@require_safe
def sagaInfo(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("games_saga_list")

    games = []
    for sagaGame in saga.games.all():
        sagaGame.played = False
        sagaGame.finished = False

        if request.user.is_authenticated:
            try:
                gameuser = GameUser.objects.get(game=sagaGame.game, user=request.user)
                if gameuser.played:
                    sagaGame.played = True
                if gameuser.finished:
                    sagaGame.finished = True
            except:
                pass

        games.append(sagaGame)

    return render(request, "games/saga_info.html", {"saga": saga, "games": games})


@login_required
@permission_required("games.change_saga")
def sagaEdit(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("games_sagas_list")

    if request.method == "POST":
        form = SagaForm(request.POST, instance=saga)

        if form.is_valid():
            saga = form.save()
            return redirect("games_saga_info", "%06d" % saga.id)
    else:
        form = SagaForm(instance=saga)

    return render(request, "games/saga_edit.html", {"saga": saga, "form": form})


@login_required
@permission_required("games.delete_saga")
def sagaDelete(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("games_sagas_list")

    if request.method == "POST":
        if "delete" in request.POST:
            saga.delete()
        return redirect("games_sagas_list")

    return render(request, "games/saga_delete.html", {"saga": saga})


@login_required
@permission_required("games.add_sagagame")
def sagaAddGame(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("games_sagas_list")

    if request.method == "POST":
        form = SagaGameForm(request.POST)

        if form.is_valid():
            sagaGame = form.save(commit=False)
            if sagaGame:
                sagaGame.saga = saga
                sagaGame.save()
            return redirect("games_saga_info", "%06d" % saga.id)
    else:
        form = SagaGameForm()

    return render(request, "games/saga_add_game.html", {"saga": saga, "form": form})


@login_required
@permission_required("games.change_sagagame")
def sagaEditGames(request, id):
    try:
        saga = Saga.objects.get(id=id)
    except Saga.DoesNotExist:
        return redirect("games_sagas_list")

    games = saga.games.all()

    if games.count() == 0:
        extra = 1
    else:
        extra = 0

    SagaGameFormSet = modelformset_factory(SagaGame, form=SagaGameForm, extra=extra, can_order=True, can_delete=True)

    if request.method == "POST":
        formset = SagaGameFormSet(request.POST, queryset=games)

        if formset.is_valid():
            for form in formset.ordered_forms:
                form.instance._order = form.cleaned_data["ORDER"] - 1
            instances = formset.save(commit=False)
            for instance in instances:
                wantedOrder = instance._order
                instance.saga = saga
                instance.save()
                if instance._order != wantedOrder:  # new instance have automatic order
                    instance._order = wantedOrder
                    instance.save()
            for form in formset.deleted_forms:
                form.instance.delete()
            return redirect("games_saga_info", "%06d" % saga.id)
    else:
        formset = SagaGameFormSet(queryset=games)

    return render(request, "games/saga_edit_games.html", {"saga": saga, "formset": formset})



@login_required
def gamesSearch(request):
    query = request.GET.get("q", "").strip()

    if query:
        games = Game.objects.filter(Q(title__icontains=query) | Q(title_vo__icontains=query))
        sagas = Saga.objects.filter(Q(title__icontains=query) | Q(title_vo__icontains=query))
    else:
        games = Game.objects.none()
        sagas = Saga.objects.none()

    return render(request, "games/search.html", {"query": query, "games": games, "sagas": sagas})


@login_required
@require_safe
def gamesLibrary(request):
    games = request.user.games.filter(bought=True).select_related("game").all()
    return render(request, "games/library.html", {"games": games})
