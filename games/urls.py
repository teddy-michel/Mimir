from django.urls import re_path

from . import views


urlpatterns = [
    re_path(r"^$", views.index, name="games_home"),

    re_path(r"^games$", views.gamesList, name="games_list"),
    re_path(r"^games/create$", views.gameCreate, name="game_create"),
    re_path(r"^games/api/list$", views.gamesAPIList, name="games_api_list"),
    re_path(r"^games/api/search$", views.gamesAPISearch, name="games_api_search"),
    re_path(r"^games/(?P<id>[0-9]+)$", views.gameInfo, name="game_info"),
    re_path(r"^games/(?P<id>[0-9]+)/edit$", views.gameEdit, name="game_edit"),
    re_path(r"^games/(?P<id>[0-9]+)/delete$", views.gameDelete, name="game_delete"),
    re_path(r"^games/(?P<id>[0-9]+)/add_attribute$", views.gameAddAttribute, name="game_add_attribute"),
    re_path(r"^games/(?P<id>[0-9]+)/add_tag$", views.gameAddTag, name="game_add_tag"),
    re_path(r"^games/(?P<id>[0-9]+)/add_link$", views.gameAddLink, name="game_add_link"),
    re_path(r"^games/(?P<id>[0-9]+)/edit_attributes$", views.gameEditAttributes, name="game_edit_attributes"),
    re_path(r"^games/(?P<id>[0-9]+)/edit_tags$", views.gameEditTags, name="game_edit_tags"),
    re_path(r"^games/(?P<id>[0-9]+)/edit_links$", views.gameEditLinks, name="game_edit_links"),
    re_path(r"^games/(?P<id>[0-9]+)/edit_user$", views.gameEditUserInfo, name="game_edit_user"),

    re_path(r"^sagas$", views.sagasList, name="games_sagas_list"),
    re_path(r"^sagas/create$", views.sagaCreate, name="games_saga_create"),
    re_path(r"^sagas/(?P<id>[0-9]+)$", views.sagaInfo, name="games_saga_info"),
    re_path(r"^sagas/(?P<id>[0-9]+)/edit$", views.sagaEdit, name="games_saga_edit"),
    re_path(r"^sagas/(?P<id>[0-9]+)/delete$", views.sagaDelete, name="games_saga_delete"),
    re_path(r"^sagas/(?P<id>[0-9]+)/add_game$", views.sagaAddGame, name="games_saga_add_game"),
    re_path(r"^sagas/(?P<id>[0-9]+)/edit_games$", views.sagaEditGames, name="games_saga_edit_games"),

    re_path(r"^search$", views.gamesSearch, name="games_search"),

    re_path(r"^library$", views.gamesLibrary, name="games_library"),
]
