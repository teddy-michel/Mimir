# Mimir

Application de gestion de médiathèques (films, livres, jeux vidéos).

## Installation

* Installer Python et les modules nécessaires :
    * Django
    * PyMySQL
    * bs4
    * ...
* Installer un serveur de base de données (par exemple MariaDB).
* Installer un serveur HTTP (par exemple Apache).
* Configurer l'application dans le fichier "Mimir/settings.py", en particulier la section "DATABASES". Modifier la valeur de "SECRET_KEY".
* Configurer le serveur HTTP pour qu'il serve l'application.
